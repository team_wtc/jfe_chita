﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

using static System.Diagnostics.Debug;

namespace AlarmRestruct
{
    public partial class RestructForm
    {
        async Task<(bool Auth, string IPAddress, int MonitoringId, string MonitoringName, int DisplayDays)> TerminalAuth()
        {
            bool AuthResult = false;
            int MonId = -1;
            string MonAddress = string.Empty;
            string MonName = string.Empty;
            int MonDisplayDays = 0;

            await Task.Factory.StartNew( () =>
            {
                try
                {
                    using ( var Conn = new SqlConnection( _ConnectionString ) )
                    {
                        var Entry = Dns.GetHostEntry( Dns.GetHostName() );
                        foreach ( var Addr in Entry.AddressList )
                        {
                            if ( Addr.AddressFamily != AddressFamily.InterNetwork )
                            {
                                continue;
                            }
                            if ( IPAddress.IsLoopback( Addr ) == true )
                            {
                                continue;
                            }

                            var CommandText = $"SELECT mon_id, mon_display_name, mon_alarm_max_day FROM MONITOR WHERE mon_ip_address = N'{Addr.ToString()}'";
                            WriteLine( CommandText );

                            using ( var Cmd = new SqlCommand( CommandText, Conn ) )
                            {
                                if ( Conn.State != ConnectionState.Open )
                                {
                                    Conn.Open();
                                }

                                if ( Conn.State == ConnectionState.Open )
                                {
                                    using ( var Reader = Cmd.ExecuteReader() )
                                    {
                                        if ( Reader.HasRows == true )
                                        {
                                            Reader.Read();
                                            AuthResult = true;
                                            MonAddress = Addr.ToString();
                                            MonId = Reader.GetInt32( 0 );
                                            MonName = Reader.GetString( 1 );
                                            MonDisplayDays = Reader.GetInt32( 2 );
                                            Conn.Close();
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                    AuthResult = false;
                    MonId = -1;
                    MonName = string.Empty;
                    MonAddress = string.Empty;
                    MonDisplayDays = 0;
                }
            } );

            return (AuthResult, MonAddress, MonId, MonName, MonDisplayDays);
        }

        async Task<int> AlarmSaveDays()
        {
            return await Task.Factory.StartNew<int>( () =>
            {
                try
                {
                    var CommandText = $"SELECT system_value FROM SYSTEM WHERE system_key = N'AlarmSaveDays'";
                    WriteLine( CommandText );
                    int Count = -1;
                    using ( var Conn = new SqlConnection( _ConnectionString ) )
                    using ( var Cmd = new SqlCommand( CommandText, Conn ) )
                    {
                        if ( Conn.State != ConnectionState.Open )
                        {
                            Conn.Open();
                        }

                        if ( Conn.State == ConnectionState.Open )
                        {
                            using ( var Reader = Cmd.ExecuteReader() )
                            {
                                if ( Reader.HasRows == true )
                                {
                                    Reader.Read();
                                    Count = Convert.ToInt32( Reader.GetString( 0 ) );
                                    Conn.Close();
                                }
                            }
                        }
                    }
                    return Count;
                }
                catch
                {
                    return -1;
                }
            } );
        }

        async Task<string[]> AlarmNames()
        {
            return await Task.Factory.StartNew<string[]>( () =>
            {
                try
                {
                    var CommandText = $"SELECT system_key, system_value FROM SYSTEM WHERE system_key IN ( N'AlarmNameA', N'AlarmNameB', N'AlarmNameC' )";
                    WriteLine( CommandText );
                    string[] Names = new string[ 3 ];

                    using ( var Conn = new SqlConnection( _ConnectionString ) )
                    using ( var Cmd = new SqlCommand( CommandText, Conn ) )
                    {
                        if ( Conn.State != ConnectionState.Open )
                        {
                            Conn.Open();
                        }

                        if ( Conn.State == ConnectionState.Open )
                        {
                            using ( var Reader = Cmd.ExecuteReader() )
                            {
                                if ( Reader.HasRows == true )
                                {
                                    while ( Reader.Read() == true )
                                    {
                                        switch ( Reader.GetString( 0 ) )
                                        {
                                            case "AlarmNameA":
                                                Names[ 0 ] = Reader.GetString( 1 );
                                                break;
                                            case "AlarmNameB":
                                                Names[ 1 ] = Reader.GetString( 1 );
                                                break;
                                            case "AlarmNameC":
                                                Names[ 2 ] = Reader.GetString( 1 );
                                                break;
                                        }
                                    }
                                }
                            }

                            Conn.Close();
                        }
                    }
                    return Names;
                }
                catch
                {
                    return null;
                }
            } );
        }

        async Task<int> AlarmCount( int DisplayMaxDays )
        {
            return await Task.Factory.StartNew<int>( () =>
            {
                try
                {
                    DateTime Limit = DateTime.Today.AddDays( -DisplayMaxDays );
                    var CommandText = $"SELECT COUNT(*) FROM ALARM WHERE alm_date >= N'{Limit.ToString( "yyyy-MM-dd" )}  0:0:0'";
                    WriteLine( CommandText );
                    int Count = -1;
                    using ( var Conn = new SqlConnection( _ConnectionString ) )
                    using ( var Cmd = new SqlCommand( CommandText, Conn ) )
                    {
                        if ( Conn.State != ConnectionState.Open )
                        {
                            Conn.Open();
                        }

                        if ( Conn.State == ConnectionState.Open )
                        {
                            using ( var Reader = Cmd.ExecuteReader() )
                            {
                                if ( Reader.HasRows == true )
                                {
                                    Reader.Read();
                                    Count = Reader.GetInt32( 0 );
                                }
                            }
                        }
                        Conn.Close();
                    }
                    return Count;
                }
                catch
                {
                    return -1;
                }
            } );
        }

        async Task<bool> LoadInformation()
        {
            (bool Auth, string MonitoringAddress, int MonitoringId, string MonitoringName, int MonitoringMaxDays) Result = await TerminalAuth();

            if ( Result.Auth == false )
            {
                MessageBox.Show( "このPCはモニタリングテーブルに登録されていません。", "端末認証" );
                return false;
            }

            TerminalInfoLabel.Text = $"ID={Result.MonitoringId} {Result.MonitoringName} ({Result.MonitoringAddress})";
            DisplayDaysLabel.Text = $"{Result.MonitoringMaxDays}";

            int Days = await AlarmSaveDays();
            if ( Days < 0 )
            {
                return false;
            }
            AlarmSaveDaysLabel.Text = $"{Days}";

            string[] AlarmNameList = await AlarmNames();
            if ( ( AlarmNameList == null ) || ( AlarmNameList.Length != 3 ) )
            {
                MessageBox.Show( "警報の表示名称を取得できません。", "システム設定" );
                return false;
            }

            AlarmNameALabel.Text = AlarmNameList[ 0 ];
            AlarmNameBLabel.Text = AlarmNameList[ 1 ];
            AlarmNameCLabel.Text = AlarmNameList[ 2 ];

            DateTime StartDate = DateTime.Today.AddDays( -Result.MonitoringMaxDays );
            StartDateLabel.Text = $"{StartDate.ToString( "yyyy年 MM月 dd日" )}";

            int Count = await AlarmCount( Result.MonitoringMaxDays );
            if ( Count < 0 )
            {
                return false;
            }
            AlarmCountLabel.Text = $"{Count}";
            RestructProgressBar.Maximum = Count;
            return true;
        }

        async Task<AlarmDataSet> RestructAlarm( )
        {
            const string IMAGE_FILE_NAME_PATTERN = "^(?<A>\\d+?)_(?<T>\\d)_(?<DATE>\\d+?)_(?<TIME>\\d+?)$";

            int DisplayMaxDays = Convert.ToInt32( DisplayDaysLabel.Text );
            string ImageFolder = ImageFolderTextBox.Text;
            string MovieFolder = MovieFolderTextBox.Text;
            AlarmDataSet DS = new AlarmDataSet();
            DS.ALARM.TableNewRow += this.ALARM_TableNewRow;

            return await Task.Factory.StartNew<AlarmDataSet>( () =>
            {
                try
                {
                    Dictionary<int, string> ImageFileList = new Dictionary<int, string>();
                    Dictionary<int, string> MovieFileList = new Dictionary<int, string>();

                    DirectoryInfo ImageFolderInfo = new DirectoryInfo( ImageFolderTextBox.Text );
                    foreach ( var ImageFileInfo in ImageFolderInfo.GetFiles( "*.jpg", SearchOption.TopDirectoryOnly ) )
                    {
                        var PtnMatch = Regex.Match( Path.GetFileNameWithoutExtension( ImageFileInfo.FullName ), IMAGE_FILE_NAME_PATTERN );
                        if ( PtnMatch.Success == true )
                        {
                            ImageFileList[ Convert.ToInt32( PtnMatch.Groups[ "A" ].Value ) ] = ImageFileInfo.FullName;
                        }
                    }

                    DirectoryInfo MovieFolderInfo = new DirectoryInfo( MovieFolderTextBox.Text );
                    foreach ( var MovieFileInfo in MovieFolderInfo.GetFiles( "*.mp4", SearchOption.AllDirectories ) )
                    {
                        if ( int.TryParse( Path.GetFileNameWithoutExtension( MovieFileInfo.FullName ), out int MovieAlarmId ) == true )
                        {
                            MovieFileList[ MovieAlarmId ] = MovieFileInfo.FullName;
                        }
                    }

                    DateTime Limit = DateTime.Today.AddDays( -DisplayMaxDays );
                    var CommandText =
                    $"SELECT ALARM.alm_id, ALARM.alm_date, ALARM.alm_detector_id, ALARM.alm_type, ALARM.alm_event_image, DETECTOR.ai_gate_name " +
                    $"FROM ALARM INNER JOIN DETECTOR ON ALARM.alm_detector_id = DETECTOR.ai_id " +
                    $"WHERE alm_date >= N'{Limit.ToString( "yyyy-MM-dd" )}  0:0:0'";
                    WriteLine( CommandText );

                    using ( var Conn = new SqlConnection( _ConnectionString ) )
                    using ( var Cmd = new SqlCommand( CommandText, Conn ) )
                    {
                        if ( Conn.State != ConnectionState.Open )
                        {
                            Conn.Open();
                        }

                        if ( Conn.State == ConnectionState.Open )
                        {
                            using ( var Reader = Cmd.ExecuteReader() )
                            {
                                if ( Reader.HasRows == true )
                                {

                                    while ( Reader.Read() == true )
                                    {
                                        List<byte> AlmImgBuffer = new List<byte>();

                                        var AlmId = Reader.GetInt32( 0 );
                                        var AlmDate = Reader.GetDateTime( 1 );
                                        var AlmDetector = Reader.GetInt32( 2 );
                                        var AlmType = Reader.GetInt32( 3 );
                                        using ( var Strm = Reader.GetStream( 4 ) )
                                        {
                                            var ImgBuffer = new byte[ Strm.Length ];
                                            Strm.Read( ImgBuffer, 0, ImgBuffer.Length );
                                            AlmImgBuffer.AddRange( ImgBuffer );
                                        }
                                        var AlmGateName = Reader.GetString( 5 );

                                        var SaveImagePath = Path.Combine( ImageFolder, $"{AlmId}_{AlmType}_{AlmDate.ToString( "yyyyMMdd_HHmmssfff" )}.jpg" );
                                        if ( ImageFileList.ContainsKey( AlmId ) == true )
                                        {
                                            ImageFileList.Remove( AlmId );
                                        }
                                        else
                                        {
                                            File.WriteAllBytes( SaveImagePath, AlmImgBuffer.ToArray() );
                                        }

                                        if ( MovieFileList.ContainsKey( AlmId ) == true )
                                        {
                                            MovieFileList.Remove( AlmId );
                                        }

                                        var Row = DS.ALARM.NewALARMRow();
                                        Row.alarm_id = AlmId;
                                        Row.alarm_date = AlmDate;
                                        Row.alarm_gate_id = AlmDetector;
                                        Row.alarm_gate_name = AlmGateName;
                                        Row.alarm_type = AlmType;
                                        switch ( AlmType )
                                        {
                                            case 1:
                                                Row.alarm_type_name = AlarmNameALabel.Text;
                                                break;
                                            case 2:
                                                Row.alarm_type_name = AlarmNameBLabel.Text;
                                                break;
                                            case 3:
                                                Row.alarm_type_name = AlarmNameCLabel.Text;
                                                break;
                                            default:
                                                Row.alarm_type_name = "";
                                                break;
                                        }
                                        Row.alarm_image_path = SaveImagePath;
                                        DS.ALARM.AddALARMRow( Row );
                                    }
                                }
                            }
                        }
                        Conn.Close();
                    }

                    foreach ( var ImgKey in ImageFileList.Keys )
                    {
                        File.Delete( ImageFileList[ ImgKey ] );
                    }
                    foreach ( var MovKey in MovieFileList.Keys )
                    {
                        File.Delete( MovieFileList[ MovKey ] );
                    }

                    DS.AcceptChanges();
                    return DS;
                }
                catch
                {
                    DS.RejectChanges();
                    DS.Dispose();
                    return null;
                }
            } );
        }

        private void ALARM_TableNewRow( object sender, DataTableNewRowEventArgs e )
        {
            if ( InvokeRequired == true )
            {
                BeginInvoke( (EventHandler<DataTableNewRowEventArgs> )ALARM_TableNewRow, sender, e );
                return;
            }

            RestructProgressBar.PerformStep();
        }
    }
}
