﻿namespace AlarmRestruct
{
    partial class RestructForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.RestructButton = new System.Windows.Forms.Button();
            this.TerminalInfoLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label3 = new System.Windows.Forms.Label();
            this.AlarmSaveDaysLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DisplayDaysLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ConnectionStringLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AlarmNameALabel = new System.Windows.Forms.Label();
            this.AlarmNameBLabel = new System.Windows.Forms.Label();
            this.AlarmNameCLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StartDateLabel = new System.Windows.Forms.Label();
            this.AlarmCountLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ReloadButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.ImageFolderTextBox = new System.Windows.Forms.TextBox();
            this.MovieFolderTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ImageFolderSelectButton = new System.Windows.Forms.Button();
            this.MovieFolderSelectButton = new System.Windows.Forms.Button();
            this.RestructProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "データベース：";
            // 
            // RestructButton
            // 
            this.RestructButton.Location = new System.Drawing.Point(107, 191);
            this.RestructButton.Name = "RestructButton";
            this.RestructButton.Size = new System.Drawing.Size(263, 52);
            this.RestructButton.TabIndex = 5;
            this.RestructButton.Text = "画像抽出";
            this.RestructButton.UseVisualStyleBackColor = true;
            this.RestructButton.Click += new System.EventHandler(this.RestructButton_Click);
            // 
            // TerminalInfoLabel
            // 
            this.TerminalInfoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TerminalInfoLabel.Location = new System.Drawing.Point(107, 37);
            this.TerminalInfoLabel.Name = "TerminalInfoLabel";
            this.TerminalInfoLabel.Size = new System.Drawing.Size(180, 23);
            this.TerminalInfoLabel.TabIndex = 6;
            this.TerminalInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "端末情報：";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 256);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(700, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(303, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "警報保存日数：";
            // 
            // AlarmSaveDaysLabel
            // 
            this.AlarmSaveDaysLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AlarmSaveDaysLabel.Location = new System.Drawing.Point(392, 37);
            this.AlarmSaveDaysLabel.Name = "AlarmSaveDaysLabel";
            this.AlarmSaveDaysLabel.Size = new System.Drawing.Size(85, 23);
            this.AlarmSaveDaysLabel.TabIndex = 10;
            this.AlarmSaveDaysLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(513, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "表示日数：";
            // 
            // DisplayDaysLabel
            // 
            this.DisplayDaysLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DisplayDaysLabel.Location = new System.Drawing.Point(578, 37);
            this.DisplayDaysLabel.Name = "DisplayDaysLabel";
            this.DisplayDaysLabel.Size = new System.Drawing.Size(85, 23);
            this.DisplayDaysLabel.TabIndex = 12;
            this.DisplayDaysLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(483, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "日";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(671, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "日";
            // 
            // ConnectionStringLabel
            // 
            this.ConnectionStringLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ConnectionStringLabel.Location = new System.Drawing.Point(107, 9);
            this.ConnectionStringLabel.Name = "ConnectionStringLabel";
            this.ConnectionStringLabel.Size = new System.Drawing.Size(579, 23);
            this.ConnectionStringLabel.TabIndex = 6;
            this.ConnectionStringLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "警報種別：";
            // 
            // AlarmNameALabel
            // 
            this.AlarmNameALabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AlarmNameALabel.Location = new System.Drawing.Point(107, 65);
            this.AlarmNameALabel.Name = "AlarmNameALabel";
            this.AlarmNameALabel.Size = new System.Drawing.Size(180, 23);
            this.AlarmNameALabel.TabIndex = 16;
            this.AlarmNameALabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AlarmNameBLabel
            // 
            this.AlarmNameBLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AlarmNameBLabel.Location = new System.Drawing.Point(297, 65);
            this.AlarmNameBLabel.Name = "AlarmNameBLabel";
            this.AlarmNameBLabel.Size = new System.Drawing.Size(180, 23);
            this.AlarmNameBLabel.TabIndex = 17;
            this.AlarmNameBLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AlarmNameCLabel
            // 
            this.AlarmNameCLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AlarmNameCLabel.Location = new System.Drawing.Point(483, 65);
            this.AlarmNameCLabel.Name = "AlarmNameCLabel";
            this.AlarmNameCLabel.Size = new System.Drawing.Size(180, 23);
            this.AlarmNameCLabel.TabIndex = 18;
            this.AlarmNameCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "抽出開始日：";
            // 
            // StartDateLabel
            // 
            this.StartDateLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.StartDateLabel.Location = new System.Drawing.Point(107, 93);
            this.StartDateLabel.Name = "StartDateLabel";
            this.StartDateLabel.Size = new System.Drawing.Size(180, 23);
            this.StartDateLabel.TabIndex = 20;
            this.StartDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AlarmCountLabel
            // 
            this.AlarmCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AlarmCountLabel.Location = new System.Drawing.Point(392, 93);
            this.AlarmCountLabel.Name = "AlarmCountLabel";
            this.AlarmCountLabel.Size = new System.Drawing.Size(85, 23);
            this.AlarmCountLabel.TabIndex = 21;
            this.AlarmCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(315, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "対象警報数：";
            // 
            // ReloadButton
            // 
            this.ReloadButton.Location = new System.Drawing.Point(488, 93);
            this.ReloadButton.Name = "ReloadButton";
            this.ReloadButton.Size = new System.Drawing.Size(200, 23);
            this.ReloadButton.TabIndex = 23;
            this.ReloadButton.Text = "再読込";
            this.ReloadButton.UseVisualStyleBackColor = true;
            this.ReloadButton.Click += new System.EventHandler(this.ReloadButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "静止画フォルダー：";
            // 
            // ImageFolderTextBox
            // 
            this.ImageFolderTextBox.Location = new System.Drawing.Point(107, 141);
            this.ImageFolderTextBox.Name = "ImageFolderTextBox";
            this.ImageFolderTextBox.Size = new System.Drawing.Size(528, 19);
            this.ImageFolderTextBox.TabIndex = 25;
            // 
            // MovieFolderTextBox
            // 
            this.MovieFolderTextBox.Location = new System.Drawing.Point(107, 166);
            this.MovieFolderTextBox.Name = "MovieFolderTextBox";
            this.MovieFolderTextBox.Size = new System.Drawing.Size(528, 19);
            this.MovieFolderTextBox.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 12);
            this.label11.TabIndex = 27;
            this.label11.Text = "動画フォルダー：";
            // 
            // ImageFolderSelectButton
            // 
            this.ImageFolderSelectButton.Location = new System.Drawing.Point(641, 139);
            this.ImageFolderSelectButton.Name = "ImageFolderSelectButton";
            this.ImageFolderSelectButton.Size = new System.Drawing.Size(47, 23);
            this.ImageFolderSelectButton.TabIndex = 28;
            this.ImageFolderSelectButton.Text = "参照";
            this.ImageFolderSelectButton.UseVisualStyleBackColor = true;
            this.ImageFolderSelectButton.Click += new System.EventHandler(this.ImageFolderSelectButton_Click);
            // 
            // MovieFolderSelectButton
            // 
            this.MovieFolderSelectButton.Location = new System.Drawing.Point(641, 164);
            this.MovieFolderSelectButton.Name = "MovieFolderSelectButton";
            this.MovieFolderSelectButton.Size = new System.Drawing.Size(47, 23);
            this.MovieFolderSelectButton.TabIndex = 29;
            this.MovieFolderSelectButton.Text = "参照";
            this.MovieFolderSelectButton.UseVisualStyleBackColor = true;
            this.MovieFolderSelectButton.Click += new System.EventHandler(this.MovieFolderSelectButton_Click);
            // 
            // RestructProgressBar
            // 
            this.RestructProgressBar.Location = new System.Drawing.Point(392, 191);
            this.RestructProgressBar.Name = "RestructProgressBar";
            this.RestructProgressBar.Size = new System.Drawing.Size(296, 52);
            this.RestructProgressBar.Step = 1;
            this.RestructProgressBar.TabIndex = 30;
            // 
            // RestructForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 278);
            this.Controls.Add(this.RestructProgressBar);
            this.Controls.Add(this.MovieFolderSelectButton);
            this.Controls.Add(this.ImageFolderSelectButton);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.MovieFolderTextBox);
            this.Controls.Add(this.ImageFolderTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ReloadButton);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.AlarmCountLabel);
            this.Controls.Add(this.StartDateLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.AlarmNameCLabel);
            this.Controls.Add(this.AlarmNameBLabel);
            this.Controls.Add(this.AlarmNameALabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.DisplayDaysLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AlarmSaveDaysLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ConnectionStringLabel);
            this.Controls.Add(this.TerminalInfoLabel);
            this.Controls.Add(this.RestructButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RestructForm";
            this.Text = "表示用警報データ再構築";
            this.Load += new System.EventHandler(this.RestructForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button RestructButton;
        private System.Windows.Forms.Label TerminalInfoLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label AlarmSaveDaysLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label DisplayDaysLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label ConnectionStringLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label AlarmNameALabel;
        private System.Windows.Forms.Label AlarmNameBLabel;
        private System.Windows.Forms.Label AlarmNameCLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label StartDateLabel;
        private System.Windows.Forms.Label AlarmCountLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button ReloadButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ImageFolderTextBox;
        private System.Windows.Forms.TextBox MovieFolderTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button ImageFolderSelectButton;
        private System.Windows.Forms.Button MovieFolderSelectButton;
        private System.Windows.Forms.ProgressBar RestructProgressBar;
    }
}

