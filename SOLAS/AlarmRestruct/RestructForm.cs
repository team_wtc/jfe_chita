﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CES.SOLAS.Lib.Data;

namespace AlarmRestruct
{
    public partial class RestructForm : Form
    {
        readonly SOLASDataSet _SOLASDataSet = new SOLASDataSet();
        readonly string _ConnectionString;

        public RestructForm()
        {
            InitializeComponent();
            _ConnectionString = $"Data Source={Properties.Settings.Default.DBServer};Initial Catalog=SOLAS;Persist Security Info=True;User ID=sa;Password={Properties.Settings.Default.DBPassword}";
        }

        private async void RestructForm_Load( object sender, EventArgs e )
        {
            ConnectionStringLabel.Text = _ConnectionString;

            RestructProgressBar.Visible = false;
            ImageFolderTextBox.Text = Properties.Settings.Default.ImageFolder;
            MovieFolderTextBox.Text = Properties.Settings.Default.MovieFolder;
            if ( await LoadInformation() != true )
            {
                Close();
                return;
            }
        }

        private async void ReloadButton_Click( object sender, EventArgs e )
        {
            if ( await LoadInformation() != true )
            {
                Close();
                return;
            }
        }

        private void ImageFolderSelectButton_Click( object sender, EventArgs e )
        {
            using ( var Dlg = new FolderBrowserDialog()
            {
                Description = "静止画保存先フォルダー選択",
                RootFolder= Environment.SpecialFolder.Desktop,
                SelectedPath= ImageFolderTextBox.Text,
                ShowNewFolderButton=true
            } )
            {
                if ( Dlg.ShowDialog() == DialogResult.OK )
                {
                    ImageFolderTextBox.Text = Dlg.SelectedPath;
                }
            }
        }

        private void MovieFolderSelectButton_Click( object sender, EventArgs e )
        {
            using ( var Dlg = new FolderBrowserDialog()
            {
                Description = "動画保存先フォルダー選択",
                RootFolder = Environment.SpecialFolder.Desktop,
                SelectedPath = MovieFolderTextBox.Text,
                ShowNewFolderButton = true
            } )
            {
                if ( Dlg.ShowDialog() == DialogResult.OK )
                {
                    MovieFolderTextBox .Text = Dlg.SelectedPath;
                }
            }
        }

        private async void RestructButton_Click( object sender, EventArgs e )
        {
            RestructProgressBar.Visible = true;
            Cursor = Cursors.WaitCursor;

            AlarmDataSet DS = await RestructAlarm();

            var DlgResult = MessageBox.Show(
                            $"警報情報の再構築を完了しました。{Environment.NewLine}" +
                            $"再構築されたデータセットを保存しますか？",
                            "警報情報再構築",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question );

            if ( DlgResult == DialogResult.Yes )
            {
                using ( var Dlg = new SaveFileDialog()
                {
                    AddExtension = true,
                    AutoUpgradeEnabled = true,
                    CheckFileExists = false,
                    CheckPathExists = true,
                    CreatePrompt = true,
                    DefaultExt = ".xml",
                    FileName = System.IO.Path.Combine( Application.StartupPath, "Monitoring.xml" ),
                    Filter = "データセットファイル|*.xml",
                    InitialDirectory = Application.StartupPath,
                    OverwritePrompt = true,
                    RestoreDirectory = true,
                    ShowHelp = false,
                    Title = "データセット保存",
                    ValidateNames = true
                } )
                {
                    if ( Dlg.ShowDialog() == DialogResult.OK )
                    {
                        DS.WriteXml( Dlg.FileName );
                        MessageBox.Show( $"データセットを({Dlg.FileName})に保存しました。", "警報情報再構築" );
                    }
                }
            }

            Cursor = Cursors.Default;
            RestructProgressBar.Visible = false;
        }
    }
}
