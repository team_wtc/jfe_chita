﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace CES.SOLAS.Lib.AlarmDevice
{
    public class DN1500GX : IAlarmDevice, IDisposable
    {
        const string ClearPatternString = "00000";
        const string BuzzerStopPatternString = "XXX00";
        const string ApplicationErrorPatternString = "11120";
        const string ManagementPathString = @"\\.\root\snmp\localhost:SNMP_RFC1213_MIB_system";
        const string EventQueryString = @"SELECT * FROM SnmpNotification";
        const string CommandString = "ACOP";

        readonly string _DeviceAddress;
        readonly int _DevicePort;
        readonly string _AppErrorPattern;

        TcpClient _TcpClient;

        public DN1500GX( string Address, int Port , string ApplicationErrorPattern )
        {
            if ( string.IsNullOrWhiteSpace( ApplicationErrorPattern ) == true )
            {
                _AppErrorPattern = ApplicationErrorPatternString;
            }
            else
            {
                _AppErrorPattern = ApplicationErrorPattern;
            }
            _DeviceAddress = Address;
            _DevicePort = Port;
            _TcpClient = null;
        }

        private void Connect()
        {
            try
            {
                if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
                {
                    return;
                }

                _TcpClient = new TcpClient();
                _TcpClient.Connect( _DeviceAddress, _DevicePort );
                if ( _TcpClient.Connected == false )
                {
                    try
                    {
                        _TcpClient.Dispose();
                        _TcpClient = null;
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public void Disconnect()
        {
            try
            {
                if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
                {
                    _TcpClient.Client.Shutdown( SocketShutdown.Both );
                    _TcpClient.Close();
                    _TcpClient.Dispose();
                    _TcpClient = null;
                }
            }
            catch
            {
            }
        }

        private void SendCommand( byte[] SendData )
        {
            try
            {
                Connect();

                if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
                {
                    _TcpClient.Client.Send( SendData );
                    RecvResponse();
                }
            }
            catch
            {
                throw;
            }
        }

        private void RecvResponse()
        {
            byte[] RecvBuffer = new byte[ 1024 ];
            var Len = _TcpClient.Client.Receive( RecvBuffer );
            if ( Len <= 0 )
            {
                throw new ApplicationException( "Socketが切断されました。" );
            }

            List<byte> RecvData = new List<byte>();
            for ( int i = 0 ; i < Len ; i++ )
            {
                RecvData.Add( RecvBuffer[ i ] );
            }

            var ResponseString = Encoding.ASCII.GetString( RecvData.ToArray() );
            if ( ResponseString.StartsWith( Environment.NewLine ) == false )
            {
                if ( ResponseString.StartsWith( "ER01" ) == true )
                {
                    throw new ApplicationException( "エラーレスポンス ER01:無効なコマンド" );
                }
                else if ( ResponseString.StartsWith( "ER02" ) == true )
                {
                    throw new ApplicationException( "エラーレスポンス ER02:行末コードが異なる" );
                }
                else if ( ResponseString.StartsWith( "ER03" ) == true )
                {
                    throw new ApplicationException( "エラーレスポンス ER03:コマンド引数の間違い" );
                }
                else if ( ResponseString.StartsWith( "ER04" ) == true )
                {
                    throw new ApplicationException( "エラーレスポンス ER04:コマンド実行失敗" );
                }
                else
                {
                    throw new ApplicationException( $"不明なエラーレスポンス({ResponseString})" );
                }
            }
        }
        #region IAlarmDevice Support

        public void Alarm( string AlarmPattern )
        {
            var PatternStr = ( AlarmPattern.Length > 5 ) ? AlarmPattern.Substring( 0, 5 ) : AlarmPattern;
            if ( DisableBuzzer == true )
            {
                PatternStr = $"{PatternStr.Substring( 0, 3 )}00";
            }

            var CommandStr = $"{CommandString} {PatternStr}{Environment.NewLine}";
            SendCommand( Encoding.ASCII.GetBytes( CommandStr ) );
        }

        public void Clear()
        {
            Alarm( ClearPatternString );
        }

        public void BuzzerStop()
        {
            Alarm( BuzzerStopPatternString );
        }

        public void ApplicationError()
        {
            Alarm( _AppErrorPattern );
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        public bool DisableBuzzer
        {
            get;
            set;
        } = false;

        protected virtual void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    Disconnect();
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~DN1500GX() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }

        #endregion
    }
}
