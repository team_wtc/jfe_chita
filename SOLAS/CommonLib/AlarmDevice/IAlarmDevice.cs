﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CES.SOLAS.Lib.AlarmDevice
{
    public interface IAlarmDevice
    {
        bool DisableBuzzer
        {
            get;set;
        }

        void Alarm( string AlarmPattern );

        void Clear();

        void BuzzerStop();
    }
}
