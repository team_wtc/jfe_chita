﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CES.SOLAS.Lib.Camera
{
    using UI;

    public class ESCA : HTTPAccess
    {
        const int DEFAULT_CHANNEL_NUMBER = 0;
        const int DEFAULT_PRESET_NUMBER = 1;

        enum CGI_MODE
        {
            CGI_GET_PTZ,
            CGI_SET_PTZ,
            CGI_GET_VIDEO,
            CGI_SET_PRESET,
            CGI_CALL_PRESET,
            CGI_VIDEO_PLAYBACK
        }

        public enum STREAMING_MODE
        {
            VIDEO=0,
            COMPOSITE=1
        }

        public enum ENCODING_LEVEL
        {
            BASELINE=0,
            MAIN=1,
            HIGH=2
        }

        public enum ENCODING_TYPE
        {
            H264=0,
            MJPEG=1,
            JPEG=2,
            MPEG4=3,
            H265=4
        }

        public enum BITRATE_TYPE
        {
            CBR=0,
            VBR=1,
            FIXQP=2
        }

        public enum QUALITY
        {
            HIGH=0,
            MEDIUM=1,
            LOW=2
        }

        public enum RESOLUTION : uint
        {
            /// <summary>
            /// 176x144
            /// </summary>
            QCIF=0x000001,
            /// <summary>
            /// 352x288
            /// </summary>
            CIF=0x000002,
            /// <summary>
            /// 720x240
            /// </summary>
            HD1=0x000004,
            /// <summary>
            /// 720x480
            /// </summary>
            D1=0x000008,
            /// <summary>
            /// 160x120
            /// </summary>
            QQVGA=0x000010,
            /// <summary>
            /// 320x240
            /// </summary>
            QVGA=0x000020,
            /// <summary>
            /// 640x480
            /// </summary>
            VGA=0x000040,
            /// <summary>
            /// 800x600
            /// </summary>
            SVGA=0x000080,
            /// <summary>
            /// 1024x768
            /// </summary>
            XGA=0x000100,
            /// <summary>
            /// 1280x768
            /// </summary>
            WXGA=0x000200,
            /// <summary>
            /// 1280x1024
            /// </summary>
            SXGA=0x000400,
            /// <summary>
            /// 1600x1200
            /// </summary>
            UXGA=0x000800,
            /// <summary>
            /// 1280x720
            /// </summary>
            _720P=0x001000,
            /// <summary>
            /// 1280x960
            /// </summary>
            _960P = 0x002000,
            /// <summary>
            /// 1920x1080
            /// </summary>
            _1080P = 0x004000,
            /// <summary>
            /// 2048x1536
            /// </summary>
            _3MEGA = 0x008000,
            /// <summary>
            /// 2272x1704
            /// </summary>
            _4MEGA = 0x010000,
            /// <summary>
            /// 2592x1944
            /// </summary>
            _5MEGA=0x020000,
            /// <summary>
            /// 3840x2160
            /// </summary>
            UltraHD=0x040000,
            /// <summary>
            /// 800x480
            /// </summary>
            WVGA=0x080000,
            /// <summary>
            /// 400x240
            /// </summary>
            WQVGA=0x100000
        }

        public enum STANDARD
        {
            PAL=0,
            NTSC=1
        }

        readonly Random _Random;
        readonly int _Channel;
        readonly int _HomePositionPresetNo;

        CGI_MODE _CGIMode;
        int _nRanId;

        public ESCA(
            ISOLASApp SOLASApp,
            string CameraAddress,
            string CameraAccount,
            string CameraPassword,
            int Pan = 0,
            int Tilt = 0,
            int Zoom = 0,
            int ZoomRatio = 0,
            int CameraPort = DEFAULT_CGI_PORT,
            int Channel = DEFAULT_CHANNEL_NUMBER,
            int PresetNo=DEFAULT_PRESET_NUMBER )
            : base( SOLASApp, CameraAddress, CameraAccount, CameraPassword )
        {
            _Random = new Random();
            _Channel = Channel;
            _HomePositionPresetNo = PresetNo;
            this.Pan = Pan;
            this.Tilt = Tilt;
            this.Zoom = Zoom;
            this.ZoomRatio = ZoomRatio;
        }

        public int Pan
        {
            get;
            private set;
        }
        public int Tilt
        {
            get;
            private set;
        }
        public int Zoom
        {
            get;
            private set;
        }
        public int ZoomRatio
        {
            get;
            private set;
        }

        public STREAMING_MODE StreamingMode
        {
            get;
            private set;
        } = STREAMING_MODE.VIDEO;

        public ENCODING_LEVEL EncodingLevel
        {
            get;
            private set;
        } = ENCODING_LEVEL.BASELINE;

        public int FrameRate
        {
            get;
            private set;
        } = 1;

        public bool FrameRarePriority
        {
            get;
            private set;
        } = true;

        public int IFrameInterval
        {
            get;
            private set;
        } = 1;

        public ENCODING_TYPE EncodingType
        {
            get;
            private set;
        } = ENCODING_TYPE.H264;

        public int BitRate
        {
            get;
            private set;
        }

        public BITRATE_TYPE BitRateType
        {
            get;
            private set;
        }

        public QUALITY Quality
        {
            get;
            private set;
        }

        public RESOLUTION Resolution
        {
            get;
            private set;
        }

        public STANDARD Standard
        {
            get;
            private set;
        }

        public int IFrameQuality
        {
            get;
            private set;
        }

        public int PFrameQuality
        {
            get;
            private set;
        }

        public (int Width, int Height) ImageSize
        {
            get
            {
                switch ( Resolution )
                {
                    case RESOLUTION.CIF:
                        return (352, 288);
                    case RESOLUTION.D1:
                        return (720, 480);
                    case RESOLUTION.HD1:
                        return (720, 240);
                    case RESOLUTION.QCIF:
                        return (176, 144);
                    case RESOLUTION.QQVGA:
                        return (160, 120);
                    case RESOLUTION.QVGA:
                        return (320, 240);
                    case RESOLUTION.SVGA:
                        return (800, 600);
                    case RESOLUTION.SXGA:
                        return (1280, 1024);
                    case RESOLUTION.UltraHD:
                        return (3840, 2160);
                    case RESOLUTION.UXGA:
                        return (1600, 1200);
                    case RESOLUTION.VGA:
                        return (640, 480);
                    case RESOLUTION.WQVGA:
                        return (400, 240);
                    case RESOLUTION.WVGA:
                        return (800, 480);
                    case RESOLUTION.WXGA:
                        return (1280, 768);
                    case RESOLUTION.XGA:
                        return (1024, 768);
                    case RESOLUTION._1080P:
                        return (1920, 1080);
                    case RESOLUTION._3MEGA:
                        return (2048, 1536);
                    case RESOLUTION._4MEGA:
                        return (2272, 1704);
                    case RESOLUTION._5MEGA:
                        return (2592, 1944);
                    case RESOLUTION._720P:
                        return (1280, 720);
                    case RESOLUTION._960P:
                        return (1280, 960);
                }

                return (0, 0);
            }
        }

        public bool CheckPTZ()
        {
            _CGIMode = CGI_MODE.CGI_GET_PTZ;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Get( out string ResponceString );

            ThrowException( ResponceString );

            ReadPTZ( ResponceString, out int NewPan, out int NewTilt, out int NewZoom, out int NewZoomRatio );
            _SOLASApp.Debug( $"ESCA CheckPTZ Pan={NewPan} Tilt={NewTilt} Zoom={NewZoom} Zoom Ratio={NewZoomRatio}" );

            return (
                ( this.Pan >= ( NewPan - 5 ) ) && ( this.Pan <= ( NewPan + 5 ) ) &&
                ( this.Tilt >= ( NewTilt - 5 ) ) && ( this.Tilt <= ( NewTilt + 5 ) ) &&
                ( this.Zoom >= ( NewZoom - 5 ) ) && ( this.Zoom <= ( NewZoom + 5 ) ) &&
                ( this.ZoomRatio >= ( NewZoomRatio - 5 ) ) && ( this.ZoomRatio <= ( NewZoomRatio + 5 ) ) );
        }

        public void RTSPInfo( out string ResponceString )
        {
            _CGIMode = CGI_MODE.CGI_VIDEO_PLAYBACK;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Get( out ResponceString );
            ThrowException( ResponceString );
        }

        public void VideInfo( out string ResponceString )
        {
            _CGIMode = CGI_MODE.CGI_GET_VIDEO;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Get( out ResponceString );
            ThrowException( ResponceString );

            ReadVideoInfo( ResponceString );
        }

        public void UpdatePTZ()
        {
            _CGIMode = CGI_MODE.CGI_GET_PTZ;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Get( out string ResponceString );

            ThrowException( ResponceString );

            ReadPTZ( ResponceString, out int NewPan, out int NewTilt, out int NewZoom, out int NewZoomRatio );
            _SOLASApp.Debug( $"ESCA UpdatePTZ Pan={NewPan} Tilt={NewTilt} Zoom={NewZoom} Zoom Ratio={NewZoomRatio}" );

            this.Pan = NewPan;
            this.Tilt = NewTilt;
            this.Zoom = NewZoom;
            this.ZoomRatio = NewZoomRatio;
        }

        public void ResetPTZ()
        {
            _CGIMode = CGI_MODE.CGI_SET_PTZ;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Post( Encoding.ASCII.GetBytes( _nRanId.ToString( "D8" ) ), out string ResponceString );
            ThrowException( ResponceString );
        }

        public void SetPreset()
        {
            _CGIMode = CGI_MODE.CGI_SET_PRESET;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Post( Encoding.ASCII.GetBytes( _nRanId.ToString( "D8" ) ), out string ResponceString );
            ThrowException( ResponceString );
        }

        public void GotoPreset()
        {
            _CGIMode = CGI_MODE.CGI_CALL_PRESET;
            _nRanId = _Random.Next( minValue: 10000000, maxValue: 99999999 );

            Post( Encoding.ASCII.GetBytes( _nRanId.ToString( "D8" ) ), out string ResponceString );
            ThrowException( ResponceString );
        }

        protected override void AddHeader( HttpWebRequest Request )
        {
            const string AUTHORIZATION_HEADER_NAME = "Authorization";
            const string USER_AGENT_HEADER_NAME = "User_Agent";
            const string USER_AGENT_HEADER_VALUE = "IPC_CGI";

            string AuthParamString = $"{_CameraAccount}:{_CameraPassword}";
            Request.Headers[ AUTHORIZATION_HEADER_NAME ] = $"Basic {Convert.ToBase64String( Encoding.ASCII.GetBytes( AuthParamString ) )}";
            Request.Headers[ USER_AGENT_HEADER_NAME ] = USER_AGENT_HEADER_VALUE;
        }

        protected override Uri CreateUri()
        {
            const string CGI_PTZ_PATH = "/cgi-bin/param.cgi";
            const string CGI_PRESET_PATH = "/cgi-bin/control.cgi";
            const string CGI_VIDEO_CALLBACK_PATH = "/cgi-bin/video.cgi";

            UriBuilder Builder = new UriBuilder()
            {
                Host = _CameraAddress,
                Port = _CameraPort,
                Scheme = ( _CameraPort == DEFAULT_CGI_SSL_PORT ) ? Uri.UriSchemeHttps : Uri.UriSchemeHttp
                //Path = CGI_PATH
            };

            if ( _CGIMode == CGI_MODE.CGI_GET_PTZ )
            {
                Builder.Path = CGI_PTZ_PATH;
                Builder.Query =
                    $"action=list&" +
                    $"group=PTZPOS&" +
                    $"channel={_Channel}&" +
                    $"nRanId={_nRanId}";
            }
            else if ( _CGIMode == CGI_MODE.CGI_SET_PTZ )
            {
                Builder.Path = CGI_PTZ_PATH;
                Builder.Query =
                    $"action=update&" +
                    $"group=PTZPOS&" +
                    $"channel={_Channel}&" +
                    $"PTZPOS.panpos={Pan}&" +
                    $"PTZPOS.tiltpos={Tilt}&" +
                    $"PTZPOS.zoompos={Zoom}&" +
                    $"PTZPOS.zoomratio={ZoomRatio}&" +
                    $"nRanId={_nRanId}";
            }
            else if ( _CGIMode == CGI_MODE.CGI_GET_VIDEO )
            {
                Builder.Path = CGI_PTZ_PATH;
                Builder.Query =
                    $"action=list&" +
                    $"group=VENC&" +
                    $"channel={_Channel}&" +
                    $"streamType=2&" +
                    $"nRanId={_nRanId}";
            }
            else if ( _CGIMode == CGI_MODE.CGI_SET_PRESET )
            {
                Builder.Path = CGI_PRESET_PATH;
                Builder.Query =
                    $"action=update&" +
                    $"group=PTZCTRL&" +
                    $"channel={_Channel}&" +
                    $"PTZCTRL.action=10&" +
                    $"PTZCTRL.no={_HomePositionPresetNo}&" +
                    $"nRanId={_nRanId}";
            }
            else if ( _CGIMode == CGI_MODE.CGI_CALL_PRESET )
            {
                Builder.Path = CGI_PRESET_PATH;
                Builder.Query =
                    $"action=update&" +
                    $"group=PTZCTRL&" +
                    $"channel={_Channel}&" +
                    $"PTZCTRL.action=11&" +
                    $"PTZCTRL.no={_HomePositionPresetNo}&" +
                    $"nRanId={_nRanId}";
            }
            else if ( _CGIMode == CGI_MODE.CGI_VIDEO_PLAYBACK )
            {
                Builder.Path = CGI_VIDEO_CALLBACK_PATH;
                Builder.Query =
                    $"action=list&" +
                    $"group=RTSP&" +
                    $"channel={_Channel}&" +
                    $"nRanId={_nRanId}";
            }

            return Builder.Uri;
        }

        private void ThrowException( string ResponceString )
        {
            const string ERR_CODE_PATTERN = @"^root\.ERR\.no\s*=\s*(?<no>\d+)$";
            const string ERR_MSG_PATTERN = @"^root\.ERR\.des\s*=\s*(?<msg>.*)$";

            var Lines = ResponceString.Replace( "\r\n", "\n" ).Split( '\n' );

            int? ErrNo = null;
            string ErrMsg = null;

            foreach ( var LineString in Lines )
            {
                var NoMatch = Regex.Match( LineString, ERR_CODE_PATTERN );
                if ( NoMatch.Success == true )
                {
                    ErrNo = Convert.ToInt32( NoMatch.Groups[ "no" ].Value );
                    continue;
                }

                var MsgMatch = Regex.Match( LineString, ERR_MSG_PATTERN );
                if ( MsgMatch.Success == true )
                {
                    ErrMsg = MsgMatch.Groups[ "msg" ].Value;
                }

                if ( ( ErrNo.HasValue == true ) && ( string.IsNullOrWhiteSpace( ErrMsg ) == false ) )
                {
                    break;
                }
            }

            if ( ( ErrNo.HasValue == true ) && ( ErrNo.Value != 0 ) )
            {
                if ( string.IsNullOrWhiteSpace( ErrMsg ) == false )
                {
                    throw new ApplicationException( $"ESCA CGI Error({ErrNo.Value}) {ErrMsg}" );
                }
                else
                {
                    throw new ApplicationException( $"ESCA CGI Error({ErrNo.Value})" );
                }
            }

        }

        private void ReadPTZ( string ResponceString, out int Pan,out int Tilt,out int Zoom,out int ZoomRtio )
        {
            const string PAN_PATTERN = @"^root\.PTZPOS\.panpos\s*=\s*(?<p>\d+)$";
            const string TILT_PATTERN = @"^root\.PTZPOS\.tiltpos\s*=\s*(?<t>\d+)$";
            const string ZOOM_PATTERN = @"^root\.PTZPOS\.zoompos\s*=\s*(?<z>\d+)$";
            const string RATIO_PATTERN = @"^root\.PTZPOS\.zoomratio\s*=\s*(?<r>\d+)$";

            var Lines = ResponceString.Replace( "\r\n", "\n" ).Split( '\n' );

            Pan = -1;
            Tilt = -1;
            Zoom = -1;
            ZoomRtio = -1;

            foreach ( var LineString in Lines )
            {
                var ValMatch = Regex.Match( LineString, PAN_PATTERN );
                if ( ValMatch.Success == true )
                {
                    Pan = Convert.ToInt32( ValMatch.Groups[ "p" ].Value );
                    continue;
                }

                ValMatch = Regex.Match( LineString, TILT_PATTERN );
                if ( ValMatch.Success == true )
                {
                    Tilt = Convert.ToInt32( ValMatch.Groups[ "t" ].Value );
                    continue;
                }

                ValMatch = Regex.Match( LineString, ZOOM_PATTERN );
                if ( ValMatch.Success == true )
                {
                    Zoom = Convert.ToInt32( ValMatch.Groups[ "z" ].Value );
                    continue;
                }

                ValMatch = Regex.Match( LineString, RATIO_PATTERN );
                if ( ValMatch.Success == true )
                {
                    ZoomRtio = Convert.ToInt32( ValMatch.Groups[ "r" ].Value );
                }

                if ( ( Pan >= 0 ) && ( Tilt >= 0 ) && ( Zoom >= 0 ) && ( ZoomRatio >= 0 ) )
                {
                    break;
                }
            }
        }

        private void ReadVideoInfo( string ResponceString )
        {
            const string PATTERN = @"^root\.VENC\.(?<NAME>.*)=\s*(?<VALUE>\d+)$";

            var Lines = ResponceString.Replace( "\r\n", "\n" ).Split( '\n' );

            foreach ( var LineString in Lines )
            {
                var ValMatch = Regex.Match( LineString, PATTERN );
                if ( ValMatch.Success == false )
                {
                    continue;
                }

                switch ( ValMatch.Groups[ "NAME" ].Value )
                {
                    case "streamMixType":
                        if ( Enum.IsDefined( typeof( STREAMING_MODE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            StreamingMode = ( STREAMING_MODE )Enum.ToObject( typeof( STREAMING_MODE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "h264EncLvl":
                        if ( Enum.IsDefined( typeof( ENCODING_LEVEL ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            EncodingLevel = ( ENCODING_LEVEL )Enum.ToObject( typeof( ENCODING_LEVEL ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "frameRate":
                        FrameRate = Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value );
                        break;
                    case "frPreeferred":
                        FrameRarePriority = ValMatch.Groups[ "VALUE" ].Value.Equals( "1" );
                        break;
                    case "iFrameIntv":
                        IFrameInterval = Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value );
                        break;
                    case "veType":
                        if ( Enum.IsDefined( typeof( ENCODING_TYPE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            EncodingType = ( ENCODING_TYPE )Enum.ToObject( typeof( ENCODING_TYPE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "bitRate":
                        BitRate = ( Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) * 1000 );   // Kbit -> bit
                        break;
                    case "bitRateType":
                        if ( Enum.IsDefined( typeof( BITRATE_TYPE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            BitRateType = ( BITRATE_TYPE )Enum.ToObject( typeof( BITRATE_TYPE ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "quality":
                        if ( Enum.IsDefined( typeof( QUALITY ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            Quality = ( QUALITY )Enum.ToObject( typeof( QUALITY ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "iQp":
                        IFrameQuality = Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value );
                        break;
                    case "pQp":
                        PFrameQuality = Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value );
                        break;
                    case "resolution":
                        if ( Enum.IsDefined( typeof( RESOLUTION ), Convert.ToUInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            Resolution = ( RESOLUTION )Enum.ToObject( typeof( RESOLUTION ), Convert.ToUInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                    case "standard":
                        if ( Enum.IsDefined( typeof( STANDARD ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) ) == true )
                        {
                            Standard = ( STANDARD )Enum.ToObject( typeof( STANDARD ), Convert.ToInt32( ValMatch.Groups[ "VALUE" ].Value ) );
                        }
                        break;
                }
            }
        }
    }
}
