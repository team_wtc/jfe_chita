﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Mime;

using static System.Web.HttpUtility;

namespace CES.SOLAS.Lib.Camera
{
    using UI;

    public abstract class HTTPAccess
    {
        protected const int DEFAULT_CGI_PORT = 80;
        protected const int DEFAULT_CGI_SSL_PORT = 443;

        protected readonly string _CameraAddress;
        protected readonly int _CameraPort;
        protected readonly string _CameraAccount;
        protected readonly string _CameraPassword;
        protected readonly ISOLASApp _SOLASApp;
        public HTTPAccess( ISOLASApp SOLASApp,  string CameraAddress, string CameraAccount, string CameraPassword, int CameraPort = DEFAULT_CGI_PORT )
        {
            _CameraAddress = CameraAddress;
            _CameraAccount = CameraAccount;
            _CameraPassword = CameraPassword;
            _CameraPort = CameraPort;
            _SOLASApp = SOLASApp;
        }

        protected abstract Uri CreateUri();

        protected abstract void AddHeader( HttpWebRequest Request );

        public virtual void Get( out string ResponceString )
        {
            try
            {
                var Request = HttpWebRequest.CreateHttp( CreateUri() );
               
                Request.Credentials = new NetworkCredential( _CameraAccount, _CameraPassword );
                AddHeader( Request );

                _SOLASApp.Debug( $"HTTPAccess GET { Request.RequestUri}" );

                using ( var Responce = Request.GetResponse() )
                using ( var Reader = new StreamReader( Responce.GetResponseStream() ) )
                {
                    ResponceString = Reader.ReadToEnd();
                    _SOLASApp.Debug( $"HTTPAccess GET Responce {ResponceString}" );
                }
            }
            catch ( Exception )
            {
                throw;
            }
        }

        public virtual void Post( byte[] PostData, out string ResponceString )
        {
            try
            {
                var Request = HttpWebRequest.CreateHttp( CreateUri() );

                Request.Credentials = new NetworkCredential( _CameraAccount, _CameraPassword );
                AddHeader( Request );
                _SOLASApp.Debug( $"HTTPAccess POST { Request.RequestUri}" );

                Request.Method = HttpMethod.Post.Method;
                Request.ContentType = MediaTypeNames.Text.Plain;
                Request.ContentLength = PostData.Length;

                using ( var Writer = new BinaryWriter( Request.GetRequestStream() ) )
                {
                    Writer.Write( PostData );
                }

                var Responce = Request.GetResponse();
                using ( var Reader = new StreamReader( Responce.GetResponseStream() ) )
                {
                    ResponceString = Reader.ReadToEnd();
                    _SOLASApp.Debug( $"HTTPAccess POST Responce {ResponceString}" );
                }
            }
            catch ( Exception )
            {
                throw;
            }
        }

        public virtual void Post( string PostString, out string ResponceString )
        {
            Post( Encoding.ASCII.GetBytes( PostString ), out ResponceString );
        }
    }
}
