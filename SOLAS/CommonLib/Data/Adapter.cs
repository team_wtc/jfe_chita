﻿using System.Data.SqlClient;
using System.ComponentModel;

namespace CES.SOLAS.Lib.Data
{
    using SOLASDataSetTableAdapters;

    public class Adapter
    {
        const string CONNECTION_DATABASE = "SOLAS";
        const string CONNECTION_USER = "sa";

        readonly string _ConnectionString;

        public Adapter( string ServerAddress, string SAPassword )
        {
            _ConnectionString = $"Data Source={ServerAddress};Initial Catalog={CONNECTION_DATABASE};Persist Security Info=True;User ID={CONNECTION_USER};Password={SAPassword}";
        }

        public SYSTEMTableAdapter SYSTEMTableAdapter() => new SYSTEMTableAdapter( _ConnectionString );
        public DETECTORTableAdapter DETECTORTableAdapter() => new DETECTORTableAdapter( _ConnectionString );
        public MONITORTableAdapter MONITORTableAdapter() => new MONITORTableAdapter( _ConnectionString );
        public ALARMTableAdapter ALARMTableAdapter() => new ALARMTableAdapter( _ConnectionString );
        public IMAGETableAdapter IMAGETableAdapter() => new IMAGETableAdapter( _ConnectionString );
        public OPERATIONTableAdapter OPERATIONTableAdapter() => new OPERATIONTableAdapter( _ConnectionString );
    }
}

namespace CES.SOLAS.Lib.Data.SOLASDataSetTableAdapters
{
    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class SYSTEMTableAdapter : Component
    {
        public SYSTEMTableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }

    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class DETECTORTableAdapter : Component
    {
        public DETECTORTableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }

    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class MONITORTableAdapter : Component
    {
        public MONITORTableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }

    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class ALARMTableAdapter : Component
    {
        public ALARMTableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }

    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class IMAGETableAdapter : Component
    {
        public IMAGETableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }

    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    public partial class OPERATIONTableAdapter : Component
    {
        public OPERATIONTableAdapter( string ConnectionString ) : base()
        {
            this._connection = new SqlConnection( ConnectionString );
        }
    }
}

