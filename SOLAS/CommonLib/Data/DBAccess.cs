﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.Data
{
    using static System.Diagnostics.Debug;

    public abstract class DBAccess<T> where T : IDBAccess, new()
    {
        protected static T _Instance;

        public static T Instance
        {
            get
            {
                if ( _Instance == null )
                {
                    _Instance = new T();
                }

                return _Instance;
            }
        }
        protected readonly SOLASDataSet _SOLASDataSet = new SOLASDataSet();

        protected Adapter _Adapter = null;

        public abstract void InitDBAccess( string DBServer, string SAPassword );

        protected virtual void LoadPCInfo()
        {
            Assert( _Adapter != null, "SQLDataAdapter が初期化されていません。" );

            using ( var MonAdapter = _Adapter.MONITORTableAdapter() )
            {
                MonAdapter.Fill( _SOLASDataSet.MONITOR );
            }

            using ( var DetAdapter = _Adapter.DETECTORTableAdapter() )
            {
                DetAdapter.Fill( _SOLASDataSet.DETECTOR );
            }
        }
        protected virtual int DeleteAlarm( int SaveDays )
        {
            DateTime Limit = DateTime.Today.AddDays( -( SaveDays - 1 ) );
            using ( var AlarmAdapter = _Adapter.ALARMTableAdapter() )
            {
                return AlarmAdapter.DeleteOldAlarm( Limit );
            }
        }


        protected virtual int GetMonitoringCount => _SOLASDataSet.MONITOR.Count;
        protected virtual int GetDetectorCount => _SOLASDataSet.DETECTOR.Count;

        protected virtual string GateName( int DetectorId )
        {
            if ( _SOLASDataSet.DETECTOR.FindByai_id( DetectorId ) is SOLASDataSet.DETECTORRow Detector )
            {
                return Detector.ai_gate_name;
            }

            return string.Empty;
        }

        protected virtual string GetMonitorStreamURL( int DetectorId )
        {
            if ( _SOLASDataSet.DETECTOR.FindByai_id( DetectorId ) is SOLASDataSet.DETECTORRow Detector )
            {
                return Detector.ai_monitor_streaming_url;
            }

            return string.Empty;
        }


        public bool GetDetectorEnableCameraControl( int DetectorId )
        {
            if ( _SOLASDataSet.DETECTOR.FindByai_id( DetectorId ) is SOLASDataSet.DETECTORRow Detector )
            {
                return Detector.ai_camera_control_enable;
            }

            return false;
        }

        protected virtual void LoadSystemInfo()
        {
            Assert( _Adapter != null, "SQLDataAdapter が初期化されていません。" );

            using ( var Adapter = _Adapter.SYSTEMTableAdapter() )
            {
                Adapter.Fill( _SOLASDataSet.SYSTEM );
            }
        }

        protected virtual uint GetPersonId => GetUintValue( "PersonId" );

        protected virtual uint GetMarkerId => GetUintValue( "MarkerId" );
        protected virtual uint GetCarId => GetUintValue( "CarId" );
        protected virtual uint GetCareerPaletteId => GetUintValue( "CareerPaletteId" );

        protected virtual string GetPersonName => GetStringValue( "PersonName", "Person" );

        protected virtual string GetMarkerName => GetStringValue( "MarkerName", "Marker" );

        protected virtual string GetCarName => GetStringValue( "CarName", "Car" );
        protected virtual string GetCareerPaletteName => GetStringValue( "CareerPaletteName", "CareerPalette" );

        protected virtual int GetAreaChangeWaitFrames => GetIntValue( "AreaChangeWaitFrames", 5 );
        protected virtual int GetCarStayAlarmWaitSeconds => GetIntValue( "CarStayAlarmWaitSeconds", 120 );

        protected virtual bool GetEnableCareerPaletteStayAlarm => GetBooleanValue( "EnableCareerPaletteStayAlarm", false );

        public virtual int GetAlarmSaveDays => GetIntValue( "AlarmSaveDays", 365 );

        protected virtual int GetAlarmMoviePreSeconds
        {
            get => GetIntValue( "AlarmMoviePreSeconds", 15 );
            set => SetIntValue( "AlarmMoviePreSeconds", value );
        }

        protected virtual int GetAlarmMoviePostSeconds
        {
            get => GetIntValue( "AlarmMoviePostSeconds", 15 );
            set => SetIntValue( "AlarmMoviePostSeconds", value );
        }


    protected virtual Scalar GetPersonColor
        {
            get
            {
                var ColorName = GetStringValue( "PersonColor", Color.Yellow.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetMarkerColor
        {
            get
            {
                var ColorName = GetStringValue( "MarkerColor", Color.Yellow.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetCarColor
        {
            get
            {
                var ColorName = GetStringValue( "CarColor", Color.Yellow.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetCareerPaletteColor
        {
            get
            {
                var ColorName = GetStringValue( "CareerPaletteColor", Color.Yellow.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetOKColor
        {
            get
            {
                var ColorName = GetStringValue( "OKColor", Color.Cyan.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetAlarmColor
        {
            get
            {
                var ColorName = GetStringValue( "AlarmColor", Color.Red.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }
        protected virtual Scalar GetStagnantColor
        {
            get
            {
                var ColorName = GetStringValue( "StagnantColor", Color.LightPink.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Yellow;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }

        protected virtual Scalar GetAreaColorA
        {
            get
            {
                var ColorName = GetStringValue( "AreaColorA", Color.LightPink.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.LightPink;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }

        protected virtual Scalar GetAreaColorB
        {
            get
            {
                var ColorName = GetStringValue( "AreaColorB", Color.LightPink.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.LightPink;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }

        protected virtual Scalar GetNeutralAreaColor
        {
            get
            {
                var ColorName = GetStringValue( "NeutralAreaColor", Color.LightPink.Name );
                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.LightPink;
                }

                return Scalar.FromRgb( ColorValue.R, ColorValue.G, ColorValue.B );
            }
        }

        protected virtual Color GetDetectorBackColor( int MonitorId, int GateId )
        {
            if ( _SOLASDataSet.MONITOR.FindBymon_id( MonitorId ) is SOLASDataSet.MONITORRow MonitorRow )
            {
                string ColorName = "Black";
                if ( ( MonitorRow.mon_aipc_01 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_01_back_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_01_back_color;
                }
                else if ( ( MonitorRow.mon_aipc_02 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_02_back_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_02_back_color;
                }
                else if ( ( MonitorRow.mon_aipc_03 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_03_back_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_03_back_color;
                }

                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Black;
                }

                return ColorValue;
            }

            return Color.Black;
        }
        protected virtual Color GetDetectorForeColor( int MonitorId, int GateId )
        {
            if ( _SOLASDataSet.MONITOR.FindBymon_id( MonitorId ) is SOLASDataSet.MONITORRow MonitorRow )
            {
                string ColorName = "Black";
                if ( ( MonitorRow.mon_aipc_01 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_01_fore_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_01_fore_color;
                }
                else if ( ( MonitorRow.mon_aipc_02 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_02_fore_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_02_fore_color;
                }
                else if ( ( MonitorRow.mon_aipc_03 == GateId ) && ( string.IsNullOrWhiteSpace( MonitorRow.mon_aipc_03_fore_color ) == false ) )
                {
                    ColorName = MonitorRow.mon_aipc_03_fore_color;
                }

                var ColorValue = Color.FromName( ColorName );
                if ( ColorValue.A == 0 && ColorValue.R == 0 && ColorValue.G == 0 && ColorValue.B == 0 )
                {
                    ColorValue = Color.Black;
                }

                return ColorValue;
            }

            return Color.Black;
        }

        protected virtual uint GetMaxPredictFrame => GetUintValue( "MaxPredictFrame", 15 );

        protected virtual float GetIOUThreshold => GetFloatValue( "IOUThreshold", 0.3F );

        protected virtual int GetMessageBoxTimeout => GetIntValue( "MessageBoxTimeout", 10 );

        protected virtual int GetStillImageShortInterval
        {
            get => GetIntValue( "StillImageShortInterval", 30 );
            set => SetIntValue( "StillImageShortInterval", value );
        }

        protected virtual int GetStillImageLongInterval
        {
            get => GetIntValue( "StillImageLongInterval", 600 );
            set => SetIntValue( "StillImageLongInterval", value );
        }

    protected virtual int GetHeartBeatCycle => GetIntValue( "HeartBeatCycle", 600 );
        protected virtual string GetAlarmName( ALARM_TYPE AlarmType )
        {
            switch ( AlarmType )
            {
                case ALARM_TYPE.ALARM_TYPE_A:
                    return GetStringValue( "AlarmNameA", "Alarm-A" );
                case ALARM_TYPE.ALARM_TYPE_B:
                    return GetStringValue( "AlarmNameB", "Alarm-B" );
                case ALARM_TYPE.ALARM_TYPE_C:
                    return GetStringValue( "AlarmNameC", "Alarm-C" );
            }

            return string.Empty;
        }

        protected virtual int InsertNewAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage )
        {
            using ( var GDIBmp = BitmapConverter.ToBitmap( AlarmImage ) )
            using ( var MemStream = new MemoryStream() )
            using ( var SolasDataSet = new SOLASDataSet() )
            using ( var Adapter = _Adapter.ALARMTableAdapter() )
            {
                GDIBmp.Save( MemStream, ImageFormat.Jpeg );

                var AlarmRow = SolasDataSet.ALARM.NewALARMRow();
                AlarmRow.alm_date = Timestamp;
                AlarmRow.alm_detector_id = DetectorId;
                AlarmRow.alm_type = ( int )AlarmType;
                AlarmRow.alm_event_image = MemStream.ToArray();

                SolasDataSet.ALARM.AddALARMRow( AlarmRow );

                try
                {
                    if ( Adapter.Update( SolasDataSet ) > 0 )
                    {
                        return AlarmRow.alm_id;
                    }

                    return 0;
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected virtual int GetTcpKeepAliveWait=> GetIntValue( "TcpKeepAliveWait", 300 );

        protected virtual int GetTcpKeepAliveInterval => GetIntValue( "TcpKeepAliveInterval", 3 );

        public virtual int PreAlarmFrameCount( int DetectorId )
        {
            if ( _SOLASDataSet.DETECTOR.FindByai_id( DetectorId ) is SOLASDataSet.DETECTORRow Detector )
            {
                return Detector.ai_pre_alarm_frame_count;
            }

            return -1;
        }

        public virtual int PostAlarmFrameCount( int DetectorId )
        {
            if ( _SOLASDataSet.DETECTOR.FindByai_id( DetectorId ) is SOLASDataSet.DETECTORRow Detector )
            {
                return Detector.ai_post_alarm_frame_count;
            }

            return -1;
        }

        public virtual float GetStillImageShrinkRatio => GetFloatValue( "StillImageShrinkRatio", 0.5F );
        public virtual float GetStreamingImageShrinkRatio => GetFloatValue( "StreamingImageShrinkRatio", 0.7F );

        public virtual int GetStreamingSkipFrame => GetIntValue( "StreamingSkipFrame", 1 );


        protected virtual string GetStringValue( string Key, string DefaultValue = "" )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                return SYSTEMRow.system_value;
            }

            return DefaultValue;
        }

        protected virtual int GetIntValue( string Key, int DefaultValue = -1 )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                if ( int.TryParse( SYSTEMRow.system_value, out int Result ) == true )
                {
                    return Result;
                }
            }

            return DefaultValue;
        }

        protected virtual void SetIntValue( string Key, int IntValue )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                SYSTEMRow.system_value = $"{IntValue}";
            }
            else
            {
                var NewRow = _SOLASDataSet.SYSTEM.NewSYSTEMRow();
                NewRow.system_key = Key;
                NewRow.system_value = $"{IntValue}";
                NewRow.Setsystem_descriptionNull();
                _SOLASDataSet.SYSTEM.AddSYSTEMRow( NewRow );
            }

            using ( var Adapter = _Adapter.SYSTEMTableAdapter() )
            {
                Adapter.Update( _SOLASDataSet.SYSTEM );
            }
        }

        protected virtual uint GetUintValue( string Key, uint DefaultValue = uint.MaxValue )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                if ( uint.TryParse( SYSTEMRow.system_value, out uint Result ) == true )
                {
                    return Result;
                }
            }

            return DefaultValue;
        }

        protected virtual float GetFloatValue( string Key, float DefaultValue = 0F )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                if ( float.TryParse( SYSTEMRow.system_value, out float Result ) == true )
                {
                    return Result;
                }
            }

            return DefaultValue;
        }

        protected virtual double GetDoubleValue( string Key, double DefaultValue = 0.0 )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                if ( double.TryParse( SYSTEMRow.system_value, out double Result ) == true )
                {
                    return Result;
                }
            }

            return DefaultValue;
        }

        protected virtual bool GetBooleanValue( string Key, bool DefaultValue = false )
        {
            Assert( _SOLASDataSet != null, "DataSet が初期化されていません。" );
            Assert( _SOLASDataSet.SYSTEM.Count > 0, "SYSTEM Table が初期化されていません。" );

            if ( _SOLASDataSet.SYSTEM.FindBysystem_key( Key ) is SOLASDataSet.SYSTEMRow SYSTEMRow )
            {
                switch ( SYSTEMRow.system_value.ToLower() )
                {
                    case "true":
                    case "on":
                    case "yes":
                    case "ok":
                    case "1":
                        return true;
                    case "false":
                    case "off":
                    case "no":
                    case "ng":
                    case "0":
                        return false;
                    default:
                        break;
                }
            }

            return DefaultValue;
        }
    }
}
