﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GDIPoint = System.Drawing.Point;

using OpenCvSharp;

namespace CES.SOLAS.Lib.Data
{
    public interface IDBAccess
    {
        TRIGGER_PROCESS_TYPE ProcessType
        {
            get;
        }

        int ApplicationId
        {
            get;
        }

        int UDPListenerPort
        {
            get;
        }

        int TCPMovieListenerPort
        {
            get;
        }

        int TCPImageListenerPort
        {
            get;
        }

        int HeartBeatCycle
        {
            get;
        }

        string DisplayName
        {
            get;
        }

        MONITORING_TYPE MonitoringType
        {
            get;
        }

        string GetDetectTargetStream
        {
            get;
        }

        int MonitoringCount
        {
            get;
        }

        int DetectorCount
        {
            get;
        }


        bool EnableDetect
        {
            get;
            set;
        }

        bool EnableCameraControl
        {
            get;
        }

        uint PersonId
        {
            get;
        }

        uint MarkerId
        {
            get;
        }

        uint CarId
        {
            get;
        }

        uint CareerPaletteId
        {
            get;
        }

        Scalar PersonColor
        {
            get;
        }
        Scalar MarkerColor
        {
            get;
        }
        Scalar CarColor
        {
            get;
        }

        Scalar CareerPaletteColor
        {
            get;
        }

        Scalar OKColor
        {
            get;
        }
        Scalar AlarmColor
        {
            get;
        }
        Scalar StagnantColor
        {
            get;
        }

        Scalar AreaColorA
        {
            get;
        }

        Scalar AreaColorB
        {
            get;
        }

        Scalar NeutralAreaColor
        {
            get;
        }

        Color DetectorBackColor( int GateId );
        Color DetectorForeColor( int GateId );

        string PersonName
        {
            get;
        }

        string MarkerName
        {
            get;
        }

        string CarName
        {
            get;
        }

        string CareerPaletteName
        {
            get;
        }

        uint MaxPredictFrame
        {
            get;
        }
        float IOUThreshold
        {
            get;
        }

        int MessageBoxTimeout
        {
            get;
        }

        int StillImageShortInterval
        {
            get;
            set;
        }

        int StillImageLongInterval
        {
            get;
            set;
        }

        int TcpKeepAliveWait
        {
            get;
        }

        int TcpKeepAliveInterval
        {
            get;
        }

        float StillImageShrinkRatio
        {
            get;
        }

        float StreamingImageShrinkRatio
        {
            get;
        }

        float DetectThresholdPerson
        {
            get;
        }

        float DetectThresholdMarker
        {
            get;
        }
        float DetectThresholdCar
        {
            get;
        }
        float DetectThresholdCareerPalette
        {
            get;
        }

        bool EnableDetectAfterAlarm
        {
            get;
        }

        int StreamingSkipFrame
        {
            get;
        }

        int AlarmMoviePreSeconds
        {
            get;
            set;
        }

        int AlarmMoviePostSeconds
        {
            get;
            set;
        }

        GDIPoint[] DetectArea
        {
            get;
        }

        GDIPoint[] DetectAreaA
        {
            get;
        }

        GDIPoint[] DetectAreaB
        {
            get;
        }

        int MaxDisplayDays
        {
            get;
        }

        string AlarmDeviceAddress
        {
            get;
        }

        int AlarmDevicePort
        {
            get;
        }

        bool DisableBuzzer
        {
            get;
            set;
        }

        int AreaChangeWaitFrames
        {
            get;
        }

        int CarStayAlarmWaitSeconds
        {
            get;
        }

        bool EnableCareerPaletteStayAlarm
        {
            get;
        }

        float TrackingReductionRate
        {
            get;
        }

        int GetAlarmSaveDays
        {
            get;
        }

        int InsertAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage );

        string AlarmName( ALARM_TYPE AlarmType );

        void GetMonitorTarget( out int[] RecNo );

        void InitDBAccess( string DBServer, string SAPassword );

        void MonitoringHandShake();

        void DetectorHandShake();

        int AddNewAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImageData );

        int AddNewImage( int DetectorId, DateTime Timestamp, Mat ImageData );

        byte[] AlarmImage( int AlarmId, bool Thumbnail );

        byte[] StillImage( int ImageId );

        void GetMonitoringPCInfo( int Index, out int MonitoringId, out string IPAddress, out int UdpTriggerPort, out int TcpImageListenerPort, out MONITORING_TYPE MonitoringType );
        void GetDetectorPCInfo( int Index, out int DetectorId, out string IPAddress, out int Port );

        string GateName( int DetectorId );

        void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait );

        void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio );

        bool DetectorEnableCameraControl( int DetectorId );
        int PreAlarmFrameCount( int DetectorId );

        int PostAlarmFrameCount( int DetectorId );

        string AlarmPattern( int GateId, ALARM_TYPE AlarmType );

        int DeleteOldAlarm();

        string MonitorStreamURL( int DetectorId );
    }
}
