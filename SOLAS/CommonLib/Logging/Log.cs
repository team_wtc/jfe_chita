﻿using System;
using System.Text;
using System.IO;


namespace CES.SOLAS.Lib.Logging
{
    /// <summary>
    /// ログ種別
    /// </summary>
    public enum LOG_TYPE
    {
        /// <summary>
        /// 情報ログ
        /// </summary>
        INFO,
        /// <summary>
        /// 警告ログ
        /// </summary>
        WARN,
        /// <summary>
        /// エラーログ
        /// </summary>
        ERROR,
        /// <summary>
        /// デバッグログ
        /// </summary>
        DEBUG,
        /// <summary>
        /// ログ削除（ログ出力では使用されない）
        /// </summary>
        DELETE
    }

    public class Log
    {
        readonly string _LogFolder;
        readonly string _LogName;
        readonly string _LogExt;
        readonly long _LogSize;

        public Log( string LogFolder, string LogName, string LogExt = "log", long LogSize = 1024L * 1024L * 5, int SaveDays = 31, bool DebugLog = false )
        {
            _LogFolder = LogFolder;
            _LogName = LogName;
            _LogExt = LogExt;
            _LogSize = LogSize;
            this.SaveDays = SaveDays;
#if DEBUG
            this.DebugLog = true;
#else
            this.DebugLog = DebugLog;
#endif
        }

        public bool DebugLog
        {
            get;
            set;
        }

        public int SaveDays
        {
            get;
            set;
        }

        public string LogString( LOG_TYPE LogType, string ThreadName, string Message )
        {
            string LogTypeString = string.Empty;
            switch ( LogType )
            {
                case LOG_TYPE.INFO:
                    LogTypeString = "[INFO ]";
                    break;
                case LOG_TYPE.WARN:
                    LogTypeString = "[WARN ]";
                    break;
                case LOG_TYPE.ERROR:
                    LogTypeString = "[ERROR]";
                    break;
                case LOG_TYPE.DEBUG:
                    LogTypeString = "[DEBUG]";
                    break;
            }

            return $"{DateTime.Now.ToString( "HH:mm:ss.fff" )} " +
                $"[{ThreadName,-10}] " +
                $"{LogTypeString} {Message}";
        }

        private string CreateLogFilePath()
        {
            string Folder = Path.Combine( _LogFolder, DateTime.Today.ToString( "yyyy_MM_dd" ) );
            try
            {
                if ( Directory.Exists( Folder ) == false )
                {
                    Directory.CreateDirectory( Folder );
                }
            }
            catch
            {
            }

            string LogFile = Path.Combine( Folder, $"{_LogName}.{_LogExt}" );

            ShiftLog( LogFile );

            return LogFile;
        }

        private void ShiftLog( string LogFile )
        {
            if ( File.Exists( LogFile ) == false )
            {
                return;
            }

            var LogFileInfo = new FileInfo( LogFile );
            if ( LogFileInfo.Length < _LogSize )
            {
                return;
            }

            var ShiftLogFile = Path.Combine( Path.GetDirectoryName( LogFile ), $"{_LogName}_{LogFileInfo.LastWriteTime.ToString( "HHmmss" )}.{_LogExt}" );

            LogFileInfo.MoveTo( ShiftLogFile );
        }

        private void WriteLog( string LogString )
        {
            try
            {
                using ( var FileStream = File.Open( CreateLogFilePath(), FileMode.Append, FileAccess.Write, FileShare.ReadWrite ) )
                using(var Writer = new StreamWriter( FileStream ,Encoding.UTF8) )
                {
                    if ( LogString.EndsWith( Environment.NewLine ) == true )
                    {
                        Writer.Write( LogString );
                    }
                    else
                    {
                        Writer.WriteLine( LogString );
                    }
                }
            }
            catch
            {
            }
        }

        public void Info( string ThreadName, string Message )
        {
            WriteLog( LogString( LOG_TYPE.INFO, ThreadName, Message ) );
        }

        public void Warn( string ThreadName, string Message )
        {
            WriteLog( LogString( LOG_TYPE.WARN, ThreadName, Message ) );
        }

        public void Error( string ThreadName, string Message )
        {
            WriteLog( LogString( LOG_TYPE.ERROR, ThreadName, Message ) );
        }
        public void Debug( string ThreadName, string Message )
        {
            if ( DebugLog == true )
            {
                WriteLog( LogString( LOG_TYPE.DEBUG, ThreadName, Message ) );
            }
        }
    }
}
