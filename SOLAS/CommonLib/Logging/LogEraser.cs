﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;

namespace CES.SOLAS.Lib.Logging
{
    using static System.Diagnostics.Debug;

    public class LogEraser : Threads.OneshotThread
    {
        readonly string _LogFolder;
        readonly int _SaveDays;
        readonly Logger _Logger;

        public LogEraser( Logger Logger, string LogFolder, int SaveDays ) : base( null, "Log del", ThreadPriority.BelowNormal )
        {
            Assert( Directory.Exists( LogFolder ), $"Exists({LogFolder})" );

            _Logger = Logger;
            _LogFolder = LogFolder;
            _SaveDays = SaveDays;
        }

        protected override void OneshotRoutine()
        {
            const string DATE_DIR_NAME_PATTERN = @"^(?<Y>\d{4})_(?<M>\d{2})_(?<D>\d{2})$";

            DateTime SaveLimit = DateTime.Today.AddDays( -_SaveDays );
            _Logger.Info( $"{SaveLimit.ToString( "yyyy年MM月dd日" )}より古い日付のログを削除" );

            foreach ( var SubDir in Directory.GetDirectories( _LogFolder ) )
            {
                var DirNameParts = SubDir.Split( Path.DirectorySeparatorChar );
                if ( ( DirNameParts == null ) || ( DirNameParts.Length <= 0 ) )
                {
                    continue;
                }

                var DirName = DirNameParts[ DirNameParts.Length - 1 ];

                var DirNameMatch = Regex.Match( DirName, DATE_DIR_NAME_PATTERN );

                if ( DirNameMatch.Success == false )
                {
                    continue;
                }

                DateTime DirDate = DateTime.MinValue;
                try
                {
                    DirDate = new DateTime(
                        Convert.ToInt32( DirNameMatch.Groups[ "Y" ].Value ),
                        Convert.ToInt32( DirNameMatch.Groups[ "M" ].Value ),
                        Convert.ToInt32( DirNameMatch.Groups[ "D" ].Value ),
                        0, 0, 0, 0, DateTimeKind.Local );
                }
                catch ( Exception e )
                {
                    _Logger.Error( $"フォルダー({SubDir})の日付変換に失敗({e.Message})" );
                    continue;
                }
                if ( DirDate >= SaveLimit )
                {
                    continue;
                }


                try
                {
                    Directory.Delete( SubDir, true );
                    _Logger.Info( $"フォルダー({SubDir})と、フォルダー内の全ファイルを削除" );
                }
                catch ( Exception e )
                {
                    _Logger.Error( $"フォルダー({SubDir})の削除に失敗({e.Message})" );
                }
            }
        }
    }
}
