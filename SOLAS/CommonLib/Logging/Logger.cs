﻿using System;
using System.Threading;

namespace CES.SOLAS.Lib.Logging
{
    public class Logger : Threads.QueueThread<LogInfo>
    {
        readonly Log _Log;
        readonly string _LogFolder;

        LogEraser _LogEraser = null;

        public Logger( string LogFolder, string LogName, string LogExt = "log", long LogSize = 1024L * 1024L * 5, int SaveDays = 31, bool DebugLog = false )
            : base( null, "Log", ThreadPriority.Normal, false, 0 )
        {
            _LogFolder = LogFolder;
            _Log = new Log( LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );
        }

        public void Info( string LogMessage )
        {
            if ( string.IsNullOrWhiteSpace( Thread.CurrentThread.Name ) == false )
            {
                Enqueue( new LogInfo( LOG_TYPE.INFO, Thread.CurrentThread.Name, LogMessage ) );
            }
            else
            {
                Enqueue( new LogInfo( LOG_TYPE.INFO, $"{Thread.CurrentThread.ManagedThreadId:D10}", LogMessage ) );
            }
        }

        public void Warn( string LogMessage )
        {
            if ( string.IsNullOrWhiteSpace( Thread.CurrentThread.Name ) == false )
            {
                Enqueue( new LogInfo( LOG_TYPE.WARN, Thread.CurrentThread.Name, LogMessage ) );
            }
            else
            {
                Enqueue( new LogInfo( LOG_TYPE.WARN, $"{Thread.CurrentThread.ManagedThreadId:D10}", LogMessage ) );
            }
        }

        public void Error( string LogMessage )
        {
            if ( string.IsNullOrWhiteSpace( Thread.CurrentThread.Name ) == false )
            {
                Enqueue( new LogInfo( LOG_TYPE.ERROR, Thread.CurrentThread.Name, LogMessage ) );
            }
            else
            {
                Enqueue( new LogInfo( LOG_TYPE.ERROR, $"{Thread.CurrentThread.ManagedThreadId:D10}", LogMessage ) );
            }
        }

        public void Debug( string LogMessage )
        {
            if ( string.IsNullOrWhiteSpace( Thread.CurrentThread.Name ) == false )
            {
                Enqueue( new LogInfo( LOG_TYPE.DEBUG, Thread.CurrentThread.Name, LogMessage ) );
            }
            else
            {
                Enqueue( new LogInfo( LOG_TYPE.DEBUG, $"{Thread.CurrentThread.ManagedThreadId:D10}", LogMessage ) );
            }
        }

        public void Exception( string LogMessage, Exception Exp )
        {
            string ThreadName = Thread.CurrentThread.Name;
            if ( string.IsNullOrWhiteSpace( Thread.CurrentThread.Name ) == true )
            {
                ThreadName = $"{Thread.CurrentThread.ManagedThreadId:D10}";
            }

            Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, LogMessage ) );
            Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, $"{Exp.GetType()}" ) );
            Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, Exp.Message ) );

            if ( string.IsNullOrWhiteSpace( Exp.StackTrace ) == false )
            {
                Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, "----- Stack Trace -----" ) );
                var StackTrace = Exp.StackTrace.Replace( "\r\n", "\n" ).Split( '\n' );
                foreach ( var TraceLine in StackTrace )
                {
                    Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, TraceLine ) );
                }
            }

            var Inner = Exp.InnerException;
            while ( Inner != null )
            {
                Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, $"----- Inner Exception -----" ) );
                Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, $"{Inner.GetType()}" ) );
                Enqueue( new LogInfo( LOG_TYPE.ERROR, ThreadName, Inner.Message ) );

                Inner = Inner.InnerException;
            }
        }

        public void Delete()
        {
            Enqueue( new LogInfo( LOG_TYPE.DELETE, string.Empty, string.Empty ) );
        }

        public bool DebugLog
        {
            get => _Log.DebugLog;
            set => _Log.DebugLog = value;
        }

        public int SaveDays
        {
            get => _Log.SaveDays;
            set => _Log.SaveDays = value;
        }

        protected override bool ItemRoutine( LogInfo Item )
        {
            switch ( Item.LogType )
            {
                case LOG_TYPE.INFO:
                    _Log.Info( Item.ThreadName, Item.LogMessage );
                    break;
                case LOG_TYPE.WARN:
                    _Log.Warn( Item.ThreadName, Item.LogMessage );
                    break;
                case LOG_TYPE.ERROR:
                    _Log.Error( Item.ThreadName, Item.LogMessage );
                    break;
                case LOG_TYPE.DEBUG:
                    _Log.Debug( Item.ThreadName, Item.LogMessage );
                    break;
                case LOG_TYPE.DELETE:
                    if ( _LogEraser != null )
                    {
                        _LogEraser.Wait();
                        _LogEraser = null;
                    }

                    _LogEraser = new LogEraser( this, _LogFolder, SaveDays );
                    _LogEraser.Start();
                    break;
            }
            return true;
        }
    }

    public class LogInfo
    {
        public LogInfo( LOG_TYPE Type, string Name, string Message )
        {
            LogType = Type;
            LogMessage = Message;
            ThreadName = Name;
        }

        public LOG_TYPE LogType
        {
            get;
            private set;
        }

        public string ThreadName
        {
            get;
            private set;
        }

        public string LogMessage
        {
            get;
            private set;
        }
    }
}
