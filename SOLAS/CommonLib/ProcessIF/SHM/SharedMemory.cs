﻿#if SHARED_MAPPED_FILE
using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace CES.SOLAS.Lib.ProcessIF.SHM
{
    public sealed class SharedMemory:IDisposable
    {
        const string MAPPED_FILE_NAME = "SOLAS_Shared_Memory_File";
        const string GLOBAL_MUTEX_NAME = "SOLAS_Global_Mutex";
        const long MAPPED_FILE_CAPACITY = 1024L * 1024L * 2L;   // 2MB

        readonly bool _Owner;
        readonly MemoryMappedFile _MemoryMappedFile;
        readonly Mutex _Mutex;

        public SharedMemory( bool Owner = false )
        {
            _Owner = Owner;

            if ( _Owner == true )
            {
                try
                {
                    _MemoryMappedFile = MemoryMappedFile.CreateOrOpen( MAPPED_FILE_NAME, MAPPED_FILE_CAPACITY );
                }
                catch ( Exception )
                {
                    throw;
                }


                _Mutex = new Mutex( false, GLOBAL_MUTEX_NAME, out bool Created );
                if ( Created == false )
                {
                    //throw new ApplicationException( $"Mutex \"{GLOBAL_MUTEX_NAME}\" は既に作成されています。" );
                }
                using ( var ViewAccessor = _MemoryMappedFile.CreateViewAccessor() )
                {
                    try
                    {
                        _Mutex.WaitOne();
                    }
                    catch ( AbandonedMutexException )
                    {

                    }
                    catch ( Exception )
                    {
                        throw;
                    }

                    ViewAccessor.Write( 0L, ( int )0 );

                    try
                    {
                        _Mutex.ReleaseMutex();
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                _MemoryMappedFile = MemoryMappedFile.OpenExisting( MAPPED_FILE_NAME );
                _Mutex = Mutex.OpenExisting( GLOBAL_MUTEX_NAME );
            }
        }

        public void WriteData( int AlarmId, DateTime Timestamp, byte[] Data )
        {
            if ( _Mutex.WaitOne( 300 ) == false )
            {
                return;
            }

            try
            {
                using ( var ViewAccessor = _MemoryMappedFile.CreateViewAccessor() )
                {
                    //while ( true )
                    //{
                    //    _Mutex.WaitOne();
                    //    int Len = ViewAccessor.ReadInt32( 0L );

                    //    if ( Len == 0 )
                    //    {
                    //        break;
                    //    }

                    //    _Mutex.ReleaseMutex();
                    //    Thread.Sleep( 10 );
                    //}

                    ViewAccessor.Write( 0L, Data.Length );
                    ViewAccessor.Write( 4L, AlarmId );
                    ViewAccessor.Write( 8L, Timestamp.Ticks );
                }

                using ( var ViewStream = _MemoryMappedFile.CreateViewStream() )
                {
                    //  Len+Id+Time
                    ViewStream.Seek( ( long )( sizeof( int ) + sizeof( int ) + sizeof( long ) ), SeekOrigin.Begin );
                    ViewStream.Write( Data, 0, Data.Length );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                _Mutex.ReleaseMutex();
            }
        }

        public void ReadData(out int AlarmId, out DateTime Timestamp, out byte[] Data )
        {
            if ( _Mutex.WaitOne( 300 ) == false )
            {
                AlarmId = 0;
                Timestamp = DateTime.MinValue;
                Data = null;
                return;
            }

            try
            {
                int Len = 0;
                using ( var ViewAccessor = _MemoryMappedFile.CreateViewAccessor() )
                {
                    //while ( true )
                    //{
                    //    _Mutex.WaitOne();
                    //    Len = ViewAccessor.ReadInt32( 0L );

                    //    if ( Len > 0 )
                    //    {
                    //        AlarmId = ViewAccessor.ReadInt32( 4L );
                    //        Timestamp = new DateTime( ViewAccessor.ReadInt64( 8L ) );
                    //        break;
                    //    }

                    //    _Mutex.ReleaseMutex();
                    //    Thread.Sleep( 10 );
                    //}

                    Len = ViewAccessor.ReadInt32( 0L );

                    if ( Len <= 0 )
                    {
                        AlarmId = 0;
                        Timestamp = DateTime.MinValue;
                        Data = null;
                        return;
                    }

                    AlarmId = ViewAccessor.ReadInt32( 4L );
                    Timestamp = new DateTime( ViewAccessor.ReadInt64( 8L ) );

                    using ( var ViewStream = _MemoryMappedFile.CreateViewStream() )
                    {
                        Data = new byte[ Len ];
                        //  Len+Id+Time
                        ViewStream.Seek( ( long )( sizeof( int ) + sizeof( int ) + sizeof( long ) ), SeekOrigin.Begin );
                        ViewStream.Read( Data, 0, Len );
                    }

                    ViewAccessor.Write( 0L, ( int )0 );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                _Mutex.ReleaseMutex();
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    _MemoryMappedFile.Dispose();
                    _Mutex.Dispose();
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~SharedMemory() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
#endif
