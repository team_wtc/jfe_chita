﻿#if SHARED_TCP
using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.Lib.ProcessIF.SHM
{
    public sealed class SharedMemory : IDisposable
    {
        const int LOCAL_LISTENER_PORT = 21240;

        readonly bool _Owner;
        readonly TcpListener _TcpListener = null;
        readonly Logging.Logger _Logger;

        TcpClient _TcpClient = null;
        bool _TcpListenerActive = false;

        public SharedMemory( Logging.Logger Logger, bool Owner = false )
        {
            _Owner = Owner;
            _Logger = Logger;

            if ( _Owner == true )
            {
                try
                {
                    _TcpListener = new TcpListener( IPAddress.Loopback, LOCAL_LISTENER_PORT );
                    _TcpListener.Server.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
                    _TcpListener.Start();
                    _TcpListenerActive = true;
                    _TcpListener.BeginAcceptTcpClient( AcceptCallback, null );
                    _Logger.Info( $"SharedMemory TcpListener start. ({_TcpListener.LocalEndpoint})" );
                }
                catch ( Exception )
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    _TcpClient = new TcpClient();
                    _TcpClient.Connect( new IPEndPoint( IPAddress.Loopback, LOCAL_LISTENER_PORT ) );
                    _Logger.Info( $"SharedMemory TcpClient connected. ({IPAddress.Loopback}:{LOCAL_LISTENER_PORT})" );
                }
                catch ( Exception )
                {
                    throw;
                }
            }
        }

        private void AcceptCallback( IAsyncResult ar )
        {
            if ( _TcpClient != null )
            {
                if ( _TcpClient.Connected == true )
                {
                    try
                    {
                        _Logger.Debug( $"SharedMemory TcpListener close old socket. ({_TcpClient.Client.RemoteEndPoint})" );
                        _TcpClient.Client.Shutdown( SocketShutdown.Both );
                        _TcpClient.Close();
                    }
                    catch
                    {
                    }
                }

                _TcpClient = null;
            }

            try
            {
                _TcpClient = _TcpListener.EndAcceptTcpClient( ar );
                _Logger.Debug( $"SharedMemory TcpListener accepted. ({_TcpClient.Client.RemoteEndPoint})" );
            }
            catch
            {
                _TcpClient = null;
            }
            finally
            {
                try
                {
                    if ( _TcpListenerActive == true )
                    {
                        _TcpListener.BeginAcceptTcpClient( AcceptCallback, null );
                    }
                }
                catch
                {
                }
            }
        }

        public void WriteData( int AlarmId, DateTime Timestamp, byte[] Data )
        {
            if ( _TcpClient == null )
            {
                return;
            }

            try
            {
                List<byte> SendBuffer = new List<byte>();
                SendBuffer.AddRange( BitConverter.GetBytes( Data.Length ) );
                SendBuffer.AddRange( BitConverter.GetBytes( AlarmId ) );
                SendBuffer.AddRange( Data );

                _TcpClient.GetStream().Write( SendBuffer.ToArray(), 0, SendBuffer.Count );
            }
            catch(Exception e)
            {
                _Logger.Debug( $"SharedMemory WriteData error. ({e.Message})" );
                try
                {
                    _TcpClient.Client.Shutdown( SocketShutdown.Both );
                    _TcpClient.Close();
                }
                catch
                {
                }
                _TcpClient = null;
            }
            finally
            {
            }
        }

        public void ReadData( out int AlarmId, out DateTime Timestamp, out byte[] Data )
        {
            try
            {
                if ( _TcpClient == null )
                {
                    AlarmId = 0;
                    Timestamp = DateTime.MinValue;
                    Data = null;
                    return;
                }

                while ( _TcpClient.Available < 8 )
                {
                    Thread.Sleep( 1 );
                }

                byte[] HeaderBytes = new byte[ 8 ];
                if ( _TcpClient.GetStream().Read( HeaderBytes, 0, 8 ) < 0 )
                {
                    try
                    {
                        _TcpClient.Client.Shutdown( SocketShutdown.Both );
                        _TcpClient.Close();
                    }
                    catch
                    {
                    }
                    _TcpClient = null;
                    AlarmId = 0;
                    Timestamp = DateTime.MinValue;
                    Data = null;
                    return;
                }

                int DataLen = BitConverter.ToInt32( HeaderBytes, 0 );
                AlarmId = BitConverter.ToInt32( HeaderBytes, 4 );

                List<byte> RecvBuffer = new List<byte>();
                while ( RecvBuffer.Count < DataLen )
                {
                    int AvailableBytes = 0;
                    while ( _TcpClient.Available <= 0 )
                    {
                        Thread.Sleep( 1 );
                    }
                    AvailableBytes = _TcpClient.Available;

                    byte[] TempBuffer = new byte[ AvailableBytes ];
                    if ( _TcpClient.GetStream().Read( TempBuffer, 0, AvailableBytes ) < 0 )
                    {
                        try
                        {
                            _TcpClient.Client.Shutdown( SocketShutdown.Both );
                            _TcpClient.Close();
                        }
                        catch
                        {
                        }
                        _TcpClient = null;
                        AlarmId = 0;
                        Timestamp = DateTime.MinValue;
                        Data = null;
                        return;
                    }

                    RecvBuffer.AddRange( TempBuffer );
                }

                Timestamp = DateTime.Now;
                Data = RecvBuffer.ToArray();
            }
            catch(Exception e)
            {
                _Logger.Debug( $"SharedMemory ReadData error. ({e.Message})" );
                throw;
            }
            finally
            {
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    _TcpListener?.Stop();
                    _TcpClient?.Close();
                    _TcpListenerActive = false;
                    _Logger.Debug( "SharedMemory disposed." );
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~SharedMemory() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
#endif