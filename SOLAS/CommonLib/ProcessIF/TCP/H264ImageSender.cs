﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using GDIBitmap = System.Drawing.Bitmap;
using GDIImage = System.Drawing.Image;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    using Lib.Threads;
    using Lib.UI;
    using Lib.Logging;
    using CvSize = OpenCvSharp.Size;

    public class H264ImageSender : QueueThread<byte[]>
    {
        const int IMAGE_BUFFER_SIZE = 2 * 1024 * 1024;

        readonly IPEndPoint _RemoteEndPoint;
        readonly Logger _Logger;
        readonly int _ApplicationId;

        TcpClient _TcpClient;
        DateTime? _LastSendTime = null;
        Task _OpenSocketTask = null;

        public H264ImageSender( Logger Logger, int ApplicationId, string MonitoringAppAddress, int MonitoringAppPort )
            :base( null ,"H264Sender", ThreadPriority.Normal ,false,0)
        {
            _Logger = Logger;
            _ApplicationId = ApplicationId;
            _RemoteEndPoint = new IPEndPoint( IPAddress.Parse( MonitoringAppAddress ), MonitoringAppPort );
        }

        public H264ImageSender( ISOLASApp SOLASApp, string MonitoringAppAddress, int MonitoringAppPort )
            : base( SOLASApp, "H264Sender", ThreadPriority.Normal, false, 0 )
        {
            _Logger = SOLASApp.Logger;
            _ApplicationId = SOLASApp.ApplicationId;
            _RemoteEndPoint = new IPEndPoint( IPAddress.Parse( MonitoringAppAddress ), MonitoringAppPort );
        }

        protected override bool Init()
        {
            _Logger?.Info( $"H264ImgSender Startup." );
            return true;
        }

        protected override void Terminate()
        {
            CloseSocket();
            _Logger?.Info( $"ImgSender Terminate." );
        }

        private void OpenSocket()
        {
            if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
            {
                return;
            }

            if ( _OpenSocketTask != null )
            {
                return;
            }

            _OpenSocketTask = Task.Factory.StartNew( () =>
            {
                var Client = new TcpClient();
                try
                {
                    Client.Connect( _RemoteEndPoint );
                    if ( Client.Connected == true )
                    {
                        _TcpClient = Client;
                        //SetKeepAlive();
                        _Logger?.Info( $"Image Send Socket Open.({_RemoteEndPoint})" );
                    }
                    else
                    {
                        Client?.Dispose();
                        _TcpClient = null;
                    }
                }
                catch ( Exception e )
                {
                    _Logger?.Exception( "Socket Connection Fail.", e );
                    Client?.Dispose();
                    _TcpClient = null;
                }
            } ).ContinueWith( ( tsk ) =>
            {
                _OpenSocketTask = null;
            } );
        }

        private void CloseSocket()
        {
            try
            {
                _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                _TcpClient?.Close();
                _TcpClient?.Dispose();
                _TcpClient = null;
                _Logger?.Info( $"Image Send Socket Close.({_RemoteEndPoint})" );
            }
            catch ( Exception e )
            {
                _Logger?.Exception( "Socket Close Fail.", e );
            }
        }

        protected override bool ItemRoutine( byte[] EncodedData )
        {
            try
            {
                OpenSocket();

                if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
                {
                    //  ストリーミングモードのモニタアプリなら送信
                    var Packet = new ImagePacket(
                        _ApplicationId,
                        EncodedData );

                    _LastSendTime = DateTime.Now;
                    _TcpClient.Client.Send( Packet.Payload );
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _Logger?.Exception( $"Send Error. (RemoteEndPoint={_TcpClient.Client.RemoteEndPoint})", e );
                CloseSocket();
            }
            finally
            {
            }

            return true;
        }
    }
}
