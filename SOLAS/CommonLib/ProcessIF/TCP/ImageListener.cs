﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    using Lib.Threads;
    using Lib.UI;

    public class ImageListener : OneshotThread
    {
        readonly TcpListener _TcpListener;
        readonly IPEndPoint _IPEndPoint;
        //readonly List<ImageReceiver> _ImageReceivers = new List<ImageReceiver>();
        readonly List<OneshotThread> _ImageReceivers = new List<OneshotThread>();
        readonly bool _EncoderListener;
        readonly QueueThread<(int AlarmId, DateTime Timestamp, byte[] ImgData)> _Encoder;

        public ImageListener( ISOLASApp SOLASApp, int Port, bool EncoderListener, QueueThread<(int AlarmId, DateTime Timestamp, byte[] ImgData)> Encoder )
            : base( SOLASApp, "Img Lstn", ThreadPriority.Normal )
        {
            _Encoder = Encoder;
            _EncoderListener = EncoderListener;
            if ( _EncoderListener == true )
            {
                _IPEndPoint = new IPEndPoint( IPAddress.Loopback, Port );
            }
            else
            {
                _IPEndPoint = new IPEndPoint( IPAddress.Any, Port );
            }
            _TcpListener = new TcpListener( _IPEndPoint );
            _TcpListener.Server.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
        }

        protected override bool Init()
        {
            try
            {
                _TcpListener.Start();
                _SOLASApp?.Info( $"Listener Start. ({_IPEndPoint})" );
                return base.Init();
            }
            catch(Exception e)
            {
                _SOLASApp?.Exception( "Listener Start Fail.", e );
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _TcpListener.Stop();
                _SOLASApp?.Info( $"Listener Stop. ({_IPEndPoint})" );
            }
            catch ( Exception e )
            {
                _SOLASApp?.Exception( "Listener Stop Fail.", e );
            }
        }

        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( _TcpListener.Pending() == false )
                {
                    Thread.Sleep( 10 );
                    continue;
                }

                var Client = _TcpListener.AcceptTcpClient();
                //SetKeepAlive( Client );
                _SOLASApp?.Info( $"Connection from {Client.Client.RemoteEndPoint}" );
                //  TODO:
                //
                if ( _SOLASApp?.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR )
                {
                    //if ( _SOLASApp?.MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                    //{
                    //    var Recver = new ImageReceiver( _SOLASApp, Client, _EncoderListener, _Encoder );
                    //    Recver.Start();
                    //    _ImageReceivers.Add( Recver );
                    //}
                    //else if ( _SOLASApp?.MonitoringType == MONITORING_TYPE.MON_STREAMING )
                    //{
                    //    var Recver = new ImageReceiver( _SOLASApp, Client, _EncoderListener, _Encoder );
                    //    Recver.Start();
                    //    _ImageReceivers.Add( Recver );
                    //}
                    var Recver = new ImageReceiver( _SOLASApp, Client, _EncoderListener, _Encoder );
                    Recver.Start();
                    _ImageReceivers.Add( Recver );
                }
            }
        }

        private void SetKeepAlive( TcpClient ClientSock )
        {
            var KeepAliveStructValue = new byte[ sizeof( uint ) * 3 ];
            BitConverter.GetBytes( 1 ).CopyTo( KeepAliveStructValue, 0 );
            BitConverter.GetBytes( _SOLASApp.TcpKeepAliveWait ).CopyTo( KeepAliveStructValue, 4 );
            BitConverter.GetBytes( _SOLASApp.TcpKeepAliveInterval ).CopyTo( KeepAliveStructValue, 8 );

            ClientSock.Client.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true );
            ClientSock.Client.IOControl( IOControlCode.KeepAliveValues, KeepAliveStructValue, null );
        }
    }
}
