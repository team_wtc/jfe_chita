﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    public class ImagePacket
    {
        public enum PACKET_TYPE : byte
        {
            STILL_IMAGE_PACKET = 0x01,
            ENCODE_IMAGE_PACKET = 0x02
        }

        /// <summary>
        /// 受信データ展開用コンストラクタ
        /// </summary>
        /// <param name="Payload"></param>
        public ImagePacket( byte[] Payload )
        {
            if ( Payload.Length < 24 )
            {
                throw new ApplicationException( "Image Packet Length Error." );
            }

            this.Payload = Payload;

            if ( Enum.IsDefined( typeof( PACKET_TYPE ), this.Payload[ 0 ] ) == false )
            {
                throw new ApplicationException( $"Image Packet type Error. value={this.Payload[ 0 ]}" );
            }

            PacketType = ( PACKET_TYPE )Enum.ToObject( typeof( PACKET_TYPE ), this.Payload[ 0 ] );
            DetectorId = BitConverter.ToInt32( Payload, 1 );
            AlarmId = BitConverter.ToInt32( Payload, 5 );
            AlarmType = BitConverter.ToInt32( Payload, 9 );
            Timestamp = new DateTime( BitConverter.ToInt64( Payload, 13 ) );
            ImageLength = BitConverter.ToInt32( Payload, 21 );
            if ( ImageLength > 0 )
            {
                if ( ImageLength != ( Payload.Length - 25 ) )
                {
                    throw new ApplicationException( $"Image Packet Data Length Error. ImageLength={ImageLength} Payload.Length={Payload.Length}" );
                }

                ImageData = new byte[ ImageLength ];
                Array.Copy( Payload, 25, ImageData, 0, ImageLength );
            }
        }

        /// <summary>
        /// 送信データ生成用コンストラクタ
        /// </summary>
        /// <param name="DetectorId"></param>
        /// <param name="Timestamp"></param>
        /// <param name="DataLength"></param>
        /// <param name="ImageBytes"></param>
        public ImagePacket( int DetectorId, byte[] EncodedBytes )
        {
            this.PacketType = PACKET_TYPE.ENCODE_IMAGE_PACKET;
            this.DetectorId = DetectorId;
            this.AlarmId = 0;
            this.AlarmType = 0;
            this.Timestamp = DateTime.Now;
            if ( ( EncodedBytes != null ) && ( EncodedBytes.Length > 0 ) )
            {
                this.ImageLength = EncodedBytes.Length;
                this.ImageData = new byte[ EncodedBytes.Length ];
                Array.Copy( EncodedBytes, ImageData, EncodedBytes.Length );
            }
            else
            {
                this.ImageLength = 0;
                this.ImageData = null;
            }

            var PayloadBuffer = new List<byte>
            {
                ( byte )this.PacketType
            };
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.DetectorId ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.AlarmId ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.AlarmType ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.Timestamp.Ticks ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.ImageLength ) );
            if ( ( this.ImageLength > 0 ) && ( this.ImageData != null ) )
            {
                PayloadBuffer.AddRange( this.ImageData );
            }

            this.Payload = PayloadBuffer.ToArray();
        }

        public ImagePacket( int DetectorId, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, int DataLength, byte[] ImageBytes )
        {
            this.PacketType = PACKET_TYPE.STILL_IMAGE_PACKET;
            this.DetectorId = DetectorId;
            this.AlarmId = AlarmId;
            this.AlarmType = ( int )AlarmType;
            this.Timestamp = Timestamp;
            this.ImageLength = DataLength;
            if ( ( DataLength > 0 ) && ( ImageBytes != null ) && ( ImageBytes.Length > 0 ) )
            {
                this.ImageData = new byte[ DataLength ];
                Array.Copy( ImageBytes, ImageData, DataLength );
            }
            else
            {
                this.ImageData = null;
            }

            var PayloadBuffer = new List<byte>
            {
                ( byte )this.PacketType
            };
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.DetectorId ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.AlarmId ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.AlarmType ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.Timestamp.Ticks ) );
            PayloadBuffer.AddRange( BitConverter.GetBytes( this.ImageLength ) );
            if ( ( this.ImageLength > 0 ) && ( this.ImageData != null ) )
            {
                PayloadBuffer.AddRange( this.ImageData );
            }

            this.Payload = PayloadBuffer.ToArray();
        }

        public PACKET_TYPE PacketType
        {
            get;
            private set;
        }

        public int DetectorId
        {
            get;
            private set;
        }

        public int AlarmId
        {
            get;
            private set;
        }

        public int AlarmType
        {
            get;
            private set;
        }

        public DateTime Timestamp
        {
            get;
            private set;
        }

        public int ImageLength
        {
            get;
            private set;
        }

        public byte[] ImageData
        {
            get;
            private set;
        }

        public byte[] Payload
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return
                $"PacketType={PacketType} DetectorId={DetectorId} AlarmId={AlarmId} " +
                $"AlarmType={Enum.ToObject( typeof( ALARM_TYPE ), AlarmType )} " +
                $"Timestamp={Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                $"ImageLength={ImageLength} ImageData={ImageData.Length}bytes Payload={Payload.Length}bytes";
        }
    }
}
