﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;

using OpenH264Lib;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    using Lib.Threads;
    using Lib.UI;

    public class ImageReceiver : OneshotThread
    {
        readonly TcpClient _TcpClient;
        readonly bool _EncoderListener;
        readonly QueueThread<(int AlarmId, DateTime Timestamp, byte[] ImgData)> _Encoder;

        Decoder _Decoder = null;

        public ImageReceiver( ISOLASApp SOLASApp, TcpClient ConnectedClient ,bool EncoderListener, QueueThread<(int AlarmId, DateTime Timestamp, byte[] ImgData)> Encoder )
            : base( SOLASApp, $"Recv{( ( IPEndPoint )ConnectedClient.Client.RemoteEndPoint ).Port}", ThreadPriority.Normal )
        {
            _Encoder = Encoder;
            _EncoderListener = EncoderListener;
            _TcpClient = ConnectedClient;
        }

        protected override bool Init()
        {
            _SOLASApp?.Info( $"ImageReceiver Start. {_TcpClient.Client.RemoteEndPoint}" );
            return base.Init();
        }

        protected override void Terminate()
        {
            _SOLASApp?.Info( $"ImageReceiver Terminate. {_TcpClient.Client.RemoteEndPoint}" );
            try
            {
                _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                _TcpClient?.Close();
                _TcpClient?.Dispose();
            }
            catch
            {

            }
        }

        protected override void OneshotRoutine()
        {
            try
            {
                while ( true )
                {
                    if ( _TcpClient.Client.Poll( 100 * 1000, SelectMode.SelectRead ) == false )
                    {
                        continue;
                    }

                    byte[] HeaderBuffer = new byte[ 1 ];
                    List<byte> PacketBytes = new List<byte>();

                    while ( PacketBytes.Count < 25 )
                    {
                        if ( _TcpClient.Client.Receive( HeaderBuffer ) <= 0 )
                        {
                            _SOLASApp?.Warn( $"Socket Disconnect. {_TcpClient.Client.RemoteEndPoint}" );
                            PacketBytes.Clear();
                            break;
                        }
                        PacketBytes.AddRange( HeaderBuffer );
                    }
                    if ( PacketBytes.Count <= 0 )
                    {
                        break;
                    }

                    var Imagesize = BitConverter.ToInt32( PacketBytes.ToArray(), 21 );
                    _SOLASApp.Debug( $"★ Receive Image Size ={Imagesize}" );

                    if ( Imagesize > 0 )
                    {
                        int TotalSize = ( PacketBytes.Count + Imagesize );
                        _SOLASApp.Debug( $"★ Total Payload Size ={TotalSize}" );

                        //while ( _TcpClient.Available < Imagesize )
                        //{
                        //    _SOLASApp.Debug( $"★ Wait Image Data Available={_TcpClient.Available} Imagesize={Imagesize}" );
                        //    Thread.Sleep( 10 );
                        //}

                        while ( PacketBytes.Count < TotalSize )
                        {
                            if ( _TcpClient.Available <= 0 )
                            {
                                Thread.Sleep( 10 );
                                continue;
                            }

                            int BufferSize = _TcpClient.Available;
                            if ( ( PacketBytes.Count + BufferSize ) > TotalSize )
                            {
                                BufferSize = ( TotalSize - PacketBytes.Count );
                            }
                            var ImageBuffer = new byte[ BufferSize ];

                            var RecvLen = _TcpClient.Client.Receive( ImageBuffer );
                            if ( RecvLen <= 0 )
                            {
                                _SOLASApp?.Warn( $"Socket Disconnect. {_TcpClient.Client.RemoteEndPoint}" );
                                break;
                            }

                            //_SOLASApp.Debug( $"★ Receive Image Bytes ={RecvLen}" );

                            if ( RecvLen >= ImageBuffer.Length )
                            {
                                PacketBytes.AddRange( ImageBuffer );
                            }
                            else
                            {
                                byte[] DstArray = new byte[ RecvLen ];
                                Array.Copy( ImageBuffer, 0, DstArray, 0, RecvLen );
                                PacketBytes.AddRange( DstArray );
                            }

                            //_SOLASApp.Debug( $"★ Packet Buffer Bytes ={PacketBytes.Count}" );
                        }
                    }

                    try
                    {
                        var Pkt = new ImagePacket( PacketBytes.ToArray() );
                        if ( _EncoderListener == true )
                        {
                            _Encoder?.Enqueue( (Pkt.AlarmId, Pkt.Timestamp, Pkt.ImageData) );
                        }
                        else
                        {
                            if ( Pkt.PacketType == ImagePacket.PACKET_TYPE.STILL_IMAGE_PACKET )
                            {
                                _SOLASApp?.StillImageReceive( Pkt );
                            }
                            else if ( Pkt.PacketType == ImagePacket.PACKET_TYPE.ENCODE_IMAGE_PACKET )
                            {
                                if ( _SOLASApp?.MonitoringType == MONITORING_TYPE.MON_STREAMING )
                                {
                                    if ( _Decoder == null )
                                    {
                                        _Decoder = new Decoder( "openh264-1.8.0-win64.dll" );
                                    }

                                    _SOLASApp?.LiveImageReceive( Pkt.DetectorId, _Decoder.Decode( Pkt.ImageData, Pkt.ImageLength ) );
                                    //if ( Img == null )
                                    //{
                                    //    _SOLASApp.Debug( $"★ Encoded Packet Decode. Img=null" );
                                    //}
                                    //else
                                    //{
                                    //    _SOLASApp.Debug( $"★ Encoded Packet Decode. Img={Img.Size}" );
                                    //}
                                }
                            }
                        }
                    }
                    catch
                    {
                        _SOLASApp?.Error( $"★ Receive Packet PacketBytes.Count={PacketBytes.Count}, Image Size={Imagesize} Total=({Imagesize + 25})" );
                        throw;
                    }
                }
            }
            catch (Exception e )
            {
                _SOLASApp?.Exception( $"Receive Error.({_TcpClient.Client.RemoteEndPoint})", e );
            }
        }
    }
}
