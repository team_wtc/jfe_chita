﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    public class MovieReceiver : Threads.OneshotThread
    {
        public event EventHandler<MovieDownloadStatusEventArgs> DownloadProgress;

        readonly string _RemoteAddress;
        readonly int _Port;
        readonly int _AlarmId;
        readonly int _DetectorId;
        readonly TcpClient _TcpClient;
        readonly string _BaseFolder;

        public MovieReceiver( UI.ISOLASApp SOLASApp, string RemoteAddress, int Port, int DetectorId, int AlarmId, string BaseFolder )
            : base( SOLASApp, "Receiver", ThreadPriority.Normal )
        {
            _RemoteAddress = RemoteAddress;
            _Port = Port;
            _DetectorId = DetectorId;
            _AlarmId = AlarmId;
            _BaseFolder = BaseFolder;
            _TcpClient = new TcpClient();
        }

        protected override bool Init()
        {
            try
            {
                IPEndPoint RemotePoint = new IPEndPoint( IPAddress.Parse( _RemoteAddress ), _Port );
                _TcpClient.Connect( RemotePoint );
                if ( _TcpClient.Connected == false )
                {
                    LocalErrorEvent( $"AI検知PC({_RemoteAddress}:{_Port})に接続できません。" );
                    _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})接続失敗" );
                    return false;
                }

                if ( _TcpClient.Client.Send( BitConverter.GetBytes( _AlarmId ) ) != sizeof( int ) )
                {
                    LocalErrorEvent( $"AI検知PC({_RemoteAddress}:{_Port})への要求送信に失敗しました。" );
                    _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})送信失敗" );
                    return false;
                }

                InitialEvent();
                _SOLASApp.Info( $"AI検知PC({_RemoteAddress}:{_Port})動画ダウンロード開始" );
                return true;
            }
            catch(Exception e)
            {
                LocalErrorEvent( $"AI検知PC({_RemoteAddress}:{_Port})通信エラー({e.Message})" );
                _SOLASApp.Exception( $"AI検知PC({_RemoteAddress}:{_Port})通信エラー", e );
                _TcpClient.Dispose();
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _TcpClient.Close();
                _TcpClient.Dispose();
            }
            catch
            {

            }
        }

        protected override void OneshotRoutine()
        {
            try
            {
                byte[] tempBuffer = new byte[ sizeof( int ) ];

                //  ID受信
                while ( _TcpClient.Available < sizeof( int ) )
                {
                    Thread.Sleep( 10 );
                }
                int RecvLen = _TcpClient.Client.Receive( tempBuffer );
                if ( RecvLen <= 0 )
                {
                    RemoteErrorEvent( "ソケットが切断されました。" );
                    _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})ソケット切断" );
                    return;
                }

                int AlarmId = BitConverter.ToInt32( tempBuffer, 0 );
                if ( AlarmId != _AlarmId )
                {
                    RemoteErrorEvent( $"要求と異なる警報IDが返されました。 要求ID={_AlarmId} 応答ID={AlarmId}" );
                    _SOLASApp.Warn( $"警報ID不一致 {AlarmId}:{_AlarmId}" );
                    return;
                }

                //  データ長受信
                while ( _TcpClient.Available < sizeof( int ) )
                {
                    Thread.Sleep( 10 );
                }
                RecvLen = _TcpClient.Client.Receive( tempBuffer );
                if ( RecvLen <= 0 )
                {
                    RemoteErrorEvent( "ソケットが切断されました。" );
                    _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})ソケット切断" );
                    return;
                }
                int DataLength = 0;
                try
                {
                    DataLength = BitConverter.ToInt32( tempBuffer, 0 );
                }
                catch(Exception ee)
                {
                    RemoteErrorEvent( $"不正なデータ長が返されました。({ee.Message}) [{tempBuffer[ 0 ]:X2}][{tempBuffer[ 1 ]:X2}][{tempBuffer[ 2 ]:X2}][{tempBuffer[ 3 ]:X2}]" );
                    _SOLASApp.Exception( $"AI検知PC({_RemoteAddress}:{_Port})データ長エラー", ee );
                    return;
                }

                if ( DataLength <= 0 )
                {
                    if ( DataLength == 0 )
                    {
                        RemoteErrorEvent( $"AI検知PC({_RemoteAddress}:{_Port})に警報ID={_AlarmId}の動画ファイルがありません。" );
                    }
                    else
                    {
                        RemoteErrorEvent( $"AI検知PC({_RemoteAddress}:{_Port})側で警報ID={_AlarmId}の動画ファイルアクセスエラーが発生しました。。" );
                    }
                    _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})ダウンロードキャンセル DataLength={DataLength}" );
                    return;
                }

                List<byte> MovieBuffer = new List<byte>();

                while ( MovieBuffer.Count < DataLength )
                {
                    while ( _TcpClient.Available <= 0 )
                    {
                        Thread.Sleep( 10 );
                    }

                    tempBuffer = new byte[ _TcpClient.Available ];
                    RecvLen = _TcpClient.Client.Receive( tempBuffer );
                    if ( RecvLen == 0 )
                    {
                        RemoteErrorEvent( "ソケットが切断されました。" );
                        _SOLASApp.Warn( $"AI検知PC({_RemoteAddress}:{_Port})ソケット切断" );
                        return;
                    }

                    if ( RecvLen == tempBuffer.Length )
                    {
                        MovieBuffer.AddRange( tempBuffer );
                    }
                    else
                    {
                        for ( int i = 0 ; i < RecvLen ; i++ )
                        {
                            MovieBuffer.Add( tempBuffer[ i ] );
                        }
                    }

                    ProgressEvent( MovieBuffer.Count, DataLength );
                }

                //  TODO
                //  動画ファイル保存処理
                var MoviePath = Path.Combine( _BaseFolder, $"{_DetectorId}", $"{_AlarmId}.mp4" );
                var MovieFolder = Path.GetDirectoryName( MoviePath );
                if ( Directory.Exists( MovieFolder ) == false )
                {
                    Directory.CreateDirectory( MovieFolder );
                }
                File.WriteAllBytes( MoviePath, MovieBuffer.ToArray() );
                CompleteEvent( MovieBuffer.Count, MoviePath );

                MovieBuffer.Clear();
                GC.Collect();
                _SOLASApp.Info( $"AI検知PC({_RemoteAddress}:{_Port})動画ダウンロード完了" );
            }
            catch(Exception e)
            {
                LocalErrorEvent( $"動画ファイルダウンロード中にエラーが発生しました。({e.Message})" );
                _SOLASApp.Exception( $"AI検知PC({_RemoteAddress}:{_Port})通信エラー", e );
            }
        }

        private void InitialEvent()
        {
            SendEvent( new MovieDownloadStatusEventArgs() );
        }
        private void LocalErrorEvent( string ErrMsg )
        {
            SendEvent( new MovieDownloadStatusEventArgs( false, ErrMsg ) );
        }
        private void RemoteErrorEvent( string ErrMsg )
        {
            SendEvent( new MovieDownloadStatusEventArgs( true, ErrMsg ) );
        }
        private void ProgressEvent( int RecvLen, int TotalLen )
        {
            SendEvent( new MovieDownloadStatusEventArgs( RecvLen, TotalLen ) );
        }
        private void CompleteEvent( int TotalLen, string MovieFilePath )
        {
            SendEvent( new MovieDownloadStatusEventArgs( TotalLen, MovieFilePath ) );
        }


        private void SendEvent( MovieDownloadStatusEventArgs Args )
        {
            if ( DownloadProgress != null )
            {
                if ( DownloadProgress.Target is Control Cntl )
                {
                    Cntl.BeginInvoke( DownloadProgress, this, Args );
                }
                else
                {
                    DownloadProgress( this, Args );
                }
            }
        }
    }

    public class MovieDownloadStatusEventArgs : EventArgs
    {
        public enum DOWNLOAD_STATE
        {
            /// <summary>
            /// 未設定
            /// </summary>
            DLST_UNKNOWN,
            /// <summary>
            /// 要求送信済
            /// </summary>
            DLST_REQ,
            /// <summary>
            /// ダウンロード中
            /// </summary>
            DLST_DOWNLOADING,
            /// <summary>
            /// ダウンロード完了
            /// </summary>
            DLST_COMPLETE,
            /// <summary>
            /// 対象動画ファイル無し
            /// </summary>
            DLST_NO_FILE,
            /// <summary>
            /// リモート側でエラー発生
            /// </summary>
            DLST_REMOTE_FAIL,
            /// <summary>
            /// 要求送信に失敗
            /// </summary>
            DLST_LOCAL_FAIL
        }

        /// <summary>
        /// 初期化正常用(要求送信済み)
        /// </summary>
        public MovieDownloadStatusEventArgs() : base()
        {
            State = DOWNLOAD_STATE.DLST_REQ;
            RecvLen = 0;
            TotalLen = 0;
            Progress = 0;
            Message = "ダウンロード要求を送信しました。";
            MovieFile = string.Empty;
        }
        /// <summary>
        /// エラー通知用
        /// </summary>
        /// <param name="RemoteFail"></param>
        /// <param name="Message"></param>
        public MovieDownloadStatusEventArgs( bool RemoteFail, string ErrorMessage ) : base()
        {
            if ( RemoteFail == true )
            {
                State = DOWNLOAD_STATE.DLST_REMOTE_FAIL;
            }
            else
            {
                State = DOWNLOAD_STATE.DLST_LOCAL_FAIL;
            }
            Message = ErrorMessage;
            RecvLen = 0;
            TotalLen = 0;
            Progress = 0;
            MovieFile = string.Empty;
        }
        /// <summary>
        /// ダウンロード中（途中経過通知）
        /// </summary>
        /// <param name="RecvBytes"></param>
        /// <param name="TotalBytes"></param>
        public MovieDownloadStatusEventArgs( int RecvBytes, int TotalBytes ) : base()
        {
            State = DOWNLOAD_STATE.DLST_DOWNLOADING;
            RecvLen = RecvBytes;
            TotalLen = TotalBytes;
            Progress = Convert.ToInt32( ( float )RecvBytes / ( float )TotalBytes * 100F );
            Message = $"ダウンロード中... {RecvBytes}/{TotalBytes}";
            MovieFile = string.Empty;
        }
        /// <summary>
        /// ダウンロード完了
        /// </summary>
        /// <param name="RecvBytes"></param>
        /// <param name="TotalBytes"></param>
        public MovieDownloadStatusEventArgs( int RecvBytes, string FilePath ) : base()
        {
            State = DOWNLOAD_STATE.DLST_COMPLETE;
            RecvLen = RecvBytes;
            TotalLen = RecvBytes;
            Progress = 100;
            Message = "ダウンロードを完了しました。";
            MovieFile = FilePath;
        }

        public DOWNLOAD_STATE State
        {
            get;
            private set;
        } = DOWNLOAD_STATE.DLST_UNKNOWN;

        public int RecvLen;

        public int TotalLen;

        public int Progress;

        public string Message;

        public string MovieFile;
    }
}
