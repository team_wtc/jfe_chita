﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using GDIBitmap = System.Drawing.Bitmap;
using GDIImage = System.Drawing.Image;

namespace CES.SOLAS.Lib.ProcessIF.TCP
{
    using Lib.Threads;
    using Lib.UI;
    using CvSize = OpenCvSharp.Size;

    public class StillImageSender : QueueThread<(int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp,  Mat Img)>
    {
        //const int IMAGE_BUFFER_SIZE = 2 * 1024 * 1024;

        readonly IPEndPoint _RemoteEndPoint;
        readonly MONITORING_TYPE _MonitoringType;
        //readonly MemoryStream _MemoryStream;
        //readonly byte[] _ImageBuffer;
        readonly bool _LocalEncoder;

        TcpClient _TcpClient;
        DateTime? _LastSendTime = null;
        Task _OpenSocketTask = null;
        int _FrameCount = 0;
        bool _EnableDetect;

        public StillImageSender(ISOLASApp SOLASApp, MONITORING_TYPE MonitoringType, bool DetectEnable, string MonitoringAppAddress, int MonitoringAppPort, bool LocalEncoder )
            :base( SOLASApp ,"ImgSender", ThreadPriority.BelowNormal, false, 0 )
        {
            _LocalEncoder = LocalEncoder;
            _MonitoringType = MonitoringType;
            _RemoteEndPoint = new IPEndPoint( IPAddress.Parse( MonitoringAppAddress ), MonitoringAppPort );
            _EnableDetect = DetectEnable;
        }

        public bool EnableDetect
        {
            get
            {
                return _EnableDetect;
            }
            set
            {
                if ( _EnableDetect != value )
                {
                    _EnableDetect = value;
                    _LastSendTime = null;
                }
            }
        }

        protected override bool Init()
        {
            _SOLASApp?.Info( $"ImgSender Startup. Monitor = {_RemoteEndPoint}" );
            _FrameCount = _SOLASApp.StreamingSkipFrame;
            return true;
        }

        protected override void Terminate()
        {
            CloseSocket();
            //_MemoryStream.Dispose();
            _SOLASApp?.Info( $"ImgSender Terminate. Monitor = {_RemoteEndPoint}" );
        }

        private void OpenSocket()
        {
            if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
            {
                return;
            }

            if ( _OpenSocketTask != null )
            {
                return;
            }

            _OpenSocketTask = Task.Factory.StartNew( () =>
            {
                var Client = new TcpClient();
                try
                {
                    Client.Connect( _RemoteEndPoint );
                    if ( Client.Connected == true )
                    {
                        _TcpClient = Client;
                        //SetKeepAlive();
                        _SOLASApp?.Info( $"Image Send Socket Open.({_RemoteEndPoint})" );
                    }
                    else
                    {
                        Client?.Dispose();
                        _TcpClient = null;
                    }
                }
                catch ( Exception e )
                {
                    _SOLASApp?.Warn( $"Socket Connection Fail.{e.Message}" );
                    //_SOLASApp.Exception( "Socket Connection Fail.", e );
                    Client?.Dispose();
                    _TcpClient = null;
                }
            } ).ContinueWith( ( tsk ) =>
            {
                _OpenSocketTask = null;
            } );
        }

        private void CloseSocket()
        {
            try
            {
                _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                _TcpClient?.Close();
                _TcpClient?.Dispose();
                _TcpClient = null;
                _SOLASApp?.Info( $"Image Send Socket Close.({_RemoteEndPoint})" );
            }
            catch ( Exception e )
            {
                _SOLASApp?.Exception( "Socket Close Fail.", e );
            }
        }

        protected override bool ItemRoutine( (int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat Img) Item )
        {
            try
            {
                OpenSocket();

                if ( ( _TcpClient != null ) && ( _TcpClient.Connected == true ) )
                {
                    bool PacketSendFlag = false;

                    if ( ( Item.Img == null ) || ( Item.Img.IsDisposed == true ) )
                    {
                        PacketSendFlag = true;
                        _FrameCount = 0;
                    }
                    else if ( _LocalEncoder == true )
                    {
                        PacketSendFlag = true;
                        _FrameCount = 0;
                    }
                    //  ストリーミングモードのモニタアプリなら警報を含む場合のみ送信
                    else if ( _MonitoringType == MONITORING_TYPE.MON_STREAMING )
                    {
                        //  警報を含む場合は送信
                        if ( Item.AlarmId > 0 )
                        {
                            PacketSendFlag = true;
                            _FrameCount = 0;
                            _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Alarm={Item.AlarmId} Now = {DateTime.Now} Monitor = {_RemoteEndPoint}" );
                        }
                    }
                    //  静止画モードのモニタアプリなら送信タイミングをチェック
                    else if ( _MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                    {
                        //  検知オブジェクト無しなら長いインターバルで送信
                        if ( Item.DetectCount <= 0 )
                        {
                            if ( EnableDetect == true )
                            {
                                //  初回は必ず送信
                                if ( _LastSendTime.HasValue == false )
                                {
                                    PacketSendFlag = true;
                                    _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Interval={_SOLASApp.StillImageLongInterval} Last Send = 初回 Now = {DateTime.Now} Monitor = {_RemoteEndPoint}" );
                                }
                                //  2回目以降は前回送信時刻から長いインターバル以上経過していたら送信
                                else if ( _LastSendTime.Value.AddSeconds( _SOLASApp.StillImageLongInterval ) <= DateTime.Now )
                                {
                                    PacketSendFlag = true;
                                    _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Interval={_SOLASApp.StillImageLongInterval} Last Send = {_LastSendTime.Value} Now = {DateTime.Now} Monitor = {_RemoteEndPoint}" );
                                }
                            }
                        }
                        //  検知オブジェクト有り
                        else
                        {
                            if ( EnableDetect == true )
                            {
                                //  エラー無しなら短いインターバルで送信
                                if ( Item.AlarmId <= 0 )
                                {
                                    //  初回は必ず送信
                                    if ( _LastSendTime.HasValue == false )
                                    {
                                        PacketSendFlag = true;
                                        _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Interval={_SOLASApp.StillImageShortInterval} Last Send = 初回 Now = {DateTime.Now}" );
                                    }
                                    //  2回目以降は前回送信時刻から長いインターバル以上経過していたら送信
                                    else if ( _LastSendTime.Value.AddSeconds( _SOLASApp.StillImageShortInterval ) <= DateTime.Now )
                                    {
                                        PacketSendFlag = true;
                                        _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Interval={_SOLASApp.StillImageShortInterval} Last Send = {_LastSendTime.Value} Now = {DateTime.Now} Monitor = {_RemoteEndPoint}" );
                                    }
                                }
                                //  エラーがある場合は必ず送信
                                else
                                {
                                    PacketSendFlag = true;
                                    _SOLASApp?.Info( $"静止画送信 Detect={Item.DetectCount} Alarm={Item.AlarmId} Now = {DateTime.Now} Monitor = {_RemoteEndPoint}" );
                                }
                            }
                        }
                    }

                    if ( PacketSendFlag == true )
                    {
                        var ImgSize = ImageShrink( Item.Img, out byte[] ImageBuffer );
                        ImagePacket Packet = null;
                        try
                        {
                            Packet = new ImagePacket(
                                _SOLASApp.ApplicationId,
                                Item.AlarmId,
                                Item.AlarmType,
                                Item.Timestamp,
                                ImgSize,
                                ImageBuffer );
                        }
                        catch ( Exception e )
                        {
                            _SOLASApp?.Exception( $"StillImagePacket Create Error.", e );
                            return true;
                        }

                        _LastSendTime = DateTime.Now;
                        _TcpClient.Client.Send( Packet.Payload );
                        _SOLASApp?.Info( $"静止画送信完了 Monitor={_RemoteEndPoint} Packet={Packet}" );
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp?.Exception( $"Send Error. (RemoteEndPoint={_TcpClient.Client.RemoteEndPoint})", e );
                CloseSocket();
            }
            finally
            {
                Item.Img?.Dispose();
            }

            return true;
        }

        private int ImageShrink( Mat Item, out byte[] ImageBuffer )
        {
            //_MemoryStream.Position = 0L;
            if ( ( Item == null ) || ( Item.IsDisposed == true ) )
            {
                ImageBuffer = null;
                return 0;
            }

            //Bitmap ShrinkBmp = null;
            //int BmpWidth = Convert.ToInt32( Math.Truncate( ( double )Item.Width * ( double )_SOLASApp.StreamingImageShrinkRatio ) );
            //int BmpHeight = Convert.ToInt32( Math.Truncate( ( double )Item.Height * ( double )_SOLASApp.StreamingImageShrinkRatio ) );
            int BmpWidth = Item.Width;
            int BmpHeight = Item.Height;

            try
            {
                if ( _LocalEncoder == false )
                {
                    if ( _MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                    {
                        BmpWidth = Convert.ToInt32( Math.Truncate( ( double )Item.Width * ( double )_SOLASApp.StillImageShrinkRatio ) );
                        BmpHeight = Convert.ToInt32( Math.Truncate( ( double )Item.Height * ( double )_SOLASApp.StillImageShrinkRatio ) );
                    }
                    else
                    {
                        BmpWidth = Convert.ToInt32( Math.Truncate( ( double )Item.Width * ( double )_SOLASApp.StreamingImageShrinkRatio ) );
                        BmpHeight = Convert.ToInt32( Math.Truncate( ( double )Item.Height * ( double )_SOLASApp.StreamingImageShrinkRatio ) );
                    }
                }

                using ( var ItemBitmap = BitmapConverter.ToBitmap( Item ) )
                using ( var ShrinkBmp = new GDIBitmap( BmpWidth, BmpHeight ) )
                using ( var Graph = Graphics.FromImage( ( GDIImage )ShrinkBmp ) )
                using ( var MemStream = new MemoryStream() )
                {
                    Graph.CompositingMode = CompositingMode.SourceCopy;
                    Graph.CompositingQuality = CompositingQuality.HighQuality;
                    Graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Graph.DrawImage( ItemBitmap, new Rectangle( 0, 0, BmpWidth, BmpHeight ) );
                    ShrinkBmp.Save( MemStream, ImageFormat.Jpeg );
                    ImageBuffer = MemStream.ToArray();
                }

                return ImageBuffer.Length;
            }
            catch
            {
                ImageBuffer = null;
                return 0;
            }
            finally
            {
                //ShrinkBmp?.Dispose();
            }

        }

        private void SetKeepAlive()
        {
            var KeepAliveStructValue = new byte[ sizeof( uint ) * 3 ];
            BitConverter.GetBytes( 1 ).CopyTo( KeepAliveStructValue, 0 );
            BitConverter.GetBytes( _SOLASApp.TcpKeepAliveWait ).CopyTo( KeepAliveStructValue, 4 );
            BitConverter.GetBytes( _SOLASApp.TcpKeepAliveInterval ).CopyTo( KeepAliveStructValue, 8 );

            _TcpClient.Client.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true );
            _TcpClient.Client.IOControl( IOControlCode.KeepAliveValues, KeepAliveStructValue, null );
        }
    }
}
