﻿using System;
using System.Collections.Generic;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class AlarmPacket : Packet
    {
        public AlarmPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_NEW_ALARM, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == ( sizeof( int ) + sizeof( byte ) ) ) ), "警報IDパラメータ不正" );

            AlarmId = BitConverter.ToInt32( ParameterBytes, 0 );

            Assert( Enum.IsDefined( typeof( ALARM_TYPE ), ParameterBytes[ 4 ] ), $"警報種別不正({ParameterBytes[ 4 ]})" );
            AlarmType = ( ALARM_TYPE )Enum.ToObject( typeof( ALARM_TYPE ), ParameterBytes[ 4 ] );
        }

        public AlarmPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_NEW_ALARM, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public AlarmPacket( int ProcessId, int AlarmId, ALARM_TYPE AlarmType ) : this( ProcessId, AlarmId, AlarmType, DateTime.Now )
        {
        }

        public AlarmPacket( int ProcessId, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_NEW_ALARM, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.AlarmId = AlarmId;
            this.AlarmType = AlarmType;
        }

        public int AlarmId
        {
            get;
            set;
        } = 0;

        public ALARM_TYPE AlarmType
        {
            get;
            set;
        } = ALARM_TYPE.ALARM_TYPE_UNKNOWN;

        protected override byte[] CreateParameterBytes()
        {
            List<byte> Buffer = new List<byte>();

            Assert( AlarmId > 0, "警報ID未設定" );
            Buffer.AddRange( BitConverter.GetBytes( AlarmId ) );

            Assert( AlarmType != ALARM_TYPE.ALARM_TYPE_UNKNOWN, "警報種別未設定" );
            Buffer.Add( ( byte )AlarmType );

            return Buffer.ToArray();
        }
    }
}
