﻿using System;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class AppStatusPacket : Packet
    {
        public AppStatusPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_APP_STATUS, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == sizeof( byte ) ) ), "起動状態通知パラメータ不正" );
            Assert( Enum.IsDefined( typeof( APP_STATUS ), ParameterBytes[ 0 ] ), $"不正な起動状態({ ParameterBytes[ 0 ]})" );

            Status = ( APP_STATUS )Enum.ToObject( typeof( APP_STATUS ), ParameterBytes[ 0 ] );
        }

        public AppStatusPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_APP_STATUS, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public AppStatusPacket( int ProcessId, APP_STATUS Status ) : this( ProcessId, Status, DateTime.Now )
        {
        }

        public AppStatusPacket( int ProcessId, APP_STATUS Status, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_APP_STATUS, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.Status = Status;
        }

        public APP_STATUS Status
        {
            get;
            set;
        } = APP_STATUS.APP_STS_UNKNOWN;

        protected override byte[] CreateParameterBytes()
        {
            return new byte[] { ( byte )Status };
        }

        public override string ToString()
        {
            return $"{base.ToString()} Status={Status}";
        }
    }
}
