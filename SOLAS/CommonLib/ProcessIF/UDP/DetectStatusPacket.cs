﻿using System;

using static System.Diagnostics.Trace;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class DetectStatusPacket : Packet
    {
        public DetectStatusPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_DETECT_STATUS, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == sizeof( byte ) ) ), "検知状態通知パラメータ不正" );

            Suppress = ( ParameterBytes[ 0 ] > 0 );
        }

        public DetectStatusPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_DETECT_STATUS, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public DetectStatusPacket( int ProcessId, bool Suppress ) : this( ProcessId, Suppress, DateTime.Now )
        {
        }

        public DetectStatusPacket( int ProcessId, bool Suppress, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_DETECT_STATUS, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.Suppress = Suppress;
        }

        public bool Suppress
        {
            get;
            set;
        } = false;

        protected override byte[] CreateParameterBytes()
        {
            return new byte[] { ( Suppress == false ) ? ( byte )0 : ( byte )1 };
        }
    }
}
