﻿using System;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class DetectSuppressPacket : Packet
    {
        public DetectSuppressPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_DETECT_SUPPRESS, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == sizeof( byte ) ) ), "検知抑制パラメータ不正" );

            Suppress = ( ParameterBytes[ 0 ] > 0 );
        }

        public DetectSuppressPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_DETECT_SUPPRESS, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId )
        {
        }

        public DetectSuppressPacket( int ProcessId, bool Suppress ) : this( ProcessId, Suppress, DateTime.Now )
        {
        }

        public DetectSuppressPacket( int ProcessId, bool Suppress, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_DETECT_SUPPRESS, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
            this.Suppress = Suppress;
        }

        public bool Suppress
        {
            get;
            set;
        } = false;

        protected override byte[] CreateParameterBytes()
        {
            return new byte[] { ( Suppress == false ) ? ( byte )0 : ( byte )1 };
        }

        public override string ToString()
        {
            return $"{base.ToString()} Suppress={Suppress}";
        }
    }
}
