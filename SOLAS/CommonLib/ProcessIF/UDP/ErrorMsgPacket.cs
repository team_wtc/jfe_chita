﻿using System;
using System.Text;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class ErrorMsgPacket : Packet
    {
        public ErrorMsgPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_ERROR_MSG, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length > 0 ) ), "メッセージパラメータ不正" );

            try
            {
                Message = Encoding.UTF8.GetString( ParameterBytes );
            }
            catch ( Exception e )
            {
                Assert( false, $"文字列変換失敗({e.Message})" );
            }
        }

        public ErrorMsgPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_ERROR_MSG, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public ErrorMsgPacket( int ProcessId, string Message ) : this( ProcessId, Message, DateTime.Now )
        {
        }

        public ErrorMsgPacket( int ProcessId, string Message, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_ERROR_MSG, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.Message = Message;
        }

        public string Message
        {
            get;
            set;
        } = String.Empty;

        protected override byte[] CreateParameterBytes()
        {
            byte[] MsgBytes = Encoding.UTF8.GetBytes( Message );
            Assert( ( ( MsgBytes.Length > 0 ) && ( MsgBytes.Length <= 1400 ) ), $"メッセージ文字列長エラー({MsgBytes.Length}bytes)" );
            return MsgBytes;
        }

        public override string ToString()
        {
            return $"{base.ToString()} ({Message})";
        }
    }
}
