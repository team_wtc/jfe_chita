﻿using System;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class HeartBeatPacket : Packet
    {
        public HeartBeatPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_HEARTBEAT, $"トリガ区分不一致({Trigger})" );
        }

        public HeartBeatPacket( int ProcessId ) : this( ProcessId, DateTime.Now )
        {
        }

        public HeartBeatPacket( int ProcessId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_HEARTBEAT, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
        }

        protected override byte[] CreateParameterBytes()
        {
            return new byte[ 0 ];
        }
    }
}
