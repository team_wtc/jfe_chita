﻿using System;
using System.Collections.Generic;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class HomePositionPacket : Packet
    {
        public HomePositionPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_CURRENT_HOME, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == ( sizeof( int ) * 4 ) ) ), "HOMEポジション通知パラメータ不正" );

            Pan = BitConverter.ToInt32( ParameterBytes, ( sizeof( int ) * 0 ) );
            Tilt = BitConverter.ToInt32( ParameterBytes, ( sizeof( int ) * 1 ) );
            Zoom = BitConverter.ToInt32( ParameterBytes, ( sizeof( int ) * 2 ) );
            ZoomRatio = BitConverter.ToInt32( ParameterBytes, ( sizeof( int ) * 3 ) );
        }

        public HomePositionPacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_CURRENT_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public HomePositionPacket(
            int ProcessId,
            int Pan,
            int Tilt,
            int Zoom,
            int ZoomRatio ) : this( ProcessId, Pan, Tilt, Zoom, ZoomRatio, DateTime.Now )
        {
        }

        public HomePositionPacket(
            int ProcessId,
            int Pan,
            int Tilt,
            int Zoom,
            int ZoomRatio,
            DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_CURRENT_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.Pan = Pan;
            this.Tilt = Tilt;
            this.Zoom = Zoom;
            this.ZoomRatio = ZoomRatio;
        }

        public int Pan
        {
            get;
            set;
        } = -1;
        public int Tilt
        {
            get;
            set;
        } = -1;
        public int Zoom
        {
            get;
            set;
        } = -1;
        public int ZoomRatio
        {
            get;
            set;
        } = -1;

        protected override byte[] CreateParameterBytes()
        {
            List<byte> ParamBytes = new List<byte>();

            Assert( Pan >= 0, "Pan未設定" );
            Assert( Tilt >= 0, "Tilt未設定" );
            Assert( Zoom >= 0, "Zoom未設定" );
            Assert( ZoomRatio >= 0, "ZoomRatio未設定" );

            ParamBytes.AddRange( BitConverter.GetBytes( Pan ) );
            ParamBytes.AddRange( BitConverter.GetBytes( Tilt ) );
            ParamBytes.AddRange( BitConverter.GetBytes( Zoom ) );
            ParamBytes.AddRange( BitConverter.GetBytes( ZoomRatio ) );

            return ParamBytes.ToArray();
        }

        public override string ToString()
        {
            return $"{base.ToString()} Pan={Pan} Tilt={Tilt} Zoom={Zoom} ZoomRatio={ZoomRatio}";
        }
    }
}
