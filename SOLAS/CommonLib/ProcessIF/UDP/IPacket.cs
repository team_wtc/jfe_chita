﻿using System;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public interface IPacket
    {
        TRIGGER_TYPE Trigger
        {
            get;
            set;
        }

        TRIGGER_PROCESS_TYPE ProcessType
        {
            get;
            set;
        }

        int ProcessId
        {
            get;
            set;
        }

        DateTime Timestamp
        {
            get;
            set;
        }

        byte[] CreatePacketBytes();
    }
}
