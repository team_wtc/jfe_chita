﻿using System;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class ImagePacket : Packet
    {
        public ImagePacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_NEW_IMAGE, $"トリガ区分不一致({Trigger})" );
            Assert( ( ( ParameterBytes != null ) && ( ParameterBytes.Length == sizeof( int ) ) ), "静止画IDパラメータ不正" );

            ImageId = BitConverter.ToInt32( ParameterBytes, 0 );
        }

        public ImagePacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_NEW_IMAGE, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId )
        {
        }

        public ImagePacket( int ProcessId, int ImageId ) : this( ProcessId, ImageId, DateTime.Now )
        {
        }

        public ImagePacket( int ProcessId, int ImageId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_NEW_IMAGE, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, ProcessId, Timestamp )
        {
            this.ImageId = ImageId;
        }

        public int ImageId
        {
            get;
            set;
        } = 0;

        protected override byte[] CreateParameterBytes()
        {
            Assert( ImageId > 0, "静止画ID未設定" );
            return BitConverter.GetBytes( ImageId );
        }
    }
}
