﻿using System;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class MoveHomePacket : Packet
    {
        public MoveHomePacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_MOVE_HOME, $"トリガ区分不一致({Trigger})" );
        }

        public MoveHomePacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_MOVE_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId )
        {
        }

        public MoveHomePacket( int ProcessId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_MOVE_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
        }

        protected override byte[] CreateParameterBytes()
        {
            return new byte[ 0 ];
        }
    }
}
