﻿using System;
using System.Collections.Generic;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib
{
    public enum TRIGGER_PROCESS_TYPE : byte
    {
        /// <summary>
        /// 不明
        /// </summary>
        TRG_PROC_UNKNOWN = 0,
        /// <summary>
        /// Monitoring.exe
        /// </summary>
        TRG_PROC_MONITOR = 1,
        /// <summary>
        /// Detector.exe
        /// </summary>
        TRG_PROC_DETECTOR = 2
    }

    public enum APP_STATUS : byte
    {
        /// <summary>
        /// 不明
        /// </summary>
        APP_STS_UNKNOWN = 255,
        /// <summary>
        /// 起動直後
        /// </summary>
        APP_STS_STARTUP = 0,
        /// <summary>
        /// 初期化中
        /// </summary>
        APP_STS_INIT = 1,
        /// <summary>
        /// 動作中
        /// </summary>
        APP_STS_RUNNING = 2,
        /// <summary>
        /// 動作中（発報抑制中）
        /// </summary>
        APP_STS_RUNNING_NO_DETECT = 3,
        /// <summary>
        /// 停止中
        /// </summary>
        APP_STS_STOPING = 8,
        /// <summary>
        /// 停止
        /// </summary>
        APP_STS_STOPED = 9,
        /// <summary>
        /// 起動不可
        /// </summary>
        APP_STS_FAIL = 99
    }

    /// <summary>
    /// 静止画区分
    /// </summary>
    public enum IMAGE_TYPE
    {
        /// <summary>
        /// 通常静止画
        /// </summary>
        NORMAL_IMAGE = 1,
        /// <summary>
        /// 警報静止画
        /// </summary>
        ALARM_IMAGE = 2,
        /// <summary>
        /// 警報静止画縮小版
        /// </summary>
        ALARM_SMALL_IMAGE = 3
    }

    public enum ALARM_TYPE
    {
        ALARM_TYPE_UNKNOWN = 0,
        ALARM_TYPE_A = 1,
        ALARM_TYPE_B = 2,
        ALARM_TYPE_C = 3,
        ALARM_TYPE_PERSON = ALARM_TYPE_A,
        ALARM_TYPE_CAR = ALARM_TYPE_B,
        ALARM_TYPE_CAR_STAY = ALARM_TYPE_C
    }

    public enum MONITORING_TYPE
    {
        MON_UNKNOWN = 0,
        MON_STREAMING = 1,
        MON_STILL_IMAGE = 2
    }
}

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public enum TRIGGER_TYPE : byte
    {
        /// <summary>
        /// 不明
        /// </summary>
        TRG_UNKNOWN = 0,
        /// <summary>
        /// 新規警報
        /// </summary>
        TRG_NEW_ALARM = 1,
        /// <summary>
        /// 新規静止画
        /// </summary>
        TRG_NEW_IMAGE = 2,
        /// <summary>
        /// 検知抑制要求
        /// </summary>
        TRG_DETECT_SUPPRESS = 3,
        /// <summary>
        /// 検知状態通知
        /// </summary>
        //TRG_DETECT_STATUS = 4,
        /// <summary>
        /// HOMEポジション登録
        /// </summary>
        TRG_SET_HOME = 5,
        /// <summary>
        /// HOMEポジション移動
        /// </summary>
        TRG_MOVE_HOME = 6,
        /// <summary>
        /// HOMEポジション通知
        /// </summary>
        TRG_CURRENT_HOME = 7,
        /// <summary>
        /// エラー通知
        /// </summary>
        TRG_ERROR_MSG = 8,
        /// <summary>
        /// 再起動要求
        /// </summary>
        TRG_APP_RESTART = 9,
        /// <summary>
        /// 起動状況通知
        /// </summary>
        TRG_APP_STATUS = 10,
        /// <summary>
        /// ハートビート
        /// </summary>
        TRG_HEARTBEAT = 11,
        /// <summary>
        /// 停止要求
        /// </summary>
        TRG_APP_STOP = 12
    }

    public abstract class Packet : IPacket
    {
        const int HEADER_SIZE = 12;

        protected byte[] ParameterBytes;

        public static TRIGGER_TYPE GetTriggerType( byte TriggerTypeByte )
        {
            if ( Enum.IsDefined( typeof( TRIGGER_TYPE ), TriggerTypeByte ) == false )
            {
                return TRIGGER_TYPE.TRG_UNKNOWN;
            }
            return ( TRIGGER_TYPE )Enum.ToObject( typeof( TRIGGER_TYPE ), TriggerTypeByte );
        }

        public static TRIGGER_TYPE GetTriggerType( byte[] Payload )
        {
            return GetTriggerType( Payload[ 0 ] );
        }

        public static IPacket Analyze( byte[] Payload )
        {
            switch ( GetTriggerType( Payload ) )
            {
                case TRIGGER_TYPE.TRG_NEW_ALARM:
                    return new AlarmPacket( Payload );
                case TRIGGER_TYPE.TRG_NEW_IMAGE:
                    return new ImagePacket( Payload );
                case TRIGGER_TYPE.TRG_DETECT_SUPPRESS:
                    return new DetectSuppressPacket( Payload );
                case TRIGGER_TYPE.TRG_SET_HOME:
                    return new SetHomePacket( Payload );
                case TRIGGER_TYPE.TRG_MOVE_HOME:
                    return new MoveHomePacket( Payload );
                case TRIGGER_TYPE.TRG_CURRENT_HOME:
                    return new HomePositionPacket( Payload );
                case TRIGGER_TYPE.TRG_ERROR_MSG:
                    return new ErrorMsgPacket( Payload );
                case TRIGGER_TYPE.TRG_APP_RESTART:
                    return new RestartPacket( Payload );
                case TRIGGER_TYPE.TRG_APP_STATUS:
                    return new AppStatusPacket( Payload );
                case TRIGGER_TYPE.TRG_HEARTBEAT:
                    return new HeartBeatPacket( Payload );
                case TRIGGER_TYPE.TRG_APP_STOP:
                    return new StopPacket( Payload );
                default:
                    break;
            }

            return null;
        }

        public Packet( byte[] Payload )
        {
            Assert( Enum.IsDefined( typeof( TRIGGER_TYPE ), Payload[ 0 ] ), $"不正なトリガ({Payload[ 0 ]})" );
            Trigger = ( TRIGGER_TYPE )Enum.ToObject( typeof( TRIGGER_TYPE ), Payload[ 0 ] );

            Assert( Enum.IsDefined( typeof( TRIGGER_PROCESS_TYPE ), Payload[ 1 ] ), $"不正なプロセス区分({Payload[ 1 ]})" );
            ProcessType = ( TRIGGER_PROCESS_TYPE )Enum.ToObject( typeof( TRIGGER_PROCESS_TYPE ), Payload[ 1 ] );

            Assert( Payload[ 2 ] > 0, $"不正なプロセスID({( ( int )Payload[ 2 ] )})" );
            ProcessId = ( int )Payload[ 2 ];

            try
            {
                Timestamp = new DateTime(
                    ( int )Payload[ 3 ] * 100 + ( int )Payload[ 4 ],     //  Year
                    ( int )Payload[ 5 ],                                //  Month
                    ( int )Payload[ 6 ],                                //  Day
                    ( int )Payload[ 7 ],                                //  Hour
                    ( int )Payload[ 8 ],                                //  Min
                    ( int )Payload[ 9 ],                                //  Sec
                    ( int )Payload[ 10 ] * 100 + ( int )Payload[ 11 ],        //  Msec
                    DateTimeKind.Local );
            }
            catch
            {
                Assert( false, $"不正なタイムスタンプ" +
                    $"{Payload[ 3 ]}," +
                    $"{Payload[ 4 ]}," +
                    $"{Payload[ 5 ]}," +
                    $"{Payload[ 6 ]}," +
                    $"{Payload[ 7 ]}," +
                    $"{Payload[ 8 ]}," +
                    $"{Payload[ 9 ]}," +
                    $"{Payload[ 10 ]}," +
                    $"{Payload[ 11 ]}" );
            }

            if ( Payload.Length > HEADER_SIZE )
            {
                int CopyLength = ( Payload.Length - HEADER_SIZE );
                ParameterBytes = new byte[ CopyLength ];
                Array.Copy( sourceArray: Payload, sourceIndex: HEADER_SIZE, destinationArray: ParameterBytes, destinationIndex: 0, length: CopyLength );
            }
        }

        public Packet( TRIGGER_TYPE Trigger, TRIGGER_PROCESS_TYPE ProcessType, int ProcessId ) : this( Trigger, ProcessType, ProcessId, DateTime.Now )
        {
        }

        public Packet( TRIGGER_TYPE Trigger, TRIGGER_PROCESS_TYPE ProcessType, int ProcessId, DateTime Timestamp )
        {
            Assert( ( ( ProcessId > 0 ) && ( ProcessId <= byte.MaxValue ) ), $"プロセスID想定範囲外({ProcessId})" );

            this.Trigger = Trigger;
            this.ProcessType = ProcessType;
            this.ProcessId = ProcessId;
            this.Timestamp = Timestamp;
        }

        public TRIGGER_TYPE Trigger
        {
            get;
            set;
        } = TRIGGER_TYPE.TRG_UNKNOWN;

        public TRIGGER_PROCESS_TYPE ProcessType
        {
            get;
            set;
        } = TRIGGER_PROCESS_TYPE.TRG_PROC_UNKNOWN;

        public int ProcessId
        {
            get;
            set;
        } = 0;

        public DateTime Timestamp
        {
            get;
            set;
        } = DateTime.Now;

        protected abstract byte[] CreateParameterBytes();

        public byte[] CreatePacketBytes()
        {
            List<byte> PacketByteArray = new List<byte>
            {
                ( byte )Trigger,
                ( byte )ProcessType,
                ( byte )ProcessId,
                ( byte )( Timestamp.Year / 100 ),
                ( byte )( Timestamp.Year % 100 ),
                ( byte )Timestamp.Month,
                ( byte )Timestamp.Day,
                ( byte )Timestamp.Hour,
                ( byte )Timestamp.Minute,
                ( byte )Timestamp.Second,
                ( byte )( Timestamp.Millisecond / 100 ),
                ( byte )( Timestamp.Millisecond % 100 )
            };

            PacketByteArray.AddRange( CreateParameterBytes() );

            return PacketByteArray.ToArray();
        }

        public override string ToString()
        {
            return $"ID:[{this.ProcessId}]({this.ProcessType}) [{this.Trigger}]({this.Timestamp.ToString("yyyy/MM/dd HH:mm:ss.fff")})";
        }
    }
}
