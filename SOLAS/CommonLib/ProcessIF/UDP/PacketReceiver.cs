﻿using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    using Lib.UI;

    public delegate void PacketReceiveCallback( IPacket PacketObject );

    public class PacketReceiver : Threads.OneshotThread
    {
        readonly int _BindPort;
        readonly PacketReceiveCallback _Callback;
        readonly TriggerCallback _UICallback;
        readonly UdpClient _UdpClient;

        public PacketReceiver(ISOLASApp SOLASApp, int Port, TriggerCallback UICallback ) : base( SOLASApp, "UDPRecver", ThreadPriority.Normal )
        {
            _BindPort = Port;
            _UICallback = UICallback;
            _Callback = _UICallback.Callback;

            try
            {
                //_UdpClient = new UdpClient( _BindPort, AddressFamily.InterNetwork );
                _UdpClient = new UdpClient(AddressFamily.InterNetwork);
                _UdpClient.Client.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
                _UdpClient.Client.Bind( new IPEndPoint( IPAddress.Any, _BindPort ) );
            }
            catch ( Exception e )
            {
                Assert( false, $"UDP(port={_BindPort})バインドエラー({e.Message})" );
            }
        }
        public PacketReceiver( ISOLASApp SOLASApp, int Port, PacketReceiveCallback Callback ) : base( SOLASApp, "UDPRecver", ThreadPriority.Normal )
        {
            _BindPort = Port;
            _Callback = Callback;
            _UICallback = null;

            try
            {
                _UdpClient = new UdpClient( _BindPort, AddressFamily.InterNetwork );
            }
            catch ( Exception e )
            {
                Assert( false, $"UDP(port={_BindPort})バインドエラー({e.Message})" );
            }
        }

        protected override void Terminate()
        {
            try
            {
                _UdpClient.Close();
                _UdpClient.Dispose();
            }
            catch
            {

            }
            base.Terminate();
        }

        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( _UdpClient.Client.Poll( 100, SelectMode.SelectRead ) == false )
                {
                    continue;
                }

                IPEndPoint RemoteEndPoint = new IPEndPoint( IPAddress.Any, 0 );
                byte[] Payload = _UdpClient.Receive( ref RemoteEndPoint );
                if ( ( Payload == null ) || ( Payload.Length <= 0 ) )
                {
                    continue;
                }
                IPacket Pkt = Packet.Analyze( Payload );
                if ( Pkt == null )
                {
                    continue;
                }

                Assert( _Callback != null, "コールバック未設定" );
                _SOLASApp?.Info( $"Receive ({RemoteEndPoint}) [{Pkt.ToString()}] " );
                _Callback( Pkt );
            }
        }
    }
}
