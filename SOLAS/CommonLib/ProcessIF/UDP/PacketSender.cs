﻿using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public class PacketSender : Threads.QueueThread<IPacket>
    {
        readonly string _RemoteAddress;
        readonly int _RemotePort;
        readonly IPEndPoint _RemoteEndPoint;

        public PacketSender(
            UI.ISOLASApp SOLASApp,
            TRIGGER_PROCESS_TYPE ProcessType,
            string RemoteAddress,
            int RemotePort ) : base( SOLASApp, "UDPSender", ThreadPriority.Normal, false, 0 )
        {
            _RemoteAddress = RemoteAddress;
            _RemotePort = RemotePort;

            _RemoteEndPoint = new IPEndPoint( IPAddress.Parse( RemoteAddress ), _RemotePort );
        }

        public APP_STATUS RemoteStatus
        {
            get;
            set;
        } = APP_STATUS.APP_STS_UNKNOWN;

        public void NewAlarm( int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp )
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, "NewAlarm送信方向不正" );
            Assert( AlarmType == ALARM_TYPE.ALARM_TYPE_A || AlarmType == ALARM_TYPE.ALARM_TYPE_B || AlarmType == ALARM_TYPE.ALARM_TYPE_C, "アラーム種別不正" );

            Enqueue( new AlarmPacket( _SOLASApp.ApplicationId, AlarmId, AlarmType, Timestamp ) );
        }

        public void NewImage( int ImageId, DateTime Timestamp )
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, "NewImage送信方向不正" );

            Enqueue( new ImagePacket( _SOLASApp.ApplicationId, ImageId, Timestamp ) );
        }

        public void DetectSuppress( bool Suppress )
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "DetectSuppress送信方向不正" );

            Enqueue( new DetectSuppressPacket( _SOLASApp.ApplicationId, Suppress ) );
        }

        public void SetHomePosition()
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "SetHomePosition送信方向不正" );

            Enqueue( new SetHomePacket( _SOLASApp.ApplicationId ) );
        }

        public void MoveHomePosition()
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "MoveHomePosition送信方向不正" );

            Enqueue( new MoveHomePacket( _SOLASApp.ApplicationId ) );
        }

        public void HomePosition(int Pan,int Tilt,int Zoom,int ZoomRatio)
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, "HomePosition送信方向不正" );

            Enqueue( new HomePositionPacket( _SOLASApp.ApplicationId, Pan, Tilt, Zoom, ZoomRatio ) );
        }

        public void ErrorMessage( string Message )
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, "ErrorMessage送信方向不正" );

            Enqueue( new ErrorMsgPacket( _SOLASApp.ApplicationId, Message ) );
        }

        public void Restart()
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "Restart送信方向不正" );

            Enqueue( new RestartPacket( _SOLASApp.ApplicationId ) );
        }

        public void DetectorStop()
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "Restart送信方向不正" );

            Enqueue( new StopPacket( _SOLASApp.ApplicationId ) );
        }

        public void ApplicationStatus( APP_STATUS Status )
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, "ApplicationStatus送信方向不正" );

            Enqueue( new AppStatusPacket( _SOLASApp.ApplicationId, Status ) );
        }

        public void HeartBeat()
        {
            Assert( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, "HeartBeat送信方向不正" );

            if ( ( RemoteStatus == APP_STATUS.APP_STS_RUNNING ) ||
                ( RemoteStatus == APP_STATUS.APP_STS_RUNNING_NO_DETECT ) ||
                ( RemoteStatus == APP_STATUS.APP_STS_UNKNOWN ) )
            {
                Enqueue( new HeartBeatPacket( _SOLASApp.ApplicationId ) );
            }
        }

        protected override bool ItemRoutine( IPacket Item )
        {
            using ( UdpClient UdpSocket = new UdpClient() )
            {
                byte[] Payload = Item.CreatePacketBytes();

                try
                {
                    UdpSocket.Send( Payload, Payload.Length, _RemoteEndPoint );
                    _SOLASApp?.Info( $"Send ({_RemoteEndPoint}) [{Item.ToString()}] " );
                }
                catch ( Exception e )
                {
                    Assert( false, $"({_RemoteEndPoint})へのパケット送信失敗({e.Message})" );
                }
            }

            return true;
        }
    }
}
