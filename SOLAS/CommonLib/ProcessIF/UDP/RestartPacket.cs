﻿using System;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class RestartPacket : Packet
    {
        public RestartPacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_APP_RESTART, $"トリガ区分不一致({Trigger})" );
        }

        public RestartPacket( int ProcessId ) : this( ProcessId, DateTime.Now )
        {
        }

        public RestartPacket( int ProcessId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_APP_RESTART, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
        }

        protected override byte[] CreateParameterBytes()
        {
            return new byte[ 0 ];
        }
    }
}
