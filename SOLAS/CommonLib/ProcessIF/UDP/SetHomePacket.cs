﻿using System;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class SetHomePacket : Packet
    {
        public SetHomePacket( byte[] Payload ) : base( Payload )
        {
            Assert( Trigger == TRIGGER_TYPE.TRG_SET_HOME, $"トリガ区分不一致({Trigger})" );
        }

        public SetHomePacket( int ProcessId ) : base( TRIGGER_TYPE.TRG_SET_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId )
        {
        }

        public SetHomePacket( int ProcessId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_SET_HOME, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
        }

        protected override byte[] CreateParameterBytes()
        {
            return new byte[ 0 ];
        }
    }
}
