﻿using System;

using static System.Diagnostics.Debug;
using static System.Net.IPAddress;

namespace CES.SOLAS.Lib.ProcessIF.UDP
{
    public sealed class StopPacket : Packet
    {
        public StopPacket( byte[] Payload ) : base( Payload )
        {
        }

        public StopPacket( int ProcessId ) : this( ProcessId, DateTime.Now )
        {
        }

        public StopPacket( int ProcessId, DateTime Timestamp ) : base( TRIGGER_TYPE.TRG_APP_STOP, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, ProcessId, Timestamp )
        {
        }

        protected override byte[] CreateParameterBytes()
        {
            return new byte[ 0 ];
        }
    }
}
