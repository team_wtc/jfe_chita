﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    using Lib.Utility;


    public abstract class DataStoreThread<TKey, TValue> : IntervalThread
    {
        const int DEFAILT_DATA_STORE_INTERVAL = 50;

        protected readonly Dictionary<TKey, TValue> _DataStore = new Dictionary<TKey, TValue>();
        protected readonly Dictionary<TKey, TValue> _DeleteStore = new Dictionary<TKey, TValue>();
        protected readonly object _DataStoreLock = new object();


        public DataStoreThread( UI.ISOLASApp SOLASApp, string ThreadName )
            : base( SOLASApp, DEFAILT_DATA_STORE_INTERVAL, ThreadName )
        {
        }

        public virtual void AddItem( TKey KeyItem, TValue ValueItem )
        {
            lock ( _DataStoreLock )
            {
                _DataStore.Add( KeyItem, ValueItem );
            }
        }

        protected abstract bool IsMove( TKey KeyItem );
        protected abstract bool IsDelete( TKey KeyItem );

        protected abstract void DisposeItem( TValue ValueItem );

        protected override void Terminate()
        {
            lock ( _DataStoreLock )
            {
                foreach ( var Key in _DeleteStore.Keys )
                {
                    DisposeItem( _DeleteStore[ Key ] );
                }
                foreach ( var Key in _DataStore.Keys )
                {
                    DisposeItem( _DataStore[ Key ] );
                }
            }
        }

        protected override bool IntervalJob()
        {
            MoveItem();
            DeleteItem();
            return true;
        }

        protected void MoveItem()
        {
            bool MoveItemFlag = false;

            do
            {
                MoveItemFlag = false;

                lock ( _DataStoreLock )
                {
                    foreach ( var Key in _DataStore.Keys )
                    {
                        if ( IsMove( Key ) == true )
                        {
                            _DeleteStore.Add( Key, _DataStore[ Key ] );
                            _DataStore.Remove( Key );
                            MoveItemFlag = true;
                            break;
                        }
                    }
                }
            } while ( MoveItemFlag == true );
        }

        protected void DeleteItem()
        {
            bool DeleteItemFlag = false;

            do
            {
                DeleteItemFlag = false;
                foreach ( var Key in _DeleteStore.Keys )
                {
                    if ( IsDelete( Key ) == true )
                    {
                        DisposeItem( _DeleteStore[ Key ] );
                        _DeleteStore.Remove( Key );
                        DeleteItemFlag = true;
                        break;
                    }
                }
            } while ( DeleteItemFlag == true );
        }

        protected virtual TValue[] ItemArray()
        {
            List<TValue> Items = new List<TValue>();

            lock ( _DataStoreLock )
            {
                foreach ( var Key in _DataStore.Keys )
                {
                    Items.Add( _DataStore[ Key ] );
                }
            }

            return Items.ToArray();
        }
    }
}
