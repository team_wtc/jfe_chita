﻿using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    public abstract class EventThread : ThreadBase
    {
        protected readonly int _EventTimeOut;
        protected readonly AutoResetEvent _AutoResetEvent;

        public EventThread( UI.ISOLASApp SOLASApp, string Name = "", int EventTimeOut = Timeout.Infinite, ThreadPriority Priority=ThreadPriority.Normal ):base( SOLASApp, Name , Priority )
        {
            _EventTimeOut = EventTimeOut;
            _AutoResetEvent = new AutoResetEvent( false );
        }

        protected virtual void EventTimeOut()
        {
        }

        public void DoEvent()
        {
            _AutoResetEvent.Set();
        }

        protected abstract bool EventRoutine();

        protected override void Routine()
        {
            while ( true )
            {
                if ( _AutoResetEvent.WaitOne( _EventTimeOut ) == false )
                {
                    EventTimeOut();
                    continue;
                }

                if ( EventRoutine() == false )
                {
                    _AutoResetEvent.Dispose();
                    break;
                }
            }
        }
    }
}
