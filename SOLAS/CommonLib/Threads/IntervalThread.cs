﻿using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    public abstract class IntervalThread : ThreadBase
    {
        protected const int DEFAULT_INTERVAL = 100;

        protected readonly int _Interval;

        public IntervalThread( UI.ISOLASApp SOLASApp, int Interval = DEFAULT_INTERVAL, string Name = "", ThreadPriority Priority = ThreadPriority.Normal ) : base( SOLASApp, Name, Priority )
        {
            _Interval = Interval;
        }

        protected abstract bool IntervalJob();

        protected override void Routine()
        {
            do
            {
                Thread.Sleep( _Interval );
            } while ( IntervalJob() == true );
        }
    }
}
