﻿using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    public abstract class OneshotThread : ThreadBase
    {
        public OneshotThread( UI.ISOLASApp SOLASApp, ThreadPriority Priority ) : base( SOLASApp, Priority )
        {
        }

        public OneshotThread( UI.ISOLASApp SOLASApp, string ThreadName, ThreadPriority Priority ) : base( SOLASApp, ThreadName, Priority )
        {
        }

        protected abstract void OneshotRoutine();

        protected override void Routine()
        {
            OneshotRoutine();
        }
    }
}
