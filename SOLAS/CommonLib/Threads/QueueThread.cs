﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    using static System.Threading.Thread;
#if DEBUG
    using static System.Diagnostics.Debug;
#endif
    using Lib.Utility;

    public abstract class QueueThread<T> : ThreadBase
    {
        protected readonly Queue<T> _Queue = null;
        protected readonly object _QueueLock = new object();

        protected readonly Utility.Average _Average;
        protected bool _ForceClear;
        protected int _ForceClearCount;

        public QueueThread( UI.ISOLASApp SOLASApp, ThreadPriority Priority, bool Clear, int Count, Average Average = null ) : base( SOLASApp, Priority )
        {
            _Average = Average;
            _Queue = new Queue<T>();
            _ForceClear = Clear;
            _ForceClearCount = Count;
        }

        public QueueThread( UI.ISOLASApp SOLASApp, string ThreadName, ThreadPriority Priority , bool Clear, int Count, Average Average = null ) : base( SOLASApp, ThreadName, Priority )
        {
            _Average = Average;
            _Queue = new Queue<T>();
            _ForceClear = Clear;
            _ForceClearCount = Count;
        }

        public QueueThread( UI.ISOLASApp SOLASApp, string ThreadName, ThreadPriority Priority, IEnumerable<T> DataList, bool Clear, int Count, Average Average = null ) : base( SOLASApp, ThreadName, Priority )
        {
            _Average = Average;
            _Queue = new Queue<T>( DataList );
            _ForceClear = Clear;
            _ForceClearCount = Count;
        }

        public virtual void Enqueue( T Item )
        {
            lock ( _QueueLock )
            {
                if ( ( _ForceClear == true ) && ( _Queue.Count >= _ForceClearCount ) )
                {
                    _SOLASApp.Warn( $"$$$ Queue Clear. {_Queue.Count} items" );
                    while ( _Queue.Count > 0 )
                    {
                        ItemDispose( _Queue.Dequeue() );
                    }
                }

                _Queue.Enqueue( Item );
            }
        }

        protected virtual void ItemDispose( T Item )
        {
            if ( Item is IDisposable DisposableObject )
            {
                try
                {
                    DisposableObject.Dispose();
                }
                catch
                {
                }
            }
        }

        protected virtual T Dequeue()
        {
            while ( true )
            {
                lock ( _QueueLock )
                {
                    if ( _Queue.Count > 0 )
                    {
                        return _Queue.Dequeue();
                    }
                }

                Sleep( 10 );
            }
        }

        protected abstract bool ItemRoutine( T Item );

        protected override void Routine()
        {
            while ( ItemRoutine( Dequeue() ) == true )
                ;
        }
    }
}
