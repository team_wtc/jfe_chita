﻿using System.Threading;

namespace CES.SOLAS.Lib.Threads
{
    using Lib.UI;

    /// <summary>
    /// 全てのスレッドの規定となるクラス
    /// </summary>
    public abstract class ThreadBase
    {
        /// <summary>
        /// スレッドオブジェクト
        /// </summary>
        protected readonly Thread _Thread;
        protected readonly ISOLASApp _SOLASApp;
        /// <summary>
        /// インスタンス初期化
        /// </summary>
        /// <param name="ThreadName">スレッドの名称</param>
        /// <param name="Priority">スレッドの優先度</param>
        public ThreadBase( ISOLASApp SOLASApp, string ThreadName, ThreadPriority Priority )
        {
            _SOLASApp = SOLASApp;

            _Thread = new Thread( _Core )
            {
                Priority = Priority,
                Name = ( string.IsNullOrWhiteSpace( ThreadName ) == false ) ? ThreadName : string.Empty,
                IsBackground = true
            };
        }

        public ThreadBase( ISOLASApp SOLASApp, string ThreadName ) : this( SOLASApp, ThreadName, ThreadPriority.Normal )
        {
        }

        public ThreadBase( ISOLASApp SOLASApp, ThreadPriority Priority ) : this( SOLASApp, null, Priority )
        {
        }

        public ThreadBase( ISOLASApp SOLASApp ) : this( SOLASApp, null, ThreadPriority.Normal )
        {
        }

        public virtual void Start()
        {
            _Thread.Start();
        }

        public virtual void Stop()
        {
            if ( _Thread.IsAlive == true )
            {
                _Thread.Abort();
            }
            _Thread.Join();
        }

        public virtual bool Wait( int MilliSecond )
        {
            return _Thread.Join( MilliSecond );
        }

        public bool IsAlive => _Thread.IsAlive;

        public virtual void Wait()
        {
            _Thread.Join();
        }

        protected virtual bool Init()
        {
            return true;
        }

        protected virtual void Terminate()
        {
        }

        protected abstract void Routine();

        void _Core()
        {
            try
            {
                if ( Init() == false )
                {
                    _SOLASApp?.Debug( "Thread Initialize Failure." );
                    return;
                }
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return;
            }
            catch
            {
                return;
            }

            try
            {
                //_SOLASApp?.Debug( "Start up." );
                Routine();
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
            }
            catch
            {

            }
            finally
            {
                Terminate();
                //_SOLASApp?.Debug( "Terminate." );
            }
        }
    }
}
