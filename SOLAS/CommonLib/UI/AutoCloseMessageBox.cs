﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.SOLAS.Lib.UI
{
    using ThreadTimer = System.Threading.Timer;

    public class AutoCloseMessageBox
    {
        const int DEFAULT_SHOW_TIMEOUT = 10000;     // 表示時間10秒（10000ミリ秒）
        const int TEXT_MAX_LENGTH = byte.MaxValue; //最大の文字列長の指定
        const int WM_CLOSE = 0x0010;

        static uint MessageId = 0;

        readonly ThreadTimer _timeoutTimer;
        readonly string _caption;
        readonly string _Message;

        public AutoCloseMessageBox( string text, string caption,
            int timeout = DEFAULT_SHOW_TIMEOUT,
            MessageBoxButtons Buttons = MessageBoxButtons.OK,
            MessageBoxIcon Icon = MessageBoxIcon.Information,
            MessageBoxDefaultButton DefaultButton = MessageBoxDefaultButton.Button1 )
        {
            if ( text.Length > TEXT_MAX_LENGTH ) //内容の長さを制限する追加部分
            {
                _Message = text.Substring( 0, TEXT_MAX_LENGTH );
            }
            else
            {
                _Message = text;
            }

            unchecked
            {
                ++MessageId;
            }
            _caption =$"#{MessageId} {caption}";
            _timeoutTimer = new ThreadTimer( OnTimerElapsed, null, timeout, Timeout.Infinite );

            Task.Factory.StartNew( () =>
            {
                MessageBox.Show( text, _caption, Buttons, Icon, DefaultButton );
            } );
        }

        public static void Show( string text, string caption,
            int timeout = DEFAULT_SHOW_TIMEOUT,
            MessageBoxButtons Buttons = MessageBoxButtons.OK,
            MessageBoxIcon Icon = MessageBoxIcon.Information,
            MessageBoxDefaultButton DefaultButton = MessageBoxDefaultButton.Button1 )
        {
            new AutoCloseMessageBox( text, caption, timeout, Buttons, Icon, DefaultButton );
        }

        void OnTimerElapsed( object state )
        {
            IntPtr mbWnd = FindWindow( null, _caption );
            if ( mbWnd != IntPtr.Zero )
            {
                SendMessage( mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero );
            }
            _timeoutTimer.Dispose();
        }

        [System.Runtime.InteropServices.DllImport( "user32.dll", SetLastError = true )]
        static extern IntPtr FindWindow( string lpClassName, string lpWindowName );

        [System.Runtime.InteropServices.DllImport( "user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto )]
        static extern IntPtr SendMessage( IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam );
    }
}
