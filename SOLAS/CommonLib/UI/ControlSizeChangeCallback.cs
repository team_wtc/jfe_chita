﻿using System;
using System.Windows.Forms;

namespace CES.SOLAS.Lib.UI
{
    public class ControlSizeChangeCallback
    {
        const int DEFAULT_MARGIN = 3;
        public SizeChangeCallback Callback;

        readonly Control _Control;
        readonly int _Margin;

        int? _LastGetImageWidth = null;
        int? _LastGetImageHeight = null;

        public ControlSizeChangeCallback( Control Control, int Margin = DEFAULT_MARGIN )
        {
            _Margin = Margin;
            Callback = SizeChange;
            _Control = Control;
            _Control.Parent.Resize += this.Control_Resize;
        }

        private void Control_Resize( object sender, EventArgs e )
        {
            CalcControlSize();
        }

        private void SizeChange( int Width, int Height )
        {
            _LastGetImageWidth = Width;
            _LastGetImageHeight = Height;

            _Control.Invoke( ( Action )CalcControlSize );
        }

        private void CalcControlSize()
        {
            if ( ( _LastGetImageWidth.HasValue == false ) || ( _LastGetImageHeight.HasValue == false ) )
            {
                return;
            }

            float FormClientWidth = ( float )( _Control.Parent.ClientSize.Width - _Margin );
            float FormClientHeight = ( float )( _Control.Parent.ClientSize.Height - _Margin );

            float ImageWidth = ( float )_LastGetImageWidth.Value;
            float ImageHeight = ( float )_LastGetImageHeight.Value;

            float WidthRatio = ImageWidth / FormClientWidth;
            float HeightRatio = ImageHeight / FormClientHeight;

            int NewWidth = _Control.Parent.ClientSize.Width;
            int NewHeight = _Control.Parent.ClientSize.Height;

            if ( ( WidthRatio >= 1.0F ) && ( HeightRatio >= 1.0F ) )
            {
                //  幅高さ両方とも画像が大きい場合、比率の大きい方で割る
                float Ratio = ( WidthRatio >= HeightRatio ) ? WidthRatio : HeightRatio;
                NewWidth = ( int )( ImageWidth / Ratio );
                NewHeight = ( int )( ImageHeight / Ratio );
            }
            else if ( ( WidthRatio < 1.0F ) && ( HeightRatio < 1.0F ) )
            {
                //  幅高さ両方とも画像が小さい場合、比率を計算しなおして小さい方をかける
                WidthRatio = FormClientWidth / ImageWidth;
                HeightRatio = FormClientHeight / ImageHeight;
                float Ratio = ( WidthRatio >= HeightRatio ) ? HeightRatio : WidthRatio;
                NewWidth = ( int )( ImageWidth * Ratio );
                NewHeight = ( int )( ImageHeight * Ratio );
            }
            else if ( ( WidthRatio >= 1.0F ) && ( HeightRatio < 1.0F ) )
            {
                //  画像の幅が大きいが、高さが小さい場合、幅の比率で割る
                NewWidth = ( int )( ImageWidth / WidthRatio );
                NewHeight = ( int )( ImageHeight / WidthRatio );
            }
            else if ( ( WidthRatio < 1.0F ) && ( HeightRatio >= 1.0F ) )
            {
                //  画像の高さが大きいが、幅が小さい場合、高さの比率で割る
                NewWidth = ( int )( ImageWidth / HeightRatio );
                NewHeight = ( int )( ImageHeight / HeightRatio );
            }

            //  フォームのクライアント領域のサイズからパネルの開始位置を算出する
            int NewLeft = ( ( ( _Control.Parent.ClientSize.Width - ( _Margin * 2 ) ) - NewWidth ) / 2 ) + _Margin;
            int NewTop = ( ( ( _Control.Parent.ClientSize.Height - ( _Margin * 2 ) ) - NewHeight ) / 2 ) + _Margin;

            _Control.Location = new System.Drawing.Point( NewLeft, NewTop );
            _Control.Size = new System.Drawing.Size( NewWidth, NewHeight );
        }
    }
}
