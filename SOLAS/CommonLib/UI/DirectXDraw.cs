﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Runtime.ExceptionServices;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;

using GDIBitmap = System.Drawing.Bitmap;
using GDIRectangle = System.Drawing.Rectangle;
using GDIPixelFormat = System.Drawing.Imaging.PixelFormat;

using SDMath = SharpDX.Mathematics.Interop;

using D2DFactory = SharpDX.Direct2D1.Factory;
using D2DAlphaMode = SharpDX.Direct2D1.AlphaMode;
using D2DPixelFormat = SharpDX.Direct2D1.PixelFormat;
using D2DBitmap = SharpDX.Direct2D1.Bitmap;

namespace CES.SOLAS.Lib
{
    public delegate void SizeChangeCallback( int Width, int Height );
}

namespace CES.SOLAS.Lib.UI
{
    public class DirectXDraw
    {
        readonly D2DFactory _Factory = new D2DFactory();
        readonly WindowRenderTarget _WindowRenderTarget;
        readonly RenderTargetProperties _RenderTargetProperties;

        readonly SizeChangeCallback _SizeChangeCallback;
        readonly ControlSizeChangeCallback _ControlSizeChangeCallback = null;
        readonly object _Lock = new object();

        int? _LastSendWidth = null;
        int? _LastSendHeight = null;

        D2DBitmap _Image = null;

        public DirectXDraw( IntPtr Handle, ControlSizeChangeCallback ControlSizeChangeCallback )
            : this( Handle, ControlSizeChangeCallback.Callback )
        {
            _ControlSizeChangeCallback = ControlSizeChangeCallback;
        }

        public DirectXDraw( IntPtr Handle, SizeChangeCallback Callback )
        {
            _SizeChangeCallback = Callback;

            var hrtProps = new HwndRenderTargetProperties()
            {
                Hwnd = Handle,
                PixelSize = new Size2( 100, 100 ),
                PresentOptions = PresentOptions.None
            };

            _RenderTargetProperties = new RenderTargetProperties(
                RenderTargetType.Software,
                new D2DPixelFormat( Format.Unknown, D2DAlphaMode.Unknown ),
                0,
                0,
                RenderTargetUsage.None,
                FeatureLevel.Level_DEFAULT );

            _WindowRenderTarget = new WindowRenderTarget( _Factory, _RenderTargetProperties, hrtProps )
            {
                AntialiasMode = AntialiasMode.PerPrimitive,
                TextAntialiasMode = TextAntialiasMode.Cleartype
            };

            Clear();
        }

        public void FromGDIBitmap( GDIBitmap FrameImage )
        {
            bool LockState = Monitor.TryEnter( _Lock, 1000 );
            try
            {
                if ( LockState == true )
                {
                    //lock ( _Lock )
                    //{
                    _Image?.Dispose();
                    _Image = null;

                    var BitmapData = FrameImage.LockBits(
                            new GDIRectangle( 0, 0, FrameImage.Width, FrameImage.Height ),
                            ImageLockMode.ReadOnly,
                            GDIPixelFormat.Format32bppArgb );

                    using ( var SDDataStream = new DataStream( BitmapData.Scan0, BitmapData.Stride * BitmapData.Height, true, false ) )
                    {
                        var SDD2DPixelFormat = new D2DPixelFormat( Format.B8G8R8A8_UNorm, D2DAlphaMode.Premultiplied );
                        var BmpProperties = new BitmapProperties( SDD2DPixelFormat );

                        _Image = new D2DBitmap(
                                _WindowRenderTarget,
                                new Size2( FrameImage.Width, FrameImage.Height ),
                                SDDataStream,
                                BitmapData.Stride,
                                BmpProperties );

                        FrameImage.UnlockBits( BitmapData );
                    }

                    if ( ( _Image != null ) && ( _SizeChangeCallback != null ) )
                    {
                        if ( ( _LastSendWidth.HasValue == false ) || ( _LastSendWidth.Value != _Image.PixelSize.Width ) ||
                            ( _LastSendHeight.HasValue == false ) || ( _LastSendHeight.Value != _Image.PixelSize.Height ) )
                        {
                            _LastSendWidth = _Image.PixelSize.Width;
                            _LastSendHeight = _Image.PixelSize.Height;
                            _SizeChangeCallback( _LastSendWidth.Value, _LastSendHeight.Value );
                        }
                    }
                    //}
                }
            }
            catch
            {

            }
            finally
            {
                if ( LockState == true )
                {
                    Monitor.Exit( _Lock );
                }
            }
        }
        public void FromMat( Mat FrameMat )
        {
            if ( ( FrameMat != null ) && ( FrameMat.IsDisposed == false ) )
            {
                using ( var FrameImage = BitmapConverter.ToBitmap( FrameMat ) )
                {
                    FromGDIBitmap( FrameImage );
                }
            }
            else
            {
                Clear();
                //using ( var BrankMat = new Mat( rows: 1080, cols: 1920, type: MatType.CV_8UC3, s: Scalar.Black ) )
                //using ( var FrameImage = BitmapConverter.ToBitmap( BrankMat ) )
                //{
                //    FromGDIBitmap( FrameImage );
                //}
            }
        }

        public void FromByteArray( byte[] FrameBytes )
        {
            if ( ( FrameBytes != null ) && ( FrameBytes.Length > 0 ) )
            {
                using ( var ImageStream = new MemoryStream( FrameBytes ) )
                using ( var FrameImage = GDIBitmap.FromStream( ImageStream ) )
                {
                    FromGDIBitmap( FrameImage as GDIBitmap );
                }
            }
            else
            {
                Clear();
                //using ( var BrankMat = new Mat( rows: 1080, cols: 1920, type: MatType.CV_8UC3, s: Scalar.Black ) )
                //using ( var FrameImage = BitmapConverter.ToBitmap( BrankMat ) )
                //{
                //    FromGDIBitmap( FrameImage );
                //}
            }
        }

        private void Clear()
        {
            using ( var BrankMat = new Mat( rows: 1080, cols: 1920, type: MatType.CV_8UC3, s: Scalar.Black ) )
            using ( var FrameImage = BitmapConverter.ToBitmap( BrankMat ) )
            {
                FromGDIBitmap( FrameImage );
            }
        }

        [HandleProcessCorruptedStateExceptions]
        public void Draw()
        {
            bool LockState = Monitor.TryEnter( _Lock, 1000 );
            try
            {
                if ( LockState == true )
                {
                    //lock ( _Lock )
                    //{
                    if ( _Image is D2DBitmap Bmp )
                    {
                        if ( ( _WindowRenderTarget.PixelSize.Width != Bmp.PixelSize.Width ) ||
                        ( _WindowRenderTarget.PixelSize.Height != Bmp.PixelSize.Height ) )
                        {
                            _WindowRenderTarget.Resize( new Size2( Bmp.PixelSize.Width, Bmp.PixelSize.Height ) );
                        }

                        _WindowRenderTarget.BeginDraw();

                        _WindowRenderTarget.DrawBitmap(
                            Bmp,
                            new SDMath.RawRectangleF( 0F, 0F, ( float )Bmp.PixelSize.Width, ( float )Bmp.PixelSize.Height ),
                            1.0f,
                            BitmapInterpolationMode.NearestNeighbor );

                        _WindowRenderTarget.EndDraw();
                    }
                    //}
                }
            }
            catch
            {
            }
            finally
            {
                if ( LockState == true )
                {
                    Monitor.Exit( _Lock );
                }
            }
        }
    }
}
