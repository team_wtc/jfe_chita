﻿using System;
using System.Threading;

namespace CES.SOLAS.Lib.UI
{
    public class DirectXImageDrawer : Threads.EventThread
    {
        const int DRAW_INTERVAL = 1000 / 20;
        readonly DirectXDraw _DirectXDraw;

        public DirectXImageDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, ControlSizeChangeCallback ControlSizeChangeCallback )
            : base( SOLASApp, ThreadName, DRAW_INTERVAL, ThreadPriority.Normal )
        {
            _DirectXDraw = new DirectXDraw( Handle, ControlSizeChangeCallback );
        }

        public DirectXImageDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, SizeChangeCallback Callback )
            : base( SOLASApp, ThreadName, DRAW_INTERVAL, ThreadPriority.Normal )
        {
            _DirectXDraw = new DirectXDraw( Handle, Callback );
        }

        protected override void EventTimeOut()
        {
            _DirectXDraw.Draw();
        }

        protected override bool EventRoutine()
        {
            _DirectXDraw.Draw();
            return true;
        }

        public void NewImage( byte[] ImageBytes )
        {
            _DirectXDraw.FromByteArray( ImageBytes );
            DoEvent();
        }
    }
}
