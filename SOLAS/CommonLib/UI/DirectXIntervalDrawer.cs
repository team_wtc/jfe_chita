﻿using System;
using System.Threading;
using System.Drawing;

using OpenCvSharp;

namespace CES.SOLAS.Lib.UI
{
    public class DirectXIntervalDrawer : Threads.IntervalThread
    {
        const int DRAW_INTERVAL = 50;
        readonly DirectXDraw _DirectXDraw;

        public DirectXIntervalDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, ControlSizeChangeCallback ControlSizeChangeCallback )
            : base( SOLASApp, DRAW_INTERVAL, ThreadName, ThreadPriority.Normal )
        {
            _DirectXDraw = new DirectXDraw( Handle, ControlSizeChangeCallback );
        }

        public DirectXIntervalDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, SizeChangeCallback Callback )
            : base( SOLASApp, DRAW_INTERVAL, ThreadName, ThreadPriority.Normal )
        {
            _DirectXDraw = new DirectXDraw( Handle, Callback );
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "DirectXIntervalDrawer Startup." );
            return base.Init();
        }

        protected override void Terminate()
        {
            base.Terminate();
            _SOLASApp.Info( "DirectXIntervalDrawer Terminate." );
        }

        public void NewImage( byte[] ImageBytes )
        {
            _DirectXDraw.FromByteArray( ImageBytes );
        }

        public void NewImage( Mat ImageMatrix )
        {
            _DirectXDraw.FromMat( ImageMatrix );
        }

        public void NewImage( Bitmap Img )
        {
            _DirectXDraw.FromGDIBitmap( Img );
        }

        protected override bool IntervalJob()
        {
            _DirectXDraw.Draw();
            return true;
        }
    }
}
