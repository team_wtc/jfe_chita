﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Drawing;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using static System.Diagnostics.Debug;

    public class DirectXStreamDrawer : Threads.QueueThread<Mat>
    {
        readonly DirectXDraw _DirectXDraw;
        readonly Stopwatch _Stopwatch;

        public DirectXStreamDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, ControlSizeChangeCallback ControlSizeChangeCallback, Utility.Average Average = null )
            : base( SOLASApp, ThreadName, ThreadPriority.Normal,  false, 0, Average )
        {
            _DirectXDraw = new DirectXDraw( Handle, ControlSizeChangeCallback );
            _Stopwatch = Stopwatch.StartNew();
        }

        public DirectXStreamDrawer( UI.ISOLASApp SOLASApp, string ThreadName, IntPtr Handle, SizeChangeCallback Callback, Utility.Average Average = null )
            : base( SOLASApp, ThreadName, ThreadPriority.Normal, false, 0, Average )
        {
            _DirectXDraw = new DirectXDraw( Handle, Callback );
            _Stopwatch = Stopwatch.StartNew();
        }

        public void Redraw()
        {
            _DirectXDraw.Draw();
        }

        public void Enqueue( Bitmap LiveImage )
        {
            if ( LiveImage == null )
            {
                Enqueue( ( Mat )null );
            }
            else
            {
                Enqueue( BitmapConverter.ToMat( LiveImage ) );
            }
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "DirectXStreamDrawer Startup." );
            return base.Init();
        }

        protected override void Terminate()
        {
            base.Terminate();
            _SOLASApp.Info( "DirectXStreamDrawer Terminate." );
        }
        protected override bool ItemRoutine( Mat Item )
        {
            try
            {
                _Stopwatch.Restart();

                _DirectXDraw.FromMat( Item );
                _DirectXDraw.Draw();

                Item?.Dispose();

                _Stopwatch.Stop();
                _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
                return true;
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "Draw New Image error.", e );
                return false;
            }
        }
    }
}
