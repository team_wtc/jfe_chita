﻿using System.Threading;
using System.Windows.Forms;

namespace CES.SOLAS.Lib.UI
{
    public abstract class FormThread<T> : Threads.OneshotThread where T : Form, new()
    {
        protected T _Instance = null;

        public FormThread( ISOLASApp SOLASApp ) : this( SOLASApp, "Form" )
        {
        }

        public FormThread( ISOLASApp SOLASApp, string ThreadName ) : base( SOLASApp, ThreadName, ThreadPriority.Normal )
        {
            _Thread.SetApartmentState( ApartmentState.STA );
        }

        public virtual void Start( params object[] args )
        {
            base.Start();
        }

        protected abstract bool FormInitialyze();

        protected override bool Init()
        {
            _Instance = new T();

            return FormInitialyze();
        }

        protected override void OneshotRoutine()
        {
            Application.Run( _Instance );
        }
    }
}
