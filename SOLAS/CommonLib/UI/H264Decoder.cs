﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using RtspClientSharp.RawFrames;
using RtspClientSharp.RawFrames.Video;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using OpenH264Lib;

namespace CES.SOLAS.Lib.UI
{
    using Threads;

    public class H264Decoder : QueueThread<byte[]>
    {
        readonly Decoder _Decoder = new Decoder( "openh264-1.8.0-win64.dll" );
        readonly QueueThread<Mat> _AIDetector;

        long MaxTime = 0L;
        uint FrameCount = 0;

        public H264Decoder(ISOLASApp SOLASApp, QueueThread<Mat> AIDetector ) :base( SOLASApp,"Decoder",ThreadPriority.Highest, false, 0 )
        {
            _AIDetector = AIDetector;
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "H.264 Decoder Startup." );
            return base.Init();
        }

        protected override void Terminate()
        {
            _SOLASApp.Info( "H.264 Decoder Terminate." );
        }
        protected override bool ItemRoutine( byte[] Item )
        {
            Stopwatch _Stopwatch = Stopwatch.StartNew();
            unchecked
            {
                FrameCount++;
            }
            try
            {
                if ( ( Item == null ) || ( Item.Length <= 0 ) )
                {
                    _AIDetector.Enqueue( null );
                }
                else
                {
                    var Img = _Decoder.Decode( Item, Item.Length );
                    if ( Img != null )
                    {
                        _AIDetector.Enqueue( BitmapConverter.ToMat( Img ) );
                        _SOLASApp.Info( $"{_Thread.Name}(#{FrameCount:D8}) Decode Image = {Img.Size}" );
                        Img.Dispose();
                    }
                    else
                    {
                        _AIDetector.Enqueue( null );
                        _SOLASApp.Info( $"{_Thread.Name}(#{FrameCount:D8}) Decode Image = null {Item.Length}" );
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch(Exception e )
            {
                _SOLASApp.Exception( "Decode Error.", e );
            }
            finally
            {
                _Stopwatch.Stop();
                if ( MaxTime < _Stopwatch.ElapsedMilliseconds )
                {
                    MaxTime = _Stopwatch.ElapsedMilliseconds;
                }
                _SOLASApp.Info( $"{_Thread.Name}(#{FrameCount:D8}) Max Decode Time = {MaxTime}ms" );
            }

            return true;
        }
    }
}
