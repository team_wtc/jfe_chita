﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Drawing.Imaging;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using Threads;
    using ProcessIF.SHM;

    public class H264Encoder : QueueThread<(int AlarmId, DateTime Timestamp, Mat Img)>
    {
        const string ENCODE_PROC_NAME = "EncodeApp.exe";

        readonly int _Width;
        readonly int _Height;
        readonly int _BitRate;
        readonly int _FrameRate;
        readonly int _KeyFrameInterval;
        SharedMemory _SharedMemory;

        Stopwatch _Stopwatch;
        Process _Process;
        bool _Terminate = false;

        public H264Encoder( ISOLASApp SOLASApp, int Width, int Height, int BitRate, int FrameRate, int KeyFrameInterval ) : base( SOLASApp, "Encoder", ThreadPriority.Highest, true, FrameRate * 30 )
        {
            _Width = Width;
            _Height = Height;
            _BitRate = BitRate;
            _FrameRate = FrameRate;
            _KeyFrameInterval = KeyFrameInterval;
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "H.264 Encoder Startup." );
            try
            {
                _SharedMemory = new SharedMemory( _SOLASApp.Logger, true );
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "SharedMemory", e );
                return false;
            }
            _Stopwatch = Stopwatch.StartNew();
            return StartEncodingProcess();
        }

        private bool StartEncodingProcess()
        {
            try
            {
                OldEncodingProcessKill();

                var StartInfo = new ProcessStartInfo( ENCODE_PROC_NAME, $"{_Width} {_Height} {_BitRate} {_FrameRate} {_KeyFrameInterval}" )
                {
                    CreateNoWindow = true,
                    ErrorDialog = true,
                    LoadUserProfile = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                };

                _Process = Process.Start( StartInfo );

                _Process.EnableRaisingEvents = true;
                _Process.Exited += this._Process_Exited;
                _Process.OutputDataReceived += ( sender, e ) => { if ( e.Data != null ) { _SOLASApp.Info( e.Data ); } }; // 標準出力に書き込まれた文字列を取り出す
                _Process.BeginOutputReadLine();

                _SOLASApp.Info( $"Process({ENCODE_PROC_NAME}) 起動" );
                return true;
            }
            catch ( Exception e )
            {
                _Process = null;
                _SOLASApp.Exception( $"Process({ENCODE_PROC_NAME}) 起動エラー", e );
                return false;
            }
        }

        private void OldEncodingProcessKill()
        {
            foreach ( var Proc in Process.GetProcessesByName( ENCODE_PROC_NAME ) )
            {
                _SOLASApp.Info( $"残留プロセス({ENCODE_PROC_NAME} pid:{Proc.Id}) 強制終了" );
                Proc.EnableRaisingEvents = false;
                Proc.Kill();
                Proc.Dispose();
            }
        }

        private void _Process_Exited( object sender, EventArgs e )
        {
            _Process?.CancelOutputRead();
            _Process = null;
            _SOLASApp.Info( $"Process({ENCODE_PROC_NAME}) 終了検知" );

            if ( _Terminate == false )
            {
                Thread.Sleep( 1000 );
                _SOLASApp.Info( $"Process({ENCODE_PROC_NAME}) 再起動" );
                StartEncodingProcess();
            }
        }

        protected override void Terminate()
        {
            _Terminate = true;

            if ( ( _Process != null ) && ( _Process.HasExited == false ) )
            {
                _Process.StandardInput?.WriteLine( "stop" );
                if ( _Process.WaitForExit( 3000 ) == false )
                {
                    _Process.Kill();
                }
            }

            //while ( _Process != null )
            //{
            //    Thread.Sleep( 10 );
            //}

            _SharedMemory?.Dispose();
            _SOLASApp.Info( "H.264 Encoder Terminate." );
        }
        protected override bool ItemRoutine( (int AlarmId, DateTime Timestamp, Mat Img) Item )
        {
            _Stopwatch.Restart();

            try
            {
                using ( var Bmp = BitmapConverter.ToBitmap( Item.Img ) )
                using ( var MemStream = new MemoryStream() )
                {
                    Bmp.Save( MemStream, ImageFormat.Jpeg );
                    _SharedMemory.WriteData( Item.AlarmId, Item.Timestamp, MemStream.ToArray() );
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "H.264 Encoder Error.", e );
            }
            finally
            {
                if ( ( Item.Img != null ) && ( Item.Img.IsDisposed == false ) )
                {
                    Item.Img.Dispose();
                }
                _Stopwatch.Stop();
            }

            return true;
        }
    }
}
