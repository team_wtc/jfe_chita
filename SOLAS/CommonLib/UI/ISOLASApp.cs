﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using GDIPoint = System.Drawing.Point;

using OpenCvSharp;

namespace CES.SOLAS.Lib.UI
{
    using Lib.Logging;
    using Lib.ProcessIF.TCP;

    public interface ISOLASApp
    {
        /// <summary>
        /// 自アプリケーションの区分
        /// </summary>
        TRIGGER_PROCESS_TYPE ProcessType
        {
            get;
        }

        /// <summary>
        /// 自アプリケーションのDB登録ID
        /// </summary>
        int ApplicationId
        {
            get;
        }

        MONITORING_TYPE MonitoringType
        {
            get;
        }

        string DisplayName
        {
            get;
        }

        APP_STATUS DetectorStatus
        {
            get;
            set;
        }

        Logger Logger
        {
            get;
        }

        /// <summary>
        /// 自アプリケーションの初期化
        /// </summary>
        void AppInitialyze(
            string DBServer,
            string SAPassword,
            TriggerCallback TrgCallback,
            //FindMovieFile FindMovieFile,
            string LogFolder,
            string LogName,
            string LogExt,
            long LogSize,
            int SaveDays,
            bool DebugLog );

        /// <summary>
        /// 自アプリケーションの終了処理
        /// </summary>
        void AppFinalyze();

        void InitMonitoring( Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3, out int DetectId1, out int DetectId2, out int DetectId3 );
        void InitAlarmImageDrawer( Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3 );

        void StopMonitoring();

        void StopAlarmImageDrawer();

        string GateName( int DetectorId );

        void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait );

        void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio );

        bool EnableDetect
        {
            get;
            set;
        }

        bool ManualDetectDisable
        {
            get;
            set;
        }

        uint PersonId
        {
            get;
        }

        uint MarkerId
        {
            get;
        }

        uint CarId
        {
            get;
        }

        uint CareerPaletteId
        {
            get;
        }

        Scalar PersonColor
        {
            get;
        }
        Scalar MarkerColor
        {
            get;
        }
        Scalar CarColor
        {
            get;
        }
        Scalar CareerPaletteColor
        {
            get;
        }

        Scalar OKColor
        {
            get;
        }
        Scalar AlarmColor
        {
            get;
        }
        Scalar StagnantColor
        {
            get;
        }

        Scalar AreaColorA
        {
            get;
        }

        Scalar AreaColorB
        {
            get;
        }

        Scalar NeutralAreaColor
        {
            get;
        }

        Color DetectorBackColor( int GateId );
        Color DetectorForeColor( int GateId );


        string PersonName
        {
            get;
        }

        string MarkerName
        {
            get;
        }

        string CarName
        {
            get;
        }

        string CareerPaletteName
        {
            get;
        }

        float DetectThresholdPerson
        {
            get;
        }

        float DetectThresholdMarker
        {
            get;
        }
        float DetectThresholdCar
        {
            get;
        }
        float DetectThresholdCareerPalette
        {
            get;
        }

        bool EnableDetectAfterAlarm
        {
            get;
        }

        uint MaxPredictFrame
        {
            get;
        }
        float IOUThreshold
        {
            get;
        }

        int MessageBoxTimeout
        {
            get;
        }

        bool EnableCameraControl
        {
            get;
        }

        int PreAlarmFrameCount
        {
            get;
        }

        int PostAlarmFrameCount
        {
            get;
        }

        int StillImageShortInterval
        {
            get;
            set;
        }

        int StillImageLongInterval
        {
            get;
            set;
        }

        int TcpKeepAliveWait
        {
            get;
        }

        int TcpKeepAliveInterval
        {
            get;
        }

        float StillImageShrinkRatio
        {
            get;
        }

        float StreamingImageShrinkRatio
        {
            get;
        }

        int StreamingSkipFrame
        {
            get;
        }

        int AlarmMoviePreSeconds
        {
            get;
            set;
        }

        int AlarmMoviePostSeconds
        {
            get;
            set;
        }


        int HeartBeatCycle
        {
            get;
        }

        GDIPoint[] DetectArea
        {
            get;
        }

        GDIPoint[] DetectAreaA
        {
            get;
        }

        GDIPoint[] DetectAreaB
        {
            get;
        }

        int AreaChangeWaitFrames
        {
            get;
        }

        int CarStayAlarmWaitSeconds
        {
            get;
        }

        bool EnableCareerPaletteStayAlarm
        {
            get;
        }

        float TrackingReductionRate
        {
            get;
        }
        int AlarmSaveDays
        {
            get;
        }

        string AlarmMovieFolder
        {
            get;
        }
        void StillImageReceive( ImagePacket Packet );

        void LiveImageReceive( int DetectorId, Bitmap Image );

        void StillImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat StillImage );

        void H264EncodedImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, byte[] EncodedData );

        bool DetectorEnableCameraControl( int DetectorId );

        string AlarmName( ALARM_TYPE AlarmType );

        int InsertAlarm(  ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage );

        int DeleteOldAlarm();

        string MonitorStreamURL( int DetectorId );
        //void WriteAlarmMovie( int AlarmId, DateTime Timestamp );

        //void AddAtillImageToStore( DateTime Timestamp, Mat AlarmImage );
        void AddStockImage( DateTime Timestamp, Mat FrameImage );
        void EraceStockImage( DateTime Timestamp );

        (long Ticks, Mat FrameImage) ReadStockImage( DateTime Timestamp );

        #region ログ
        /// <summary>
        /// 情報ログ出力
        /// </summary>
        /// <param name="InfoMessage"></param>
        void Info( string InfoMessage );
        /// <summary>
        /// 警告ログ出力
        /// </summary>
        /// <param name="WarnMessage"></param>
        void Warn( string WarnMessage );
        /// <summary>
        /// エラーログ出力
        /// </summary>
        /// <param name="ErrorMessage"></param>
        void Error( string ErrorMessage );
        /// <summary>
        /// デバッグログ出力
        /// </summary>
        /// <param name="DebugMessage"></param>
        void Debug( string DebugMessage );
        /// <summary>
        /// 例外詳細ログ出力
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Exp"></param>
        void Exception( string Message, Exception Exp );
        /// <summary>
        /// ログ削除
        /// </summary>
        void LogDelete();
        #endregion


        #region UDP Detector.exe → Monitoring.exe
        /// <summary>
        /// すべてのMonitoring.exeに新規警報発生を通知する
        /// </summary>
        /// <param name="AlarmId"></param>
        /// <param name="AlarmType"></param>
        /// <param name="Timestamp"></param>
        void NewAlarm( int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp );
        /// <summary>
        /// 静止画モードののMonitoring.exeに静止画取得を通知する
        /// </summary>
        /// <param name="ImageId"></param>
        /// <param name="Timestamp"></param>
        void NewImage( int ImageId, DateTime Timestamp );
        /// <summary>
        /// すべてのMonitoring.exeにDetector.exeの発報抑制状態を通知する
        /// </summary>
        /// <param name="Suppress"></param>
        //void DetectStatus( bool Suppress );
        /// <summary>
        /// すべてのMonitoring.exeにDetector.exeのHOMEポジション位置情報を通知する。
        /// </summary>
        /// <param name="Pan"></param>
        /// <param name="Tilt"></param>
        /// <param name="Zoom"></param>
        /// <param name="ZoomRatio"></param>
        void HomePosition( int Pan, int Tilt, int Zoom, int ZoomRatio );
        /// <summary>
        /// すべてのMonitoring.exeにDetector.exeのエラー情報を送信する。
        /// </summary>
        /// <param name="Message"></param>
        void ErrorMessage( string Message );
        /// <summary>
        /// Monitoring.exeにDetector.exeの動作状態を通知する。
        /// </summary>
        /// <param name="Status"></param>
        void ApplicationStatus( int MonitoringId, APP_STATUS Status );
        #endregion

        #region UDP Monitoring.exe → Detector.exe
        /// <summary>
        /// 指定したDetector.exeに発報抑制または抑制解除を要求する。
        /// </summary>
        /// <param name="DetectorId"></param>
        /// <param name="Suppress"></param>
        void DetectSuppress( int DetectorId, bool Suppress );
        /// <summary>
        /// 指定したDetector.exeに現在位置のHOMEポジション登録を要求する。
        /// </summary>
        void SetHomePosition( int DetectorId );
        /// <summary>
        /// 指定したDetector.exeにHOMEポジションへの移動を要求する。
        /// </summary>
        /// <param name="DetectorId"></param>
        void MoveHomePosition( int DetectorId );
        /// <summary>
        /// 指定したDetector.exeにアプリケーションの再起動を要求する。
        /// </summary>
        /// <param name="DetectorId"></param>
        void Restart( int DetectorId );
        /// <summary>
        /// 全てのDetector.exeに動作状態通知を要求する。
        /// </summary>
        void HeartBeat();
        #endregion
    }
}

