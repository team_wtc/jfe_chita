﻿using System;
using System.Threading;
using System.IO;

namespace CES.SOLAS.Lib.UI
{
    using Lib.Threads;

    public class ImageCleaner : IntervalThread
    {
        const int CLEAN_NTERVAL = 30;

        DateTime _NextLogDelete = DateTime.Today;

        public ImageCleaner( ISOLASApp SOLASApp ) : base( SOLASApp, CLEAN_NTERVAL, "Cleaner" )
        {

        }
        protected override bool Init()
        {
            _SOLASApp.Info( $"Frame Image Cleaner Startup." );
            return base.Init();
        }

        protected override void Terminate()
        {
            _SOLASApp.Info( $"Frame Image Cleaner Terminate." );
            base.Terminate();
        }
        protected override bool IntervalJob()
        {
            try
            {
                if ( _NextLogDelete < DateTime.Now )
                {
                    AlarmMovieDelete();
                    _NextLogDelete = _NextLogDelete.AddDays( 1.0 );
                }

                _SOLASApp.EraceStockImage( DateTime.Now.AddSeconds( -( _SOLASApp.AlarmMoviePreSeconds * 2 ) ) );

                return true;
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return false;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "Image List Remove Error.", e );
                return true;
            }
            finally
            {
                GC.Collect();
            }
        }

        private void AlarmMovieDelete()
        {
            var Limit = DateTime.Today.AddDays( -_SOLASApp. AlarmSaveDays );
            _SOLASApp.Info( $"{Limit.ToString("yyyy/MM/dd")} 以前の警報動画ファイル削除実行" );

            var MovieFolderInfo = new DirectoryInfo( _SOLASApp.AlarmMovieFolder );
            foreach ( var MovieFile in MovieFolderInfo.GetFiles( "*.mp4", SearchOption.TopDirectoryOnly ) )
            {
                if ( MovieFile.CreationTime < Limit )
                {
                    try
                    {
                        MovieFile.Delete();
                        _SOLASApp.Info( $"警報動画ファイル[{MovieFile.FullName}]削除" );
                    }
                    catch ( Exception e )
                    {
                        _SOLASApp.Exception( $"警報動画ファイル[{MovieFile.FullName}]失敗", e );
                    }
                }
            }
        }
    }
}
