﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using Threads;
    using ProcessIF.SHM;

    public class ImageStocker : QueueThread<(int AlarmId, DateTime Timestamp, Mat Img)>
    {
        readonly int _Width;
        readonly int _Height;
        readonly int _BitRate;
        readonly int _FrameRate;
        readonly int _KeyFrameInterval;
        readonly List<MovieWriter> _MovieWriters = new List<MovieWriter>();
        readonly ImageCleaner _ImageCleaner;

        Stopwatch _Stopwatch;

        public ImageStocker( ISOLASApp SOLASApp, int Width, int Height, int BitRate, int FrameRate, int KeyFrameInterval ) : base( SOLASApp, "Encoder", ThreadPriority.Highest, true, FrameRate * 30 )
        {
            _Width = Width;
            _Height = Height;
            _BitRate = BitRate;
            _FrameRate = FrameRate;
            _KeyFrameInterval = KeyFrameInterval;
            _ImageCleaner = new ImageCleaner( SOLASApp );
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "Frame Image Stocker Startup." );
            _ImageCleaner.Start();
            _Stopwatch = Stopwatch.StartNew();
            return true;
        }

        protected override void Terminate()
        {
            _ImageCleaner.Stop();

            while ( _MovieWriters.Count > 0 )
            {
                if ( _MovieWriters[ 0 ].IsAlive == true )
                {
                    _MovieWriters[ 0 ].Stop();
                }
                else
                {
                    _MovieWriters[ 0 ].Wait();
                }

                _MovieWriters.RemoveAt( 0 );
            }
            _SOLASApp.Info( "Frame Image Stocker Terminate." );
        }
        protected override bool ItemRoutine( (int AlarmId, DateTime Timestamp, Mat Img) Item )
        {
            _Stopwatch.Restart();

            try
            {
                _SOLASApp.AddStockImage( Item.Timestamp, Item.Img );
                if ( Item.AlarmId > 0 )
                {
                    StartImageWriter( Item.AlarmId, Item.Timestamp );
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "Frame Image Stocker Error.", e );
            }
            finally
            {
                _Stopwatch.Stop();
            }

            return true;
        }

        protected void StartImageWriter( int AlarmId, DateTime Timestamp )
        {
            var Writer = new MovieWriter( _SOLASApp, AlarmId, Timestamp, _FrameRate, _Width, _Height );
            _MovieWriters.Add( Writer );
            Writer.Start();

            bool ThreadTerminated = false;
            do
            {
                ThreadTerminated = false;

                foreach ( var ExistWriter in _MovieWriters )
                {
                    if ( ExistWriter.IsAlive == false )
                    {
                        ExistWriter.Wait();
                        _MovieWriters.Remove( ExistWriter );
                        ThreadTerminated = true;
                        break;
                    }
                }
            } while ( ThreadTerminated == true );
        }
    }
}
