﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using Lib.Threads;

    public class MovieWriter : OneshotThread
    {
        public readonly DateTime PreAlarmTime;
        public readonly DateTime PostAlarmTime;

        readonly int _AlarmId;
        readonly DateTime _AlarmTimestamp;
        readonly VideoWriter _VideoWriter;
        readonly double _Fps = 0.0;
        readonly int _Width = 0;
        readonly int _Height = 0;

        string _MovieFilePath = string.Empty;
        string _TempFilePath = string.Empty;

        public MovieWriter( ISOLASApp SOLASApp, int AlarmId, DateTime AlarmTimestamp, int FrameRate, int Width, int Height )
            : base( SOLASApp, $"mov {AlarmId}", ThreadPriority.Normal )
        {
            _AlarmId = AlarmId;
            _AlarmTimestamp = AlarmTimestamp;
            PreAlarmTime = AlarmTimestamp.AddSeconds( -_SOLASApp.AlarmMoviePreSeconds );
            PostAlarmTime= AlarmTimestamp.AddSeconds( _SOLASApp.AlarmMoviePostSeconds );
            _Fps = ( double )FrameRate;
            _Width = Width;
            _Height = Height;
            _VideoWriter = new VideoWriter();
        }

        protected override bool Init()
        {
            _SOLASApp.Info(
                $"動画出力スレッド起動 ({PreAlarmTime.ToString("yyyy/MM/dd HH:mm:ss.fff")}" +
                $"～{_AlarmTimestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" +
                $"～{PostAlarmTime.ToString( "yyyy/MM/dd HH:mm:ss.fff" )})" );

            var MovieFileName = $"{_AlarmId}.mp4";

            _TempFilePath = Path.Combine( Path.GetTempPath(), MovieFileName );
            _MovieFilePath = Path.Combine( _SOLASApp.AlarmMovieFolder, MovieFileName );

            var WriteFolder = Path.GetDirectoryName( _MovieFilePath );
            if ( Directory.Exists( WriteFolder ) == false )
            {
                try
                {
                    Directory.CreateDirectory( WriteFolder );
                }
                catch ( Exception e )
                {
                    _SOLASApp.Exception( "動画フォルダー生成失敗", e );
                    return false;
                }
            }

            try
            {
                _VideoWriter.Open(
                    _TempFilePath, "avc1", _Fps, new OpenCvSharp.Size( _Width, _Height ) );
                if ( _VideoWriter.IsOpened() == false )
                {
                    _SOLASApp.Error( $"警報動画ファイル({_TempFilePath})をオープンできません。(OpenCV Error)" );
                    return false;
                }
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "動画ファイルオープンエラー", e );
                return false;
            }

            _SOLASApp.Info( $"警報動画一時ファイル({_TempFilePath})オープン" );
            return base.Init();
        }

        protected override void Terminate()
        {
            try
            {
                _SOLASApp.Info( $"警報動画ファイル({_TempFilePath})クローズ" );
                _VideoWriter?.Release();
                _VideoWriter?.Dispose();

                var MovieFileInfo = new FileInfo( _MovieFilePath );
                if ( MovieFileInfo.Exists == true )
                {
                    MovieFileInfo.Delete();
                }

                var TempFileInfo = new FileInfo( _TempFilePath );
                if ( TempFileInfo.Exists == false )
                {
                    return;
                }

                TempFileInfo.MoveTo( _MovieFilePath );
                _SOLASApp.Info( $"警報動画ファイル({_MovieFilePath})保存" );
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch( Exception e )
            {
                _SOLASApp.Exception( "動画ファイル保存エラー", e );
            }

            base.Terminate();
        }

        protected override void OneshotRoutine()
        {
            int FrameCount = 0;
            try
            {
                DateTime TimeStamp = PreAlarmTime;
                while ( TimeStamp < PostAlarmTime )
                {
                    var (Ticks, FrameImage) = _SOLASApp.ReadStockImage( TimeStamp );

                    if ( ( FrameImage != null ) && ( FrameImage.Empty() == false ) && ( FrameImage.IsDisposed == false ) )
                    {
                        TimeStamp = new DateTime( Ticks );

                        if ( FrameImage.IsDisposed == false )
                        {
                            _VideoWriter.Write( FrameImage );
                            //_SOLASApp.Debug( $"イメージ書き込み Frame:{FrameCount} {TimeStamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" );
                            ++FrameCount;
                        }
                        Thread.Sleep( 10 );
                    }
                    else
                    {
                        if ( PostAlarmTime > DateTime.Now )
                        {
                            Thread.Sleep( 10 );
                            continue;
                        }
                        _SOLASApp.Info( $"警報動画ファイル書き込みフレーム数で中断 {FrameCount}" );
                        break;
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( $"動画ファイル画像抽出エラー Frame:{FrameCount}", e );
            }
        }
    }
}
