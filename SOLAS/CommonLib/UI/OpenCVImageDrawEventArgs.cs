﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCvSharp;

namespace CES.SOLAS.Lib.UI
{
    public class OpenCVImageDrawEventArgs : EventArgs
    {
        public OpenCVImageDrawEventArgs(int DetectorId, Mat SrcImage ) : base()
        {
            Image = SrcImage;
            this.DetectorId = DetectorId;
        }

        public int DetectorId
        {
            get;
            private set;
        }

        public Mat Image
        {
            get;
            private set;
        }
    }
}
