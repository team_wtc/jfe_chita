﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using Threads;
    using Utility;

    public class OpenCVImageDrawer : QueueThread<Mat>
    {
        public event EventHandler<OpenCVImageDrawEventArgs> NewImage;

        readonly Stopwatch _Stopwatch;
        readonly int _DetectorId;

        public OpenCVImageDrawer( UI.ISOLASApp SOLASApp, int DetectorId, string ThreadName, Average Average = null )
           : base( SOLASApp, ThreadName, ThreadPriority.Normal, false, 0, Average )
        {
            _Stopwatch = Stopwatch.StartNew();
            _DetectorId = DetectorId;
        }

        public void SetImage( byte[] ImageBytes )
        {
            using ( var MemStream = new MemoryStream( ImageBytes ) )
            using ( var GDIBmp = Bitmap.FromStream( MemStream ) )
            {
                Enqueue( BitmapConverter.ToMat( ( Bitmap )GDIBmp ) );
            }
        }

        protected override bool ItemRoutine( Mat Item )
        {
            try
            {
                _Stopwatch.Restart();

                if ( ( Item != null ) && ( Item.IsDisposed == false ) )
                {
                    if ( NewImage != null )
                    {
                        //_SOLASApp.Debug( $"NewImage Event Handler Call own Detector Id = {_DetectorId}" );
                        if ( ( NewImage.Target != null ) && ( NewImage.Target is Control Ctrl ) )
                        {
                            Ctrl.Invoke( NewImage, this, new OpenCVImageDrawEventArgs( _DetectorId, Item ) );
                        }
                        else
                        {
                            NewImage( this, new OpenCVImageDrawEventArgs( _DetectorId, Item ) );
                        }
                    }
                    Item.Dispose();
                }
                else
                {
                    if ( NewImage != null )
                    {
                        if ( ( NewImage.Target != null ) && ( NewImage.Target is Control Ctrl ) )
                        {
                            Ctrl.Invoke( NewImage, this, new OpenCVImageDrawEventArgs( _DetectorId, null ) );
                        }
                        else
                        {
                            NewImage( this, new OpenCVImageDrawEventArgs( _DetectorId, null ) );
                        }
                    }
                }

                _Stopwatch.Stop();
                _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                try
                {
                    _SOLASApp.Exception( "OpenCV Image Draw Error.", e );
                }
                catch
                {
                }
            }

            return true;
        }
    }
}
