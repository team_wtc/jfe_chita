﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using System.Drawing;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Lib.UI
{
    using Utility;
    using Threads;

    using static System.Diagnostics.Debug;

    public class OpenCvStreamReceiver : OneshotThread
    {
        readonly string _StreamingURL;
        readonly Average _Average = null;
        readonly Stopwatch _Stopwatch;
        readonly QueueThread<Mat> _NextThread;
        readonly Action _RestartCallback;

        VideoCapture _VideoCapture;
        //bool _Restart = false;
        bool _Exit = false;
        Mat _ConnectionImage = null;
        long _FrameReadWaitTime = 1000L;

        public OpenCvStreamReceiver( UI.ISOLASApp SOLASApp, string ThreadName, string StreamingURL, QueueThread<Mat> NextThread, Average Average = null, Action Callback = null )
            : base( SOLASApp, ThreadName, ThreadPriority.Normal )
        {
            _StreamingURL = StreamingURL;
            _NextThread = NextThread;
            _Stopwatch = Stopwatch.StartNew();
            _Average = Average;
            _RestartCallback = Callback;
        }

        public virtual void SetConnectionImage( Bitmap ConnImage )
        {
            SetConnectionImage( BitmapConverter.ToMat( ConnImage ) );
        }

        public virtual void SetConnectionImage( Mat ConnImage = null )
        {
            _ConnectionImage?.Dispose();
            _ConnectionImage = ConnImage;
        }

        public override void Stop()
        {
            _Exit = true;

            try
            {
                _VideoCapture?.Dispose();
                _VideoCapture = null;
            }
            catch
            {

            }
            base.Stop();
        }

        protected override bool Init()
        {
            _SOLASApp.Info( $"OpenCvStreamReceiver Startup. URL={_StreamingURL}" );
            return base.Init();
        }

        protected override void Terminate()
        {
            try
            {
                _VideoCapture?.Dispose();
                _SOLASApp.Info( $"OpenCvStreamReceiver Terminate. URL={_StreamingURL}" );

                base.Terminate();
            }
            catch
            {

            }
        }

        [HandleProcessCorruptedStateExceptions]
        public void RestartStreaming()
        {
            _SOLASApp.Warn( "ストリーミング受信タイムアウトによる強制中断" );
            //_Restart = true;
            try
            {
                _VideoCapture?.Dispose();
                _VideoCapture = null;
            }
            catch
            {
                _VideoCapture = null;
            }

            //if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
            //{
            //    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
            //    _SOLASApp.ErrorMessage( $"ストリーミング受信がタイムアウトしました。 URL={_StreamingURL}" );
            //    _NextThread.Enqueue( ( Mat )null );
            //}

            //_RestartCallback?.Invoke();
        }

        [HandleProcessCorruptedStateExceptions]
        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( OpenURL() == false )
                {
                    if ( _Exit == true )
                    {
                        return;
                    }

                    continue;
                }

                try
                {
                    _Stopwatch.Restart();

                    var FrameMat = new Mat();
                    if ( ( _VideoCapture.Read( FrameMat ) == true ) && ( FrameMat.Empty() == false ) )
                    {
                        _NextThread?.Enqueue( FrameMat );
                        _Stopwatch.Stop();
                        if ( _FrameReadWaitTime > _Stopwatch.ElapsedMilliseconds )
                        {
                            _Average?.AddValue( _FrameReadWaitTime );
                            Thread.Sleep( Convert.ToInt32( _FrameReadWaitTime - _Stopwatch.ElapsedMilliseconds ) );
                        }
                        else
                        {
                            _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
                        }
//#if DEBUG
//                        if ( _StreamingURL.ToLower().StartsWith( "rtsp" ) == false )
//                        {
//                            while ( ( _VideoCapture != null ) && ( _VideoCapture.IsDisposed == false ) && ( _VideoCapture.IsOpened() == true ) )
//                            {
//                                Thread.Sleep( 10 );
//                            }
//                        }
//#endif
                    }
                    else
                    {
                        FrameMat?.Dispose();
                        _VideoCapture.Dispose();
                        _VideoCapture = null;
                        var ErrorMessage = $"ストリーミング受信が中断されました。 URL={_StreamingURL}";
                        _SOLASApp.Warn( ErrorMessage );
                        if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                        {
                            _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                            _SOLASApp.ErrorMessage( ErrorMessage );
                            _NextThread.Enqueue( ( Mat )null );
                        }
                        if ( _RestartCallback != null )
                        {
                            _RestartCallback.Invoke();
                            return;
                        }
                    }
                }
                catch ( ThreadAbortException )
                {
                    throw;
                }
                catch ( AccessViolationException )
                {
                    if ( _Exit == true )
                    {
                        return;
                    }

                    //if ( _Restart == true )
                    //{
                    //    _Restart = false;
                    //}
                    _SOLASApp.Error( "ストリーミング中断(AccessViolationException)" );
                    _NextThread.Enqueue( ( Mat )null );
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( "ストリーミング中断(AccessViolationException)" );
                    if ( _RestartCallback != null )
                    {
                        _RestartCallback.Invoke();
                        return;
                    }
                }
                catch ( Exception e )
                {
                    if ( _Exit == true )
                    {
                        return;
                    }

                    _SOLASApp.Exception( "ストリーミング例外", e );
                    //if ( _Restart == true )
                    //{
                    //    _Restart = false;
                    //}
                    _NextThread.Enqueue( ( Mat )null );
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( $"ストリーミング受信エラー{Environment.NewLine}{e.Message}" );
                    if ( _RestartCallback != null )
                    {
                        _RestartCallback.Invoke();
                        return;
                    }
                }
            }
        }

        private bool OpenURL()
        {
            if ( ( _VideoCapture != null ) && ( _VideoCapture.IsDisposed == false ) && ( _VideoCapture.IsOpened() == true ) )
            {
                return true;
            }

            if ( _VideoCapture != null )
            {
                if ( _VideoCapture.IsOpened() == true )
                {
                    _VideoCapture.Dispose();
                }
                if ( _VideoCapture.IsDisposed == false )
                {
                    _VideoCapture.Dispose();
                }
            }

            try
            {
                _SOLASApp.Info( $"ストリーミング接続開始　URL={_StreamingURL}" );
                _VideoCapture = new VideoCapture();
                _VideoCapture.Open( _StreamingURL );
                if ( _VideoCapture.IsOpened() == true )
                {
                    if ( _StreamingURL.ToLower().StartsWith( "rtsp" ) == false )
                    {
                        if ( ( _VideoCapture.Fps > 1.0 ) && ( _VideoCapture.Fps <= 30.0 ) )
                        {
                            _FrameReadWaitTime = ( long )( 1000.0 / _VideoCapture.Fps );
                        }
                        else
                        {
                            _FrameReadWaitTime = Convert.ToInt64( Math.Truncate( ( 1000.0 / 15.0 ) + 0.5 ) );
                        }
                    }
                    else
                    {
                        _FrameReadWaitTime = 0;
                    }
                    _SOLASApp.Info( $"ストリーミング接続完了　URL={_StreamingURL} FrameRate={_VideoCapture.Fps:F2} Codec={_VideoCapture.FourCC}" );

                    if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                    {
                        if ( _SOLASApp.EnableDetect == true )
                        {
                            _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_RUNNING );
                        }
                        else
                        {
                            _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_RUNNING_NO_DETECT );
                        }
                    }
                    return true;
                }
                else
                {
                    var ErrorMessage = $"ストリーミングを開始できません。　URL={_StreamingURL}";
                    _SOLASApp.Warn( ErrorMessage );
                    if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                    {
                        _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                        _SOLASApp.ErrorMessage( ErrorMessage );
                        _NextThread.Enqueue( ( Mat )null );
                    }
                    //_RestartCallback?.Invoke();
                    Thread.Sleep( 10000 );
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e)
            {
                _SOLASApp.Exception( "ストリーミング開始", e );
                if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                {
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( $"ストリーミング開始失敗　URL={_StreamingURL}{Environment.NewLine}{e.Message}" );
                    _NextThread.Enqueue( ( Mat )null );
                }
                //_RestartCallback?.Invoke();
                Thread.Sleep( 10000 );
            }

            return false;
        }
    }
}
