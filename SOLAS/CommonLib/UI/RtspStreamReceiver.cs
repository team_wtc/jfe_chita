﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using System.Drawing;
using System.Net;
using System.Collections.Generic;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using RtspClientSharp;
using RtspClientSharp.RawFrames;
using RtspClientSharp.RawFrames.Video;

using OpenH264Lib;

namespace CES.SOLAS.Lib.UI
{
    using Utility;
    using Threads;

    using static System.Diagnostics.Debug;

    public class RtspStreamReceiver : OneshotThread
    {
        readonly string _StreamingURL;
        readonly string _CameraAccount;
        readonly string _CameraPassword;
        readonly Average _Average = null;
        readonly Stopwatch _Stopwatch;
        //readonly QueueThread<byte[]> _H264DecoderThread;
        readonly QueueThread<Mat> _AIDetectorThread;
        readonly Action _RestartCallback;
        readonly ConnectionParameters _ConnectionParameters;
        readonly Decoder _Decoder = new Decoder( "openh264-1.8.0-win64.dll" );

        //bool _Exit = false;
        Mat _ConnectionImage = null;
        RtspClient _RtspClient = null;
        CancellationTokenSource _CancellationTokenSource = null;

        public RtspStreamReceiver( UI.ISOLASApp SOLASApp, string ThreadName, string StreamingURL, string CameraAccount, string CameraPassword, QueueThread<Mat> AIDetectorThread, Average Average = null, Action Callback = null )
            : base( SOLASApp, ThreadName, ThreadPriority.Highest )
        {
            _StreamingURL = StreamingURL;
            _CameraAccount = CameraAccount;
            _CameraPassword = CameraPassword;
            _AIDetectorThread = AIDetectorThread;
            _Stopwatch = Stopwatch.StartNew();
            _Average = Average;
            _RestartCallback = Callback;
            _ConnectionParameters = new ConnectionParameters( new Uri( _StreamingURL ), new NetworkCredential( _CameraAccount, _CameraPassword ) )
            {
                CancelTimeout = TimeSpan.FromSeconds( 10 ),
                ConnectTimeout = TimeSpan.FromSeconds( 10 ),
                ReceiveTimeout = TimeSpan.FromSeconds( 10 ),
                RtpTransport = RtpTransportProtocol.TCP,
                RequiredTracks = RequiredTracks.Video
            };
        }

        public virtual void SetConnectionImage( Bitmap ConnImage )
        {
            SetConnectionImage( BitmapConverter.ToMat( ConnImage ) );
        }

        public virtual void SetConnectionImage( Mat ConnImage = null )
        {
            _ConnectionImage?.Dispose();
            _ConnectionImage = ConnImage;
        }

        protected override bool Init()
        {
            _SOLASApp.Info( $"RtspStreamReceiver Startup. URL={_StreamingURL}" );
            return OpenURL();
        }

        protected override void Terminate()
        {
            try
            {
                _ConnectionImage?.Dispose();
                _SOLASApp.Info( $"StreamReceiver Terminate. URL={_StreamingURL}" );

                base.Terminate();
            }
            catch
            {

            }
        }

        [HandleProcessCorruptedStateExceptions]
        public void RestartStreaming()
        {
            _SOLASApp.Warn( "ストリーミング受信タイムアウトによる強制中断" );
            try
            {
                _CancellationTokenSource?.Cancel();
            }
            catch
            {
            }
        }

        [HandleProcessCorruptedStateExceptions]
        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( OpenURL() == false )
                {
                    //if ( _Exit == true )
                    //{
                    //    return;
                    //}
                    Thread.Sleep( 30000 );
                    continue;
                }

                try
                {
                    _Stopwatch.Restart();
                    _RtspClient.ReceiveAsync( _CancellationTokenSource.Token ).Wait();
                }
                catch ( ThreadAbortException )
                {
                    throw;
                }
                catch ( AccessViolationException )
                {
                    //if ( _Exit == true )
                    //{
                    //    return;
                    //}

                    _SOLASApp.Error( "ストリーミング中断(AccessViolationException)" );
                    _AIDetectorThread.Enqueue( null );
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( "ストリーミング中断(AccessViolationException)" );
                    _RestartCallback?.Invoke();
                    return;
                }
                catch ( Exception e )
                {
                    //if ( _Exit == true )
                    //{
                    //    return;
                    //}

                    _SOLASApp.Exception( "ストリーミング例外", e );
                    _AIDetectorThread.Enqueue( null );
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( $"ストリーミング受信エラー{Environment.NewLine}{e.Message}" );
                    _RestartCallback?.Invoke();
                    return;
                }
            }
        }

        private bool OpenURL()
        {
            if ( ( _RtspClient != null ) && ( _CancellationTokenSource != null ) && ( _CancellationTokenSource.IsCancellationRequested == false ) )
            {
                return true;
            }


            try
            {
                _SOLASApp.Info( $"ストリーミング接続開始　URL={_StreamingURL}" );
                _CancellationTokenSource?.Dispose();
                _RtspClient?.Dispose();

                _CancellationTokenSource = new CancellationTokenSource();
                _RtspClient = new RtspClient( _ConnectionParameters );
                _RtspClient.FrameReceived += this._RtspClient_FrameReceived;

                _RtspClient.ConnectAsync( _CancellationTokenSource.Token ).Wait();
                _SOLASApp.Info( $"ストリーミング接続完了　URL={_StreamingURL}" );

                if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                {
                    if ( _SOLASApp.EnableDetect == true )
                    {
                        _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_RUNNING );
                    }
                    else
                    {
                        _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_RUNNING_NO_DETECT );
                    }
                }
                return true;
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "ストリーミング開始", e );
                if ( _SOLASApp.ProcessType == TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR )
                {
                    _SOLASApp.ApplicationStatus( -1, APP_STATUS.APP_STS_FAIL );
                    _SOLASApp.ErrorMessage( $"ストリーミング開始失敗　URL={_StreamingURL}{Environment.NewLine}{e.Message}" );
                    _AIDetectorThread.Enqueue( null );
                }
                //_RestartCallback?.Invoke();
                _CancellationTokenSource?.Dispose();
                _RtspClient?.Dispose();
                _CancellationTokenSource = null;
                _RtspClient = null;

                Thread.Sleep( 30000 );
                _RestartCallback?.Invoke();
            }

            return false;
        }

        private void _RtspClient_FrameReceived( object sender, RawFrame e )
        {
            List<byte> EncodedImageBuffer = new List<byte>();

            if ( e is RawH264IFrame IFrame )
            {
                //_SOLASApp.Info( $"Receive RawH264IFrame SPS_PPS:{IFrame.SpsPpsSegment.Count} FRAME:{IFrame.FrameSegment.Count}" );
                var SpsPpsBuffer = new byte[ IFrame.SpsPpsSegment.Count ];
                Array.Copy( IFrame.SpsPpsSegment.Array, IFrame.SpsPpsSegment.Offset, SpsPpsBuffer, 0, IFrame.SpsPpsSegment.Count );
                EncodedImageBuffer.AddRange( SpsPpsBuffer );
                //_SOLASApp.Debug( "RtspClient FrameReceived IFrame" );
            }

            var FrameBuffer = new byte[ e.FrameSegment.Count ];
            Array.Copy( e.FrameSegment.Array, e.FrameSegment.Offset, FrameBuffer, 0, e.FrameSegment.Count );
            EncodedImageBuffer.AddRange( FrameBuffer );

            var Img = _Decoder.Decode( EncodedImageBuffer.ToArray(), EncodedImageBuffer.Count );
            if ( Img != null )
            {
                _AIDetectorThread.Enqueue( BitmapConverter.ToMat( Img ) );
                Img.Dispose();
            }
            else
            {
                _AIDetectorThread.Enqueue( null );
                _SOLASApp.Debug( $"{_Thread.Name} Decode Image = null {e.GetType()} {e.FrameSegment.Count}" );
            }

            _Stopwatch.Stop();

            _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
            _Stopwatch.Restart();
        }
    }
}
