﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;

using OpenCvSharp;

using static System.Diagnostics.Debug;
using static System.Threading.Thread;

namespace CES.SOLAS.Lib.UI
{
    using Logging;
    using ProcessIF.UDP;
    using ProcessIF.TCP;
    using Data;

    public abstract class SOLASApp<T> where T : ISOLASApp, new()
    {
        const int DEFAULT_MIN_WORKER_THREADS = 30;

        private static T _Instance;

        public static T Instance
        {
            get
            {
                if ( _Instance == null )
                {
                    _Instance = new T();
                }

                return _Instance;
            }
        }

        protected TRIGGER_PROCESS_TYPE _ProcessType = TRIGGER_PROCESS_TYPE.TRG_PROC_UNKNOWN;
        protected int _ApplicationId = -1;

        protected Logger _Logger = null;

        protected PacketReceiver _PacketReceiver = null;
        protected TriggerCallback _TriggerCallback = null;

        //protected MovieReqListener _MovieReqListener = null;

        protected ImageListener _ImageListener = null;

        protected readonly Dictionary<int, PacketSender> _PacketSenders = new Dictionary<int, PacketSender>();

        protected readonly Dictionary<int, StillImageSender> _StillImageSenders = new Dictionary<int, StillImageSender>();

        protected readonly Dictionary<int, H264ImageSender> _EncodedImageSenders = new Dictionary<int, H264ImageSender>();

        //  2020/06/26 フレーム画像保持
        protected readonly List<(long Ticks, Mat FrameImage)> _FrameImageStock = new List<(long Ticks, Mat FrameImage)>();
        public abstract void AddStockImage( DateTime Timestamp, Mat FrameImage );
        public abstract void EraceStockImage( DateTime Timestamp );

        public abstract (long Ticks, Mat FrameImage) ReadStockImage( DateTime Timestamp );

        public TRIGGER_PROCESS_TYPE ProcessType => _ProcessType;

        public int ApplicationId => _ApplicationId;

        protected IDBAccess _DBAccess = null;

        public virtual void AppInitialyze(
            string DBServer,
            string SAPassword,
            TriggerCallback TrgCallback,
            //FindMovieFile FindMovieFile,
            string LogFolder,
            string LogName,
            string LogExt = "log",
            long LogSize = ( 1024L * 1024L * 5L ),
            int SaveDays = 31,
            bool DebugLog = false )
        {
            try
            {
                CurrentThread.Name = "Main";
            }
            catch
            {
            }

            try
            {
                InitLog( LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );
            }
            catch ( Exception e1 )
            {
                throw new ApplicationException( $"ログ初期化失敗({e1.Message})", e1 );
            }

            Info( $"********** {LogName} Start up. **********" );

            ThreadPool.GetMinThreads( out int WorkerThreadCount, out int CompletionthreadCount );
            if ( WorkerThreadCount < DEFAULT_MIN_WORKER_THREADS )
            {
                WorkerThreadCount = DEFAULT_MIN_WORKER_THREADS;
                ThreadPool.SetMinThreads( WorkerThreadCount, CompletionthreadCount );
            }



            Info( "データベース初期化" );

            while ( true )
            {
                try
                {
                    InitDBAccess( DBServer, SAPassword );
                    break;
                }
                catch ( Exception e2 )
                {
                    if ( IsServerDown( e2 ) == true )
                    {
                        continue;
                    }

                    throw new ApplicationException( $"DB初期化失敗({e2.Message})", e2 );
                }
            }


            InitUdpSender();
            InitUdpReceiver( _DBAccess.UDPListenerPort, TrgCallback );

            //InitMovieReqListener( _DBAccess.TCPMovieListenerPort );

            InitStillImageListener();
            InitImageSender();

            InitCameraAccess();
        }

        private bool IsServerDown( Exception e )
        {
            if ( e is SqlException SqlExp )
            {
                if ( SqlExp.Class >= 20 )
                {
                    foreach ( var Err in SqlExp.Errors )
                    {
                        if ( Err is SqlError SqlErr )
                        {
                            //  Number 2 接続エラー
                            //  Number -1 は念のため
                            //  https://docs.microsoft.com/ja-jp/previous-versions/sql/sql-server-2008-r2/cc645611%28v%3dsql.105%29
                            if ( ( SqlErr.Number == 2 ) || ( SqlErr.Number == -1 ) )
                            {
                                Warn( $"Wait for SQL Server..." );
                                //  5秒待ってリトライ
                                Sleep( 5000 );
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
        public virtual void AppFinalyze()
        {
            TerminateStillImageSender();
            TerminateStillImageListener();

            TerminateCameraAccess();

            TerminateUdpSender();
            TerminatetUdpReceiver();

            Info( $"********** Terminat. **********" );

            Sleep( 100 );
            TerminateLog();
        }

        protected virtual void InitLog( string LogFolder, string LogName, string LogExt, long LogSize, int SaveDays, bool DebugLog )
        {
            _Logger = new Logger( LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );
            _Logger.Start();
        }

        protected virtual void TerminateLog()
        {
            _Logger?.Stop();
        }

        protected virtual void InitUdpReceiver( int Port, TriggerCallback Callback )
        {
            _TriggerCallback = Callback;
            _PacketReceiver = new PacketReceiver( Instance, Port, _TriggerCallback );
            _PacketReceiver.Start();
        }

        protected abstract void InitUdpSender();
        protected abstract void TerminateUdpSender();

        //protected abstract void InitMovieReqListener( int Port );
        //protected abstract void TerminateMovieReqListener();

        protected abstract void InitCameraAccess();

        protected abstract void TerminateCameraAccess();

        protected abstract void InitDBAccess( string DBServer, string SAPassword );

        protected abstract void InitStillImageListener();

        protected abstract void InitImageSender();

        protected abstract void TerminateStillImageListener();

        protected abstract void TerminateStillImageSender();

        protected virtual void TerminatetUdpReceiver()
        {
            _PacketReceiver?.Stop();
        }


        public virtual void Info( string InfoMessage )
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Info( InfoMessage );
        }

        public virtual void Warn( string WarnMessage )
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Warn( WarnMessage );
        }
        public virtual void Error( string ErrorMessage )
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Error( ErrorMessage );
        }
        public virtual void Debug( string DebugMessage )
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Debug( DebugMessage );
        }

        public virtual void Exception( string Message, Exception Exp )
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Exception( Message, Exp );
        }

        public virtual void LogDelete()
        {
            //Assert( _Logger != null && _Logger.IsAlive == true, "Logging.Logger is null." );

            _Logger?.Delete();
        }

        public int PreAlarmFrameCount => _DBAccess.PreAlarmFrameCount( ApplicationId );

        public int PostAlarmFrameCount => _DBAccess.PostAlarmFrameCount( ApplicationId );
    }
}
