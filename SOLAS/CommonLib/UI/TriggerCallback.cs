﻿using System;
using System.Windows.Forms;

namespace CES.SOLAS.Lib.UI
{
    using ProcessIF.UDP;

    public abstract class TriggerCallback
    {
        public event EventHandler<TriggerReceiveEventArgs> TriggerReceive;

        public readonly ProcessIF.UDP.PacketReceiveCallback Callback;

        public TriggerCallback()
        {
            Callback = TriggerReceiveCallback;
        }

        protected abstract void TriggerReceiveCallback( ProcessIF.UDP.IPacket Packet );

        //private void TriggerReceiveCallback( ProcessIF.UDP.IPacket Packet )
        //{
        //    Assert( TriggerReceive != null, "イベントハンドラ未設定" );

        //}

        protected void EventHandlerCall( IPacket Packet )
        {
            if ( TriggerReceive != null )
            {
                if ( ( TriggerReceive.Target != null ) && ( TriggerReceive.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke( TriggerReceive, this, new TriggerReceiveEventArgs( Packet ) );
                }
                else
                {
                    TriggerReceive( this, new TriggerReceiveEventArgs( Packet ) );
                }
            }
        }

    }
}
