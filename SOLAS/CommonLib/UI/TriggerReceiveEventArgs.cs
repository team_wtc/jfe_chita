﻿using System;

namespace CES.SOLAS.Lib.UI
{
    using ProcessIF.UDP;

    public class TriggerReceiveEventArgs : EventArgs
    {
        public TriggerReceiveEventArgs(IPacket Pkt ) : base()
        {
            Packet = Pkt;
        }

        public IPacket Packet
        {
            get;
            private set;
        }

        public TRIGGER_TYPE TriggerType => Packet.Trigger;
        public TRIGGER_PROCESS_TYPE ProcessType => Packet.ProcessType;
        public int ProcessId => Packet.ProcessId;
        public DateTime Timestamp => Packet.Timestamp;
    }
}
