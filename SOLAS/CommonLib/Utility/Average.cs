﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CES.SOLAS.Lib.Utility
{
    public class Average : IDisposable
    {
        const int MAX_VALUE_COUNT_DEFAULT = 100;
        const int DEFAULT_TIMEOUT = 5 * 1000;

        readonly List<float> _Values = new List<float>();
        readonly int _MaxCount = MAX_VALUE_COUNT_DEFAULT;
        //readonly AutoResetEvent _AutoResetEvent = new AutoResetEvent( false );
        readonly int _Timeout = 0;

        Action _TimeoutCallback;
        float _AverageValue;
        float _MinValue;
        float _MaxValue;
        Task _TimeoutChecker = null;
        bool _Disposed = false;
        //bool _Reset = false;
        DateTime? _LastAddTime = null;

        public Average( int MaxCount = MAX_VALUE_COUNT_DEFAULT, int Timeout = DEFAULT_TIMEOUT, Action TimeoutCallback = null )
        {
            _MaxCount = MaxCount;
            _Timeout = Timeout;
            _TimeoutCallback = TimeoutCallback;
        }

        public Action TimeoutCallback
        {
            get => _TimeoutCallback;
            set => _TimeoutCallback = value;
        }

        public void Reset()
        {
            //_Reset = true;
            _LastAddTime = null;
        }

        private void Timeout()
        {
            while ( _Disposed == false )
            {
                if ( _LastAddTime.HasValue == false )
                {
                    Thread.Sleep( 100 );
                }
                else if ( _LastAddTime.Value.AddMilliseconds( _Timeout ) < DateTime.Now )
                {
                    _TimeoutCallback();
                    _LastAddTime = null;
                }
            }
        }

        public void AddValue( byte Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }

        public void AddValue( short Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }
        public void AddValue( int Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }
        public void AddValue( long Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }

        public void AddValue( ushort Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }
        public void AddValue( uint Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }
        public void AddValue( ulong Item )
        {
            AddValue( Convert.ToSingle( Item ) );
        }

        public void AddValue( float Item )
        {
            if ( ( _TimeoutCallback != null ) && ( _Timeout > 0 ) && ( _TimeoutChecker == null ) )
            {
                _TimeoutChecker = Task.Factory.StartNew( Timeout );
            }

            _LastAddTime = DateTime.Now;

            _Values.Add( Item );
            while ( _Values.Count > _MaxCount )
            {
                _Values.RemoveAt( 0 );
            }

            _MinValue = float.MaxValue;
            _MaxValue = float.MinValue;
            float Total = 0F;
            foreach ( var Tval in _Values )
            {
                Total += Tval;
                _MaxValue = ( Tval > _MaxValue ) ? Tval : _MaxValue;
                _MinValue = ( Tval < _MinValue ) ? Tval : _MinValue;
            }

            if ( Total <= 0F )
            {
                _AverageValue = 0.001F;
            }
            else
            {
                _AverageValue = ( Total / _Values.Count );
            }

            //_AutoResetEvent.Set();
        }

        public float Avg => _AverageValue;
        public float Min => _MinValue;
        public float Max => _MaxValue;

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    //_AutoResetEvent.Dispose();
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
                _Disposed = true;

                _TimeoutChecker?.Wait();
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~Average() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
