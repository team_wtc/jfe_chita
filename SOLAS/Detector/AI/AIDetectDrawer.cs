﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Drawing;

using OpenCvSharp;
using OpenCvSharp.Extensions;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using Lib.UI;
    using Lib.Threads;

    public class AIDetectDrawer : QueueThread<(Mat Img, List<YoloResult> Results)>
    {
        readonly QueueThread<Mat> _NextThread;
        readonly Stopwatch _Stopwatch;
        readonly Dictionary<uint, (string Name, Scalar DrawColor)> _DrawColors = new Dictionary<uint, (string Name, Scalar DrawColor)>();

        public AIDetectDrawer(ISOLASApp SOLASApp, QueueThread<Mat> NextThread, Lib.Utility.Average Average =null)
            :base( SOLASApp, "Image", ThreadPriority.Normal, false,0, Average )
        {
            _NextThread = NextThread;
            _Stopwatch = Stopwatch.StartNew();

            _DrawColors[ ( uint )DetectorApp.Instance.PersonId ] = (DetectorApp.Instance.PersonName, DetectorApp.Instance.PersonColor);
            _DrawColors[ ( uint )DetectorApp.Instance.MarkerId ] = (DetectorApp.Instance.MarkerName, DetectorApp.Instance.MarkerColor);
            _DrawColors[ ( uint )DetectorApp.Instance.CarId ] = (DetectorApp.Instance.CarName, DetectorApp.Instance.CarColor);
        }

        protected override bool ItemRoutine( (Mat Img, List<YoloResult> Results) Item )
        {
            try
            {
                _Stopwatch.Restart();

                foreach ( var result in Item.Results )
                {
                    if ( _DrawColors.ContainsKey( result.ObjId ) == false )
                    {
                        continue;
                    }

                    Cv2.Rectangle(
                        Item.Img,
                        new Rect( ( int )result.X, ( int )result.Y, ( int )result.Width, ( int )result.Height ),
                        _DrawColors[ result.ObjId ].DrawColor,
                        2 );
#if DEBUG
                    Cv2.PutText(
                        Item.Img,
                        _DrawColors[ result.ObjId ].Name,
                        new OpenCvSharp.Point( ( int )result.X, ( int )result.Y - 5 ),
                        HersheyFonts.HersheyComplex,
                        0.8,
                        _DrawColors[ result.ObjId ].DrawColor,
                        2 );
#endif
                }

#if DEBUG
                Cv2.PutText(
                    Item.Img,
                    $"Streaming {Program._StreamingAverage.Avg:F2} ms ({( 1000F / Program._StreamingAverage.Avg ):F2} fps)",
                    new OpenCvSharp.Point( 10, 50 ),
                    HersheyFonts.HersheyComplex,
                    1.0,
                    Scalar.Cyan,
                    2 );
                Cv2.PutText(
                    Item.Img,
                    $"Detect {Program._DetectAverage.Avg:F2} ms ({( 1000F / Program._DetectAverage.Avg ):F2} fps)",
                    new OpenCvSharp.Point( 10, 80 ),
                    HersheyFonts.HersheyComplex,
                    1.0,
                    Scalar.Cyan,
                    2 );
                Cv2.PutText(
                    Item.Img,
                    $"Image {Program._DetectDrawAverage.Avg:F2} ms ({( 1000F / Program._DetectDrawAverage.Avg ):F2} fps)",
                    new OpenCvSharp.Point( 10, 110 ),
                    HersheyFonts.HersheyComplex,
                    1.0,
                    Scalar.Cyan,
                    2 );
                Cv2.PutText(
                    Item.Img,
                    $"DirectX {Program._DirectXAverage.Avg:F2} ms ({( 1000F / Program._DirectXAverage.Avg ):F2} fps)",
                    new OpenCvSharp.Point( 10, 140 ),
                    HersheyFonts.HersheyComplex,
                    1.0,
                    Scalar.Cyan,
                    2 );
#endif

                _NextThread.Enqueue( Item.Img );

                _Stopwatch.Stop();
                _Average.AddValue( _Stopwatch.ElapsedMilliseconds );
            }
            catch
            {

            }

            return true;
        }
    }
}
