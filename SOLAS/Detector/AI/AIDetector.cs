﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using Lib.UI;
    using Lib.Threads;

    public class AIDetector : QueueThread<Mat>
    {
        const float DEFAULT_THRESHOLD = 0.1F;

        readonly string _Config;
        readonly string _Weights;
        readonly float _Threshold;
        readonly QueueThread<(Mat Img, List<YoloResult> Results)> _NextThread;
        readonly Action _Callback;
        readonly Stopwatch _Stopwatch;

        Darknet _DarknetDetector = null;

        public AIDetector(
            ISOLASApp SOLASApp,
            string Config,
            string Weights,
            QueueThread<(Mat Img, List<YoloResult> Results)> NextThread,
            Action LoadCompleteCallback,
            bool EnableDetect = true,
            Lib.Utility.Average Average = null,
            float Threshold = DEFAULT_THRESHOLD )
            : base( SOLASApp, "Detect", ThreadPriority.Normal, false, 0, Average )
        {
            _Config = Config;
            _Weights = Weights;
            _Threshold = Threshold;
            _NextThread = NextThread;
            _Callback = LoadCompleteCallback;
            this.EnableDetect = EnableDetect;
            _Stopwatch = Stopwatch.StartNew();
        }

        public bool EnableDetect
        {
            get;
            set;
        }

        protected override bool Init()
        {
            try
            {
                _SOLASApp.Info( $"AIDetector Startup. Loading Weights A.({_Weights})" );
                _DarknetDetector = new Darknet( _Config, _Weights );
                _SOLASApp.Info( $"AIDetector Startup. Loading Weights B.({_Weights})" );

                using ( var TempImage = new Mat( rows: 1080, cols: 1920, type: MatType.CV_8UC3, s: Scalar.Black ) )
                {
                    _DarknetDetector.Detect( TempImage, _Threshold );
                }
                _SOLASApp.Info( $"Loading Weights Complete.({_Weights})" );

                _Callback();

                return true;
            }
            catch(Exception e)
            {
                _SOLASApp.Exception( $"Darknet Loading Error.", e );
                return false;
            }
        }

        protected override void Terminate()
        {
            _DarknetDetector?.Dispose();
            base.Terminate();
            _SOLASApp.Info( $"AIDetector Terminate." );
        }

        protected override bool ItemRoutine( Mat Item )
        {
            try
            {
                _Stopwatch.Restart();
                if ( ( Item != null ) && ( Item.IsDisposed == false ) )
                {
                    if ( EnableDetect == true )
                    {
                        var Result = _DarknetDetector.Detect( Item, _Threshold );
                        _NextThread.Enqueue( (Item, Result) );
                    }
                    else
                    {
                        _NextThread.Enqueue( (Item, new List<YoloResult>()) );
                    }
                }
                else
                {
                        _NextThread.Enqueue( (null, new List<YoloResult>()) );
                }

                _Stopwatch.Stop();
                _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
            }
            catch
            {

            }

            return true;
        }
    }
}
