﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using Lib.UI;
    using Lib.Utility;
    using Lib.Threads;

    public class AITracker : QueueThread<(Mat Img, List<YoloResult> Results)>
    {
        readonly QueueThread<Mat> _NextThread;
        readonly Stopwatch _Stopwatch;
        readonly AlarmRegister _AlarmRegister;
        readonly float _SmallRatio;

        SORT _SORT = null;

        public AITracker( ISOLASApp SOLASApp, QueueThread<Mat> NextThread, AlarmRegister RegisterThread, float SmallRatio, Average Average = null )
           : base( SOLASApp, "Tracker", ThreadPriority.Normal, false, 0, Average )
        {
            _NextThread = NextThread;
            _Stopwatch = Stopwatch.StartNew();
            _AlarmRegister = RegisterThread;
            _SmallRatio = SmallRatio;
        }

        protected override bool Init()
        {
            _SOLASApp.Info( $"AITracker Startup." );
            return base.Init();
        }

        protected override void Terminate()
        {
            base.Terminate();
            _SOLASApp.Info( $"AITracker Terminate." );
        }

        protected override bool ItemRoutine( (Mat Img, List<YoloResult> Results) Item )
        {
            Mat StillImageMat = null;

            try
            {
                _Stopwatch.Restart();

                if ( ( Item.Img != null ) && ( Item.Img.IsDisposed == false ) )
                {
                    if ( _SORT == null )
                    {
                        _SORT = new SORT( Item.Img.Width, Item.Img.Height, _SOLASApp.MaxPredictFrame, _SOLASApp.IOUThreshold );
                    }

                    if ( _SORT.AreaWidth != Item.Img.Width )
                    {
                        _SORT.AreaWidth = Item.Img.Width;
                    }
                    if ( _SORT.AreaHeight != Item.Img.Height )
                    {
                        _SORT.AreaHeight = Item.Img.Height;
                    }

                    //  トラッキング更新
                    _SORT.Update( Item.Results, _SmallRatio );

                    //  アラームトラッキング描画
                    _SORT.DrawTracker( Item.Img );

                    //  詳細トラッキング描画
                    _SORT.DrawTrackerDetail( Item.Img );

                    //  転送用静止画生成
                    StillImageMat = Item.Img.Clone();

                    //  検知エリア描画
                    _SORT.DrawArea( Item.Img );

                    //  動画出力用にすべてのイメージを保存する
                    //_SOLASApp.AddAtillImageToStore( DateTime.Now, Item.Img.Clone() );

#if DEBUG
                    Cv2.PutText(
                        Item.Img,
                        $"Streaming {Program._StreamingAverage.Avg:F2} ms ({( 1000F / Program._StreamingAverage.Avg ):F2} fps)",
                        new OpenCvSharp.Point( 10, 50 ),
                        HersheyFonts.HersheyComplex,
                        1.0,
                        Scalar.Cyan,
                        2 );
                    Cv2.PutText(
                        Item.Img,
                        $"Detect {Program._DetectAverage.Avg:F2} ms ({( 1000F / Program._DetectAverage.Avg ):F2} fps)",
                        new OpenCvSharp.Point( 10, 80 ),
                        HersheyFonts.HersheyComplex,
                        1.0,
                        Scalar.Cyan,
                        2 );
                    Cv2.PutText(
                        Item.Img,
                        $"Image {Program._DetectDrawAverage.Avg:F2} ms ({( 1000F / Program._DetectDrawAverage.Avg ):F2} fps)",
                        new OpenCvSharp.Point( 10, 110 ),
                        HersheyFonts.HersheyComplex,
                        1.0,
                        Scalar.Cyan,
                        2 );
                    Cv2.PutText(
                        Item.Img,
                        $"DirectX {Program._DirectXAverage.Avg:F2} ms ({( 1000F / Program._DirectXAverage.Avg ):F2} fps)",
                        new OpenCvSharp.Point( 10, 140 ),
                        HersheyFonts.HersheyComplex,
                        1.0,
                        Scalar.Cyan,
                        2 );
#endif
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch(Exception e )
            {
                _SOLASApp.Exception( "トラッキングスレッド例外", e );
            }
            finally
            {
                if ( ( StillImageMat != null ) && ( StillImageMat.IsDisposed == false ) )
                {
                    if ( ( _AlarmRegister != null ) && ( _AlarmRegister.IsAlive == true ) )
                    {
                        _SORT.GetCount( out int DetectCount, out int PersonErrorCount, out int CarErrorCount, out int CarStayErrorCount );
                        _AlarmRegister.Enqueue( (DateTime.Now, DetectCount, PersonErrorCount, CarErrorCount, CarStayErrorCount, StillImageMat) );
                    }
                }
                else
                {
                    _AlarmRegister.Enqueue( (DateTime.Now, 0, 0, 0, 0, StillImageMat) );
                }

                _NextThread.Enqueue( Item.Img );

                _Stopwatch.Stop();
                _Average?.AddValue( _Stopwatch.ElapsedMilliseconds );
            }

            return true;
        }
    }
}
