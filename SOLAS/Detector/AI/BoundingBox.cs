﻿using System;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using static System.Math;
    using CvPoint = OpenCvSharp.Point;
    using CvRect = OpenCvSharp.Rect;

    public class BoundingBox
    {
        public BoundingBox() : this( 0F, 0F, 0F, 0F )
        {
        }

        public BoundingBox( float Left, float Top, float Right, float Bottom )
        {
            this.Left = ( Left < 0F ) ? 0F : ( Left > ( float )int.MaxValue ) ? ( float )int.MaxValue : Left;
            this.Top = ( Top < 0F ) ? 0F : ( Top > ( float )int.MaxValue ) ? ( float )int.MaxValue : Top;
            this.Right = ( Right < 0F ) ? 0F : ( Right > ( float )int.MaxValue ) ? ( float )int.MaxValue : Right;
            this.Bottom = ( Bottom < 0F ) ? 0F : ( Bottom > ( float )int.MaxValue ) ? ( float )int.MaxValue : Bottom;
        }
        public BoundingBox( YoloResult Result )
        {
            Left = Convert.ToSingle( Result.X );
            Top = Convert.ToSingle( Result.Y );
            Right = Convert.ToSingle( Result.X + Result.Width );
            Bottom = Convert.ToSingle( Result.Y + Result.Height );
            Left = ( Left < 0F ) ? 0F : ( Left > ( float )int.MaxValue ) ? ( float )int.MaxValue : Left;
            Top = ( Top < 0F ) ? 0F : ( Top > ( float )int.MaxValue ) ? ( float )int.MaxValue : Top;
            Right = ( Right < 0F ) ? 0F : ( Right > ( float )int.MaxValue ) ? ( float )int.MaxValue : Right;
            Bottom = ( Bottom < 0F ) ? 0F : ( Bottom > ( float )int.MaxValue ) ? ( float )int.MaxValue : Bottom;
        }

        public BoundingBox Clone()
        {
            return new BoundingBox( Left, Top, Right, Bottom );
        }

        public float Left
        {
            get; set;
        } = 0F;

        public float Top
        {
            get; set;
        } = 0F;
        public float Right
        {
            get; set;
        } = 0F;
        public float Bottom
        {
            get; set;
        } = 0F;

        public float Width => ( Right - Left );
        public float Height => ( Bottom - Top );

        public float Area => ( ( Right - Left ) * ( Bottom - Top ) );

        public Mat ToMat()
        {
            return new Mat(
                rows: 1,
                cols: 4,
                type: MatType.CV_32FC1,
                data: new float[] { Left, Top, Right, Bottom } );
        }

        public CvRect ToRect()
        {
            if ( ( float.IsNaN( Left ) == true ) ||
                ( float.IsNaN( Top ) == true ) ||
                ( float.IsNaN( Right ) == true ) ||
                ( float.IsNaN( Bottom ) == true ) )
            {
                return new CvRect( 0, 0, 0, 0 );
            }

            try
            {
                return new CvRect(
                    Convert.ToInt32( Left ),
                    Convert.ToInt32( Top ),
                    Convert.ToInt32( Right - Left ),
                    Convert.ToInt32( Bottom - Top ) );
            }
            catch
            {
                return new CvRect( 0, 0, 0, 0 );
            }
        }

        public CvPoint Center
        {
            get
            {
                try
                {
                    return new CvPoint( Convert.ToInt32( Left + ( Width / 2F ) ), Convert.ToInt32( Top + ( Height / 2F ) ) );
                }
                catch
                {
                    return new CvPoint( 0, 0 );
                }
            }
        }
        public float IoU( BoundingBox Box )
        {
            //  Computes IUO between two bboxes in the form[ x1, y1, x2, y2 ]
            var UnionStartX = Max( Left, Box.Left );
            var UnionStartY = Max( Top, Box.Top );
            var UnionEndX = Min( Right, Box.Right );
            var UnionEndY = Min( Bottom, Box.Bottom );
            var UnionWidth = Max( 0F, ( UnionEndX - UnionStartX ) );
            var UnionHeight = Max( 0f, ( UnionEndY - UnionStartY ) );
            var UnionArea = ( UnionWidth * UnionHeight );
            var TotalArea = ( Area + Box.Area - UnionArea );

            return UnionArea / TotalArea;
        }

        public float IoU( YoloResult Result )
        {
            return IoU( new BoundingBox( Result ) );
        }


        public static float IoU( BoundingBox BoxA, BoundingBox BoxB )
        {
            return BoxA.IoU( BoxB );
        }
        public static float IoU( BoundingBox Box, YoloResult Result )
        {
            return Box.IoU( Result );
        }

        public static BoundingBox ToSmall( BoundingBox BaseBox, float SmallRatio )
        {
            float BaseWidth = BaseBox.Right - BaseBox.Left;
            float BaseHeight = BaseBox.Bottom - BaseBox.Top;
            float SmallWidth = BaseWidth * SmallRatio;
            float SmallHeight = BaseHeight * SmallRatio;
            float SmallLeft = BaseBox.Left + ( ( BaseWidth - SmallWidth ) / 2F );
            float SmallTop = BaseBox.Top + ( ( BaseHeight - SmallHeight ) / 2F );

            return new BoundingBox( SmallLeft, SmallTop, SmallLeft + SmallWidth, SmallTop + SmallHeight );
        }

        public static BoundingBox ToSmall( YoloResult Result, float SmallRatio )
        {
            float SmallWidth = (float)Result.Width * SmallRatio;
            float SmallHeight = (float)Result.Height * SmallRatio;
            float SmallLeft = (float)Result.X + ( ( ( float )Result.Width - SmallWidth ) / 2F );
            float SmallTop = ( float )Result.Y + ( ( ( float )Result.Height - SmallHeight ) / 2F );

            return new BoundingBox( SmallLeft, SmallTop, SmallLeft + SmallWidth, SmallTop + SmallHeight );
        }
    }
}
