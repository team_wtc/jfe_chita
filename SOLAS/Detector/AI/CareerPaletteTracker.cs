﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using CvPoint = OpenCvSharp.Point;

    public class CareerPaletteTracker : Tracker, ITracker
    {
        readonly string _CareerPaletteName;
        readonly Scalar _CareerPaletteScalar;
        readonly Scalar _AlarmScalar;
        readonly Scalar _StagnantScalar;
        //readonly Stopwatch _StagnantStopwatch;

        TRACKER_DETECT_TYPE _CareerPaletteStayTrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;
        
        public CareerPaletteTracker(
            SORT SORT,
            uint TrackingId,
            uint FrameNumber,
            YoloResult Result,
            float Width,
            float Height,
            float SmallRatio = DEFAULT_BOX_SMALL_RATIO,
            int MaxTrajectory = MAX_TRAJECTORY_DEFAULT,
            int MaxOutOfRange = MAX_OUT_OF_RANGE_DEFAULT )
            : base(
                SORT,
                TrackingId,
                FrameNumber,
                Result,
                Width,
                Height,
                SmallRatio,
                MaxTrajectory,
                MaxOutOfRange )
        {
            _CareerPaletteName = DetectorApp.Instance.CareerPaletteName;
            _CareerPaletteScalar = DetectorApp.Instance.CareerPaletteColor;
            _AlarmScalar = DetectorApp.Instance.AlarmColor;
            _StagnantScalar = DetectorApp.Instance.StagnantColor;
            _Area = SORT.InDetectArea( _PursuitPoint );
        }

        public override ITracker LinkTracker
        {
            get => null;
            set => throw new NotImplementedException();
        }
        public override bool MarkerMatch
        {
            get => false;
            set => throw new NotImplementedException();
        }

        public override TRACKER_DETECT_TYPE PersonTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override TRACKER_DETECT_TYPE CarTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override TRACKER_DETECT_TYPE CarStayTrackerDetectType => _CareerPaletteStayTrackerDetectType;

        public override void DrawTrackingData( Mat DrawTarget )
        {
            if ( ( _Area == APPEARANCE_AREA.AREA_UNKNOWN ) && ( _SORT.ObjectStayDetectAreaB( _YoloResultBox ) == false ) )
            {
                return;
            }
            if ( _AreaChange == false )
            {
                return;
            }

            var DrawPoint = new CvPoint( Convert.ToInt32( _YoloResultBox.Left + 5F ), Convert.ToInt32( _YoloResultBox.Top + 25F ) );
            if ( _TimeSinceUpdate > 1 )
            {
                DrawPoint = new CvPoint( Convert.ToInt32( _PredictBox.Left + 5F ), Convert.ToInt32( _PredictBox.Top + 25F ) );
            }

            var ObjRect = _YoloResultBox.ToRect();
            Scalar DrawScalar = _CareerPaletteScalar;

            if ( _CareerPaletteStayTrackerDetectType != TRACKER_DETECT_TYPE.DETECT_NORMAL )
            {
                DrawScalar = DetectorApp.Instance.AlarmColor;
            }
            else
            {
                DrawScalar = DetectorApp.Instance.AlarmColor;
            }

            var ElapsedTime = TimeSpan.FromMilliseconds( _Stopwatch.ElapsedMilliseconds );

            Cv2.Rectangle( DrawTarget, ObjRect, DrawScalar, 2 );
            for ( int i = 1 ; i < _History.Count ; i++ )
            {
                Cv2.Line( DrawTarget, _History[ i - 1 ], _History[ i ], DrawScalar, 2 );
            }
            Cv2.Circle( DrawTarget, _PursuitPoint, 4, DrawScalar, -1 );
            Cv2.PutText( DrawTarget, $"{_CareerPaletteName}(# {TrackingId}) {ElapsedTime.ToString( @"hh\:mm\:ss" )}", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.White, 3 );
            Cv2.PutText( DrawTarget, $"{_CareerPaletteName}(# {TrackingId}) {ElapsedTime.ToString( @"hh\:mm\:ss" )}", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.Black, 1 );
        }

        public override void UpdateArea()
        {
            base.UpdateArea();

            if ( _SORT.ObjectStayDetectAreaB( _YoloResultBox ) == true )
            {
                if ( _Stopwatch.IsRunning == false )
                {
                    _Stopwatch.Start();
                }
            }
            else
            {
                if ( _Stopwatch.IsRunning == true )
                {
                    _Stopwatch.Reset();
                }
            }
            if ( _CareerPaletteStayTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_NORMAL )
            {
                var ElapsedTime = TimeSpan.FromMilliseconds( _Stopwatch.ElapsedMilliseconds );
                if ( ( ElapsedTime.TotalSeconds > DetectorApp.Instance.CarStayAlarmWaitSeconds ) &&
                    ( DetectorApp.Instance.EnableCareerPaletteStayAlarm == true ) )
                {
                    _CareerPaletteStayTrackerDetectType = TRACKER_DETECT_TYPE.DETECT_ERROR;
                }
            }
        }

        public override void DrawTrackingDetailData( Mat DrawTarget )
        {
            if ( ( _Area == APPEARANCE_AREA.AREA_UNKNOWN ) && ( _SORT.ObjectStayDetectAreaB( _YoloResultBox ) == false ) )
            {
                return;
            }

            var DrawPoint = new CvPoint( Convert.ToInt32( _YoloResultBox.Left + 5F ), Convert.ToInt32( _YoloResultBox.Top + 25F ) );
            if ( _TimeSinceUpdate > 1 )
            {
                DrawPoint = new CvPoint( Convert.ToInt32( _PredictBox.Left + 5F ), Convert.ToInt32( _PredictBox.Top + 25F ) );
            }

            var ObjRect = _YoloResultBox.ToRect();
            Scalar DrawScalar = _CareerPaletteScalar;

            if ( _CareerPaletteStayTrackerDetectType != TRACKER_DETECT_TYPE.DETECT_NORMAL )
            {
                DrawScalar = DetectorApp.Instance.AlarmColor;
            }

            var ElapsedTime = TimeSpan.FromMilliseconds( _Stopwatch.ElapsedMilliseconds );

            Cv2.Rectangle( DrawTarget, ObjRect, DrawScalar, 2 );
            for ( int i = 1 ; i < _History.Count ; i++ )
            {
                Cv2.Line( DrawTarget, _History[ i - 1 ], _History[ i ], DrawScalar, 2 );
            }
            Cv2.Circle( DrawTarget, _PursuitPoint, 4, DrawScalar, -1 );
            Cv2.PutText( DrawTarget, $"{_CareerPaletteName}(# {TrackingId}) {ElapsedTime.ToString( @"hh\:mm\:ss" )}", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.White, 3 );
            Cv2.PutText( DrawTarget, $"{_CareerPaletteName}(# {TrackingId}) {ElapsedTime.ToString( @"hh\:mm\:ss" )}", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.Black, 1 );
        }

        public override float MarkerIoU( BoundingBox Box )
        {
            return 0F;
        }

        public override float MarkerIoU( YoloResult Result )
        {
            return 0F;
        }

        public override void PersonErrorSended()
        {
        }
        public override void CarErrorSended()
        {
        }

        public override void CarStayErrorSended()
        {
            if ( _CareerPaletteStayTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_ERROR )
            {
                _CareerPaletteStayTrackerDetectType = TRACKER_DETECT_TYPE.DETECT_ERROR_SENDED;
            }
        }

        public override float IoU( BoundingBox Box )
        {
            if ( _OutOfRange == true )
            {
                return 0F;
            }

            return _YoloResultBox.IoU( Box );
        }

        public override float IoU( YoloResult Result )
        {
            return _YoloResultBox.IoU( Result );
        }

        public override bool IsOverlap( ITracker OtherTracker )
        {
            return false;
        }

        public override void ResetStatus()
        {
        }
    }
}
