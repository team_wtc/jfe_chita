﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCvSharp;
using YoloCSharp;

using CvPoint = OpenCvSharp.Point;

namespace CES.SOLAS.Detector.AI
{
    public enum APPEARANCE_AREA
    {
        AREA_UNKNOWN = 0,
        AREA_A = 1,
        AREA_B = 2,
        AREA_NEUTRAL = 3
    }

    public enum TRACKER_DETECT_TYPE
    {
        /// <summary>
        /// エラーオブジェクト無し
        /// </summary>
        DETECT_NORMAL,
        /// <summary>
        /// エラー発生
        /// </summary>
        DETECT_ERROR,
        /// <summary>
        /// エラー通知済み
        /// </summary>
        DETECT_ERROR_SENDED
    }

    public interface ITracker : IDisposable
    {
        void Update( uint FrameNumber, YoloResult Result, float SmallRatio );
        void Predict();
        void DrawTrackingData( Mat DrawTarget );

        void DrawTrackingDetailData( Mat DrawTarget );

        bool IsOverlap( ITracker OtherTracker );
        void ResetStatus();

        uint TrackingId
        {
            get;
        }

        uint FrameNumber
        {
            get;
            set;
        }

        float AreaWidth
        {
            get;
            set;
        }
        float AreaHeight
        {
            get;
            set;
        }

        ITracker LinkTracker
        {
            get;
            set;
        }

        bool MarkerMatch
        {
            get;
            set;
        }

        BoundingBox BoundingBox
        {
            get;
        }

        YoloResult YoloResult
        {
            get;
        }

        CvPoint PursuitPoint
        {
            get;
        }

        bool IsDisposed
        {
            get;
        }

        APPEARANCE_AREA Area
        {
            get;
        }

        TRACKER_DETECT_TYPE PersonTrackerDetectType
        {
            get;
        }

        TRACKER_DETECT_TYPE CarTrackerDetectType
        {
            get;
        }

        TRACKER_DETECT_TYPE CarStayTrackerDetectType
        {
            get;
        }

        void PersonErrorSended();
        void CarErrorSended();
        void CarStayErrorSended();

        float IoU( BoundingBox Box );

        float IoU( YoloResult Result );


        bool CanDelete( uint MaxAge );

        float MarkerIoU( BoundingBox Box );
        float MarkerIoU( YoloResult Result );

        void UpdateArea();
    }
}
