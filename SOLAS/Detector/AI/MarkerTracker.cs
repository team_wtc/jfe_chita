﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using CvPoint = OpenCvSharp.Point;

    public class MarkerTracker : Tracker, ITracker
    {
        readonly string _MarkerName;
        readonly Scalar _MarkerScalar;
        readonly Scalar _AlarmScalar;

        public MarkerTracker(
            SORT SORT,
            uint TrackingId,
            uint FrameNumber,
            YoloResult Result,
            float Width,
            float Height,
            float SmallRatio = DEFAULT_BOX_SMALL_RATIO,
            int MaxTrajectory = MAX_TRAJECTORY_DEFAULT,
            int MaxOutOfRange = MAX_OUT_OF_RANGE_DEFAULT )
            : base(
                SORT,
                TrackingId,
                FrameNumber,
                Result,
                Width,
                Height,
                SmallRatio,
                MaxTrajectory,
                MaxOutOfRange )
        {
            _MarkerName = DetectorApp.Instance.MarkerName;
            _MarkerScalar = DetectorApp.Instance.MarkerColor;
            _AlarmScalar = DetectorApp.Instance.AlarmColor;
        }

        public override void DrawTrackingData( Mat DrawTarget )
        {
        }

        public override float MarkerIoU( BoundingBox Box )
        {
            return 0F;
        }

        public override float MarkerIoU( YoloResult Result )
        {
            return 0F;
        }

        public override ITracker LinkTracker
        {
            get;
            set;
        } = null;

        public override bool MarkerMatch
        {
            get => false;
            set => throw new NotImplementedException();
        }

        public override TRACKER_DETECT_TYPE PersonTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override TRACKER_DETECT_TYPE CarTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override TRACKER_DETECT_TYPE CarStayTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override bool CanDelete( uint MaxAge )
        {
            if ( ( LinkTracker != null ) && ( LinkTracker.IsDisposed == false ) )
            {
                return false;
            }
            return base.CanDelete( MaxAge );
        }

        public override void PersonErrorSended()
        {
        }

        public override void CarErrorSended()
        {
        }

        public override void CarStayErrorSended()
        {
        }


        public override void DrawTrackingDetailData( Mat DrawTarget )
        {
            try
            {
                var DrawPoint = new CvPoint( Convert.ToInt32( _YoloResultBox.Right + 5F ), Convert.ToInt32( _YoloResultBox.Bottom - 5F ) );

                if ( _TimeSinceUpdate > 1 )
                {
                    DrawPoint = new CvPoint( Convert.ToInt32( _PredictBox.Right + 5F ), Convert.ToInt32( _PredictBox.Bottom - 5F ) );
                }

                if ( _TimeSinceUpdate <= 1 )
                {
                    if ( LinkTracker != null )
                    {
                        Cv2.Rectangle( DrawTarget, _YoloResultBox.ToRect(), DetectorApp.Instance.OKColor, 2 );
                    }
                    else
                    {
                        Cv2.Rectangle( DrawTarget, _YoloResultBox.ToRect(), _MarkerScalar, 2 );
                        Cv2.PutText( DrawTarget, $"{_MarkerName}(# {TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.White, 3 );
                        Cv2.PutText( DrawTarget, $"{_MarkerName}(# {TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.Black, 1 );
                    }
                }
            }
            catch
            {

            }
        }

        public override bool IsOverlap( ITracker OtherTracker )
        {
            return false;
        }

        public override void ResetStatus()
        {
            LinkTracker = null;
        }
    }
}
