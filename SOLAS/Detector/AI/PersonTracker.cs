﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using CvPoint = OpenCvSharp.Point;

    public class PersonTracker : Tracker, ITracker
    {
        readonly string _PersonName;
        readonly Scalar _PersonScalar;
        readonly Scalar _AlarmScalar;

        protected TRACKER_DETECT_TYPE _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public PersonTracker(
            SORT SORT,
            uint TrackingId,
            uint FrameNumber,
            YoloResult Result,
            float Width,
            float Height,
            float SmallRatio = DEFAULT_BOX_SMALL_RATIO,
            int MaxTrajectory = MAX_TRAJECTORY_DEFAULT,
            int MaxOutOfRange = MAX_OUT_OF_RANGE_DEFAULT )
            : base(
                SORT,
                TrackingId,
                FrameNumber,
                Result,
                Width,
                Height,
                SmallRatio,
                MaxTrajectory,
                MaxOutOfRange )
        {
            _PersonName = DetectorApp.Instance.PersonName;
            _PersonScalar = DetectorApp.Instance.PersonColor;
            _AlarmScalar = DetectorApp.Instance.AlarmColor;
            _Area = SORT.InDetectArea( _PursuitPoint );
        }

        public override void DrawTrackingData( Mat DrawTarget )
        {
            if ( _Area == APPEARANCE_AREA.AREA_UNKNOWN )
            {
                return;
            }
            if ( _AreaChange == false )
            {
                return;
            }

            var DrawPoint = new CvPoint( Convert.ToInt32( _YoloResultBox.Left ), Convert.ToInt32( _YoloResultBox.Top - 5F ) );
            if ( _TimeSinceUpdate > 1 )
            {
                DrawPoint = new CvPoint( Convert.ToInt32( _PredictBox.Left ), Convert.ToInt32( _PredictBox.Top - 5F ) );
            }

            var ObjRect = _YoloResultBox.ToRect();
#if DEBUG
            var ObjSmallRect = _YoloSmallResultBox.ToRect();
            var PredictRect = _PredictBox.ToRect();
#endif
            Scalar DrawScalar = DetectorApp.Instance.PersonColor;

            if ( MarkerMatch == false )
            {
                if ( _AreaChangeFrameCount <= DetectorApp.Instance.AreaChangeWaitFrames )
                {
                    if ( ( _TrackerDetectType != TRACKER_DETECT_TYPE.DETECT_ERROR ) && ( _TrackerDetectType != TRACKER_DETECT_TYPE.DETECT_ERROR_SENDED ) )
                    {
                        DrawScalar = DetectorApp.Instance.PersonColor;
                    }
                    else
                    {
                        DrawScalar = DetectorApp.Instance.AlarmColor;
                    }
                }
                else
                {
                    DrawScalar = DetectorApp.Instance.AlarmColor;
                }
            }
            Cv2.Rectangle( DrawTarget, ObjRect, DrawScalar, 2 );
#if DEBUG
            Cv2.Rectangle( DrawTarget, ObjSmallRect, DrawScalar, 2 );
            //Cv2.Rectangle( DrawTarget, PredictRect, Scalar.WhiteSmoke, 2 );
#endif
            for ( int i = 1 ; i < _History.Count ; i++ )
            {
                Cv2.Line( DrawTarget, _History[ i - 1 ], _History[ i ], DrawScalar, 2 );
            }
            Cv2.Circle( DrawTarget, _PursuitPoint, 4, DrawScalar, -1 );
            Cv2.PutText( DrawTarget, $"{_PersonName}(#{TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.White, 3 );
            Cv2.PutText( DrawTarget, $"{_PersonName}(#{TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, DetectorApp.Instance.AlarmColor, 1 );
        }

        protected override void CreatePredictBox( CvPoint PredictCenter )
        {
            base.CreatePredictBox( PredictCenter );
            _PredictBox = BoundingBox.ToSmall( _PredictBox, _SmallRatio );
        }
        public override float MarkerIoU( BoundingBox Box )
        {
            var PersonBox = new BoundingBox(
                _YoloResultBox.Left,
                _YoloResultBox.Top,
                _YoloResultBox.Right,
                ( _YoloResultBox.Bottom / 6F ) + _YoloResultBox.Top );

            var UnionStartX = Math.Max( PersonBox.Left, Box.Left );
            var UnionStartY = Math.Max( PersonBox.Top, Box.Top );
            var UnionEndX = Math.Min( PersonBox.Right, Box.Right );
            var UnionEndY = Math.Min( PersonBox.Bottom, Box.Bottom );
            var UnionWidth = Math.Max( 0F, ( UnionEndX - UnionStartX ) );
            var UnionHeight = Math.Max( 0f, ( UnionEndY - UnionStartY ) );
            var UnionArea = ( UnionWidth * UnionHeight );

            return UnionArea / Box.Area;
        }

        public override float MarkerIoU( YoloResult Result )
        {
            return MarkerIoU( new BoundingBox( Result ) );
        }

        public override ITracker LinkTracker
        {
            get;
            set;
        } = null;

        public override bool MarkerMatch
        {
            get;
            set;
        } = false;

        public override TRACKER_DETECT_TYPE PersonTrackerDetectType => _TrackerDetectType;
        public override TRACKER_DETECT_TYPE CarTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override TRACKER_DETECT_TYPE CarStayTrackerDetectType => TRACKER_DETECT_TYPE.DETECT_NORMAL;

        public override void UpdateArea()
        {
            base.UpdateArea();

            if ( _Area != APPEARANCE_AREA.AREA_UNKNOWN )
            {
                if ( _AreaChange == true )
                {
                    if ( MarkerMatch == false )
                    {
                        if ( ( _TrackerDetectType == TRACKER_DETECT_TYPE.DETECT_NORMAL ) &&
                            ( _AreaChangeFrameCount > DetectorApp.Instance.AreaChangeWaitFrames ) )
                        {
                            _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_ERROR;
                        }
                    }
                    else
                    {
                        _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;
                    }
                }
                else
                {
                    _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;
                }
            }
            else
            {
                _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;
            }
        }

        public override void PersonErrorSended()
        {
            if ( _TrackerDetectType == TRACKER_DETECT_TYPE.DETECT_ERROR )
            {
                _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_ERROR_SENDED;
            }
        }

        public override void CarErrorSended()
        {
        }

        public override void CarStayErrorSended()
        {
        }

        public override void DrawTrackingDetailData( Mat DrawTarget )
        {
            if ( _Area == APPEARANCE_AREA.AREA_UNKNOWN )
            {
                return;
            }
            if ( _TimeSinceUpdate > 5 )
            {
                return;
            }

            var DrawPoint = new CvPoint( Convert.ToInt32( _YoloResultBox.Left ), Convert.ToInt32( _YoloResultBox.Top - 5F ) );
            if ( _TimeSinceUpdate > 1 )
            {
                DrawPoint = new CvPoint( Convert.ToInt32( _PredictBox.Left ), Convert.ToInt32( _PredictBox.Top - 5F ) );
            }

            var ObjRect = _YoloResultBox.ToRect();
#if DEBUG
            var ObjSmallRect = _YoloSmallResultBox.ToRect();
            var PredictRect = _PredictBox.ToRect();
#endif
            Scalar DrawScalar = DetectorApp.Instance.OKColor;

            if ( _AreaChange == true )
            {
                if ( MarkerMatch == false )
                {
                    return;
                }
                if ( MarkerMatch == true )
                {
                    DrawScalar = DetectorApp.Instance.OKColor;
                }
            }
            else
            {
                if ( MarkerMatch == false )
                {
                    if ( ( _TrackerDetectType != TRACKER_DETECT_TYPE.DETECT_ERROR ) && ( _TrackerDetectType != TRACKER_DETECT_TYPE.DETECT_ERROR_SENDED ) )
                    {
                        DrawScalar = DetectorApp.Instance.PersonColor;
                    }
                    else
                    {
                        DrawScalar = DetectorApp.Instance.AlarmColor;
                    }
                }
                else
                {
                    DrawScalar = DetectorApp.Instance.OKColor;
                }
            }
            Cv2.Rectangle( DrawTarget, ObjRect, DrawScalar, 2 );
#if DEBUG
            Cv2.Rectangle( DrawTarget, ObjSmallRect, DrawScalar, 2 );
            //Cv2.Rectangle( DrawTarget, PredictRect, Scalar.WhiteSmoke, 2 );
#endif
            for ( int i = 1 ; i < _History.Count ; i++ )
            {
                Cv2.Line( DrawTarget, _History[ i - 1 ], _History[ i ], DrawScalar, 2 );
            }
            Cv2.Circle( DrawTarget, _PursuitPoint, 4, DrawScalar, -1 );
            Cv2.PutText( DrawTarget, $"{_PersonName}(#{TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.White, 3 );
            Cv2.PutText( DrawTarget, $"{_PersonName}(#{TrackingId})", DrawPoint, HersheyFonts.HersheyComplexSmall, 0.75, Scalar.Black, 1 );
        }

        public override bool IsOverlap( ITracker OtherTracker )
        {
            return ( _YoloResultBox.IoU( OtherTracker.BoundingBox ) >= _SORT.IoUThreshold );
        }

        public override void ResetStatus()
        {
            LinkTracker = null;
            MarkerMatch = false;
            _TrackerDetectType = TRACKER_DETECT_TYPE.DETECT_NORMAL;
            _Area = APPEARANCE_AREA.AREA_UNKNOWN;
            _History.Clear();
            InitKalmanFilter();
        }

        public override float IoU( YoloResult Result )
        {
            var SmallResultBoX= BoundingBox.ToSmall( Result, _SmallRatio );

            return _PredictBox.IoU( SmallResultBoX );
            //return _PredictBox.IoU( Result );
            //return _YoloResultBox.IoU( Result );
        }
    }
}
