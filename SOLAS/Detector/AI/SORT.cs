﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using GDIPoint = System.Drawing.Point;
using static System.Diagnostics.Debug;

using OpenCvSharp;
using YoloCSharp;
using CvPoint = OpenCvSharp.Point;

namespace CES.SOLAS.Detector.AI
{
    /// <summary>
    /// SORT: A Simple, Online and Realtime Tracker
    /// </summary>
    public sealed class SORT
    {
        readonly GraphicsPath DETECT_AREA_A;
        readonly GraphicsPath DETECT_AREA_B;
        readonly GraphicsPath DETECT_AREA;

        //  2020/07/20 車両停滞誤検知修正
        //readonly RectangleF DETECT_AREA_B_RECT;
        readonly Region DETECT_AREA_B_REGION;

        readonly uint _MaxAge;
        readonly float _IoUThreshold;
        readonly Dictionary<uint, Dictionary<uint, ITracker>> _Trackers;
        readonly Dictionary<uint, string> _Names;

        uint _FrameNumber = 0;
        uint _NewPersonTrackingId = 0;
        uint _NewMarkerTrackingId = 0;
        uint _NewCarTrackingId = 0;
        uint _NewCareerPaletteTrackingId = 0;

        float _AreaWidth;
        float _AreaHeight;

        public SORT( float Width, float Height, uint MaxAge, float IoUThreshold )
        {
            _MaxAge = MaxAge;
            _IoUThreshold = IoUThreshold;
            _AreaWidth = Width;
            _AreaHeight = Height;
            _Trackers = new Dictionary<uint, Dictionary<uint, ITracker>>
            {
                [ DetectorApp.Instance.PersonId ] = new Dictionary<uint, ITracker>(),
                [ DetectorApp.Instance.MarkerId ] = new Dictionary<uint, ITracker>(),
                [ DetectorApp.Instance.CarId ] = new Dictionary<uint, ITracker>(),
                [ DetectorApp.Instance.CareerPaletteId ] = new Dictionary<uint, ITracker>()
            };

            _Names = new Dictionary<uint, string>
            {
                [ DetectorApp.Instance.PersonId ] = DetectorApp.Instance.PersonName,
                [ DetectorApp.Instance.MarkerId ] = DetectorApp.Instance.MarkerName,
                [ DetectorApp.Instance.CarId ] = DetectorApp.Instance.CarName,
                [ DetectorApp.Instance.CareerPaletteId ] = DetectorApp.Instance.CareerPaletteName
            };

            DETECT_AREA = new GraphicsPath();
            DETECT_AREA.AddPolygon( DetectorApp.Instance.DetectArea );
            DETECT_AREA.CloseFigure();

            DETECT_AREA_A = new GraphicsPath();
            DETECT_AREA_A.AddPolygon( DetectorApp.Instance.DetectAreaA );
            DETECT_AREA_A.CloseFigure();

            DETECT_AREA_B = new GraphicsPath();
            DETECT_AREA_B.AddPolygon( DetectorApp.Instance.DetectAreaB );
            DETECT_AREA_B.CloseFigure();

            //  2020/07/20 車両停滞誤検知修正
            //DETECT_AREA_B_RECT = DETECT_AREA_B.GetBounds();
            DETECT_AREA_B_REGION = new Region( DETECT_AREA_B );
        }

        public APPEARANCE_AREA InDetectArea( int XPos, int YPos )
        {
            if ( DETECT_AREA_A.IsVisible( XPos, YPos ) == true )
            {
                return APPEARANCE_AREA.AREA_A;
            }

            if ( DETECT_AREA_B.IsVisible( XPos, YPos ) == true )
            {
                return APPEARANCE_AREA.AREA_B;
            }

            if ( DETECT_AREA.IsVisible( XPos, YPos ) == true )
            {
                return APPEARANCE_AREA.AREA_NEUTRAL;
            }

            return APPEARANCE_AREA.AREA_UNKNOWN;
        }

        public APPEARANCE_AREA InDetectArea( CvPoint Pt )
        {
            return InDetectArea( Pt.X, Pt.Y );
        }

        public bool ObjectStayDetectAreaB( BoundingBox Box )
        {
            //  2020/07/20 車両停滞誤検知修正
            //return DETECT_AREA_B_RECT.IntersectsWith( RectangleF.FromLTRB( Box.Left, Box.Top, Box.Right, Box.Bottom ) );
            return DETECT_AREA_B_REGION.IsVisible( RectangleF.FromLTRB( Box.Left, Box.Top, Box.Right, Box.Bottom ) );
        }

        public float AreaWidth
        {
            get
            {
                return _AreaWidth;
            }
            set
            {
                _AreaWidth = value;
                foreach ( var ObjectKey in _Trackers.Keys )
                {
                    foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                    {
                        try
                        {
                            _Trackers[ ObjectKey ][ TrackerKey ].AreaWidth = _AreaWidth;
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        public float AreaHeight
        {
            get
            {
                return _AreaHeight;
            }
            set
            {
                _AreaHeight = value;
                foreach ( var ObjectKey in _Trackers.Keys )
                {
                    foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                    {
                        try
                        {
                            _Trackers[ ObjectKey ][ TrackerKey ].AreaHeight = _AreaHeight;
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        public float IoUThreshold => _IoUThreshold;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Detects">a numpy array of detections in the format[[x1, y1, x2, y2, score], [x1, y1, x2, y2, score],...]</param>
        /// <remarks>
        /// this method must be called once for each frame even with empty detections.
        /// The number of objects returned may differ from the number of detections provided.
        /// </remarks>
        public void Update( List<YoloResult> Detects, float SmallRatio )
        {
            //  フレーム番号更新
            unchecked
            {
                _FrameNumber++;
            }

            foreach ( var ObjectKey in _Trackers.Keys )
            {
                //  トラッキング対象オブジェクトを抽出する。
                var TrackTargets = TargetObject( Detects, ObjectKey );

                //  全てのトラッカーで予測を行う
                AllPredict( ObjectKey );

                //  既に追跡しているトラッカーとオブジェクトとのマッチング
                Matching( ObjectKey, TrackTargets, out List<YoloResult> Results, SmallRatio );

                //  新しい対象オブジェクトを追加
                AddTracker( ObjectKey, Results, SmallRatio );
            }

            //  ヘルメットと人のマッチング
            Dictionary<uint, ITracker> Markers = new Dictionary<uint, ITracker>( _Trackers[ DetectorApp.Instance.MarkerId ] );
            foreach ( var PersonKey in _Trackers[ DetectorApp.Instance.PersonId ].Keys )
            {
                var MatchId = uint.MaxValue;
                var MaxMatch = 0F;

                foreach ( var MarkerKey in Markers.Keys )
                {
                    var MatchIoU = _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].MarkerIoU( Markers[ MarkerKey ].BoundingBox );
                    if ( MaxMatch < MatchIoU )
                    {
                        MaxMatch = MatchIoU;
                        MatchId = MarkerKey;
                    }
                }

                if ( Markers.ContainsKey( MatchId ) )
                {
                    if ( ( _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker == null ) ||
                        ( _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker.IsDisposed == true ) )
                    {
                        if ( ( DetectorApp.Instance.EnableDetectAfterAlarm == true ) ||
                            ( _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].PersonTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_NORMAL ) )
                        {
                            _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker = _Trackers[ DetectorApp.Instance.MarkerId ][ MatchId ];
                            _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].MarkerMatch = true;

                            _Trackers[ DetectorApp.Instance.MarkerId ][ MatchId ].LinkTracker = _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ];
                        }
                    }
                    else if ( _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker.TrackingId != _Trackers[ DetectorApp.Instance.MarkerId ][ MatchId ].TrackingId )
                    {
                        if ( ( DetectorApp.Instance.EnableDetectAfterAlarm == true ) ||
                            ( _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].PersonTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_NORMAL ) )
                        {
                            _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker.LinkTracker = null;
                            _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].LinkTracker = _Trackers[ DetectorApp.Instance.MarkerId ][ MatchId ];
                            _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ].MarkerMatch = true;

                            _Trackers[ DetectorApp.Instance.MarkerId ][ MatchId ].LinkTracker = _Trackers[ DetectorApp.Instance.PersonId ][ PersonKey ];
                        }
                    }

                    Markers.Remove( MatchId );
                }
            }

            //  検知領域の重なりチェック
            bool Reset = false;
            do
            {
                Reset = false;

                foreach ( var OuterPersonKey in _Trackers[ DetectorApp.Instance.PersonId ].Keys )
                {
                    foreach ( var InnerPersonKey in _Trackers[ DetectorApp.Instance.PersonId ].Keys )
                    {
                        if ( OuterPersonKey == InnerPersonKey )
                        {
                            continue;
                        }

                        PersonTracker PersonA = null;
                        PersonTracker PersonB = null;
                        if ( _Trackers[ DetectorApp.Instance.PersonId ][ OuterPersonKey ] is PersonTracker )
                        {
                            PersonA = _Trackers[ DetectorApp.Instance.PersonId ][ OuterPersonKey ] as PersonTracker;
                        }
                        if ( _Trackers[ DetectorApp.Instance.PersonId ][ InnerPersonKey ] is PersonTracker )
                        {
                            PersonB = _Trackers[ DetectorApp.Instance.PersonId ][ InnerPersonKey ] as PersonTracker;
                        }
                        if ( ( PersonA == null ) || ( PersonB == null ) )
                        {
                            continue;
                        }

                        //  領域の重なりチェック
                        if ( ( PersonA.IsOverlap( PersonB ) == true ) &&
                            ( PersonA.PersonTrackerDetectType != PersonB.PersonTrackerDetectType ) )
                        {
                            PersonA.ResetStatus();
                            PersonB.ResetStatus();

                            Reset = true;
                            break;
                        }
                    }

                    if ( Reset == true )
                    {
                        break;
                    }
                }
            } while ( Reset == true );


            //  古いオブジェクトを削除
            RemoveTracker();

            //  領域判定
            UpdateArea();
        }

        public void DrawArea( Mat DrawImage )
        {
            var Points = DETECT_AREA.PathPoints;
            var FirstPoint = GDIPoint.Truncate( Points[ 0 ] );
            var LastPoint = GDIPoint.Truncate( Points[ ( Points.Length - 1 ) ] );
            for ( int i = 1 ; i < Points.Length ; i++ )
            {
                var StartPoint = GDIPoint.Truncate( Points[ i - 1 ] );
                var EndPoint = GDIPoint.Truncate( Points[ i ] );
                Cv2.Line( DrawImage, StartPoint.X, StartPoint.Y, EndPoint.X, EndPoint.Y, DetectorApp.Instance.NeutralAreaColor, 2 );
            }
            Cv2.Line( DrawImage, LastPoint.X, LastPoint.Y, FirstPoint.X, FirstPoint.Y, DetectorApp.Instance.NeutralAreaColor, 2 );

            Points = DETECT_AREA_A.PathPoints;
            FirstPoint = GDIPoint.Truncate( Points[ 0 ] );
            LastPoint = GDIPoint.Truncate( Points[ ( Points.Length - 1 ) ] );
            for ( int i = 1 ; i < Points.Length ; i++ )
            {
                var StartPoint = GDIPoint.Truncate( Points[ i - 1 ] );
                var EndPoint = GDIPoint.Truncate( Points[ i ] );
                Cv2.Line( DrawImage, StartPoint.X, StartPoint.Y, EndPoint.X, EndPoint.Y, DetectorApp.Instance.AreaColorA, 2 );
            }
            Cv2.Line( DrawImage, LastPoint.X, LastPoint.Y, FirstPoint.X, FirstPoint.Y, DetectorApp.Instance.AreaColorA, 2 );

            Points = DETECT_AREA_B.PathPoints;
            FirstPoint = GDIPoint.Truncate( Points[ 0 ] );
            LastPoint = GDIPoint.Truncate( Points[ ( Points.Length - 1 ) ] );
            for ( int i = 1 ; i < Points.Length ; i++ )
            {
                var StartPoint = GDIPoint.Truncate( Points[ i - 1 ] );
                var EndPoint = GDIPoint.Truncate( Points[ i ] );
                Cv2.Line( DrawImage, StartPoint.X, StartPoint.Y, EndPoint.X, EndPoint.Y, DetectorApp.Instance.AreaColorB, 2 );
            }
            Cv2.Line( DrawImage, LastPoint.X, LastPoint.Y, FirstPoint.X, FirstPoint.Y, DetectorApp.Instance.AreaColorB, 2 );
        }

        public void DrawTracker( Mat DrawImage )
        {
            foreach ( var ObjectKey in _Trackers.Keys )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    try
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].DrawTrackingData( DrawImage );
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void DrawTrackerDetail( Mat DrawImage )
        {
            foreach ( var ObjectKey in _Trackers.Keys )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    try
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].DrawTrackingDetailData( DrawImage );
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void GetCount( out int DetectCount, out int PersonErrorCount, out int CarErrorCount, out int CarStayErrorCount )
        {
            DetectCount = 0;
            PersonErrorCount = 0;
            CarErrorCount = 0;
            CarStayErrorCount = 0;

            foreach ( var ObjectKey in _Trackers.Keys )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    if ( _Trackers[ ObjectKey ][ TrackerKey ].PersonTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_ERROR )
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].PersonErrorSended();
                        PersonErrorCount++;
                    }

                    if ( _Trackers[ ObjectKey ][ TrackerKey ].CarTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_ERROR )
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].CarErrorSended();
                        CarErrorCount++;
                    }

                    if ( _Trackers[ ObjectKey ][ TrackerKey ].CarStayTrackerDetectType == TRACKER_DETECT_TYPE.DETECT_ERROR )
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].CarStayErrorSended();
                        CarStayErrorCount++;
                    }

                    if ( _Trackers[ ObjectKey ][ TrackerKey ].Area != APPEARANCE_AREA.AREA_UNKNOWN )
                    {
                        DetectCount++;
                    }
                }
            }
        }

        private Dictionary<int, YoloResult> TargetObject( List<YoloResult> Detects, uint TrackingTargetObjectId )
        {
            //  トラッキング対象オブジェクトを抽出する。
            Dictionary<int, YoloResult> TrackTargets = new Dictionary<int, YoloResult>();
            int Count = 0;
            if ( ( Detects != null ) && ( Detects.Count > 0 ) )
            {
                float TrackingTargetDetectThreshold = 1.0F;
                if ( TrackingTargetObjectId == DetectorApp.Instance.PersonId )
                {
                    TrackingTargetDetectThreshold = DetectorApp.Instance.DetectThresholdPerson;
                }
                else if ( TrackingTargetObjectId == DetectorApp.Instance.MarkerId )
                {
                    TrackingTargetDetectThreshold = DetectorApp.Instance.DetectThresholdMarker;
                }
                else if ( TrackingTargetObjectId == DetectorApp.Instance.CarId )
                {
                    TrackingTargetDetectThreshold = DetectorApp.Instance.DetectThresholdCar;
                }
                else if ( TrackingTargetObjectId == DetectorApp.Instance.CareerPaletteId )
                {
                    TrackingTargetDetectThreshold = DetectorApp.Instance.DetectThresholdCareerPalette;
                }

                foreach ( var ResultItem in Detects )
                {
                    if ( ( ResultItem.ObjId == TrackingTargetObjectId ) &&
                        ( ResultItem.Prob >= TrackingTargetDetectThreshold ) )
                    {
                        if ( ( ResultItem.X > _AreaWidth ) || ( ResultItem.Y > _AreaHeight ) ||
                            ( ResultItem.Width > _AreaWidth ) || ( ResultItem.Height > _AreaHeight ) ||
                            ( ( ResultItem.X + ResultItem.Width ) > _AreaWidth ) ||
                            ( ( ResultItem.Y + ResultItem.Height ) > _AreaHeight ) )
                        {
                            continue;
                        }
                        TrackTargets[ ++Count ] = ResultItem;
                    }
                }
            }

            return TrackTargets;
        }

        private void AllPredict( uint ObjectKey )
        {
            //  全てのトラッカーで予測を行う
            if ( _Trackers.ContainsKey( ObjectKey ) == true )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    try
                    {
                        _Trackers[ ObjectKey ][ TrackerKey ].Predict();
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Matching( uint ObjectKey, Dictionary<int, YoloResult> Target, out List<YoloResult> NewDetect, float SmallRatio )
        {
            List<(int Det, uint Trk, float IoU)> IoUMatrix = new List<(int Det, uint Trk, float IoU)>();
            NewDetect = new List<YoloResult>();

            if ( _Trackers.ContainsKey( ObjectKey ) == false )
            {
                return;
            }

            //  全ての組み合わせでIOUを求める
            foreach ( var DetectKey in Target.Keys )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    var TempIOU = _Trackers[ ObjectKey ][ TrackerKey ].IoU( Target[ DetectKey ] );
                    if ( ( float.IsNaN( TempIOU ) == true ) || ( TempIOU < _IoUThreshold ) )
                    {
                        TempIOU = 0F;
                    }

                    IoUMatrix.Add( (DetectKey, TrackerKey, TempIOU) );
                }
            }

            //  IOUの大きいものから順にマッチング
            List<(int Det, uint Trk)> MatchList = new List<(int Det, uint Trk)>();

            int DetectId = 0;
            uint TrackerId = 0;
            float MaxIOU = 0F;

            do
            {
                DetectId = 0;
                TrackerId = 0;
                MaxIOU = 0F;

                foreach ( var (Det, Trk, IoU) in IoUMatrix )
                {
                    bool Matched = false;

                    foreach ( var MatchItem in MatchList )
                    {
                        if ( ( MatchItem.Det == Det ) || ( MatchItem.Trk == Trk ) )
                        {
                            Matched = true;
                            break;
                        }
                    }

                    if ( Matched == true )
                    {
                        continue;
                    }

                    if ( IoU > MaxIOU )
                    {
                        DetectId = Det;
                        TrackerId = Trk;
                        MaxIOU = IoU;
                    }
                }

                if ( MaxIOU > _IoUThreshold )
                {
                    MatchList.Add( (DetectId, TrackerId) );
                }

            } while ( MaxIOU > _IoUThreshold );

            //  新たな認識情報を抽出
            foreach ( var DetectKey in Target.Keys )
            {
                bool Matched = false;

                foreach ( var (Det, Trk) in MatchList )
                {
                    if ( Det == DetectKey )
                    {
                        Matched = true;
                        break;
                    }
                }

                if ( Matched == true )
                {
                    continue;
                }

                NewDetect.Add( Target[ DetectKey ] );
            }

            //  一致したものはUpdate呼び出し
            foreach ( var (Det, Trk) in MatchList )
            {
                _Trackers[ ObjectKey ][ Trk ].Update( _FrameNumber, Target[ Det ] , SmallRatio );
            }
        }

        private void AddTracker( uint ObjectId, List<YoloResult> Target, float SmallRatio )
        {
            //  新しい対象オブジェクトを追加
            if ( ( Target.Count > 0 ) && ( _Trackers.ContainsKey( ObjectId ) == true ) )
            {
                foreach ( var NewYoloResult in Target )
                {
                    //  TODO
                    if ( ObjectId == DetectorApp.Instance.PersonId )
                    {
                        unchecked
                        {
                            ++_NewPersonTrackingId;
                        }
                        _Trackers[ ObjectId ][ _NewPersonTrackingId ] = new PersonTracker( this, _NewPersonTrackingId, _FrameNumber, NewYoloResult, _AreaWidth, _AreaHeight, SmallRatio );
                    }
                    else if ( ObjectId == DetectorApp.Instance.MarkerId )
                    {
                        unchecked
                        {
                            ++_NewMarkerTrackingId;
                        }
                        _Trackers[ ObjectId ][ _NewMarkerTrackingId ] = new MarkerTracker( this, _NewMarkerTrackingId, _FrameNumber, NewYoloResult, _AreaWidth, _AreaHeight );
                    }
                    else if ( ObjectId == DetectorApp.Instance.CarId )
                    {
                        unchecked
                        {
                            ++_NewCarTrackingId;
                        }
                        _Trackers[ ObjectId ][ _NewCarTrackingId ] = new CarTracker( this, _NewCarTrackingId, _FrameNumber, NewYoloResult, _AreaWidth, _AreaHeight );
                    }
                    else if ( ObjectId == DetectorApp.Instance.CareerPaletteId )
                    {
                        unchecked
                        {
                            ++_NewCareerPaletteTrackingId;
                        }
                        _Trackers[ ObjectId ][ _NewCareerPaletteTrackingId ] = new CareerPaletteTracker( this, _NewCareerPaletteTrackingId, _FrameNumber, NewYoloResult, _AreaWidth, _AreaHeight );
                    }
                    
                }
            }
        }

        private void RemoveTracker()
        {
            //  古いオブジェクトを削除
            bool Removed = false;
            do
            {
                Removed = false;

                foreach ( var ObjectKey in _Trackers.Keys )
                {
                    foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                    {
                        if ( _Trackers[ ObjectKey ][ TrackerKey ].CanDelete( _MaxAge ) == true )
                        {
                            _Trackers[ ObjectKey ][ TrackerKey ].Dispose();
                            _Trackers[ ObjectKey ].Remove( TrackerKey );
                            //DetectorApp.Instance.Debug( $"Tracker Dispose [{ObjectKey}][{TrackerKey}]" );
                            Removed = true;
                            break;
                        }
                    }
                    if ( Removed == true )
                    {
                        break;
                    }
                }
            } while ( ( _Trackers.Count > 0 ) && ( Removed == true ) );
        }

        private void UpdateArea()
        {

            foreach ( var ObjectKey in _Trackers.Keys )
            {
                foreach ( var TrackerKey in _Trackers[ ObjectKey ].Keys )
                {
                    _Trackers[ ObjectKey ][ TrackerKey ].UpdateArea();
                }
            }
        }
    }
}