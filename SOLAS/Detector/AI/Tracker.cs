﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

using OpenCvSharp;
using YoloCSharp;

namespace CES.SOLAS.Detector.AI
{
    using CvPoint = Point;

    public abstract class Tracker : ITracker, IDisposable
    {
        protected const int Dim4 = 4;
        protected const int Dim2 = 2;
        protected const int Dim1 = 1;
        protected const int Dim0 = 0;
        protected const int MAX_TRAJECTORY_DEFAULT = 60;
        protected const int MAX_OUT_OF_RANGE_DEFAULT = 5;
        protected const float DEFAULT_BOX_SMALL_RATIO = 1.0F;

        protected static readonly float[,] InitialTransitionMatrix = new float[,]
        {
            { 1, 0, 1, 0 },
            { 0, 1, 0, 1 },
            { 0, 0, 1, 0 },
            { 0, 0, 0, 1 }
        };
        protected static readonly float[,] InitialMeasurementMatrix = new float[,]
        {
            { 1, 0 },
            { 0, 1 }
        };

        protected readonly List<CvPoint> _History;
        protected readonly int _MaxTrajectory;
        protected readonly int _MaxOutOfRange;
        protected readonly Stopwatch _Stopwatch;
        protected readonly SORT _SORT;

        protected BoundingBox _YoloResultBox;
        protected BoundingBox _YoloSmallResultBox;
        protected BoundingBox _PredictBox;
        protected YoloResult _YoloResult;
        protected CvPoint _PursuitPoint;

        protected bool _OutOfRange = false;
        protected float _HalfPredicWidth = 0F;
        protected float _HalfPredicHeight = 0F;
        protected long _LastTicks = 0L;
        protected int _OutOfRangeCount = 0;
        protected uint _TimeSinceUpdate = 0;
        protected APPEARANCE_AREA _Area = APPEARANCE_AREA.AREA_UNKNOWN;
        protected bool _AreaChange = false;
        protected uint _AreaChangeFrameCount = 0;
        protected KalmanFilter _KalmanFilter;
        protected float _SmallRatio = 0F;

        public Tracker(
            SORT SORT,
            uint TrackingId,
            uint FrameNumber,
            YoloResult Result,
            float Width,
            float Height,
            float SmallRatio = DEFAULT_BOX_SMALL_RATIO,
            int MaxTrajectory = MAX_TRAJECTORY_DEFAULT,
            int MaxOutOfRange= MAX_OUT_OF_RANGE_DEFAULT )
        {
            //  パブリックプロパティ初期化
            this.TrackingId = TrackingId;       //  トラッキングID
            this.FrameNumber = FrameNumber;     //  トラッキング開始時のフレーム番号
            this.AreaWidth = Width;             //  画像の幅
            this.AreaHeight = Height;           //  画像の高さ

            //  readonlyメンバー初期化
            _SORT = SORT;
            _History = new List<CvPoint>();     //  トラッキングオブジェクトの位置履歴
            _MaxTrajectory = MaxTrajectory;
            _MaxOutOfRange = MaxOutOfRange;

            _YoloResultBox = new BoundingBox( Result );     //  矩形の初期表示位置
            _SmallRatio = SmallRatio;
            _YoloSmallResultBox = BoundingBox.ToSmall( Result, _SmallRatio );

            _PredictBox = new BoundingBox( Result );        //  最初の予測領域
            _YoloResult = Result;
            _Stopwatch = Stopwatch.StartNew();

            AddPointHistory();                              //  トラッキングオブジェクトの位置履歴初期化
            UpdateArea();
            //DetectorApp.Instance.Debug( $"Tracker[{this.TrackingId}] First Area = {_Area}" );

            //  カルマンフィルタ生成
            InitKalmanFilter();

            CreatePredictBox( _YoloResultBox.Center );
        }

        protected void InitKalmanFilter()
        {
            _KalmanFilter = new KalmanFilter( Dim4, Dim2, Dim0 );
            using ( var TransitionMatrixMat = new Mat( rows: Dim4, cols: Dim4, type: MatType.CV_32FC1, data: InitialTransitionMatrix ) )
            using ( var MeasurementMatrixMat = new Mat( rows: Dim2, cols: Dim4, type: MatType.CV_32FC1, data: InitialMeasurementMatrix ) )
            {
                _KalmanFilter.StatePost.Set<float>( 0, 0, _YoloResultBox.Center.X );
                _KalmanFilter.StatePost.Set<float>( 1, 0, _YoloResultBox.Center.Y );
                _KalmanFilter.StatePost.Set<float>( 2, 0, 0F );
                _KalmanFilter.StatePost.Set<float>( 3, 0, 0F );

                TransitionMatrixMat.CopyTo( _KalmanFilter.TransitionMatrix );
                MeasurementMatrixMat.CopyTo( _KalmanFilter.MeasurementMatrix );

                Cv2.SetIdentity( _KalmanFilter.MeasurementMatrix, new Scalar( 1.0 ) );
                Cv2.SetIdentity( _KalmanFilter.ProcessNoiseCov, new Scalar( 1.0e-5 ) );
                Cv2.SetIdentity( _KalmanFilter.MeasurementNoiseCov, new Scalar( 0.1 ) );
                Cv2.SetIdentity( _KalmanFilter.ErrorCovPost, new Scalar( 1.0 ) );
            }
        }

        public uint TrackingId
        {
            get;
        }

        public float AreaWidth
        {
            get;
            set;
        }

        public float AreaHeight
        {
            get;
            set;
        }

        public uint FrameNumber
        {
            get;
            set;
        }
        public abstract ITracker LinkTracker
        {
            get;
            set;
        }
        public abstract bool MarkerMatch
        {
            get;
            set;
        }

        public BoundingBox BoundingBox => _YoloResultBox;

        public YoloResult YoloResult => _YoloResult;

        public CvPoint PursuitPoint => _PursuitPoint;

        public virtual bool CanDelete( uint MaxAge )
        {
            return ( ( _OutOfRange == true ) || ( _TimeSinceUpdate >= MaxAge ) );
        }

        //public virtual void Dispose()
        //{
        //    _KalmanFilter?.Dispose();
        //}

        public abstract void DrawTrackingData( Mat DrawTarget );

        public abstract bool IsOverlap( ITracker OtherTracker );
        public abstract void ResetStatus();

        public virtual void Predict()
        {
            using ( var PredictMat = _KalmanFilter.Predict() )
            {
                CvPoint pt = FromMat( PredictMat );
                CreatePredictBox( pt );
                _TimeSinceUpdate++;
            }
        }

        public virtual void Update( uint FrameNumber, YoloResult Result, float SmallRatio )
        {
            this.FrameNumber = FrameNumber;

            _TimeSinceUpdate = 0;

            var NewBox = new BoundingBox( Result );

            _YoloResultBox = NewBox;
            _YoloSmallResultBox = BoundingBox.ToSmall( _YoloResultBox, SmallRatio );
            _YoloResult = Result;
            AddPointHistory();

            long CurTicks = _Stopwatch.ElapsedMilliseconds;
            float TransitionValue = ( float )( CurTicks - _LastTicks );
            _LastTicks = CurTicks;

            using ( var Measurement = new Mat( rows: Dim2, cols: Dim1, type: MatType.CV_32FC1 ) )
            {
                _KalmanFilter.TransitionMatrix.Set<float>( 0, 2, TransitionValue );
                _KalmanFilter.TransitionMatrix.Set<float>( 1, 3, TransitionValue );

                Measurement.Set<float>( 0, 0, _YoloResultBox.Center.X );
                Measurement.Set<float>( 1, 0, _YoloResultBox.Center.Y );
                using ( var Corrected = _KalmanFilter.Correct( Measurement ) )
                {
                }
            }
        }

        protected virtual void AddPointHistory()
        {
            var PursuitX = _YoloResultBox.Center.X;
            var PursuitY = ( _YoloResultBox.Top + ( ( _YoloResultBox.Bottom - _YoloResultBox.Top ) / 10F * 9.5F ) );

            if ( _TimeSinceUpdate > 1 )
            {
                PursuitX = _PredictBox.Center.X;
                PursuitY = ( _PredictBox.Top + ( ( _PredictBox.Bottom - _PredictBox.Top ) / 10F * 9.5F ) );
            }

            //PursuitY = _YoloResultBox.Bottom;

            _PursuitPoint = new CvPoint( PursuitX, PursuitY );

            _History.Add( _PursuitPoint );
            while ( _History.Count > _MaxTrajectory )
            {
                _History.RemoveAt( 0 );
            }
        }
        protected virtual void CreatePredictBox( CvPoint PredictCenter )
        {
            //  予測点が画像の外に出ていたら前回実績を予測矩形とする
            if ( _OutOfRange == true )
            {
                _PredictBox = _YoloResultBox.Clone();
                return;
            }

            if ( ( PredictCenter.X < 0 ) || ( PredictCenter.X > AreaWidth ) ||
                ( PredictCenter.Y < 0 ) || ( PredictCenter.Y > AreaHeight ) )
            {
                _OutOfRangeCount++;

                if ( _OutOfRangeCount >= _MaxOutOfRange )
                {
                    _OutOfRange = true;
                    _PredictBox = _YoloResultBox.Clone();
                    return;
                }
            }
            else
            {
                _OutOfRangeCount = 0;
            }

            var HalfWidth = ( ( _YoloResultBox.Right - _YoloResultBox.Left ) / 2F );
            var HalfHeight = ( ( _YoloResultBox.Bottom - _YoloResultBox.Top ) / 2F );

            if ( _HalfPredicWidth < HalfWidth )
            {
                _HalfPredicWidth = HalfWidth;
            }
            else if ( ( _HalfPredicWidth * 0.9F ) > HalfWidth )
            {
                _HalfPredicWidth = ( _HalfPredicWidth * 0.9F );
            }

            if ( _HalfPredicHeight < HalfHeight )
            {
                _HalfPredicHeight = HalfHeight;
            }
            else if ( ( _HalfPredicHeight * 0.9F ) > HalfHeight )
            {
                _HalfPredicHeight = ( _HalfPredicHeight * 0.9F );
            }

            _PredictBox = new BoundingBox(
                ( ( PredictCenter.X - _HalfPredicWidth ) < 0F ) ? 0F : ( PredictCenter.X - _HalfPredicWidth ),
                ( ( PredictCenter.Y - _HalfPredicHeight ) < 0F ) ? 0F : ( PredictCenter.Y - _HalfPredicHeight ),
                ( ( PredictCenter.X + _HalfPredicWidth ) > AreaWidth ) ? AreaWidth : ( PredictCenter.X + _HalfPredicWidth ),
                ( ( PredictCenter.Y + _HalfPredicHeight ) > AreaHeight ) ? AreaHeight : ( PredictCenter.Y + _HalfPredicHeight ) );

            if ( BoundingBox.IoU( _YoloResultBox, _PredictBox ) <= 0F )
            {
                _PredictBox = _YoloResultBox.Clone();
            }
        }

        public virtual float IoU( BoundingBox Box )
        {
            if ( _OutOfRange == true )
            {
                return 0F;
            }

            return _PredictBox.IoU( Box );
            //return _YoloResultBox.IoU( Box );
        }

        public virtual float IoU( YoloResult Result )
        {
            return _PredictBox.IoU( Result );
            //return _YoloResultBox.IoU( Result );
        }

        protected CvPoint FromMat( Mat Data )
        {
            var FloatData = new float[ Data.Width * Data.Height ];
            Marshal.Copy( Data.Data, FloatData, 0, FloatData.Length );

            return new CvPoint( Convert.ToInt32( FloatData[ 0 ] ), Convert.ToInt32( FloatData[ 1 ] ) );
        }

        public abstract float MarkerIoU( BoundingBox Box );

        public abstract float MarkerIoU( YoloResult Result );

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    if ( ( LinkTracker != null ) && ( LinkTracker.IsDisposed == false ) )
                    {
                        LinkTracker.LinkTracker = null;
                        LinkTracker = null;
                    }

                    _KalmanFilter?.Dispose();
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~Tracker() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }

        public virtual void UpdateArea()
        {
            var CurArea = _SORT.InDetectArea( _PursuitPoint.X, _PursuitPoint.Y );

            //  現在位置が領域外の場合
            if ( CurArea == APPEARANCE_AREA.AREA_UNKNOWN )
            {
                if ( _Area != APPEARANCE_AREA.AREA_UNKNOWN )
                {
                    _AreaChange = false;
                    _AreaChangeFrameCount = 0;
                    _Area = APPEARANCE_AREA.AREA_UNKNOWN;
                }
                return;
            }
            //  現在位置が領域Aの場合
            else if ( CurArea == APPEARANCE_AREA.AREA_A )
            {
                //  直前の位置が領域外だった場合はエリア設定のみでエリア変化なし
                if ( _Area == APPEARANCE_AREA.AREA_UNKNOWN )
                {
                    _AreaChange = false;
                    _AreaChangeFrameCount = 0;
                    _Area = CurArea;
                }
                //  直前の位置が領域Bだった場合はエリア変化検知とする
                else if ( ( _Area == APPEARANCE_AREA.AREA_B ) || ( _Area == APPEARANCE_AREA.AREA_NEUTRAL ) )
                {
                    _AreaChange = true;
                    _AreaChangeFrameCount = 0;
                    _Area = CurArea;
                }
                //  直前の位置が領域Aだった場合は必要に応じてカウントアップ
                else if ( _Area == APPEARANCE_AREA.AREA_A )
                {
                    if ( ( _AreaChange == true ) && ( MarkerMatch == false ) )
                    {
                        _AreaChangeFrameCount++;
                    }
                }
            }
            //  現在位置が領域Bの場合
            else if ( CurArea == APPEARANCE_AREA.AREA_B )
            {
                //  直前の位置が領域外だった場合はエリア設定のみでエリア変化なし
                if ( _Area == APPEARANCE_AREA.AREA_UNKNOWN )
                {
                    _AreaChange = false;
                    _AreaChangeFrameCount = 0;
                    _Area = CurArea;
                }
                //  直前の位置が領域Aだった場合はエリア変化検知とする
                else if ( ( _Area == APPEARANCE_AREA.AREA_A ) || ( _Area == APPEARANCE_AREA.AREA_NEUTRAL ) )
                {
                    _AreaChange = true;
                    _AreaChangeFrameCount = 0;
                    _Area = CurArea;
                }
                //  直前の位置が領域Aだった場合は必要に応じてカウントアップ
                else if ( _Area == APPEARANCE_AREA.AREA_B )
                {
                    if ( ( _AreaChange == true ) && ( MarkerMatch == false ) )
                    {
                        _AreaChangeFrameCount++;
                    }
                }
            }
            //  現在位置が中立領域だった場合
            else if ( CurArea == APPEARANCE_AREA.AREA_NEUTRAL)
            {
                if ( ( _Area == APPEARANCE_AREA.AREA_A ) || ( _Area == APPEARANCE_AREA.AREA_B ) )
                {
                    if ( ( _AreaChange == true ) && ( MarkerMatch == false ) )
                    {
                        _AreaChangeFrameCount++;
                    }
                }
                else if ( _Area == APPEARANCE_AREA.AREA_UNKNOWN )
                {
                    //  ありえない・・・はず
                }
                else if ( _Area == APPEARANCE_AREA.AREA_NEUTRAL )
                {
                    //  ありえない・・・はず
                }
            }
        }

        public abstract void PersonErrorSended();
        public abstract void CarErrorSended();
        public abstract void CarStayErrorSended();

        public abstract void DrawTrackingDetailData( Mat DrawTarget );

        public bool IsDisposed => disposedValue;

        public APPEARANCE_AREA Area => _Area;

        public abstract TRACKER_DETECT_TYPE PersonTrackerDetectType
        {
            get;
        }
        public abstract TRACKER_DETECT_TYPE CarTrackerDetectType
        {
            get;
        }
        public abstract TRACKER_DETECT_TYPE CarStayTrackerDetectType
        {
            get;
        }
        #endregion
    }
}
