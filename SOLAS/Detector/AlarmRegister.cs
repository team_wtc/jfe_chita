﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using OpenCvSharp;

namespace CES.SOLAS.Detector
{
    using Lib.Threads;
    using Lib.UI;
    using Lib;

    public class AlarmRegister : QueueThread<(DateTime Timestamp, int DetectCount, int PersonErrorCount,int CarErrorCount, int StayErrorCount, Mat Img)>
    {
        protected QueueThread<(int AlarmId, DateTime Timestamp, Mat Img)> _H264Encoder;

        public AlarmRegister( ISOLASApp SOLASApp, QueueThread<(int AlarmId, DateTime Timestamp, Mat Img)> H264Encoder )
            : base( SOLASApp, "Alarm", ThreadPriority.Normal, false, 0 )
        {
            _H264Encoder = H264Encoder;
        }

        protected override bool Init()
        {
            _SOLASApp.Info( "Alarm Register Startup." );
            return base.Init();
        }

        public void ResetEncoder( QueueThread<(int AlarmId, DateTime Timestamp, Mat Img)> H264Encoder )
        {
            _H264Encoder = H264Encoder;
        }

        protected override bool ItemRoutine( (DateTime Timestamp, int DetectCount, int PersonErrorCount, int CarErrorCount, int StayErrorCount, Mat Img) Item )
        {
            //  検知オブジェクトが無い場合はそのまま静止画送信
            if ( Item.DetectCount <= 0 )
            {
                if ( ( Item.Img != null ) && ( Item.Img.IsDisposed == false ) )
                {
                    _H264Encoder?.Enqueue( (0, Item.Timestamp, Item.Img.Clone()) );
                }
                _SOLASApp.StillImageSend( Item.DetectCount, 0, ALARM_TYPE.ALARM_TYPE_UNKNOWN, Item.Timestamp, Item.Img );
            }
            //  検知オブジェクトが有る場合はエラー判定
            else
            {
                //  エラーがある場合はDB登録し個別に静止画送信
                if ( ( Item.PersonErrorCount + Item.CarErrorCount + Item.StayErrorCount ) > 0 )
                {
                    AddAlarm( Item.DetectCount, Item.PersonErrorCount, Item.CarErrorCount, Item.StayErrorCount, Item.Timestamp, Item.Img );
                }
                //  エラーが無い場合はそのまま静止画送信
                else
                {
                    if ( ( Item.Img != null ) && ( Item.Img.IsDisposed == false ) )
                    {
                        _H264Encoder?.Enqueue( (0, Item.Timestamp, Item.Img.Clone()) );
                    }
                    _SOLASApp.StillImageSend( Item.DetectCount, 0, Lib.ALARM_TYPE.ALARM_TYPE_UNKNOWN, Item.Timestamp, Item.Img );
                }
            }
            return true;
        }

        private void AddAlarm( int DetectCount, int PersonErrorCount, int CarErrorCount, int StayErrorCount, DateTime Timestamp, Mat Img )
        {
            Task.Factory.StartNew( () =>
             {
                 try
                 {
                     if ( PersonErrorCount > 0 )
                     {
                         var AlarmId = DetectorApp.Instance.InsertAlarm( ALARM_TYPE.ALARM_TYPE_PERSON, Timestamp, Img );
                         //  動画出力
                         if ( ( Img != null ) && ( Img.IsDisposed == false ) )
                         {
                             _H264Encoder?.Enqueue( (AlarmId, Timestamp, Img.Clone()) );
                         }
                         _SOLASApp.StillImageSend( DetectCount, AlarmId, ALARM_TYPE.ALARM_TYPE_PERSON, Timestamp, Img );
                         _SOLASApp.Info( $"警報登録 ID={AlarmId} TYPE=ALARM_TYPE_PERSON TIME={Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" );
                     }
                     if ( CarErrorCount > 0 )
                     {
                         var AlarmId = DetectorApp.Instance.InsertAlarm( ALARM_TYPE.ALARM_TYPE_CAR, Timestamp, Img );
                         //  動画出力
                         if ( ( Img != null ) && ( Img.IsDisposed == false ) )
                         {
                             _H264Encoder?.Enqueue( (AlarmId, Timestamp, Img.Clone()) );
                         }
                         _SOLASApp.StillImageSend( DetectCount, AlarmId, ALARM_TYPE.ALARM_TYPE_CAR, Timestamp, Img );
                         _SOLASApp.Info( $"警報登録 ID={AlarmId} TYPE=ALARM_TYPE_CAR TIME={Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" );
                     }
                     if ( StayErrorCount > 0 )
                     {
                         var AlarmId = DetectorApp.Instance.InsertAlarm( ALARM_TYPE.ALARM_TYPE_CAR_STAY, Timestamp, Img );
                         //  動画出力
                         if ( ( Img != null ) && ( Img.IsDisposed == false ) )
                         {
                             _H264Encoder?.Enqueue( (AlarmId, Timestamp, Img.Clone()) );
                         }
                         _SOLASApp.StillImageSend( DetectCount, AlarmId, ALARM_TYPE.ALARM_TYPE_CAR_STAY, Timestamp, Img );
                         _SOLASApp.Info( $"警報登録 ID={AlarmId} TYPE=ALARM_TYPE_CAR_STAY TIME={Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" );
                     }
                 }
                 catch ( ThreadAbortException )
                 {
                     throw;
                 }
                 catch(Exception e )
                 {
                     _SOLASApp.Exception( "警報登録失敗", e );
                 }
             } );
        }
    }
}
