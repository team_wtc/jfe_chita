﻿namespace CES.SOLAS.Detector
{
    partial class DetectForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetectForm));
            this.DetectImagePictureBox = new System.Windows.Forms.PictureBox();
            this.BackGroundTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DetectImagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DetectImagePictureBox
            // 
            this.DetectImagePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("DetectImagePictureBox.BackgroundImage")));
            this.DetectImagePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.DetectImagePictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetectImagePictureBox.Location = new System.Drawing.Point(0, 0);
            this.DetectImagePictureBox.Name = "DetectImagePictureBox";
            this.DetectImagePictureBox.Size = new System.Drawing.Size(800, 450);
            this.DetectImagePictureBox.TabIndex = 0;
            this.DetectImagePictureBox.TabStop = false;
            // 
            // BackGroundTimer
            // 
            this.BackGroundTimer.Interval = 1000;
            this.BackGroundTimer.Tick += new System.EventHandler(this.BackGroundTimer_Tick);
            // 
            // DetectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.DetectImagePictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetectForm";
            this.Text = "Detector";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DetectForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DetectForm_FormClosed);
            this.Load += new System.EventHandler(this.DetectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DetectImagePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox DetectImagePictureBox;
        private System.Windows.Forms.Timer BackGroundTimer;
    }
}

