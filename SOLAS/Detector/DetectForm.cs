﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.SOLAS.Detector
{
    using static Properties.Settings;

    public partial class DetectForm : Form
    {
        readonly DetectorTriggerCallback _TriggerCallback = new DetectorTriggerCallback();
        //readonly FindAlarmMovieFile _FindAlarmMovieFile = new FindAlarmMovieFile();

        bool _ForceClose = false;
        DateTime? _LogDeleteTime = null;

        public DetectForm()
        {
            InitializeComponent();
        }

        private void DetectForm_Load( object sender, EventArgs e )
        {
            try
            {
                _TriggerCallback.TriggerReceive += this._TriggerCallback_TriggerReceive;
                DetectorApp.Instance.AppInitialyze(
                    Default.DB_Server,
                    Default.DB_Password,
                    _TriggerCallback,
                    //_FindAlarmMovieFile,
                    Default.LOG_Folder,
                    Application.ProductName,
                    Default.LOG_Ext,
                    Default.LOG_Size,
                    Default.LOG_Days,
                    Default.LOG_Debug );
            }
            catch ( Exception exp )
            {
                MessageBox.Show( $"{Application.ProductName} を起動できません。{Environment.NewLine}{exp.Message}", $"{Application.ProductName}起動失敗" );
                Close();
                return;
            }

            var TargetScreen = Screen.PrimaryScreen;
#if DEBUG
            foreach ( var Scrn in Screen.AllScreens )
            {
                if ( Scrn.Primary == false )
                {
                    TargetScreen = Scrn;
                    break;
                }
            }
#endif
            Location = new Point( TargetScreen.Bounds.Left, TargetScreen.Bounds.Top );
            Size = new Size( TargetScreen.Bounds.Width, TargetScreen.Bounds.Height );

            Program._StreamingAverage.TimeoutCallback = StreamingTimeout;
            DetectorApp.Instance.InitMonitoring( DetectImagePictureBox, null, null, out _, out _, out _ );
            DetectorApp.Instance.RestartRequest += this.Instance_RestartRequest;
            BackGroundTimer.Enabled = true;
        }

        private void Instance_RestartRequest( object sender, EventArgs e )
        {
            DetectorApp.Instance.RestartStreamThread( StreamingTimeout );
        }

        private void StreamingTimeout()
        {
            DetectImagePictureBox.Image?.Dispose();
            DetectorApp.Instance.RestartStream();
            //DetectorApp.Instance.RestartStreamThread( StreamingTimeout );
        }

        private void _TriggerCallback_TriggerReceive( object sender, Lib.UI.TriggerReceiveEventArgs e )
        {
            if ( e.TriggerType == Lib.ProcessIF.UDP.TRIGGER_TYPE.TRG_APP_RESTART )
            {
                DetectorApp.Instance.Info( $"Restart {Application.ProductName}" );
                _ForceClose = true;
                Application.Restart();
            }
            else if ( e.TriggerType == Lib.ProcessIF.UDP.TRIGGER_TYPE.TRG_APP_STOP )
            {
                DetectorApp.Instance.Info( $"Stop {Application.ProductName}" );
                _ForceClose = true;
                Close();
            }
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            if ( ( keyData & Keys.LWin ) == keyData )
            {
            }

            if ( ( keyData & Keys.Escape ) == keyData )
            {
                _ForceClose = false;
                Close();
                return true;
            }
            return base.ProcessCmdKey( ref msg, keyData );
        }

        private void DetectForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( ( _ForceClose == false ) && (e.CloseReason == CloseReason.UserClosing) )
            {
                var Result = MessageBox.Show(
                    text: $"SOLAS AI検知ソフトウェアを終了します。{Environment.NewLine}" +
                    $"よろしいですか？{Environment.NewLine}{Environment.NewLine}",
                    caption: $"{Application.ProductName}の終了",
                    buttons: MessageBoxButtons.OKCancel,
                    icon: MessageBoxIcon.Question,
                    defaultButton: MessageBoxDefaultButton.Button2 );

                if ( Result == DialogResult.Cancel )
                {
                    e.Cancel = true;
                    return;
                }
            }

            DetectorApp.Instance.Info( $"SOLAS AI検知ソフトウェア({Application.ProductName})終了" );
            try
            {
                DetectorApp.Instance.StopMonitoring();
            }
            catch
            {

            }
            try
            {
                DetectorApp.Instance.AppFinalyze();
            }
            catch
            {

            }
        }

        private void DetectForm_FormClosed( object sender, FormClosedEventArgs e )
        {

        }

        private void BackGroundTimer_Tick( object sender, EventArgs e )
        {
            if ( ( _LogDeleteTime.HasValue == false ) || ( _LogDeleteTime.Value <= DateTime.Now ) )
            {
                DetectorApp.Instance.LogDelete();
                _LogDeleteTime = DateTime.Today.AddDays( 1.0 );
            }
        }
    }
}
