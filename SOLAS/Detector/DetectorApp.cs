﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using RtspClientSharp.RawFrames;

namespace CES.SOLAS.Detector
{
    using Lib;
    using Lib.Camera;
    using Lib.UI;
    using Lib.ProcessIF.TCP;
    using Lib.ProcessIF.UDP;
    using System.Drawing;
    using CES.SOLAS.Lib.Logging;

    public class DetectorApp : SOLASApp<DetectorApp>, ISOLASApp
    {
        public event EventHandler RestartRequest;

        protected readonly object _ImageStockLockObject = new object();

        protected OpenCvStreamReceiver _StreamReceiver = null;
        protected RtspStreamReceiver _RtspStreamReceiver = null;
        protected AI.AIDetector _AIDetector = null;
        protected AI.AIDetectDrawer _AIDetectDrawer = null;
        protected DirectXStreamDrawer _DirectXStreamDrawer = null;
        protected AI.AITracker _AITracker = null;
        protected StillImageSender _ImageSender = null;
        protected AlarmRegister _AlarmRegister = null;
        //protected FrameImageStore _FrameImageStore = null;
        //protected H264Encoder _H264Encoder = null;
        protected ImageStocker _ImageStocker = null;
        protected MovieReqListener _MovieReqListener = null;

        protected APP_STATUS _DetectorStatus = APP_STATUS.APP_STS_UNKNOWN;
        protected ESCAAccessor _ESCAAccessor = null;
        //protected StillImageSender _ToLocalEncoderStillImageSender = null;

        public string DisplayName => string.Empty;

        public APP_STATUS DetectorStatus
        {
            get => _DetectorStatus;
            set => _DetectorStatus = value;
        }
        public bool EnableDetect
        {
            get => _DBAccess.EnableDetect;
            set
            {
                if ( _DBAccess.EnableDetect != value )
                {
                    _DBAccess.EnableDetect = value;
                    if ( _AIDetector != null )
                    {
                        _AIDetector.EnableDetect = _DBAccess.EnableDetect;
                    }

                    foreach ( var Key in _StillImageSenders.Keys )
                    {
                        _StillImageSenders[ Key ].EnableDetect = _DBAccess.EnableDetect;
                    }

                    ApplicationStatus( -1, ( value == true ) ? APP_STATUS.APP_STS_RUNNING : APP_STATUS.APP_STS_RUNNING_NO_DETECT );
                }
            }
        }

        public bool ManualDetectDisable
        {
            get;
            set;
        } = false;

        public uint PersonId => _DBAccess.PersonId;

        public uint MarkerId => _DBAccess.MarkerId;

        public uint CarId => _DBAccess.CarId;

        public uint CareerPaletteId => _DBAccess.CareerPaletteId;

        public Scalar PersonColor => _DBAccess.PersonColor;

        public Scalar MarkerColor => _DBAccess.MarkerColor;

        public Scalar CarColor => _DBAccess.CarColor;

        public Scalar CareerPaletteColor => _DBAccess.CareerPaletteColor;

        public Scalar OKColor => _DBAccess.OKColor;

        public Scalar AlarmColor => _DBAccess.AlarmColor;

        public Scalar StagnantColor => _DBAccess.StagnantColor;

        public string PersonName => _DBAccess.PersonName;

        public string MarkerName => _DBAccess.MarkerName;

        public string CarName => _DBAccess.CarName;

        public string CareerPaletteName => _DBAccess.CareerPaletteName;

        public uint MaxPredictFrame => _DBAccess.MaxPredictFrame;

        public float IOUThreshold => _DBAccess.IOUThreshold;

        public int MessageBoxTimeout
        {
            get
            {
                try
                {
                    return ( _DBAccess.MessageBoxTimeout * 1000 );
                }
                catch
                {
                    return 5000;
                }
            }
        }

        public bool EnableCameraControl => _DBAccess.EnableCameraControl;

        public int StillImageShortInterval
        {
            get => _DBAccess.StillImageShortInterval;
            set => _DBAccess.StillImageShortInterval = value;
        }

        public int StillImageLongInterval
        {
            get => _DBAccess.StillImageLongInterval;
            set => _DBAccess.StillImageLongInterval = value;
        }

        public int TcpKeepAliveWait => _DBAccess.TcpKeepAliveWait;

        public int TcpKeepAliveInterval => _DBAccess.TcpKeepAliveInterval;

        public float StillImageShrinkRatio => _DBAccess.StillImageShrinkRatio;

        public float StreamingImageShrinkRatio => _DBAccess.StreamingImageShrinkRatio;

        public int StreamingSkipFrame => _DBAccess.StreamingSkipFrame;

        public int HeartBeatCycle => _DBAccess.HeartBeatCycle;

        public Point[] DetectAreaA => _DBAccess.DetectAreaA;

        public Point[] DetectAreaB => _DBAccess.DetectAreaB;

        public Scalar AreaColorA => _DBAccess.AreaColorA;

        public Scalar AreaColorB => _DBAccess.AreaColorB;

        public Point[] DetectArea => _DBAccess.DetectArea;

        public int AreaChangeWaitFrames => _DBAccess.AreaChangeWaitFrames;

        public int CarStayAlarmWaitSeconds => _DBAccess.CarStayAlarmWaitSeconds;

        public bool EnableCareerPaletteStayAlarm => _DBAccess.EnableCareerPaletteStayAlarm;

        public Scalar NeutralAreaColor => _DBAccess.NeutralAreaColor;

        public int AlarmMoviePreSeconds
        {
            get => _DBAccess.AlarmMoviePreSeconds;
            set => _DBAccess.AlarmMoviePreSeconds = value;
        }

        public int AlarmMoviePostSeconds
        {
            get => _DBAccess.AlarmMoviePostSeconds;
            set => _DBAccess.AlarmMoviePostSeconds = value;
        }

        public MONITORING_TYPE MonitoringType => MONITORING_TYPE.MON_UNKNOWN;

        public Logger Logger => _Logger;

        public void ApplicationStatus( int MonitoringId, APP_STATUS Status )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( MonitoringId < 0 ) || ( MonitoringId == Key ) )
                    {
                        _PacketSenders[ Key ].ApplicationStatus( Status );
                    }
                }
                catch
                {

                }
            }
            DetectorStatus = Status;
        }

        public void DetectSuppress( int DetectorId, bool Suppress )
        {
            throw new NotImplementedException();
        }

        public void ErrorMessage( string Message )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    _PacketSenders[ Key ].ErrorMessage( Message );
                }
                catch
                {

                }
            }
        }

        public void HeartBeat()
        {
            throw new NotImplementedException();
        }

        public void HomePosition( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    _PacketSenders[ Key ].HomePosition( Pan, Tilt, Zoom, ZoomRatio );
                }
                catch
                {

                }
            }
        }

        public void InitMonitoring( Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3, out int DetectId1, out int DetectId2, out int DetectId3 )
        {
            DetectId1 = 0;
            DetectId2 = 0;
            DetectId3 = 0;

            _DirectXStreamDrawer = new DirectXStreamDrawer( this, "DirectX", MonitorCntl1.Handle, new ControlSizeChangeCallback( MonitorCntl1 ), Program._DirectXAverage );
            _DirectXStreamDrawer.Start();

            if ( ( _ESCAAccessor != null ) && ( _ESCAAccessor.IsAlive == true ) && ( _ESCAAccessor.CameraInitComplete == true ) )
            {
                var (Width, Height) = _ESCAAccessor.ImageSize;
                int FrameInterval = 1;
                if ( _ESCAAccessor.IFrameInterval > _ESCAAccessor.FrameRate )
                {
                    FrameInterval = ( _ESCAAccessor.IFrameInterval / _ESCAAccessor.FrameRate );
                }
                _ImageStocker = new ImageStocker( this, Width, Height, _ESCAAccessor.BitRate, _ESCAAccessor.FrameRate, FrameInterval );
            }
            else
            {
                _ImageStocker = new ImageStocker( this, 1920, 1080, 2048 * 1000, 15, 1 );
            }
            _ImageStocker.Start();

            _AlarmRegister = new AlarmRegister( this, _ImageStocker );
            _AlarmRegister.Start();

            _AITracker = new AI.AITracker( this, _DirectXStreamDrawer, _AlarmRegister, TrackingReductionRate, Program._DetectDrawAverage );
            _AITracker.Start();

            _AIDetector = new AI.AIDetector(
                this,
                Properties.Settings.Default.AI_Config_File,
                Properties.Settings.Default.AI_Weights_File,
                _AITracker,
                DetectorInitCallback,
                EnableDetect,
                Program._DetectAverage );
            _AIDetector.Start();

            var Url = _DBAccess.GetDetectTargetStream;
            _DBAccess.GetCamera( out _, out string Account, out string Password, out _, out _, out _, out _, out _, out _, out _ );

            if ( ( Url.ToLower().StartsWith( "rtsp://" ) == false ) && ( File.Exists( Url ) == true ) )
            {
                _StreamReceiver = new OpenCvStreamReceiver( this, "movie", _DBAccess.GetDetectTargetStream, _AIDetector, Program._StreamingAverage, RestartCallback );
                _StreamReceiver.SetConnectionImage( Properties.Resources.connection );
                _Logger.Debug( $"Create OpenCvStreamReceiver ({Url})" );
            }
            else
            {
                //_H264Decoder = new H264Decoder( this, _AIDetector );
                _RtspStreamReceiver = new RtspStreamReceiver( this, "RTSP", Url, Account, Password, _AIDetector, Program._StreamingAverage, RestartCallback );
                _RtspStreamReceiver.SetConnectionImage( Properties.Resources.connection );
                _Logger.Debug( $"Create RtspStreamReceiver ({Url})" );
            }

            //_FrameImageStore = new FrameImageStore( this );
            //_FrameImageStore.Start();

            _MovieReqListener = new MovieReqListener( this, _DBAccess.TCPMovieListenerPort );
            _MovieReqListener.Start();
        }

        void RestartCallback()
        {
            if ( RestartRequest != null )
            {
                if ( ( RestartRequest.Target != null ) && ( RestartRequest.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke( RestartRequest, this, EventArgs.Empty );
                }
                else
                {
                    RestartRequest( this, EventArgs.Empty );
                }
            }
        }

        public void RestartStreamThread( Action TimeoutCallback )
        {
            if ( _StreamReceiver != null )
            {
                _StreamReceiver.Stop();

                Program._StreamingAverage?.Dispose();
                Program._StreamingAverage = new Lib.Utility.Average
                {
                    TimeoutCallback = TimeoutCallback
                };

                _StreamReceiver = new OpenCvStreamReceiver( this, "Stream", _DBAccess.GetDetectTargetStream, _AIDetector, Program._StreamingAverage, RestartCallback );
                _StreamReceiver.SetConnectionImage( Properties.Resources.connection );
                _StreamReceiver.Start();
            }
            else if ( _RtspStreamReceiver != null )
            {
                //_ImageStocker?.Stop();

                //if ( ( _ESCAAccessor != null ) && ( _ESCAAccessor.IsAlive == true ) && ( _ESCAAccessor.CameraInitComplete == true ) )
                //{
                //    var (Width, Height) = _ESCAAccessor.ImageSize;
                //    int FrameInterval = 1;
                //    if ( _ESCAAccessor.IFrameInterval > _ESCAAccessor.FrameRate )
                //    {
                //        FrameInterval = ( _ESCAAccessor.IFrameInterval / _ESCAAccessor.FrameRate );
                //    }
                //    _ImageStocker = new ImageStocker( this, Width, Height, _ESCAAccessor.BitRate, _ESCAAccessor.FrameRate, FrameInterval );
                //}
                //else
                //{
                //    _ImageStocker = new ImageStocker( this, 1920, 1080, 3000 * 1000, 15, 3 );
                //}

                //_H264Decoder?.Stop();
                _RtspStreamReceiver.Stop();

                Program._StreamingAverage?.Dispose();
                Program._StreamingAverage = new Lib.Utility.Average
                {
                    TimeoutCallback = TimeoutCallback
                };
                //_H264Decoder = new H264Decoder( this, _AIDetector );
                //_H264Decoder.Start();

                //_ImageStocker.Start();
                //_AlarmRegister.ResetEncoder( _ImageStocker );

                _DBAccess.GetCamera( out _, out string Account, out string Password, out _, out _, out _, out _, out _, out _, out _ );
                _RtspStreamReceiver = new RtspStreamReceiver( this, "RTSP", _DBAccess.GetDetectTargetStream, Account, Password, _AIDetector, Program._StreamingAverage, RestartCallback );
                _RtspStreamReceiver.SetConnectionImage( Properties.Resources.connection );
                _RtspStreamReceiver.Start();
            }
        }

        void DetectorInitCallback()
        {
            _StreamReceiver?.Start();
            _RtspStreamReceiver?.Start();
        }

        public void RestartStream()
        {
            _StreamReceiver?.RestartStreaming();
            _RtspStreamReceiver?.RestartStreaming();
            _DirectXStreamDrawer.Enqueue( BitmapConverter.ToMat( Properties.Resources.connection ) );
        }

        public void MoveHomePosition( int DetectorId )
        {
            _ESCAAccessor.Reset();
        }

        public void NewAlarm( int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp )
        {
            throw new NotImplementedException();
        }

        public void NewImage( int ImageId, DateTime Timestamp )
        {
            throw new NotImplementedException();
        }

        public void Restart( int DetectorId )
        {
            throw new NotImplementedException();
        }

        public void SetHomePosition( int DetectorId )
        {
            _ESCAAccessor.Update();
        }

        public void StopMonitoring()
        {
            Program._StreamingAverage?.Dispose();
            Program._DirectXAverage?.Dispose();
            Program._DetectDrawAverage?.Dispose();
            Program._DetectAverage?.Dispose();

            _MovieReqListener?.Stop();
            _StreamReceiver?.Stop();
            _RtspStreamReceiver?.Stop();
            //_H264Decoder?.Stop();
            _AIDetector?.Stop();
            //_AIDetectDrawer.Stop();
            _AITracker?.Stop();
            //_H264Encoder?.Stop();
            _ImageStocker?.Stop();
            _DirectXStreamDrawer.Stop();
        }

        protected override void InitDBAccess( string DBServer, string SAPassword )
        {
            DetectorDBAccess.Instance.InitDBAccess( DBServer, SAPassword );
            
            _ProcessType = DetectorDBAccess.Instance.ProcessType;
            _ApplicationId = DetectorDBAccess.Instance.ApplicationId;
            _DBAccess = DetectorDBAccess.Instance;
        }

        //protected override void InitMovieReqListener( int Port )
        //{
        //    _MovieReqListener = new MovieReqListener( this, Port );
        //    _MovieReqListener.Start();
        //}

        protected override void InitUdpSender()
        {
            int MonitoringCount = _DBAccess.MonitoringCount;
            for ( int i = 0 ; i < MonitoringCount ; i++ )
            {
                _DBAccess.GetMonitoringPCInfo( i, out int Id, out string Addr, out int Port, out _, out _ );
                _PacketSenders[ Id ] = new PacketSender( this, TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR, Addr, Port );
                _PacketSenders[ Id ].Start();
            }
        }

        protected override void TerminateUdpSender()
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                if ( _PacketSenders[ Key ] == null )
                {
                    continue;
                }

                if ( _PacketSenders[ Key ].IsAlive == true )
                {
                    _PacketSenders[ Key ].Stop();
                }
                else
                {
                    _PacketSenders[ Key ].Wait();
                }
            }
        }

        public override void AppInitialyze(
            string DBServer,
            string SAPassword,
            TriggerCallback TrgCallback,
            //FindMovieFile FindMovieFile,
            string LogFolder,
            string LogName,
            string LogExt = "log",
            long LogSize = 5242880,
            int SaveDays = 31,
            bool DebugLog = false )
        {
            base.AppInitialyze( DBServer, SAPassword, TrgCallback, LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );

            ApplicationStatus( -1, APP_STATUS.APP_STS_INIT );
        }

        public string GateName( int DetectorId )
        {
            return _DBAccess.GateName( DetectorId );
        }

        public override void AppFinalyze()
        {
            ApplicationStatus( -1, APP_STATUS.APP_STS_STOPED );

            base.AppFinalyze();
        }

        public void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait )
        {
            _DBAccess.GetCamera( out CameraAddress, out CameraAccount, out CameraPassword, out Pan, out Tilt, out Zoom, out ZoomRatio, out CameraPort, out Interval, out ResetWait );
        }

        protected override void InitCameraAccess()
        {
            _DBAccess.GetCamera(
                out string CameraAddress,
                out string CameraAccount,
                out string CameraPassword,
                out int Pan,
                out int Tilt,
                out int Zoom,
                out int ZoomRatio,
                out int CameraPort,
                out int Interval,
                out int ResetWait );
            _ESCAAccessor = new ESCAAccessor( this, CameraAddress, CameraAccount, CameraPassword, Pan, Tilt, Zoom, ZoomRatio, CameraPort, Interval, ResetWait, Properties.Settings.Default.HomePositionPresetNumber );
            _ESCAAccessor.Start();
        }

        protected override void TerminateCameraAccess()
        {
            _ESCAAccessor?.Stop();
        }

        public void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            _DBAccess.UpdatePTZ( Pan, Tilt, Zoom, ZoomRatio );
            HomePosition( Pan, Tilt, Zoom, ZoomRatio );
        }

        public string AlarmName( ALARM_TYPE AlarmType )
        {
            return _DBAccess.AlarmName( AlarmType );
        }

        public bool DetectorEnableCameraControl( int DetectorId )
        {
            return _DBAccess.DetectorEnableCameraControl( DetectorId );
        }

        protected override void InitStillImageListener()
        {
        }

        protected override void InitImageSender()
        {
            int MonitoringCount = _DBAccess.MonitoringCount;
            for ( int i = 0 ; i < MonitoringCount ; i++ )
            {
                _DBAccess.GetMonitoringPCInfo( i, out int Id, out string Addr, out _, out int TcpPort, out MONITORING_TYPE MonitoringType );
                if ( MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                {
                    _StillImageSenders[ Id ] = new StillImageSender( this, MonitoringType, _DBAccess.EnableDetect, Addr, TcpPort, false );
                    _StillImageSenders[ Id ].Start();
                }
                else if ( MonitoringType == MONITORING_TYPE.MON_STREAMING )
                {
                    _StillImageSenders[ Id ] = new StillImageSender( this, MonitoringType, _DBAccess.EnableDetect, Addr, TcpPort, false );
                    _StillImageSenders[ Id ].Start();
                }
            }
            //_ToLocalEncoderStillImageSender = new StillImageSender( this, MONITORING_TYPE.MON_UNKNOWN, true, "127.0.0.1", 6969, true );
            //_ToLocalEncoderStillImageSender.Start();
        }

        public void StillImageReceive( Lib.ProcessIF.TCP.ImagePacket Packet )
        {
        }

        public void StillImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat StillImage )
        {
            Task.Factory.StartNew( () =>
            {
                try
                {
                    //if ( ( StillImage != null ) && ( StillImage.IsDisposed == false ) )
                    //{
                    //    _ToLocalEncoderStillImageSender.Enqueue( (DetectCount, AlarmId, AlarmType, Timestamp, StillImage.Clone()) );
                    //}

                    foreach ( var Key in _StillImageSenders.Keys )
                    {
                        if ( ( StillImage != null ) && ( StillImage.IsDisposed == false ) )
                        {
                            _StillImageSenders[ Key ].Enqueue( (DetectCount, AlarmId, AlarmType, Timestamp, StillImage.Clone()) );
                        }
                        else
                        {
                            _StillImageSenders[ Key ].Enqueue( (DetectCount, AlarmId, AlarmType, Timestamp, null) );
                        }
                    }
                }
                catch
                {

                }
                finally
                {
                    StillImage?.Dispose();
                }
            } );
        }

        public void H264EncodedImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, byte[] EncodedData )
        {
            try
            {
                foreach ( var Key in _EncodedImageSenders.Keys )
                {
                    //_EncodedImageSenders[ Key ].Enqueue( (DetectCount, AlarmId, AlarmType, Timestamp, EncodedData) );
                }
            }
            catch
            {

            }
            finally
            {
            }
        }

        protected override void TerminateStillImageListener()
        {
        }

        protected override void TerminateStillImageSender()
        {
            foreach ( var Key in _StillImageSenders.Keys )
            {
                if ( _StillImageSenders[ Key ] != null )
                {
                    if ( _StillImageSenders[ Key ].IsAlive == true )
                    {
                        _StillImageSenders[ Key ].Stop();
                    }
                    else
                    {
                        _StillImageSenders[ Key ].Wait();
                    }
                }
            }
            //if ( _ToLocalEncoderStillImageSender != null )
            //{
            //    if ( _ToLocalEncoderStillImageSender.IsAlive == true )
            //    {
            //        _ToLocalEncoderStillImageSender.Stop();
            //    }
            //    else
            //    {
            //        _ToLocalEncoderStillImageSender.Wait();
            //    }
            //}
        }

        public void InitAlarmImageDrawer( Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3 )
        {
        }

        public void StopAlarmImageDrawer()
        {
        }

        public int InsertAlarm( ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage )
        {
            return _DBAccess.InsertAlarm( _ApplicationId, AlarmType, Timestamp, AlarmImage );
        }

        public Color DetectorBackColor( int GateId )
        {
            throw new NotImplementedException();
        }

        public Color DetectorForeColor( int GateId )
        {
            throw new NotImplementedException();
        }

        public int DeleteOldAlarm()
        {
            throw new NotImplementedException();
        }

        public void WriteAlarmMovie( int AlarmId, DateTime Timestamp )
        {
            //_FrameImageStore?.NewAlarm( AlarmId, Timestamp );
        }

        //public void AddAtillImageToStore( DateTime Timestamp, Mat AlarmImage )
        //{
        //    _FrameImageStore?.Enqueue( (Timestamp, AlarmImage) );
        //}

        public void LiveImageReceive( int DetectorId, Bitmap Image )
        {
            throw new NotImplementedException();
        }

        public string MonitorStreamURL( int DetectorId )
        {
            throw new NotImplementedException();
        }

        public override void AddStockImage( DateTime Timestamp, Mat FrameImage )
        {
            lock ( _ImageStockLockObject )
            {
                _FrameImageStock.Add( (Timestamp.Ticks, FrameImage) );
            }
        }

        public override void EraceStockImage( DateTime Timestamp )
        {
            const int FIRST_INDEX = 0;

            lock ( _ImageStockLockObject )
            {
                while ( _FrameImageStock.Count > 0 )
                {
                    if ( _FrameImageStock[ FIRST_INDEX ].Ticks >= Timestamp.Ticks )
                    {
                        return;
                    }
                    DateTime ImageTime = new DateTime( _FrameImageStock[ FIRST_INDEX ].Ticks );
                    //Debug( $"イメージ消去 {ImageTime.ToString("yyyy/MM/dd HH:mm:ss.fff")}" );
                    _FrameImageStock[ FIRST_INDEX ].FrameImage?.Dispose();
                    _FrameImageStock.RemoveAt( FIRST_INDEX );
                    break;
                }
            }
        }

        public override (long Ticks, Mat FrameImage) ReadStockImage( DateTime Timestamp )
        {
            //Debug( $"イメージ要求 {Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" );

            lock ( _ImageStockLockObject )
            {
                foreach ( var (Ticks, FrameImage) in _FrameImageStock )
                {
                    if ( Ticks <= Timestamp.Ticks )
                    {
                        continue;
                    }
                    return ( (Ticks, FrameImage) );
                }
            }

            return ( (0L, null) );
        }

        public float DetectThresholdPerson => _DBAccess.DetectThresholdPerson;
        public float DetectThresholdMarker => _DBAccess.DetectThresholdMarker;
        public float DetectThresholdCar => _DBAccess.DetectThresholdCar;
        public float DetectThresholdCareerPalette => _DBAccess.DetectThresholdCareerPalette;
        public bool EnableDetectAfterAlarm => _DBAccess.EnableDetectAfterAlarm;

        public float TrackingReductionRate => _DBAccess.TrackingReductionRate;

        public int AlarmSaveDays => _DBAccess.GetAlarmSaveDays;

        public string AlarmMovieFolder => @"C:\ces\SOLAS\Movie";
    }
}
