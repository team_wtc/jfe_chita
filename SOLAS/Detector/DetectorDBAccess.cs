﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CES.SOLAS.Detector
{
    using static System.Diagnostics.Debug;

    using CES.SOLAS.Lib;
    using Lib.Data;
    using OpenCvSharp;
    using System.Drawing;
    using GDIPoint = System.Drawing.Point;

    public class DetectorDBAccess : DBAccess<DetectorDBAccess>, IDBAccess
    {
        protected TRIGGER_PROCESS_TYPE _ProcessType = TRIGGER_PROCESS_TYPE.TRG_PROC_UNKNOWN;
        protected int _ApplicationId = -1;
        protected SOLASDataSet.DETECTORDataTable _OwnApplicationInfo = null;
        protected int _UdpPort = -1;
        protected int _MovieRequestPort = 0;

        public TRIGGER_PROCESS_TYPE ProcessType => _ProcessType;

        public int ApplicationId => _ApplicationId;

        public int UDPListenerPort => _UdpPort;

        public int TCPMovieListenerPort => _OwnApplicationInfo[ 0 ].ai_movie_port;

        public int TCPImageListenerPort => throw new NotSupportedException( "DetectorDBAccess では TCPImageListenerPort を使用しません。" );

        public string DisplayName => string.Empty;

        public MONITORING_TYPE MonitoringType => throw new NotSupportedException( "DetectorDBAccess では MonitoringType を使用しません。" );

        public string GetDetectTargetStream => _OwnApplicationInfo[ 0 ].ai_camera_rtsp_url;

        public int MonitoringCount => base.GetMonitoringCount;

        public int DetectorCount => base.GetDetectorCount;

        public bool EnableDetect
        {
            get => ( _OwnApplicationInfo[ 0 ].ai_disable_detection == false );
            set
            {
                _OwnApplicationInfo[ 0 ].ai_disable_detection = ( value == false );
                using ( var Adapter = _Adapter.DETECTORTableAdapter() )
                {
                    Adapter.UpdateDetectionMode( _OwnApplicationInfo[ 0 ].ai_disable_detection, _OwnApplicationInfo[ 0 ].ai_id );
                }
                _OwnApplicationInfo.AcceptChanges();
            }
        }

        public uint PersonId => base.GetPersonId;
        public uint MarkerId => base.GetMarkerId;
        public uint CarId => base.GetCarId;
        public uint CareerPaletteId => base.GetCareerPaletteId;

        public Scalar PersonColor => base.GetPersonColor;

        public Scalar MarkerColor => base.GetMarkerColor;

        public Scalar CarColor => base.GetCarColor;
        public Scalar CareerPaletteColor => base.GetCareerPaletteColor;

        public Scalar OKColor => base.GetOKColor;

        public Scalar AlarmColor => base.GetAlarmColor;

        public Scalar StagnantColor => base.GetStagnantColor;

        public string PersonName => base.GetPersonName;

        public string MarkerName => base.GetMarkerName;

        public string CarName => base.GetCarName;
        public string CareerPaletteName => base.GetCareerPaletteName;

        public uint MaxPredictFrame => base.GetMaxPredictFrame;

        public float IOUThreshold => base.GetIOUThreshold;

        public int MessageBoxTimeout => base.GetMessageBoxTimeout;

        public bool EnableCameraControl
        {
            get
            {
                Assert( _OwnApplicationInfo != null && _OwnApplicationInfo.Count == 1, "Detector Information Not Loaded." );

                return _OwnApplicationInfo[ 0 ].ai_camera_control_enable;
            }
        }

        public int StillImageShortInterval
        {
            get => base.GetStillImageShortInterval;
            set => base.GetStillImageShortInterval = value;
        }

        public int StillImageLongInterval
        {
            get => base.GetStillImageLongInterval;
            set => base.GetStillImageLongInterval = value;
        }

        public int TcpKeepAliveWait => base.GetTcpKeepAliveWait;

        public int TcpKeepAliveInterval => base.GetTcpKeepAliveInterval;

        public float StillImageShrinkRatio => base.GetStillImageShrinkRatio;

        public int HeartBeatCycle => base.GetHeartBeatCycle;

        public GDIPoint[] DetectArea
        {
            get
            {
                try
                {
                    var PointVals = _OwnApplicationInfo[ 0 ].ai_detect_area.Split( ',' );
                    Assert( ( PointVals != null ) && ( PointVals.Length > 0 ) && ( ( PointVals.Length % 2 ) == 0 ), "検知エリア座標数" );

                    List<GDIPoint> Points = new List<GDIPoint>();
                    for ( int i = 0 ; i < PointVals.Length ; i += 2 )
                    {
                        int x = Convert.ToInt32( PointVals[ i ] );
                        int y = Convert.ToInt32( PointVals[ i + 1 ] );
                        Points.Add( new GDIPoint( x, y ) );
                    }

                    return Points.ToArray();
                }
                catch
                {
                    return new GDIPoint[ 0 ];
                }
            }
        }

        public GDIPoint[] DetectAreaA
        {
            get
            {
                try
                {
                    var PointVals = _OwnApplicationInfo[ 0 ].ai_check_area_a.Split( ',' );
                    Assert( ( PointVals != null ) && ( PointVals.Length > 0 ) && ( ( PointVals.Length % 2 ) == 0 ), "検知エリア座標数" );

                    List<GDIPoint> Points = new List<GDIPoint>();
                    for ( int i = 0 ; i < PointVals.Length ; i += 2 )
                    {
                        int x = Convert.ToInt32( PointVals[ i ] );
                        int y = Convert.ToInt32( PointVals[ i + 1 ] );
                        Points.Add( new GDIPoint( x, y ) );
                    }

                    return Points.ToArray();
                }
                catch
                {
                    return new GDIPoint[ 0 ];
                }
            }
        }

        public GDIPoint[] DetectAreaB
        {
            get
            {
                try
                {
                    var PointVals = _OwnApplicationInfo[ 0 ].ai_check_area_b.Split( ',' );
                    Assert( ( PointVals != null ) && ( PointVals.Length > 0 ) && ( ( PointVals.Length % 2 ) == 0 ), "検知エリア座標数" );

                    List<GDIPoint> Points = new List<GDIPoint>();
                    for ( int i = 0 ; i < PointVals.Length ; i += 2 )
                    {
                        int x = Convert.ToInt32( PointVals[ i ] );
                        int y = Convert.ToInt32( PointVals[ i + 1 ] );
                        Points.Add( new GDIPoint( x, y ) );
                    }

                    return Points.ToArray();
                }
                catch
                {
                    return new GDIPoint[ 0 ];
                }
            }
        }

        public Scalar AreaColorA => base.GetAreaColorA;

        public Scalar AreaColorB => base.GetAreaColorB;

        public int MaxDisplayDays => throw new NotSupportedException( "DetectorDBAccess では MaxDisplayDays を使用しません。" );

        public int AddNewAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImageData )
        {
            throw new NotImplementedException();
        }

        public int AddNewImage( int DetectorId, DateTime Timestamp, Mat ImageData )
        {
            throw new NotImplementedException();
        }

        public byte[] AlarmImage( int AlarmId, bool Thumbnail )
        {
            throw new NotImplementedException();
        }

        public void DetectorHandShake()
        {
            foreach ( var Adr in Dns.GetHostAddresses( Dns.GetHostName() ) )
            {
                if ( Adr.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork )
                {
                    continue;
                }
                if ( IPAddress.Loopback.Equals( Adr ) == true )
                {
                    continue;
                }

                using ( var Adapter = _Adapter.DETECTORTableAdapter() )
                {
                    _OwnApplicationInfo = Adapter.GetDataByIPAddress( Adr.ToString() );
                    if ( ( _OwnApplicationInfo != null ) && ( _OwnApplicationInfo.Count == 1 ) )
                    {
                        return;
                    }

                    _OwnApplicationInfo?.Dispose();
                    _OwnApplicationInfo = null;
                }
            }

            throw new ApplicationException( $"{Dns.GetHostName()} の端末情報はデータベースに登録されていません。" );
        }

        public void GetDetectorPCInfo( int Index, out int DetectorId, out string IPAddress, out int Port )
        {
            throw new NotSupportedException( "DetectorDBAccess では GetDetectorPCInfo を使用しません。" );
        }

        public void GetMonitoringPCInfo( int Index, out int MonitoringId, out string IPAddress, out int UdpUdpTriggerPortPort, out int TcpImageListenerPort, out MONITORING_TYPE MonitoringType )
        {
            Assert( Index >= 0 && Index < _SOLASDataSet.MONITOR.Count, "Index out of range." );
            Assert( Enum.IsDefined( typeof( MONITORING_TYPE ), _SOLASDataSet.MONITOR[ Index ].mon_monitor_mode ), $"Monitoring Type Missmatch.({_SOLASDataSet.MONITOR[ Index ].mon_monitor_mode})" );

            MonitoringId = _SOLASDataSet.MONITOR[ Index ].mon_id;
            IPAddress = _SOLASDataSet.MONITOR[ Index ].mon_ip_address;
            UdpUdpTriggerPortPort = _SOLASDataSet.MONITOR[ Index ].mon_trigger_port;
            TcpImageListenerPort = _SOLASDataSet.MONITOR[ Index ].mon_still_image_port;
            MonitoringType = ( MONITORING_TYPE )Enum.ToObject( typeof( MONITORING_TYPE ), _SOLASDataSet.MONITOR[ Index ].mon_monitor_mode );
        }

        public void GetMonitorTarget( out int[] RecNo )
        {
            throw new NotSupportedException( "DetectorDBAccess では GetMonitorTarget を使用しません。" );
        }

        public override void InitDBAccess( string DBServer, string SAPassword )
        {
            _Adapter = new Adapter( DBServer, SAPassword );

            DetectorHandShake();
            LoadPCInfo();
            LoadSystemInfo();

            _ProcessType = TRIGGER_PROCESS_TYPE.TRG_PROC_DETECTOR;
            _ApplicationId = _OwnApplicationInfo[ 0 ].ai_id;
            _UdpPort = _OwnApplicationInfo[ 0 ].ai_trigger_port;
        }

        public void MonitoringHandShake()
        {
            throw new NotSupportedException( "DetectorDBAccess では MonitoringHandShake を使用しません。" );
        }

        public byte[] StillImage( int ImageId )
        {
            throw new NotImplementedException();
        }

        public new string GateName( int DetectorId )
        {
            return base.GateName( DetectorId );
        }

        public void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait )
        {
            Assert( _OwnApplicationInfo != null && _OwnApplicationInfo.Count == 1, "Detector Information Not Loaded." );

            CameraAddress = _OwnApplicationInfo[ 0 ].ai_camera_address;
            CameraAccount = _OwnApplicationInfo[ 0 ].ai_camera_account;
            CameraPassword = _OwnApplicationInfo[ 0 ].ai_camera_password;
            Pan = _OwnApplicationInfo[ 0 ].ai_camera_home_pan;
            Tilt = _OwnApplicationInfo[ 0 ].ai_camera_home_tilt;
            Zoom = _OwnApplicationInfo[ 0 ].ai_camera_home_zoom;
            ZoomRatio = _OwnApplicationInfo[ 0 ].ai_camera_home_zoom_ratio;
            Interval = ( _OwnApplicationInfo[ 0 ].ai_camera_check_interval * 1000 );
            ResetWait = ( _OwnApplicationInfo[ 0 ].ai_camera_reset_wait * 1000 );
            CameraPort = _OwnApplicationInfo[ 0 ].ai_camera_port;
        }

        public void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            _OwnApplicationInfo[ 0 ].ai_camera_home_pan = Pan;
            _OwnApplicationInfo[ 0 ].ai_camera_home_tilt = Tilt;
            _OwnApplicationInfo[ 0 ].ai_camera_home_zoom = Zoom;
            _OwnApplicationInfo[ 0 ].ai_camera_home_zoom_ratio = ZoomRatio;
            _OwnApplicationInfo.AcceptChanges();

            using ( var Adapter = _Adapter.DETECTORTableAdapter() )
            {
                Adapter.UpdatePTZ( Pan, Tilt, Zoom, ZoomRatio, _OwnApplicationInfo[ 0 ].ai_id );
            }
        }

        public string AlarmName( ALARM_TYPE AlarmType )
        {
            return base.GetAlarmName( AlarmType );
        }

        public bool DetectorEnableCameraControl( int DetectorId )
        {
            throw new NotSupportedException( "DetectorDBAccess では DetectorEnableCameraControl を使用しません。" );
        }

        public int InsertAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage )
        {
            return base.InsertNewAlarm( DetectorId, AlarmType, Timestamp, AlarmImage );
        }

        public string AlarmPattern( int GateId, ALARM_TYPE AlarmType )
        {
            throw new NotImplementedException();
        }

        public Color DetectorBackColor( int GateId )
        {
            throw new NotImplementedException();
        }

        public Color DetectorForeColor( int GateId )
        {
            throw new NotImplementedException();
        }

        public int DeleteOldAlarm()
        {
            return 0;
        }

        public string MonitorStreamURL( int DetectorId )
        {
            throw new NotImplementedException();
        }

        public float StreamingImageShrinkRatio => base.GetStreamingImageShrinkRatio;

        public int StreamingSkipFrame => base.GetStreamingSkipFrame;

        public string AlarmDeviceAddress => throw new NotImplementedException();

        public int AlarmDevicePort => throw new NotImplementedException();

        public bool DisableBuzzer
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public int AreaChangeWaitFrames => base.GetAreaChangeWaitFrames;

        public int CarStayAlarmWaitSeconds => base.GetCarStayAlarmWaitSeconds;

        public bool EnableCareerPaletteStayAlarm => base.GetEnableCareerPaletteStayAlarm;

        public Scalar NeutralAreaColor => base.GetNeutralAreaColor;

        public int AlarmMoviePreSeconds
        {
            get => base.GetAlarmMoviePreSeconds;
            set => base.GetAlarmMoviePreSeconds = value;
        }

        public int AlarmMoviePostSeconds
        {
            get => base.GetAlarmMoviePostSeconds;
            set => base.GetAlarmMoviePostSeconds = value;
        }

        public float DetectThresholdPerson
        {
            get
            {
                return base.GetFloatValue( "DetectThresholdPerson", 0.5F );
            }
        }

        public float DetectThresholdMarker
        {
            get
            {
                return base.GetFloatValue( "DetectThresholdMarker", 0.5F );
            }
        }
        public float DetectThresholdCar
        {
            get
            {
                return base.GetFloatValue( "DetectThresholdCar", 0.5F );
            }
        }
        public float DetectThresholdCareerPalette
        {
            get
            {
                return base.GetFloatValue( "DetectThresholdCareerPalette", 0.5F );
            }
        }
        public bool EnableDetectAfterAlarm
        {
            get
            {
                return base.GetBooleanValue( "EnableDetectAfterAlarm", true );
            }
        }

        public float TrackingReductionRate
        {
            get
            {
                return base.GetFloatValue( "TrackingReductionRate", 0.8F );
            }
        }
    }
}
