﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CES.SOLAS.Detector
{
    using Lib.UI;
    using Lib.ProcessIF.UDP;

    public class DetectorTriggerCallback : TriggerCallback
    {
        protected override void TriggerReceiveCallback( IPacket Packet )
        {
            //string PacketInfo = $"Receive ({Packet.ProcessType}:{Packet.ProcessId}:{Packet.Trigger})";

            if ( Packet.Trigger == TRIGGER_TYPE.TRG_HEARTBEAT )
            {
                DetectorApp.Instance.ApplicationStatus( Packet.ProcessId, DetectorApp.Instance.DetectorStatus );
            }
            else if ( ( Packet.Trigger == TRIGGER_TYPE.TRG_DETECT_SUPPRESS ) && ( Packet is DetectSuppressPacket SuppressPacket ) )
            {
                //PacketInfo = $"{PacketInfo} Detect Enable = {( SuppressPacket.Suppress == false )}";
                DetectorApp.Instance.EnableDetect = ( SuppressPacket.Suppress == false );
                if ( SuppressPacket.Suppress == true )
                {
                    DetectorApp.Instance.ManualDetectDisable = true;
                }
                else
                {
                    DetectorApp.Instance.ManualDetectDisable = false;
                }
            }
            else if ( ( Packet.Trigger == TRIGGER_TYPE.TRG_MOVE_HOME ) && ( Packet is MoveHomePacket ) )
            {
                DetectorApp.Instance.MoveHomePosition( DetectorApp.Instance.ApplicationId );
            }
            else if ( ( Packet.Trigger == TRIGGER_TYPE.TRG_SET_HOME ) && ( Packet is SetHomePacket ) )
            {
                DetectorApp.Instance.SetHomePosition( DetectorApp.Instance.ApplicationId );
            }
            else if ( ( Packet.Trigger == TRIGGER_TYPE.TRG_APP_RESTART ) && ( Packet is RestartPacket ) )
            {
                DetectorApp.Instance.Info( "Restart Event Handler Call." );
                EventHandlerCall( Packet );
            }
            else if ( ( Packet.Trigger == TRIGGER_TYPE.TRG_APP_STOP ) && ( Packet is StopPacket ) )
            {
                DetectorApp.Instance.Info( "Stop Event Handler Call." );
                EventHandlerCall( Packet );
            }

            //DetectorApp.Instance.Info( PacketInfo );
        }
    }
}
