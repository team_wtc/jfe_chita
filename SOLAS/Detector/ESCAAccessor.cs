﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CES.SOLAS.Detector
{
    using Lib.Camera;
    using Lib.Threads;
    using Lib.UI;

    public class ESCAAccessor : EventThread
    {
        readonly ESCA _Camera;
        readonly int _ResetWait;

        bool _Reset = false;
        bool _Update = false;
        //DateTime? _ResetTime = null;

        public ESCAAccessor(
            ISOLASApp SOLASApp,
            string CameraAddress,
            string CameraAccount,
            string CameraPassword,
            int Pan,
            int Tilt,
            int Zoom,
            int ZoomRatio,
            int CameraPort,
            int CheckInterval,
            int ResetWait,
            int PresetNo ) : base( SOLASApp, "Camera", CheckInterval )
        {
            _Camera = new ESCA( DetectorApp.Instance, CameraAddress, CameraAccount, CameraPassword, Pan, Tilt, Zoom, ZoomRatio, CameraPort, 0, PresetNo );
            _ResetWait = ResetWait;
        }

        public ESCA.STREAMING_MODE StreamingMode => _Camera.StreamingMode;

        public ESCA.ENCODING_LEVEL EncodingLevel => _Camera.EncodingLevel;

        public int FrameRate => _Camera.FrameRate;

        public bool FrameRarePriority => _Camera.FrameRarePriority;

        public int IFrameInterval => _Camera.IFrameInterval;

        public ESCA.ENCODING_TYPE EncodingType => _Camera.EncodingType;

        public int BitRate => _Camera.BitRate;

        public ESCA.BITRATE_TYPE BitRateType => _Camera.BitRateType;

        public ESCA.QUALITY Quality => _Camera.Quality;

        public ESCA.RESOLUTION Resolution => _Camera.Resolution;

        public ESCA.STANDARD Standard => _Camera.Standard;

        public int IFrameQuality => _Camera.IFrameQuality;

        public int PFrameQuality => _Camera.PFrameQuality;

        public (int Width, int Height) ImageSize => _Camera.ImageSize;

        public bool CameraInitComplete
        {
            get;
            private set;
        } = false;

        protected override bool Init()
        {
            try
            {
                _Camera.RTSPInfo( out string RTSPRes );

                if ( DetectorApp.Instance.EnableCameraControl == true )
                {
                    _SOLASApp.Info( "カメラ初期化　ホームポジション確認" );
                    //_Camera.ResetPTZ();
                    //if ( _Camera.CheckPTZ() == false )
                    //{
                    //    _SOLASApp.Info( $"カメラ初期化　ホームポジション移動（Pan={_Camera.Pan} Tilt={_Camera.Tilt} Zoom={_Camera.Zoom} ZoomRatio={_Camera.ZoomRatio}）" );
                    //    _Camera.ResetPTZ();
                    //}

                    //_Camera.VideInfo( out string Res );
                    if ( DetectorApp.Instance.EnableDetect == true )
                    {
                        _Camera.GotoPreset();
                    }
                    //CameraInitComplete = true;
                }
                else
                {
                    _SOLASApp.Info( "カメラ初期化　制御対象外" );
                }

                _Camera.VideInfo( out string Res );
                CameraInitComplete = true;
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "カメラ初期アクセスエラー", e );
                _SOLASApp.ErrorMessage( $"カメラアクセスエラー{Environment.NewLine}{e.Message}" );
                CameraInitComplete = false;
            }
            return base.Init();
        }

        protected override void EventTimeOut()
        {
            try
            {
                if ( DetectorApp.Instance.EnableCameraControl == true )
                {
                    if ( DetectorApp.Instance.EnableDetect == true )
                    {
                        _Camera.GotoPreset();
                    }
                    //if ( _Camera.CheckPTZ() == false )
                    //{
                    //    if ( _ResetTime.HasValue == false )
                    //    {
                    //        _SOLASApp.Info( "カメラホームポジションチェック PTZ不一致" );
                    //        DetectorApp.Instance.EnableDetect = false;
                    //        _ResetTime = DateTime.Now.AddMilliseconds( _ResetWait );
                    //    }
                    //    else if ( ( _ResetTime.Value <= DateTime.Now ) && ( DetectorApp.Instance.ManualDetectDisable == false ) )
                    //    {
                    //        Reset();
                    //    }
                    //}
                    //else
                    //{
                    //    _ResetTime = null;
                    //}
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "カメラアクセスエラー", e );
                _SOLASApp.ErrorMessage( $"カメラアクセスエラー{Environment.NewLine}{e.Message}" );
            }
        }

        public void Reset()
        {
            _Reset = true;
            DoEvent();
        }

        public void Update()
        {
            _Update = true;
            DoEvent();
        }

        protected override bool EventRoutine()
        {
            try
            {
                if ( DetectorApp.Instance.EnableCameraControl == true )
                {
                    if ( _Reset == true )
                    {
                        _SOLASApp.Info( "カメラホームポジションリセット実行" );
                        //_Camera.ResetPTZ();
                        _Camera.GotoPreset();
                        //if ( DetectorApp.Instance.ManualDetectDisable == false )
                        //{
                        //    DetectorApp.Instance.EnableDetect = true;
                        //}
                        //_ResetTime = null;
                        _Reset = false;
                    }
                    if ( _Update == true )
                    {
                        _SOLASApp.Info( "カメラホームポジション更新実行" );
                        //_Camera.UpdatePTZ();
                        _Camera.SetPreset();
                        _Update = false;
                        //_SOLASApp.UpdatePTZ( _Camera.Pan, _Camera.Tilt, _Camera.Zoom, _Camera.ZoomRatio );
                        _SOLASApp.ErrorMessage( $"現在の画角をカメラホームポジションに設定しました。" );
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "カメラアクセスエラー", e );
                _SOLASApp.ErrorMessage( $"カメラアクセスエラー{Environment.NewLine}{e.Message}" );
            }

            return true;
        }
    }
}
