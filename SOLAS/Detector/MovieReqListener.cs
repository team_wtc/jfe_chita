﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.Detector
{
    using Lib.Threads;
    using Lib.UI;

    public class MovieReqListener : OneshotThread
    {
        readonly TcpListener _TcpListener;
        readonly int _Port;
        readonly IPEndPoint _IPEndPoint;
        readonly List<MovieSender> _MovieSenderThreads;

        public MovieReqListener( ISOLASApp SOLASApp, int Port )
            : base( SOLASApp, "Listener",ThreadPriority.Normal )
        {
            _Port = Port;
            _IPEndPoint = new IPEndPoint( IPAddress.Any, _Port );
            _TcpListener = new TcpListener( _IPEndPoint );
            _TcpListener.Server.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
            _MovieSenderThreads = new List<MovieSender>();
        }

        protected override bool Init()
        {
            try
            {
                _TcpListener.Start();

                Task.Factory.StartNew( () =>
                {
                    while ( true )
                    {
                        while ( _MovieSenderThreads.Count > 0 )
                        {
                            _MovieSenderThreads[ 0 ].Wait();
                            _MovieSenderThreads.RemoveAt( 0 );
                        }

                        Thread.Sleep( 100 );
                    }
                } );
                _SOLASApp.Info( $"動画要求リスナー開始 bind={_IPEndPoint}" );
                return true;
            }
            catch(Exception e)
            {
                _SOLASApp.Exception( "動画要求リスナー", e );
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _TcpListener.Stop();
            }
            catch
            {

            }
            _SOLASApp.Info( $"動画要求リスナー終了 bind={_IPEndPoint}" );
        }

        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( _TcpListener.Pending() == false )
                {
                    Thread.Sleep( 10 );
                    continue;
                }

                var Client = _TcpListener.AcceptTcpClient();
                _SOLASApp.Info( $"動画要求ソケット接続 Remote={Client.Client.RemoteEndPoint}" );

                var Sender = new MovieSender( _SOLASApp, Client );
                Sender.Start();

                _MovieSenderThreads.Add( Sender );
            }
        }
    }
}
