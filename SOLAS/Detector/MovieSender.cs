﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace CES.SOLAS.Detector
{
    using Lib.Threads;
    using Lib.UI;

    public class MovieSender : OneshotThread
    {
        readonly TcpClient _TcpClient;

        public MovieSender( ISOLASApp SOLASApp, TcpClient Client ) : base( SOLASApp, $"Snd{( ( IPEndPoint )Client.Client.RemoteEndPoint ).Port:D5}", ThreadPriority.Normal )
        {
            _TcpClient = Client;
        }

        private int FindMovieFile( int AlarmId, out string MovieFilePath )
        {
            try
            {
                var MovieFilePathInfo = new FileInfo( Path.Combine( _SOLASApp.AlarmMovieFolder, $"{AlarmId}.mp4" ) );
                _SOLASApp.Info( $"警報ID={AlarmId}の動画ファイル({MovieFilePathInfo.FullName})を検索" );
                if ( MovieFilePathInfo.Exists == false )
                {
                    _SOLASApp.Info( $"動画ファイル無し" );
                    MovieFilePath = null;
                    return 0;
                }
                _SOLASApp.Info( $"動画ファイル発見 Length={MovieFilePathInfo.Length}" );
                MovieFilePath = MovieFilePathInfo.FullName;
                return Convert.ToInt32( MovieFilePathInfo.Length );
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                _SOLASApp.Exception( "動画ファイル検索エラー", e );
                MovieFilePath = null;
                return -1;
            }
        }

        protected override bool Init()
        {
            _SOLASApp.Info( $"動画ファイル送信開始" );
            return base.Init();
        }
        protected override void Terminate()
        {
            try
            {
                _TcpClient.Close();
                _TcpClient.Dispose();
            }
            catch
            {

            }
        }

        protected override void OneshotRoutine()
        {
            try
            {
                byte[] AlarmIdBuffer = new byte[ sizeof( int ) ];

                while ( _TcpClient.Available < sizeof( int ) )
                {
                    Thread.Sleep( 10 );
                }
                int LecvLen = _TcpClient.Client.Receive( AlarmIdBuffer );
                if ( LecvLen <= 0 )
                {
                    _SOLASApp.Warn( $"ソケット切断検知 Remote={_TcpClient.Client.RemoteEndPoint}" );
                    return;
                }

                List<byte> SendBuff = new List<byte>();
                SendBuff.AddRange( AlarmIdBuffer );

                var Length = FindMovieFile( BitConverter.ToInt32( AlarmIdBuffer, 0 ), out string MoviePath );
                SendBuff.AddRange( BitConverter.GetBytes( Length ) );

                if ( Length > 0)
                {
                    SendBuff.AddRange( File.ReadAllBytes( MoviePath ) );
                }

                int SendLen =_TcpClient.Client.Send( SendBuff.ToArray() );
                _SOLASApp.Warn( $"動画情報 {SendLen} バイトを送信 Remote={_TcpClient.Client.RemoteEndPoint}" );
            }
            catch(ThreadAbortException)
            {
                throw;
            }
            catch(Exception e )
            {
                _SOLASApp.Exception( "動画ファイル送信エラー", e );
            }
        }
    }
}
