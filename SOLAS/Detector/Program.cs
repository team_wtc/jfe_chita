﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CES.SOLAS.Detector
{
    using Lib.Utility;

    public static class Program
    {
        static Mutex _Mutex = null;
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [ STAThread]
        static void Main()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
                Application.ThreadException += Application_ThreadException;

                _Mutex = new Mutex( true, Application.ProductName, out bool CreateNew );
                if ( CreateNew == false )
                {
                    return;
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault( false );
                Application.Run( new DetectForm() );

                _Mutex.ReleaseMutex();
            }
            catch
            {

            }
            finally
            {
                _Mutex.Close();
            }

        }

        private static void Application_ThreadException( object sender, ThreadExceptionEventArgs e )
        {
            try
            {
                File.AppendAllText( $".\\Application_ThreadException.txt", $"{DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} : {e.Exception.GetType()} : {e.Exception.Message}{Environment.NewLine}" );
            }
            catch
            {
            }
        }

        private static void CurrentDomain_ProcessExit( object sender, EventArgs e )
        {
            try
            {
                File.AppendAllText( $".\\CurrentDomain_ProcessExit.txt", $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")} : CurrentDomain_ProcessExit{Environment.NewLine}" );
            }
            catch
            {
            }
        }

        private static void CurrentDomain_UnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            try
            {
                if ( e.ExceptionObject is Exception exp )
                {
                    File.AppendAllText( $".\\CurrentDomain_UnhandledException.txt", $"{DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} : IsTerminating={e.IsTerminating} : {exp.GetType()} : {exp.Message}{Environment.NewLine}" );
                }
                else if ( e.ExceptionObject != null )
                {
                    File.AppendAllText( $".\\CurrentDomain_UnhandledException.txt", $"{DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} : IsTerminating={e.IsTerminating} : {e.ExceptionObject.GetType()} : {e.ExceptionObject.ToString()}{Environment.NewLine}" );
                }
                else
                {
                    File.AppendAllText( $".\\CurrentDomain_UnhandledException.txt", $"{DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} : IsTerminating={e.IsTerminating} : e.ExceptionObject is null.{Environment.NewLine}" );
                }
            }
            catch
            {
            }
        }

        public static Average _StreamingAverage = new Average();
        public static Average _DetectAverage = new Average();
        public static Average _DetectDrawAverage = new Average();
        public static Average _DirectXAverage = new Average();
    }
}
