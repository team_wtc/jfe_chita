﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.DetectorChecker
{
    using Lib.ProcessIF.UDP;
    using static Properties.Settings;

    public partial class CheckerForm : Form
    {
        const string EVENT_LOG_NAME = "Application";
        const string EVENT_SRC_NAME = "Detector";
        const string EVENT_MACHINE_NAME = ".";

        readonly EventLog _EventLog = null;

        Process _Detector = null;
        bool _ForceStop = false;

        public CheckerForm()
        {
            InitializeComponent();

            try
            {
                if ( Default.CreateEventLog == true )
                {
                    if ( EventLog.SourceExists( EVENT_SRC_NAME ) == false )
                    {
                        EventLog.CreateEventSource( EVENT_SRC_NAME, EVENT_LOG_NAME );
                    }
                }
                _EventLog = new EventLog( EVENT_LOG_NAME, EVENT_MACHINE_NAME, EVENT_SRC_NAME );
            }
            catch(Exception e)
            {
                MessageBox.Show( $"イベントログアクセスに失敗しました。{Environment.NewLine}{e.Message}" );
                Environment.Exit( 255 );
            }
        }

        private void CheckerForm_Load( object sender, EventArgs e )
        {
            WindowState = FormWindowState.Minimized;
            SetMenuItemEnable();
            timer1.Enabled = true;
        }

        private void ExtToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( MessageBox.Show(
                text: $"当アプリケーションを終了すると、AI検知ソフトウェアも終了します。{Environment.NewLine}" +
                $"終了しますか？",
                caption: "終了",
                buttons: MessageBoxButtons.YesNo,
                icon: MessageBoxIcon.Question ) != DialogResult.Yes )
            {
                return;
            }
            timer1.Enabled = false;
            _ForceStop = true;
            StopDetector();

            Close();
        }

        private void StartDetectorProcess()
        {
            if ( ( _Detector != null ) && ( _Detector.HasExited == false ) )
            {
                return;
            }

            if ( _ForceStop == true )
            {
                return;
            }

            var StartInfo = new ProcessStartInfo( Default.DetectorBinPath )
            {
                ErrorDialog = true,
                WindowStyle = ProcessWindowStyle.Minimized
            };

            try
            {
                _Detector = Process.Start( StartInfo );

                _Detector.EnableRaisingEvents = true;
                _Detector.Exited += this._Detector_Exited;
                _EventLog?.WriteEntry( $"AI検知プロセス起動", EventLogEntryType.Information );
            }
            catch ( Exception e )
            {
                _EventLog?.WriteEntry( $"AI検知プロセス起動失敗{Environment.NewLine}{e.Message}", EventLogEntryType.Error );
                _Detector = null;
            }
            SetMenuItemEnable();
        }

        private void _Detector_Exited( object sender, EventArgs e )
        {
            _EventLog?.WriteEntry( $"AI検知プロセス停止を検知", EventLogEntryType.Warning );
            _Detector = null;
            SetMenuItemEnable();
        }

        private void timer1_Tick( object sender, EventArgs e )
        {
            StartDetectorProcess();
        }

        private void StartToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ForceStop = false;
        }

        private void StopToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ForceStop = true;
            StopDetector();
        }

        private void RestartToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ForceStop = false;
            StopDetector();
        }

        private void StopDetector()
        {
            if ( ( _Detector != null ) && ( _Detector.HasExited == false ) )
            {
                using ( UdpClient UdpSocket = new UdpClient() )
                {
                    var Pkt = new StopPacket( Byte.MaxValue, DateTime.Now );
                    byte[] Payload = Pkt.CreatePacketBytes();
                    IPEndPoint Remote = new IPEndPoint( IPAddress.Loopback, Default.DetectorPort );

                    try
                    {
                        UdpSocket.Send( Payload, Payload.Length, Remote );
                        _EventLog?.WriteEntry( $"AI検知プロセスへ停止要求送信", EventLogEntryType.Information );
                    }
                    catch ( Exception e )
                    {
                        _EventLog?.WriteEntry( $"AI検知プロセスへの停止要求送信に失敗{Environment.NewLine}{e.Message}", EventLogEntryType.Error );
                    }
                }
            }
        }

        private void SetMenuItemEnable()
        {
            if ( InvokeRequired == true )
            {
                BeginInvoke( ( MethodInvoker )SetMenuItemEnable );
                return;
            }

            if ( ( _Detector != null ) && ( _Detector.HasExited == false ) )
            {
                //  起動中
                StartToolStripMenuItem.Enabled = false;
                StopToolStripMenuItem.Enabled = true;
                RestartToolStripMenuItem.Enabled = true;
            }
            else
            {
                //  停止中
                StartToolStripMenuItem.Enabled = true;
                StopToolStripMenuItem.Enabled = false;
                RestartToolStripMenuItem.Enabled = false;
            }
        }

        private void CheckerForm_StyleChanged( object sender, EventArgs e )
        {
            if ( WindowState != FormWindowState.Minimized )
            {
                WindowState = FormWindowState.Minimized;
            }
        }
    }
}
