﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Collections.Generic;

using OpenH264Lib;

namespace CES.SOLAS.EncodeApp
{
    using Lib.Threads;
    using Lib.Utility;

    public class H264Encoder : QueueThread<(int AlarmId, DateTime Timestamp, byte[] ImgData)>
    {
        const string OPENH264LIB_NAME = "openh264-1.8.0-win64.dll";

        readonly Encoder _Encoder;
        readonly Stopwatch _Stopwatch;
        readonly List<byte> _FrameBuffer = new List<byte>();

        int _IFrameCount = 0;
        int _SkipFrameCounter = 0;

        public H264Encoder() : base( null, "Encoder", ThreadPriority.Highest, false, 0, new Average() )
        {
            _Encoder = new Encoder( OPENH264LIB_NAME );
            _Stopwatch = Stopwatch.StartNew();
        }

        protected override bool Init()
        {
            var FrameRate = ( Program.FrameRate / 2.0F );

            if ( _Encoder.Setup(
                            Program.Width,
                            Program.Height,
                            Program.BitRate,
                            //Program.FrameRate,
                            FrameRate,
                            Program.KeyFrameInterval,
                            OnEncodeCallback ) == 0 )
            {
                Program.Logger.Info(
                    $"エンコーダー初期化 Width={Program.Width} Height={Program.Height} " +
                    //$"BitRate={Program.BitRate} FrameRate={Program.FrameRate} KeyFrameInterval={Program.KeyFrameInterval}" );
                    $"BitRate={Program.BitRate} FrameRate={FrameRate} KeyFrameInterval={Program.KeyFrameInterval}" );
                _Stopwatch.Restart();
                return true;
            }
            else
            {
                Program.Logger.Error( "エンコーダー初期化失敗" );
                return false;
            }
        }

        protected override bool ItemRoutine( (int AlarmId, DateTime Timestamp, byte[] ImgData) Item )
        {
            if ( _SkipFrameCounter  != 0 )
            { 
                _SkipFrameCounter = ( ( _SkipFrameCounter + 1 ) % 2 );
                return true;
            }

            try
            {
                Monitor.Enter( Program.ImageListLock );

                _Stopwatch.Restart();
                using ( var MemStream = new MemoryStream( Item.ImgData ) )
                using ( var Img = Image.FromStream( MemStream ) )
                {
                    _Encoder?.Encode( ( Bitmap )Img );
                }
                //Program.Images.Add( (Item.Timestamp.Ticks, Item.ImgData) );
                //if ( Item.AlarmId > 0 )
                //{
                //    Program.Logger.Debug( $"保存中の画像数={Program.Images.Count}" );
                //    Program.StartImageWriter( Item.AlarmId, Item.Timestamp );
                //}
                return true;
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return false;
            }
            catch(Exception e)
            {
                Program.Logger.Exception( "エンコードエラー", e );
                return true;
            }
            finally
            {
                Monitor.Exit( Program.ImageListLock );
                _Stopwatch.Stop();
                _Average.AddValue( _Stopwatch.ElapsedMilliseconds );
                _SkipFrameCounter = ( ( _SkipFrameCounter + 1 ) % 2 );
            }
        }

        protected void OnEncodeCallback( byte[] data, int length, Encoder.FrameType keyFrame )
        {
            if ( ( keyFrame == Encoder.FrameType.I ) || ( keyFrame == Encoder.FrameType.IDR ) )
            {
                if ( _IFrameCount == 0 )
                {
                    _FrameBuffer.Clear();
                    _FrameBuffer.AddRange( data );
                    _IFrameCount++;
                    return;
                }
                else
                {
                    _FrameBuffer.AddRange( data );
                }
                Program.Logger.Debug( $"OnEncodeCallback keyFrame={keyFrame} Average={_Average.Avg:F2} msec" );
            }
            else
            {
                _FrameBuffer.Clear();
                _FrameBuffer.AddRange( data );
                _IFrameCount = 0;
            }

            Program.SendEncodedBytes( _FrameBuffer.ToArray() );
            //Program.Logger.Debug( $"OnEncodeCallback keyFrame={keyFrame} length={length}(data.Length={data.Length})" );
        }

    }
}
