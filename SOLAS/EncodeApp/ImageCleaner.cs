﻿using System;
using System.Threading;
using System.IO;

namespace CES.SOLAS.EncodeApp
{
    using static Properties.Settings;

    using Lib.Threads;

    public class ImageCleaner : IntervalThread
    {
        const int CLEAN_NTERVAL = 100;

        DateTime? _LastLogDelete = null;

        public ImageCleaner() : base( null, CLEAN_NTERVAL, "Cleaner" )
        {

        }
        protected override bool IntervalJob()
        {
            try
            {
                LogDelete();

                int RemoveCount = 0;
                bool Removed = false;

                Monitor.Enter( Program.ImageListLock );
                do
                {
                    Removed = false;

                    while ( Program.Images.Count > 0 )
                    {
                        DateTime LimitDate = DateTime.Now.AddSeconds( -( Program.PreAlarmTime * 2 ) );
                        DateTime ImageDate = new DateTime( Program.Images[ 0 ].TimeTicks );
                        if ( ImageDate < LimitDate )
                        {
                            Program.Images.RemoveAt( 0 );
                            RemoveCount++;
                            Removed = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                } while ( Removed == true );
                if ( RemoveCount > 0 )
                {
                    long Before = GC.GetTotalMemory( false );
                    GC.Collect();
                    long After = GC.GetTotalMemory( false );

                    //Program.Logger.Info( $"Remove {RemoveCount}Images. {Before - After}bytes release." );
                }
                return true;
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return false;
            }
            catch ( Exception e )
            {
                Program.Logger.Exception( "Image List Remove Error.", e );
                return true;
            }
            finally
            {
                Monitor.Exit( Program.ImageListLock );
                GC.Collect();
            }
        }

        private void LogDelete()
        {
            if ( ( _LastLogDelete.HasValue == false ) || ( _LastLogDelete.Value < DateTime.Now ) )
            {
                Program.Logger.Delete();
                AlarmMovieDelete();
                _LastLogDelete = DateTime.Today.AddDays( 1.0 );
            }
        }

        private void AlarmMovieDelete()
        {
            var Limit = DateTime.Today.AddDays( -Program.AlarmSaveDays );

            var MovieFolderInfo = new DirectoryInfo( Default.ALARM_Movie_Folder );
            foreach ( var MovieFile in MovieFolderInfo.GetFiles( "*.mp4", SearchOption.TopDirectoryOnly ) )
            {
                if ( MovieFile.CreationTime < Limit )
                {
                    try
                    {
                        MovieFile.Delete();
                        Program.Logger.Info( $"警報動画ファイル[{MovieFile.FullName}]削除" );
                    }
                    catch ( Exception e )
                    {
                        Program.Logger.Exception( $"警報動画ファイル[{MovieFile.FullName}]失敗", e );
                    }
                }
            }
        }
    }
}
