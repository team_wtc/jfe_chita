﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.EncodeApp
{
    using Lib.Threads;
    using Lib.ProcessIF.TCP;

    public class ImageReceiver : OneshotThread
    {
        readonly TcpClient _TcpClient;

        public ImageReceiver( TcpClient Client ) : base( null, "Img Recv", ThreadPriority.Normal )
        {
            _TcpClient = Client;
        }

        protected override bool Init()
        {
            Program.Logger.Info( $"画像受信スレッド起動 Remote={_TcpClient.Client.RemoteEndPoint}" );

            return base.Init();
        }
        protected override void Terminate()
        {
            try
            {
                Program.Logger.Info( $"画像受信スレッド終了" );
                if ( ( _TcpClient != null ) && ( _TcpClient.Client != null ) )
                {
                    _TcpClient.Client.Shutdown( SocketShutdown.Both );
                    _TcpClient.Close();
                    _TcpClient.Dispose();
                }
            }
            catch
            {
            }
        }

        protected override void OneshotRoutine()
        {
            try
            {
                while ( true )
                {
                    if ( _TcpClient.Client.Poll( 100 * 1000, SelectMode.SelectRead ) == false )
                    {
                        continue;
                    }

                    byte[] HeaderBuffer = new byte[ 1 ];
                    List<byte> PacketBytes = new List<byte>();

                    while ( PacketBytes.Count < 25 )
                    {
                        if ( _TcpClient.Client.Receive( HeaderBuffer ) <= 0 )
                        {
                            Program.Logger.Warn( $"Socket Disconnect. {_TcpClient.Client.RemoteEndPoint}" );
                            PacketBytes.Clear();
                            break;
                        }
                        PacketBytes.AddRange( HeaderBuffer );
                    }
                    if ( PacketBytes.Count <= 0 )
                    {
                        break;
                    }

                    var Imagesize = BitConverter.ToInt32( PacketBytes.ToArray(), 21 );

                    if ( Imagesize > 0 )
                    {
                        int TotalSize = ( PacketBytes.Count + Imagesize );

                        while ( PacketBytes.Count < TotalSize )
                        {
                            if ( _TcpClient.Available <= 0 )
                            {
                                Thread.Sleep( 10 );
                                continue;
                            }

                            int BufferSize = _TcpClient.Available;
                            if ( ( PacketBytes.Count + BufferSize ) > TotalSize )
                            {
                                BufferSize = ( TotalSize - PacketBytes.Count );
                            }
                            var ImageBuffer = new byte[ BufferSize ];

                            var RecvLen = _TcpClient.Client.Receive( ImageBuffer );
                            if ( RecvLen <= 0 )
                            {
                                Program.Logger.Warn( $"Socket Disconnect. {_TcpClient.Client.RemoteEndPoint}" );
                                break;
                            }


                            if ( RecvLen >= ImageBuffer.Length )
                            {
                                PacketBytes.AddRange( ImageBuffer );
                            }
                            else
                            {
                                byte[] DstArray = new byte[ RecvLen ];
                                Array.Copy( ImageBuffer, 0, DstArray, 0, RecvLen );
                                PacketBytes.AddRange( DstArray );
                            }
                        }

                        var Pkt = new ImagePacket( PacketBytes.ToArray() );

                        //  TODO
                        //  画像保存
                        Program.Images.Add( (Pkt.Timestamp.Ticks, Pkt.ImageData) );
                        //  エンコーダ―へ
                        //Program.H264Encoder.Enqueue( (Pkt.AlarmId, Pkt.Timestamp, Pkt.ImageData) );
                        //  動画作成
                        if ( Pkt.AlarmId > 0 )
                        {
                            Program.Logger.Debug(
                                $"画像読込 AlarmId={Pkt.AlarmId} " +
                                $"Timestamp={Pkt.Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"PreTime={( Pkt.Timestamp.AddSeconds( -Program.PreAlarmTime ) ).ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"PostTime={( Pkt.Timestamp.AddSeconds( Program.PostAlarmTime ) ).ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"{Pkt.ImageData.Length}bytes" );
                            Program.Logger.Debug( $"保存中の画像数={Program.Images.Count}" );
                            Program.StartImageWriter( Pkt.AlarmId, Pkt.Timestamp );
                        }



                    }
                }
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return;
            }catch(Exception e )
            {
                Program.Logger.Exception( "画像受信エラー", e );
                return;
            }
        }
    }
}
