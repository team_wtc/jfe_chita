﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace CES.SOLAS.EncodeApp
{
    using Lib.Threads;
    using Lib.UI;

    public class ImageRecvListener : OneshotThread
    {
        readonly TcpListener _TcpListener;
        readonly int _Port;
        readonly IPEndPoint _IPEndPoint;
        readonly List<ImageReceiver> _Receivers = new List<ImageReceiver>();

        public ImageRecvListener( ISOLASApp SOLASApp, int Port )
            : base( SOLASApp, "Listener",ThreadPriority.Normal )
        {
            _Port = Port;
            _IPEndPoint = new IPEndPoint( IPAddress.Loopback, _Port );
            _TcpListener = new TcpListener( _IPEndPoint );
            _TcpListener.Server.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
        }

        protected override bool Init()
        {
            try
            {
                _TcpListener.Start();

                Program.Logger.Info( $"画像受信リスナー開始 bind={_IPEndPoint}" );
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _TcpListener.Stop();

                foreach ( var Receiver in _Receivers )
                {
                    if ( Receiver != null )
                    {
                        if ( Receiver.IsAlive == true )
                        {
                            Receiver.Stop();
                        }
                        else
                        {
                            Receiver.Wait();
                        }
                    }
                }

            }
            catch
            {

            }
            Program.Logger.Info( $"画像受信リスナー終了 bind={_IPEndPoint}" );
        }

        protected override void OneshotRoutine()
        {
            while ( true )
            {
                if ( _TcpListener.Pending() == false )
                {
                    Thread.Sleep( 10 );
                    continue;
                }

                var Client = _TcpListener.AcceptTcpClient();
                Program.Logger.Info( $"画像受信ソケット接続 Remote={Client.Client.RemoteEndPoint}" );

                var Receiver = new ImageReceiver( Client );
                Receiver.Start();
                _Receivers.Add( Receiver );
            }
        }
    }
}
