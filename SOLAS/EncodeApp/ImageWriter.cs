﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.EncodeApp
{
    using Lib.Threads;

    using static Properties.Settings;

    public class ImageWriter : OneshotThread
    {
        public readonly DateTime PreAlarmTime;
        public readonly DateTime PostAlarmTime;

        readonly int _AlarmId;
        readonly DateTime _AlarmTimestamp;

        readonly VideoWriter _VideoWriter;

        long? _LastWriteTimestamp = null;
        string _MovieFilePath = string.Empty;
        string _TempFilePath = string.Empty;

        public ImageWriter( int AlarmId, DateTime AlarmTimestamp ) : base( null, $"mov {AlarmId}", ThreadPriority.Normal )
        {
            _AlarmId = AlarmId;
            _AlarmTimestamp = AlarmTimestamp;
            PreAlarmTime = AlarmTimestamp.AddSeconds( -Program.PreAlarmTime );
            PostAlarmTime= AlarmTimestamp.AddSeconds( Program.PostAlarmTime );

            _VideoWriter = new VideoWriter();
        }


        public DateTime? LastWriteTimestamp
        {
            get
            {
                if ( _LastWriteTimestamp.HasValue == false )
                {
                    return null;
                }

                return new DateTime( _LastWriteTimestamp.Value );
            }
        }

        protected override bool Init()
        {
            Program.Logger.Info(
                $"動画出力スレッド起動 ({PreAlarmTime.ToString("yyyy/MM/dd HH:mm:ss.fff")}" +
                $"～{_AlarmTimestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}" +
                $"～{PostAlarmTime.ToString( "yyyy/MM/dd HH:mm:ss.fff" )})" );

            var MovieFileName = $"{_AlarmId}.mp4";

            _TempFilePath = Path.Combine( Path.GetTempPath(), MovieFileName );
            _MovieFilePath = Path.Combine( Default.ALARM_Movie_Folder, MovieFileName );

            var WriteFolder = Path.GetDirectoryName( _MovieFilePath );
            if ( Directory.Exists( WriteFolder ) == false )
            {
                try
                {
                    Directory.CreateDirectory( WriteFolder );
                }
                catch ( Exception e )
                {
                    Program.Logger.Exception( "動画フォルダー生成失敗", e );
                    return false;
                }
            }

            try
            {
                _VideoWriter.Open( _TempFilePath, Default.ALARM_Movie_codec, Program.FrameRate, new OpenCvSharp.Size( Program.Width, Program.Height ) );
                if ( _VideoWriter.IsOpened() == false )
                {
                    Program.Logger.Error( $"警報動画ファイル({_TempFilePath})をオープンできません。(OpenCV Error)" );
                    return false;
                }
            }
            catch ( Exception e )
            {
                Program.Logger.Exception( "動画ファイルオープンエラー", e );
                return false;
            }

            Program.Logger.Info( $"警報動画一時ファイル({_TempFilePath})オープン" );
            return base.Init();
        }

        protected override void Terminate()
        {
            try
            {
                Program.Logger.Info( $"警報動画ファイル({_TempFilePath})クローズ" );
                _VideoWriter?.Release();
                _VideoWriter?.Dispose();

                var MovieFileInfo = new FileInfo( _MovieFilePath );
                if ( MovieFileInfo.Exists == true )
                {
                    MovieFileInfo.Delete();
                }

                var TempFileInfo = new FileInfo( _TempFilePath );
                if ( TempFileInfo.Exists == false )
                {
                    return;
                }

                TempFileInfo.MoveTo( _MovieFilePath );
                Program.Logger.Info( $"警報動画ファイル({_MovieFilePath})保存" );
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch( Exception e )
            {
                Program.Logger.Exception( "動画ファイル保存エラー", e );
            }

            base.Terminate();
        }

        protected override void OneshotRoutine()
        {
            try
            {
                int FrameCount = 0;
                while ( true )
                {
                    var ImgData = NextImage();
                    if ( ( ImgData != null ) && ( ImgData.Length > 0 ) )
                    {
                        using ( var MemStream = new MemoryStream( ImgData ) )
                        using ( var Img = Image.FromStream( MemStream ) )
                        using ( var ImgMat = BitmapConverter.ToMat( ( Bitmap )Img ) )
                        {
                            _VideoWriter.Write( ImgMat );
                            ++FrameCount;
                        }
                        Thread.Sleep( 10 );
                    }
                    else
                    {
                        Program.Logger.Info( $"警報動画ファイル書き込みフレーム数 {FrameCount}" );
                        break;
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                Program.Logger.Exception( "動画ファイル画像抽出エラー", e );
            }
        }

        private byte[] NextImage()
        {
            while ( true )
            {
                try
                {
                    Monitor.Enter( Program.ImageListLock );
                    if ( Program.Images.Count <= 0 )
                    {
                        throw new ApplicationException( "動画出力用画像が保存されていません。" );
                    }

                    long LastWriteValue = ( PreAlarmTime.Ticks - 1L );

                    if ( _LastWriteTimestamp.HasValue == true )
                    {
                        LastWriteValue = _LastWriteTimestamp.Value;
                    }

                    foreach ( var (TimeTicks, ImageData) in Program.Images )
                    {
                        if ( TimeTicks > PostAlarmTime.Ticks )
                        {
#if DEBUG
                            var ts = new DateTime( TimeTicks );
                            Program.Logger.Debug( $"★End of Frame TS={ts.ToString( "yyyyMMdd_HHmmssfff" )}" );
#endif
                            return null;
                        }

                        if ( TimeTicks > LastWriteValue )
                        {
                            _LastWriteTimestamp = TimeTicks;
                            return ImageData;
                        }
                    }
                }
                catch ( ThreadAbortException )
                {
                    throw;
                }
                catch ( Exception e )
                {
                    Program.Logger.Exception( "動画ファイル画像抽出エラー", e );
                    return null;
                }
                finally
                {
                    Monitor.Exit( Program.ImageListLock );
                }
            }
        }
    }
}
