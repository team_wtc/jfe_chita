﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using static System.Threading.Thread;

namespace CES.SOLAS.EncodeApp
{
    using static Properties.Settings;
    using Lib.Logging;
    using Lib.Data;
    using Lib.ProcessIF.TCP;

    public class Program
    {
        public static int Width = 1920;             // 画像の幅
        public static int Height = 1080;            // 画像の高さ
        public static int BitRate = 3000 * 1000;    // ビットレート
        public static float FrameRate = 15F;        // フレームレート
        public static float KeyFrameInterval = 1F;  // Iフレーム間隔
        public static int PreAlarmTime = 15;
        public static int PostAlarmTime = 15;
        public static int AlarmSaveDays = 30;

        public static Logger Logger;
        //public static H264Encoder H264Encoder;
        public static List<(long TimeTicks, byte[] ImageData)> Images = new List<(long TimeTicks, byte[] ImageData)>();
        public static object ImageListLock = new object();
        public static Dictionary<int, ImageWriter> ImageWriters = new Dictionary<int, ImageWriter>();

        static int _ApplicationId = -1;
        static int _MovieListenerPort = 11240;
        static SOLASDataSet.DETECTORDataTable _OwnApplicationInfo = null;
        static SOLASDataSet.MONITORDataTable _MONITORDataTable = null;
        static SOLASDataSet.SYSTEMDataTable _SYSTEMDataTable = null;
        static SHMReader _SHMReader = null;
        static ImageCleaner _ImageCleaner = null;
        static MovieReqListener _MovieReqListener = null;
        //static ImageRecvListener _ImageRecvListener = null;

        static readonly Dictionary<int, H264ImageSender> H264ImageSenders = new Dictionary<int, H264ImageSender>();

        /// <summary>
        /// コマンドライン
        /// EncodeApp Width Height Bitrate Framerate KeyFrameInterval
        /// </summary>
        /// <param name="args"></param>
        static void Main( string[] args )
        {
//#if DEBUG
//            System.Diagnostics.Debugger.Launch();
//#endif
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            if ( InitApp( args ) == false )
            {
                Logger.Error( "InitApp( args ) == false" );
                ExitApp();
                return;
            }

            //while ( Console.ReadLine().ToLower().StartsWith( "stop" ) == false )
            //{
            //}
            while ( true )
            {
                var Line = Console.ReadLine();
                if ( ( string.IsNullOrWhiteSpace( Line ) == false ) && ( Line.ToLower() is string LowerLine ) )
                {
                    if ( LowerLine.StartsWith( "stop" ) == true )
                    {
                        break;
                    }
                }
            }

            Logger.Info( "終了指示受信" );
            ExitApp();
        }

        private static void CurrentDomain_UnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            if ( e.ExceptionObject is Exception Exp )
            {
                Logger.Exception( "UnhandledException", Exp );
            }
            else if ( e.ExceptionObject != null )
            {
                Logger.Error( $"UnhandledException ExceptionObject={e.ExceptionObject.GetType()}" );
            }
            else
            {
                Logger.Error( "UnhandledException ExceptionObject=null" );
            }
        }

        static void ExitApp()
        {
            StopMovieReqListener();
            StopImageListener();
            StopSHMReader();
            //StopEncoder();
            //StopImageSender();
            StopImageCleaner();

            Logger.Info( "********** Terminate. **********" );
            System.Threading.Thread.Sleep( 200 );
            Logger.Stop();
            System.Threading.Thread.Sleep( 200 );
        }

        static bool InitApp( string[] args )
        {
            try
            {
                CurrentThread.Name = "Main";
            }
            catch
            {
            }

            try
            {
                InitLog( Default.LOG_Folder, "EncodeApp", Default.LOG_Ext, Default.LOG_Size, Default.LOG_Days, Default.LOG_Debug );
            }
            catch ( Exception e1 )
            {
                Console.WriteLine( $"ログ初期化失敗({e1.Message})", e1 );
                Environment.Exit( 255 );
            }

            Logger.Info( "********** Start up. **********" );

            try
            {
                InitDBAccess( Default.DB_Server, Default.DB_Password );
                ParseCommandLine( args );

                //StartImageSender();
                //StartEncoder();
                StartImageListener();
                StartSHMReader();
                StartImageCleaner();
                StartMovieReqListener();
            }
            catch ( Exception e2 )
            {
                Console.WriteLine( $"EncodeApp初期化失敗 {e2.GetType()} {e2.Message}" );
                Logger.Exception( "EncodeApp初期化失敗", e2 );
                return false;
            }

            Console.WriteLine( $"{System.Diagnostics.Process.GetCurrentProcess().ProcessName}(pid:{System.Diagnostics.Process.GetCurrentProcess().Id}) Start up." );
            return true;
        }

        static void StartMovieReqListener()
        {
            try
            {
                _MovieReqListener = new MovieReqListener( null, _MovieListenerPort );
            }
            catch
            {
                _MovieReqListener = null;
                throw;
            }

            _MovieReqListener.Start();
        }

        static void StopMovieReqListener()
        {
            _MovieReqListener?.Stop();
        }

        static void StartSHMReader()
        {
            try
            {
                _SHMReader = new SHMReader();
            }
            catch
            {
                _SHMReader = null;
                throw;
            }

            _SHMReader.Start();
        }

        static void StopSHMReader()
        {
            _SHMReader?.Stop();
        }

        static void StartImageListener()
        {
            try
            {
                //_ImageRecvListener = new ImageRecvListener( null, 6969 );
            }
            catch
            {
                //_ImageRecvListener = null;
                throw;
            }

            //_ImageRecvListener.Start();
        }

        static void StopImageListener()
        {
            //_ImageRecvListener?.Stop();
        }


        static void StartImageCleaner()
        {
            try
            {
                _ImageCleaner = new ImageCleaner();
            }
            catch
            {
                _ImageCleaner = null;
                throw;
            }

            _ImageCleaner.Start();
        }

        static void StopImageCleaner()
        {
            _ImageCleaner?.Stop();
        }

        //static void StartEncoder()
        //{
        //    try
        //    {
        //        H264Encoder = new H264Encoder();
        //    }
        //    catch
        //    {
        //        H264Encoder = null;
        //        throw;
        //    }

        //    H264Encoder.Start();
        //}

        //static void StopEncoder()
        //{
        //    H264Encoder?.Stop();
        //}

        //static void StartImageSender()
        //{
        //    try
        //    {
        //        foreach ( var MontorRow in _MONITORDataTable )
        //        {
        //            if ( MontorRow.mon_monitor_mode == 1 )
        //            {
        //                H264ImageSenders[ MontorRow.mon_id ] = new H264ImageSender( Program.Logger, Program._ApplicationId, MontorRow.mon_ip_address, MontorRow.mon_still_image_port );
        //                H264ImageSenders[ MontorRow.mon_id ].Start();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //static void StopImageSender()
        //{
        //    try
        //    {
        //        foreach ( var Key in H264ImageSenders.Keys )
        //        {
        //            if ( H264ImageSenders[ Key ] != null )
        //            {
        //                if ( H264ImageSenders[ Key ].IsAlive == true )
        //                {
        //                    H264ImageSenders[ Key ].Stop();
        //                }

        //                H264ImageSenders[ Key ].Wait();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public static void SendEncodedBytes(byte[] EncodedBytes )
        {
            try
            {
                foreach ( var Key in H264ImageSenders.Keys )
                {
                    H264ImageSenders[ Key ]?.Enqueue( EncodedBytes );
                }
            }
            catch
            {
                throw;
            }
        }

        public static void StartImageWriter( int AlarmId, DateTime Timestamp )
        {
            ImageWriters[ AlarmId ]= new ImageWriter( AlarmId, Timestamp );
            ImageWriters[ AlarmId ].Start();

            List<int> TerminatedThreadList = new List<int>();
            foreach ( var Key in ImageWriters.Keys )
            {
                if ( ImageWriters[ Key ].IsAlive == false )
                {
                    TerminatedThreadList.Add( Key );
                    ImageWriters[ Key ].Wait();

                }
            }
            foreach ( var RemoveKey in TerminatedThreadList )
            {
                ImageWriters.Remove( RemoveKey );
            }
        }


        static void ParseCommandLine( string[] args )
        {
            if ( ( args == null ) || ( args.Length <= 0 ) )
            {
                throw new ApplicationException( "コマンドラインが指定されていません。" );
            }
            if ( args.Length < 5 )
            {
                throw new ApplicationException( "コマンドライン引数が足りません。" );
            }

            try
            {
                Width = int.Parse( args[ 0 ] );             // 画像の幅
                Height = int.Parse( args[ 1 ] );            // 画像の高さ
                BitRate = int.Parse( args[ 2 ] );    // ビットレート
                FrameRate = float.Parse( args[ 3 ] );        // フレームレート
                KeyFrameInterval = float.Parse( args[ 4 ] );  // Iフレーム間隔

                Logger.Info( $"コマンドライン Width={Width} Height={Height} BitRate={BitRate}bps FrameRate={FrameRate:F2}fps KeyFrameInterval={KeyFrameInterval:F1}sec" );
            }
            catch
            {
                throw;
            }
        }

        static void InitLog( string LogFolder, string LogName, string LogExt, long LogSize, int SaveDays, bool DebugLog )
        {
            Logger = new Logger( LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );
            Logger.Start();
        }

        static void InitDBAccess( string DBServer, string SAPassword )
        {
            var DBAdapter = new Adapter( DBServer, SAPassword );

            DetectorHandShake( DBAdapter );
            LoadPCInfo( DBAdapter );
            LoadSystemInfo( DBAdapter );

            _ApplicationId = _OwnApplicationInfo[ 0 ].ai_id;
            _MovieListenerPort = _OwnApplicationInfo[ 0 ].ai_movie_port;
        }

        static void DetectorHandShake( Adapter DBAdapter )
        {
            foreach ( var Adr in Dns.GetHostAddresses( Dns.GetHostName() ) )
            {
                if ( Adr.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork )
                {
                    continue;
                }
                if ( IPAddress.Loopback.Equals( Adr ) == true )
                {
                    continue;
                }

                using ( var Adapter = DBAdapter.DETECTORTableAdapter() )
                {
                    _OwnApplicationInfo = Adapter.GetDataByIPAddress( Adr.ToString() );
                    if ( ( _OwnApplicationInfo != null ) && ( _OwnApplicationInfo.Count == 1 ) )
                    {
                        _ApplicationId = _OwnApplicationInfo[ 0 ].ai_id;
                        Logger.Info( $"自端末情報ロード ID={_ApplicationId} (IPAddress:{Adr.ToString()})" );
                        return;
                    }

                    _OwnApplicationInfo?.Dispose();
                    _OwnApplicationInfo = null;
                }
            }

            throw new ApplicationException( $"{Dns.GetHostName()} の端末情報はデータベースに登録されていません。" );
        }

        static void LoadPCInfo( Adapter DBAdapter )
        {
            try
            {
                using ( var MonAdapter = DBAdapter.MONITORTableAdapter() )
                {
                    _MONITORDataTable = MonAdapter.GetData();
                }

                if ( ( _MONITORDataTable == null ) || ( _MONITORDataTable.Count <= 0 ) )
                {
                    throw new ApplicationException( $"MONITORテーブルのロードに失敗しました。" );
                }

                Logger.Info( $"MONITOR端末情報ロード Count={_MONITORDataTable.Count}" );
            }
            catch
            {
                throw;
            }
        }

        static void LoadSystemInfo( Adapter DBAdapter )
        {
            try
            {
                using ( var Adapter = DBAdapter.SYSTEMTableAdapter() )
                {
                    _SYSTEMDataTable = Adapter.GetData();
                }
                if ( ( _SYSTEMDataTable == null ) || ( _SYSTEMDataTable.Count <= 0 ) )
                {
                    throw new ApplicationException( $"SYSTEMテーブルのロードに失敗しました。" );
                }
                Logger.Info( $"SYSTEM情報ロード Count={_SYSTEMDataTable.Count}" );
                GetPrePostTime();
                GetAlarmSaveDays();
            }
            catch
            {
                throw;
            }
        }

        static void GetPrePostTime()
        {
            const string PRE_TIME_KEY = "AlarmMoviePreSeconds";
            const string POST_TIME_KEY = "AlarmMoviePostSeconds";
            const int DEFAULT_TIME = 15;

            try
            {
                if ( ( _SYSTEMDataTable != null ) && ( _SYSTEMDataTable.Count > 0 ) )
                {
                    if ( _SYSTEMDataTable.FindBysystem_key( PRE_TIME_KEY ) is SOLASDataSet.SYSTEMRow PreSysRow )
                    {
                        PreAlarmTime = int.Parse( PreSysRow.system_value );
                    }
                    if ( _SYSTEMDataTable.FindBysystem_key( POST_TIME_KEY ) is SOLASDataSet.SYSTEMRow PostSysRow )
                    {
                        PostAlarmTime = int.Parse( PostSysRow.system_value );
                    }
                }
            }
            catch ( System.Threading.ThreadAbortException )
            {
                throw;
            }
            catch (Exception e )
            {
                PreAlarmTime = DEFAULT_TIME;
                PostAlarmTime = DEFAULT_TIME;
                Logger.Exception( "SYSTEMテーブルアクセスエラー", e );
            }

            Logger.Info( $"警報動画設定 Pre={PreAlarmTime}sec Post={PostAlarmTime}sec" );
        }

        static void GetAlarmSaveDays()
        {
            const string ALARM_SAVE_DAYS_KEY = "AlarmSaveDays";
            const int DEFAULT_DAYS = 30;

            try
            {
                if ( ( _SYSTEMDataTable != null ) && ( _SYSTEMDataTable.Count > 0 ) )
                {
                    if ( _SYSTEMDataTable.FindBysystem_key( ALARM_SAVE_DAYS_KEY ) is SOLASDataSet.SYSTEMRow SysRow )
                    {
                        AlarmSaveDays = int.Parse( SysRow.system_value );
                    }
                }
            }
            catch ( System.Threading.ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                AlarmSaveDays = DEFAULT_DAYS;
                Logger.Exception( "SYSTEMテーブルアクセスエラー", e );
            }

            Logger.Info( $"警報動画保持日数 {AlarmSaveDays}日" );
        }
    }
}
