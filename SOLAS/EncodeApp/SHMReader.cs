﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CES.SOLAS.EncodeApp
{
    using Lib.Threads;
    using Lib.ProcessIF.SHM;

    public class SHMReader : OneshotThread
    {
        const string OPENH264LIB_NAME = "";

        readonly SharedMemory _SharedMemory;

        public SHMReader() : base( null, "SHMReader", ThreadPriority.Highest )
        {
            _SharedMemory = new SharedMemory( Program.Logger );
        }

        protected override void OneshotRoutine()
        {
            try
            {
                while ( true )
                {
                    _SharedMemory.ReadData( out int AlarmId, out DateTime Timestamp, out byte[] ImageData );
                    if ( ( ImageData != null ) && ( ImageData.Length > 0 ) )
                    {
                        //Program.Logger.Debug( $"★AlarmId={AlarmId} Timestamp={Timestamp} ImageData={ImageData.Length}" );
                        //  TODO
                        //  画像保存
                        Program.Images.Add( (Timestamp.Ticks, ImageData) );
                        //  エンコーダ―へ
                        //Program.H264Encoder.Enqueue( (AlarmId, Timestamp, ImageData) );
                        //  動画作成
                        if ( AlarmId > 0 )
                        {
                            Program.Logger.Debug(
                                $"画像読込 AlarmId={AlarmId} " +
                                $"Timestamp={Timestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"PreTime={( Timestamp.AddSeconds( -Program.PreAlarmTime ) ).ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"PostTime={( Timestamp.AddSeconds( Program.PostAlarmTime ) ).ToString( "yyyy/MM/dd HH:mm:ss.fff" )} " +
                                $"{ImageData.Length}bytes" );
                            Program.Logger.Debug( $"保存中の画像数={Program.Images.Count}" );
                            Program.StartImageWriter( AlarmId, Timestamp );
                        }
                    }
                    else
                    {
                        //Program.Logger.Debug( $"★No Image" );
                        Thread.Sleep( 5 );
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return;
            }catch(Exception e )
            {
                Program.Logger.Exception( "共有メモり読込エラー", e );
                return;
            }
        }
    }
}
