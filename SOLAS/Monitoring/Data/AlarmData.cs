﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace CES.SOLAS.Monitor.Data
{
    using Lib;
    using Lib.ProcessIF.TCP;

    public class AlarmData
    {
        protected readonly string _AlarmDataXmlPath;
        protected readonly string _AlarmImageSaveFolder;
        protected readonly AlarmDataSet _AlarmDataSet;
        protected readonly Action _ViewListChangeCallback;
        protected readonly object _TableLock = new object();

        public AlarmData( string AlarmImageSaveFolder, Action ListChangeCallback )
        {
            _ViewListChangeCallback = ListChangeCallback;
            _AlarmImageSaveFolder = AlarmImageSaveFolder;
            _AlarmDataXmlPath = Path.Combine( Application.StartupPath, $"{Application.ProductName}.xml" );

            _AlarmDataSet = Load();
            _AlarmDataSet.ALARM.DefaultView.Sort = $"{_AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} DESC";
            _AlarmDataSet.ALARM.RowChanged += this.ALARM_RowChanged;
            _AlarmDataSet.ALARM.RowChanging += this.ALARM_RowChanging;
            _AlarmDataSet.ALARM.RowDeleted += this.ALARM_RowDeleted;
            _AlarmDataSet.ALARM.RowDeleting += this.ALARM_RowDeleting;
        }

        private void ALARM_RowDeleting( object sender, DataRowChangeEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine($"ALARM_RowDeleting {e.Action}" );
        }

        private void ALARM_RowDeleted( object sender, DataRowChangeEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( $"ALARM_RowDeleted {e.Action}" );
        }

        private void ALARM_RowChanging( object sender, DataRowChangeEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( $"ALARM_RowChanging {e.Action}" );
        }

        private void ALARM_RowChanged( object sender, DataRowChangeEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( $"ALARM_RowChanged {e.Action}" );
        }

        protected AlarmDataSet Load()
        {
            AlarmDataSet DS = new AlarmDataSet();
            if ( File.Exists( _AlarmDataXmlPath ) == true )
            {
                DS.ReadXml( _AlarmDataXmlPath );
                DS.AcceptChanges();
            }

            DS.ALARM.DefaultView.RowFilter = $"";
            return DS;
        }

        protected void Save()
        {
            try
            {
                if ( Directory.Exists( Path.GetDirectoryName( _AlarmDataXmlPath ) ) == false )
                {
                    Directory.CreateDirectory( Path.GetDirectoryName( _AlarmDataXmlPath ) );
                }

                _AlarmDataSet.AcceptChanges();
                _AlarmDataSet.WriteXml( _AlarmDataXmlPath );
            }
            catch
            {
            }
        }

        public DataView AllData()
        {
            _AlarmDataSet.ALARM.DefaultView.RowFilter = string.Empty;
            _AlarmDataSet.ALARM.DefaultView.Sort = $"{_AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} DESC";
            return _AlarmDataSet.ALARM.DefaultView;
        }

        public DataView SearchData(
            int MaxDisplayDays,
            int Gate1,
            int Gate2,
            int Gate3,
            ALARM_TYPE AlarmA,
            ALARM_TYPE AlarmB,
            ALARM_TYPE AlarmC,
            DateTime FromDate,
            DateTime ToDate )
        {
            UpdateFilter( MaxDisplayDays, Gate1, Gate2, Gate3, AlarmA, AlarmB, AlarmC, FromDate, ToDate );
            //  絞込表示順を日付降順に訂正
            _AlarmDataSet.ALARM.DefaultView.Sort = $"{ _AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} DESC";
            return _AlarmDataSet.ALARM.DefaultView;
        }

        //public DataView AlarmTable => _AlarmDataSet.ALARM.DefaultView;

        public int AddAlarm( ImagePacket Packet )
        {
            if ( Packet.AlarmId <= 0 )
            {
                return 0;
            }

            if ( Enum.IsDefined( typeof( ALARM_TYPE ), Packet.AlarmType ) == false )
            {
                return 0;
            }

            var ImagePath = Path.Combine(
                _AlarmImageSaveFolder,
                $"{Packet.AlarmId}_{Packet.AlarmType}_{Packet.Timestamp.ToString( "yyyyMMdd_HHmmssfff" )}.jpg" );

            WriteImage( ImagePath, Packet );

            lock ( _TableLock )
            {
                var NewRow = _AlarmDataSet.ALARM.NewALARMRow();
                NewRow.alarm_id = Packet.AlarmId;
                NewRow.alarm_type = Packet.AlarmType;
                NewRow.alarm_date = Packet.Timestamp;
                NewRow.alarm_gate_id = Packet.DetectorId;
                NewRow.alarm_gate_name = MonitoringApp.Instance.GateName( Packet.DetectorId );
                NewRow.alarm_type_name = MonitoringApp.Instance.AlarmName( ( ALARM_TYPE )Enum.ToObject( typeof( ALARM_TYPE ), Packet.AlarmType ) );
                NewRow.alarm_image_path = ImagePath;
                _AlarmDataSet.ALARM.AddALARMRow( NewRow );
                Save();

                MonitoringApp.Instance.Info(
                    $"NEW ALARM [{NewRow.alarm_id}][{NewRow.alarm_type_name}][{NewRow.alarm_date.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}] " +
                    $"GATE[{NewRow.alarm_gate_id}:{NewRow.alarm_gate_name}] Image Length={Packet.ImageLength} Path={ImagePath}" );
            }

            return Packet.AlarmId;
        }

        public string ImagePath(int GateId, int AlarmId)
        {
            if ( _AlarmDataSet.ALARM.FindByalarm_idalarm_gate_id( AlarmId, GateId ) is AlarmDataSet.ALARMRow AlarmRow )
            {
                return AlarmRow.alarm_image_path;
            }

            return string.Empty;
        }

        public string LastImagePath( int GateId)
        {
            DateTime? AlarmDate = null;
            string ImagePath = string.Empty;

            foreach ( DataRowView AlarmRowView in _AlarmDataSet.ALARM.DefaultView )
            {
                if ( AlarmRowView.Row is AlarmDataSet.ALARMRow AlarmRow )
                {
                    if ( AlarmRow.alarm_gate_id != GateId )
                    {
                        continue;
                    }

                    if ( ( AlarmDate.HasValue == false ) || ( AlarmDate.Value < AlarmRow.alarm_date ) )
                    {
                        AlarmDate = AlarmRow.alarm_date;
                        ImagePath = AlarmRow.alarm_image_path;
                    }
                }
            }

            return ImagePath;
        }

        private void UpdateFilter( int MaxDisplayDays, int Gate1, int Gate2, int Gate3, ALARM_TYPE AlarmA, ALARM_TYPE AlarmB, ALARM_TYPE AlarmC, DateTime FromDate, DateTime ToDate )
        {
            var Condition = new List<string>
            {

                //  日付範囲
                $"(" +
                $"({_AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} >= #{FromDate.ToString( "yyyy/MM/dd HH:mm:ss" )}#)" +
                $" AND " +
                $"({_AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} <= #{ToDate.ToString( "yyyy/MM/dd HH:mm:ss" )}#)" +
                $")",

                //  ゲート
                $"({_AlarmDataSet.ALARM.alarm_gate_idColumn.ColumnName} IN ({Gate1},{Gate2},{Gate3}))",

                //  アラーム区分
                $"({_AlarmDataSet.ALARM.alarm_typeColumn.ColumnName} IN ({( int )AlarmA},{( int )AlarmB},{( int )AlarmC}))"
            };

            var ConditionString = string.Join( " AND ", Condition.ToArray() );
            MonitoringApp.Instance.Debug( $"DataView RowFilter = {ConditionString}" );

            _AlarmDataSet.ALARM.DefaultView.RowFilter = ConditionString;
            MonitoringApp.Instance.Debug( $"DataView RowCount = {_AlarmDataSet.ALARM.DefaultView.Count}" );
        }

        public DateTime MinimumDate( int MaxDisplayDays )
        {
            return DateTime.Today.AddDays( -( MaxDisplayDays - 1 ) );
        }

        public void RemoveRow( int MaxDisplayDays )
        {
            DateTime MinimumDate = DateTime.Today.AddDays( -MaxDisplayDays );
            int DeleteCounnt = 0;

            lock ( _TableLock )
            {
                using ( var View = new DataView( _AlarmDataSet.ALARM ) )
                {
                    View.RowFilter = $"{_AlarmDataSet.ALARM.alarm_dateColumn.ColumnName} < #{MinimumDate.ToString( "yyyy/MM/dd HH:mm:ss" )}#";
                    DeleteCounnt = View.Count;

                    while ( View.Count > 0 )
                    {
                        if ( View[ 0 ].Row is AlarmDataSet.ALARMRow AlarmRow )
                        {
                            RemoveImage( AlarmRow.alarm_image_path );
                            RemoveMovie( AlarmRow.alarm_id, AlarmRow.alarm_gate_id );
                            AlarmRow.Delete();
                        }
                    }
                }

                _AlarmDataSet.AcceptChanges();
            }

            MonitoringApp.Instance.Info( $"[{DeleteCounnt}]件の警報情報を削除" );

        }

        public ListViewItem CreateListViewItem( int GateId, int AlarmId )
        {
            ListViewItem Item = null;

            if ( _AlarmDataSet.ALARM.FindByalarm_idalarm_gate_id( AlarmId, GateId ) is AlarmDataSet.ALARMRow AlarmRow )
            {
                Item = new ListViewItem( new string[] { AlarmRow.alarm_gate_name, AlarmRow.alarm_type_name, AlarmRow.alarm_date.ToString( "yyyy/MM/dd HH:mm:ss" ) } )
                {
                    Tag = AlarmRow,
                    BackColor = MonitoringApp.Instance.DetectorBackColor( GateId ),
                    ForeColor = MonitoringApp.Instance.DetectorForeColor( GateId )
                };
            }

            return Item;
        }

        public void WriteImage( string ImagePath, ImagePacket Packet )
        {
            if ( Packet.ImageLength > 0 )
            {
                try
                {
                    if ( Directory.Exists( Path.GetDirectoryName( ImagePath ) ) == false )
                    {
                        Directory.CreateDirectory( Path.GetDirectoryName( ImagePath ) );
                    }
                    File.WriteAllBytes( ImagePath, Packet.ImageData );
                }
                catch
                {
                }
            }
        }

        public Mat ReadImage( string ImagePath )
        {
            if ( File.Exists( ImagePath ) == false )
            {
                return null;
            }

            using ( var GdiImage = ( Bitmap )Bitmap.FromFile( ImagePath ) )
            {
                return BitmapConverter.ToMat( GdiImage );
            }
        }

        public void RemoveImage( string ImagePath )
        {
            if ( File.Exists( ImagePath ) == true )
            {
                try
                {
                    File.Delete( ImagePath );
                }
                catch
                {
                }
            }
        }

        public void RemoveMovie( int AlarmId, int DetectorId )
        {
            var MoviePath = Path.Combine( Properties.Settings.Default.ALARM_Movie_Folder, $"{DetectorId}", $"{AlarmId}.mp4" );
            if ( File.Exists( MoviePath ) == false )
            {
                return;
            }

            try
            {
                File.Delete( MoviePath );
                MonitoringApp.Instance?.Info( $"過去動画ファイル({MoviePath})削除" );
            }
            catch
            {
            }
        }
    }
}
