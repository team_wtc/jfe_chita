﻿namespace CES.SOLAS.Monitor
{
    partial class MonitorForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GateNameLabel1 = new System.Windows.Forms.Label();
            this.MoveHomeButton1 = new System.Windows.Forms.Button();
            this.SetHomeButton1 = new System.Windows.Forms.Button();
            this.GateAlarmPanel1 = new System.Windows.Forms.Panel();
            this.GateAlarmPictureBox1 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.AlarmInfoPanel = new System.Windows.Forms.Panel();
            this.SetupButton = new System.Windows.Forms.Button();
            this.AlarmImageClearButton = new System.Windows.Forms.Button();
            this.ClockLabelPpanel = new System.Windows.Forms.Panel();
            this.ClockLabel = new System.Windows.Forms.Label();
            this.AlarmHistoryGroupBox = new System.Windows.Forms.GroupBox();
            this.ResearchButton = new System.Windows.Forms.Button();
            this.AlarmListView = new System.Windows.Forms.ListView();
            this.ColumnHeaderGate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeaderAlarmType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeaderDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NarrowDownCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ToDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.FromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.AlarmTypeCheckBoxC = new System.Windows.Forms.CheckBox();
            this.AlarmTypeCheckBoxB = new System.Windows.Forms.CheckBox();
            this.AlarmTypeCheckBoxA = new System.Windows.Forms.CheckBox();
            this.GateCheckBox3 = new System.Windows.Forms.CheckBox();
            this.GateCheckBox2 = new System.Windows.Forms.CheckBox();
            this.GateCheckBox1 = new System.Windows.Forms.CheckBox();
            this.BuzzerDisableCheckBox = new System.Windows.Forms.CheckBox();
            this.PlaceNameLabel = new System.Windows.Forms.Label();
            this.GatePanel1 = new System.Windows.Forms.Panel();
            this.RestartButton1 = new System.Windows.Forms.Button();
            this.DetectStateLabel1 = new System.Windows.Forms.Label();
            this.DetectDisableCheckBox1 = new System.Windows.Forms.CheckBox();
            this.GateLivePanel1 = new System.Windows.Forms.Panel();
            this.GateLivePictureBoxIpl1 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.GatePanel2 = new System.Windows.Forms.Panel();
            this.RestartButton2 = new System.Windows.Forms.Button();
            this.DetectDisableCheckBox2 = new System.Windows.Forms.CheckBox();
            this.MoveHomeButton2 = new System.Windows.Forms.Button();
            this.SetHomeButton2 = new System.Windows.Forms.Button();
            this.DetectStateLabel2 = new System.Windows.Forms.Label();
            this.GateLivePanel2 = new System.Windows.Forms.Panel();
            this.GateLivePictureBoxIpl2 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.GateAlarmPanel2 = new System.Windows.Forms.Panel();
            this.GateAlarmPictureBox2 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.GateNameLabel2 = new System.Windows.Forms.Label();
            this.GatePanel3 = new System.Windows.Forms.Panel();
            this.RestartButton3 = new System.Windows.Forms.Button();
            this.DetectStateLabel3 = new System.Windows.Forms.Label();
            this.DetectDisableCheckBox3 = new System.Windows.Forms.CheckBox();
            this.MoveHomeButton3 = new System.Windows.Forms.Button();
            this.SetHomeButton3 = new System.Windows.Forms.Button();
            this.GateLivePanel3 = new System.Windows.Forms.Panel();
            this.GateLivePictureBoxIpl3 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.GateAlarmPanel3 = new System.Windows.Forms.Panel();
            this.GateAlarmPictureBox3 = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.GateNameLabel3 = new System.Windows.Forms.Label();
            this.ClockTimer = new System.Windows.Forms.Timer(this.components);
            this.ALARMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alarmDataSet = new CES.SOLAS.Monitor.Data.AlarmDataSet();
            this.GateAlarmPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox1)).BeginInit();
            this.AlarmInfoPanel.SuspendLayout();
            this.ClockLabelPpanel.SuspendLayout();
            this.AlarmHistoryGroupBox.SuspendLayout();
            this.GatePanel1.SuspendLayout();
            this.GateLivePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl1)).BeginInit();
            this.GatePanel2.SuspendLayout();
            this.GateLivePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl2)).BeginInit();
            this.GateAlarmPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox2)).BeginInit();
            this.GatePanel3.SuspendLayout();
            this.GateLivePanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl3)).BeginInit();
            this.GateAlarmPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ALARMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // GateNameLabel1
            // 
            this.GateNameLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateNameLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateNameLabel1.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.GateNameLabel1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.GateNameLabel1.Location = new System.Drawing.Point(637, 8);
            this.GateNameLabel1.Name = "GateNameLabel1";
            this.GateNameLabel1.Size = new System.Drawing.Size(155, 40);
            this.GateNameLabel1.TabIndex = 1;
            this.GateNameLabel1.Text = "GATE 4";
            this.GateNameLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MoveHomeButton1
            // 
            this.MoveHomeButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MoveHomeButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MoveHomeButton1.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.MoveHomeButton1.ForeColor = System.Drawing.Color.Black;
            this.MoveHomeButton1.Location = new System.Drawing.Point(637, 221);
            this.MoveHomeButton1.Name = "MoveHomeButton1";
            this.MoveHomeButton1.Size = new System.Drawing.Size(155, 40);
            this.MoveHomeButton1.TabIndex = 2;
            this.MoveHomeButton1.Text = "HOME移動";
            this.MoveHomeButton1.UseVisualStyleBackColor = false;
            this.MoveHomeButton1.Click += new System.EventHandler(this.MoveHomeButton1_Click);
            // 
            // SetHomeButton1
            // 
            this.SetHomeButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SetHomeButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SetHomeButton1.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.SetHomeButton1.ForeColor = System.Drawing.Color.Black;
            this.SetHomeButton1.Location = new System.Drawing.Point(637, 267);
            this.SetHomeButton1.Name = "SetHomeButton1";
            this.SetHomeButton1.Size = new System.Drawing.Size(155, 40);
            this.SetHomeButton1.TabIndex = 4;
            this.SetHomeButton1.Text = "HOME設定";
            this.SetHomeButton1.UseVisualStyleBackColor = false;
            this.SetHomeButton1.Click += new System.EventHandler(this.SetHomeButton1_Click);
            // 
            // GateAlarmPanel1
            // 
            this.GateAlarmPanel1.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateAlarmPanel1.Controls.Add(this.GateAlarmPictureBox1);
            this.GateAlarmPanel1.Location = new System.Drawing.Point(85, 1);
            this.GateAlarmPanel1.Name = "GateAlarmPanel1";
            this.GateAlarmPanel1.Size = new System.Drawing.Size(477, 356);
            this.GateAlarmPanel1.TabIndex = 5;
            // 
            // GateAlarmPictureBox1
            // 
            this.GateAlarmPictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateAlarmPictureBox1.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPictureBox1.Location = new System.Drawing.Point(2, 2);
            this.GateAlarmPictureBox1.Name = "GateAlarmPictureBox1";
            this.GateAlarmPictureBox1.Size = new System.Drawing.Size(471, 349);
            this.GateAlarmPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateAlarmPictureBox1.TabIndex = 1;
            this.GateAlarmPictureBox1.TabStop = false;
            this.GateAlarmPictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // AlarmInfoPanel
            // 
            this.AlarmInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmInfoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AlarmInfoPanel.Controls.Add(this.SetupButton);
            this.AlarmInfoPanel.Controls.Add(this.AlarmImageClearButton);
            this.AlarmInfoPanel.Controls.Add(this.ClockLabelPpanel);
            this.AlarmInfoPanel.Controls.Add(this.AlarmHistoryGroupBox);
            this.AlarmInfoPanel.Controls.Add(this.BuzzerDisableCheckBox);
            this.AlarmInfoPanel.Controls.Add(this.PlaceNameLabel);
            this.AlarmInfoPanel.Location = new System.Drawing.Point(808, 0);
            this.AlarmInfoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.AlarmInfoPanel.Name = "AlarmInfoPanel";
            this.AlarmInfoPanel.Size = new System.Drawing.Size(480, 1068);
            this.AlarmInfoPanel.TabIndex = 6;
            // 
            // SetupButton
            // 
            this.SetupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SetupButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SetupButton.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold);
            this.SetupButton.ForeColor = System.Drawing.Color.Black;
            this.SetupButton.Location = new System.Drawing.Point(319, 1023);
            this.SetupButton.Name = "SetupButton";
            this.SetupButton.Size = new System.Drawing.Size(150, 40);
            this.SetupButton.TabIndex = 16;
            this.SetupButton.Text = "通信設定";
            this.SetupButton.UseVisualStyleBackColor = false;
            this.SetupButton.Click += new System.EventHandler(this.SetupButton_Click);
            // 
            // AlarmImageClearButton
            // 
            this.AlarmImageClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AlarmImageClearButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AlarmImageClearButton.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold);
            this.AlarmImageClearButton.ForeColor = System.Drawing.Color.Black;
            this.AlarmImageClearButton.Location = new System.Drawing.Point(163, 1023);
            this.AlarmImageClearButton.Name = "AlarmImageClearButton";
            this.AlarmImageClearButton.Size = new System.Drawing.Size(150, 40);
            this.AlarmImageClearButton.TabIndex = 15;
            this.AlarmImageClearButton.Text = "警報画像消去";
            this.AlarmImageClearButton.UseVisualStyleBackColor = false;
            this.AlarmImageClearButton.Click += new System.EventHandler(this.AlarmImageClearButton_Click);
            // 
            // ClockLabelPpanel
            // 
            this.ClockLabelPpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClockLabelPpanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClockLabelPpanel.Controls.Add(this.ClockLabel);
            this.ClockLabelPpanel.Location = new System.Drawing.Point(179, 0);
            this.ClockLabelPpanel.Name = "ClockLabelPpanel";
            this.ClockLabelPpanel.Size = new System.Drawing.Size(299, 52);
            this.ClockLabelPpanel.TabIndex = 2;
            // 
            // ClockLabel
            // 
            this.ClockLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClockLabel.BackColor = System.Drawing.Color.Black;
            this.ClockLabel.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ClockLabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.ClockLabel.Location = new System.Drawing.Point(2, 2);
            this.ClockLabel.Name = "ClockLabel";
            this.ClockLabel.Size = new System.Drawing.Size(295, 48);
            this.ClockLabel.TabIndex = 2;
            this.ClockLabel.Text = "9999/99/99 (99) 99:99:99";
            this.ClockLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AlarmHistoryGroupBox
            // 
            this.AlarmHistoryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmHistoryGroupBox.Controls.Add(this.ResearchButton);
            this.AlarmHistoryGroupBox.Controls.Add(this.AlarmListView);
            this.AlarmHistoryGroupBox.Controls.Add(this.NarrowDownCheckBox);
            this.AlarmHistoryGroupBox.Controls.Add(this.label2);
            this.AlarmHistoryGroupBox.Controls.Add(this.ToDateTimePicker);
            this.AlarmHistoryGroupBox.Controls.Add(this.FromDateTimePicker);
            this.AlarmHistoryGroupBox.Controls.Add(this.AlarmTypeCheckBoxC);
            this.AlarmHistoryGroupBox.Controls.Add(this.AlarmTypeCheckBoxB);
            this.AlarmHistoryGroupBox.Controls.Add(this.AlarmTypeCheckBoxA);
            this.AlarmHistoryGroupBox.Controls.Add(this.GateCheckBox3);
            this.AlarmHistoryGroupBox.Controls.Add(this.GateCheckBox2);
            this.AlarmHistoryGroupBox.Controls.Add(this.GateCheckBox1);
            this.AlarmHistoryGroupBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AlarmHistoryGroupBox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.AlarmHistoryGroupBox.Location = new System.Drawing.Point(7, 55);
            this.AlarmHistoryGroupBox.Name = "AlarmHistoryGroupBox";
            this.AlarmHistoryGroupBox.Size = new System.Drawing.Size(468, 962);
            this.AlarmHistoryGroupBox.TabIndex = 6;
            this.AlarmHistoryGroupBox.TabStop = false;
            this.AlarmHistoryGroupBox.Text = "　発報履歴　";
            // 
            // ResearchButton
            // 
            this.ResearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ResearchButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ResearchButton.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.ResearchButton.ForeColor = System.Drawing.Color.Black;
            this.ResearchButton.Location = new System.Drawing.Point(372, 70);
            this.ResearchButton.Name = "ResearchButton";
            this.ResearchButton.Size = new System.Drawing.Size(90, 40);
            this.ResearchButton.TabIndex = 18;
            this.ResearchButton.Text = "再検索";
            this.ResearchButton.UseVisualStyleBackColor = false;
            this.ResearchButton.Visible = false;
            this.ResearchButton.Click += new System.EventHandler(this.ResearchButton_Click);
            // 
            // AlarmListView
            // 
            this.AlarmListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeaderGate,
            this.ColumnHeaderAlarmType,
            this.ColumnHeaderDate});
            this.AlarmListView.Font = new System.Drawing.Font("メイリオ", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AlarmListView.FullRowSelect = true;
            this.AlarmListView.GridLines = true;
            this.AlarmListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.AlarmListView.HideSelection = false;
            this.AlarmListView.LabelWrap = false;
            this.AlarmListView.Location = new System.Drawing.Point(6, 116);
            this.AlarmListView.MultiSelect = false;
            this.AlarmListView.Name = "AlarmListView";
            this.AlarmListView.ShowGroups = false;
            this.AlarmListView.Size = new System.Drawing.Size(453, 840);
            this.AlarmListView.TabIndex = 17;
            this.AlarmListView.UseCompatibleStateImageBehavior = false;
            this.AlarmListView.View = System.Windows.Forms.View.Details;
            this.AlarmListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.AlarmListView_ItemSelectionChanged);
            this.AlarmListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AlarmListView_MouseDoubleClick);
            // 
            // ColumnHeaderGate
            // 
            this.ColumnHeaderGate.Text = "ゲート";
            this.ColumnHeaderGate.Width = 110;
            // 
            // ColumnHeaderAlarmType
            // 
            this.ColumnHeaderAlarmType.Text = "警報区分";
            this.ColumnHeaderAlarmType.Width = 110;
            // 
            // ColumnHeaderDate
            // 
            this.ColumnHeaderDate.Text = "発生日時";
            this.ColumnHeaderDate.Width = 200;
            // 
            // NarrowDownCheckBox
            // 
            this.NarrowDownCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NarrowDownCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.NarrowDownCheckBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NarrowDownCheckBox.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.NarrowDownCheckBox.ForeColor = System.Drawing.Color.Black;
            this.NarrowDownCheckBox.Location = new System.Drawing.Point(372, 24);
            this.NarrowDownCheckBox.Name = "NarrowDownCheckBox";
            this.NarrowDownCheckBox.Size = new System.Drawing.Size(90, 86);
            this.NarrowDownCheckBox.TabIndex = 16;
            this.NarrowDownCheckBox.Text = "絞込";
            this.NarrowDownCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.NarrowDownCheckBox.UseVisualStyleBackColor = false;
            this.NarrowDownCheckBox.CheckedChanged += new System.EventHandler(this.NarrowDownCheckBox_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "～";
            // 
            // ToDateTimePicker
            // 
            this.ToDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ToDateTimePicker.CustomFormat = "yyyy/MM/dd HH:mm";
            this.ToDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToDateTimePicker.Location = new System.Drawing.Point(199, 87);
            this.ToDateTimePicker.Name = "ToDateTimePicker";
            this.ToDateTimePicker.Size = new System.Drawing.Size(157, 23);
            this.ToDateTimePicker.TabIndex = 9;
            // 
            // FromDateTimePicker
            // 
            this.FromDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FromDateTimePicker.CustomFormat = "yyyy/MM/dd HH:mm";
            this.FromDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromDateTimePicker.Location = new System.Drawing.Point(6, 87);
            this.FromDateTimePicker.Name = "FromDateTimePicker";
            this.FromDateTimePicker.Size = new System.Drawing.Size(157, 23);
            this.FromDateTimePicker.TabIndex = 8;
            // 
            // AlarmTypeCheckBoxC
            // 
            this.AlarmTypeCheckBoxC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmTypeCheckBoxC.Checked = true;
            this.AlarmTypeCheckBoxC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AlarmTypeCheckBoxC.Location = new System.Drawing.Point(242, 53);
            this.AlarmTypeCheckBoxC.Name = "AlarmTypeCheckBoxC";
            this.AlarmTypeCheckBoxC.Size = new System.Drawing.Size(112, 20);
            this.AlarmTypeCheckBoxC.TabIndex = 7;
            this.AlarmTypeCheckBoxC.Text = "警報 C";
            this.AlarmTypeCheckBoxC.UseVisualStyleBackColor = true;
            // 
            // AlarmTypeCheckBoxB
            // 
            this.AlarmTypeCheckBoxB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmTypeCheckBoxB.Checked = true;
            this.AlarmTypeCheckBoxB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AlarmTypeCheckBoxB.Location = new System.Drawing.Point(124, 53);
            this.AlarmTypeCheckBoxB.Name = "AlarmTypeCheckBoxB";
            this.AlarmTypeCheckBoxB.Size = new System.Drawing.Size(112, 20);
            this.AlarmTypeCheckBoxB.TabIndex = 6;
            this.AlarmTypeCheckBoxB.Text = "警報 B";
            this.AlarmTypeCheckBoxB.UseVisualStyleBackColor = true;
            // 
            // AlarmTypeCheckBoxA
            // 
            this.AlarmTypeCheckBoxA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmTypeCheckBoxA.Checked = true;
            this.AlarmTypeCheckBoxA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AlarmTypeCheckBoxA.Location = new System.Drawing.Point(6, 53);
            this.AlarmTypeCheckBoxA.Name = "AlarmTypeCheckBoxA";
            this.AlarmTypeCheckBoxA.Size = new System.Drawing.Size(112, 20);
            this.AlarmTypeCheckBoxA.TabIndex = 5;
            this.AlarmTypeCheckBoxA.Text = "警報 A";
            this.AlarmTypeCheckBoxA.UseVisualStyleBackColor = true;
            // 
            // GateCheckBox3
            // 
            this.GateCheckBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateCheckBox3.Checked = true;
            this.GateCheckBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GateCheckBox3.Location = new System.Drawing.Point(242, 24);
            this.GateCheckBox3.Name = "GateCheckBox3";
            this.GateCheckBox3.Size = new System.Drawing.Size(112, 20);
            this.GateCheckBox3.TabIndex = 4;
            this.GateCheckBox3.Text = "ゲート1";
            this.GateCheckBox3.UseVisualStyleBackColor = true;
            // 
            // GateCheckBox2
            // 
            this.GateCheckBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateCheckBox2.Checked = true;
            this.GateCheckBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GateCheckBox2.Location = new System.Drawing.Point(124, 24);
            this.GateCheckBox2.Name = "GateCheckBox2";
            this.GateCheckBox2.Size = new System.Drawing.Size(112, 20);
            this.GateCheckBox2.TabIndex = 3;
            this.GateCheckBox2.Text = "ゲート1";
            this.GateCheckBox2.UseVisualStyleBackColor = true;
            // 
            // GateCheckBox1
            // 
            this.GateCheckBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateCheckBox1.Checked = true;
            this.GateCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GateCheckBox1.Location = new System.Drawing.Point(6, 24);
            this.GateCheckBox1.Name = "GateCheckBox1";
            this.GateCheckBox1.Size = new System.Drawing.Size(112, 20);
            this.GateCheckBox1.TabIndex = 2;
            this.GateCheckBox1.Text = "ゲート1";
            this.GateCheckBox1.UseVisualStyleBackColor = true;
            // 
            // BuzzerDisableCheckBox
            // 
            this.BuzzerDisableCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BuzzerDisableCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.BuzzerDisableCheckBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BuzzerDisableCheckBox.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold);
            this.BuzzerDisableCheckBox.ForeColor = System.Drawing.Color.Black;
            this.BuzzerDisableCheckBox.Location = new System.Drawing.Point(7, 1023);
            this.BuzzerDisableCheckBox.Name = "BuzzerDisableCheckBox";
            this.BuzzerDisableCheckBox.Size = new System.Drawing.Size(150, 40);
            this.BuzzerDisableCheckBox.TabIndex = 5;
            this.BuzzerDisableCheckBox.Text = "鳴動停止";
            this.BuzzerDisableCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BuzzerDisableCheckBox.UseVisualStyleBackColor = false;
            this.BuzzerDisableCheckBox.CheckedChanged += new System.EventHandler(this.BuzzerDisableCheckBox_CheckedChanged);
            // 
            // PlaceNameLabel
            // 
            this.PlaceNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PlaceNameLabel.BackColor = System.Drawing.Color.Cornsilk;
            this.PlaceNameLabel.Font = new System.Drawing.Font("メイリオ", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PlaceNameLabel.ForeColor = System.Drawing.Color.Navy;
            this.PlaceNameLabel.Location = new System.Drawing.Point(6, 0);
            this.PlaceNameLabel.Name = "PlaceNameLabel";
            this.PlaceNameLabel.Size = new System.Drawing.Size(169, 52);
            this.PlaceNameLabel.TabIndex = 3;
            this.PlaceNameLabel.Text = "????";
            this.PlaceNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GatePanel1
            // 
            this.GatePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GatePanel1.Controls.Add(this.RestartButton1);
            this.GatePanel1.Controls.Add(this.DetectStateLabel1);
            this.GatePanel1.Controls.Add(this.GateAlarmPanel1);
            this.GatePanel1.Controls.Add(this.DetectDisableCheckBox1);
            this.GatePanel1.Controls.Add(this.GateLivePanel1);
            this.GatePanel1.Controls.Add(this.GateNameLabel1);
            this.GatePanel1.Controls.Add(this.SetHomeButton1);
            this.GatePanel1.Controls.Add(this.MoveHomeButton1);
            this.GatePanel1.Location = new System.Drawing.Point(0, 0);
            this.GatePanel1.Name = "GatePanel1";
            this.GatePanel1.Size = new System.Drawing.Size(800, 360);
            this.GatePanel1.TabIndex = 7;
            // 
            // RestartButton1
            // 
            this.RestartButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RestartButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RestartButton1.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.RestartButton1.ForeColor = System.Drawing.Color.Black;
            this.RestartButton1.Location = new System.Drawing.Point(637, 313);
            this.RestartButton1.Name = "RestartButton1";
            this.RestartButton1.Size = new System.Drawing.Size(155, 40);
            this.RestartButton1.TabIndex = 11;
            this.RestartButton1.Text = "再　起　動";
            this.RestartButton1.UseVisualStyleBackColor = false;
            this.RestartButton1.Click += new System.EventHandler(this.RestartButton1_Click);
            // 
            // DetectStateLabel1
            // 
            this.DetectStateLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectStateLabel1.BackColor = System.Drawing.Color.PaleGreen;
            this.DetectStateLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DetectStateLabel1.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DetectStateLabel1.ForeColor = System.Drawing.Color.Maroon;
            this.DetectStateLabel1.Location = new System.Drawing.Point(637, 54);
            this.DetectStateLabel1.Name = "DetectStateLabel1";
            this.DetectStateLabel1.Size = new System.Drawing.Size(155, 40);
            this.DetectStateLabel1.TabIndex = 10;
            this.DetectStateLabel1.Text = "検知抑制中";
            this.DetectStateLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DetectDisableCheckBox1
            // 
            this.DetectDisableCheckBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectDisableCheckBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.DetectDisableCheckBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DetectDisableCheckBox1.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DetectDisableCheckBox1.ForeColor = System.Drawing.Color.Black;
            this.DetectDisableCheckBox1.Location = new System.Drawing.Point(637, 175);
            this.DetectDisableCheckBox1.Name = "DetectDisableCheckBox1";
            this.DetectDisableCheckBox1.Size = new System.Drawing.Size(155, 40);
            this.DetectDisableCheckBox1.TabIndex = 9;
            this.DetectDisableCheckBox1.Text = "検 知 抑 制";
            this.DetectDisableCheckBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DetectDisableCheckBox1.UseVisualStyleBackColor = false;
            this.DetectDisableCheckBox1.CheckedChanged += new System.EventHandler(this.DetectDisableCheckBox1_CheckedChanged);
            // 
            // GateLivePanel1
            // 
            this.GateLivePanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateLivePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateLivePanel1.Controls.Add(this.GateLivePictureBoxIpl1);
            this.GateLivePanel1.Location = new System.Drawing.Point(0, 1);
            this.GateLivePanel1.Name = "GateLivePanel1";
            this.GateLivePanel1.Size = new System.Drawing.Size(79, 356);
            this.GateLivePanel1.TabIndex = 8;
            // 
            // GateLivePictureBoxIpl1
            // 
            this.GateLivePictureBoxIpl1.Location = new System.Drawing.Point(10, 26);
            this.GateLivePictureBoxIpl1.Name = "GateLivePictureBoxIpl1";
            this.GateLivePictureBoxIpl1.Size = new System.Drawing.Size(43, 71);
            this.GateLivePictureBoxIpl1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateLivePictureBoxIpl1.TabIndex = 12;
            this.GateLivePictureBoxIpl1.TabStop = false;
            this.GateLivePictureBoxIpl1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // GatePanel2
            // 
            this.GatePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GatePanel2.Controls.Add(this.RestartButton2);
            this.GatePanel2.Controls.Add(this.DetectDisableCheckBox2);
            this.GatePanel2.Controls.Add(this.MoveHomeButton2);
            this.GatePanel2.Controls.Add(this.SetHomeButton2);
            this.GatePanel2.Controls.Add(this.DetectStateLabel2);
            this.GatePanel2.Controls.Add(this.GateLivePanel2);
            this.GatePanel2.Controls.Add(this.GateAlarmPanel2);
            this.GatePanel2.Controls.Add(this.GateNameLabel2);
            this.GatePanel2.Location = new System.Drawing.Point(0, 360);
            this.GatePanel2.Name = "GatePanel2";
            this.GatePanel2.Size = new System.Drawing.Size(800, 360);
            this.GatePanel2.TabIndex = 9;
            // 
            // RestartButton2
            // 
            this.RestartButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RestartButton2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RestartButton2.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.RestartButton2.ForeColor = System.Drawing.Color.Black;
            this.RestartButton2.Location = new System.Drawing.Point(637, 313);
            this.RestartButton2.Name = "RestartButton2";
            this.RestartButton2.Size = new System.Drawing.Size(155, 40);
            this.RestartButton2.TabIndex = 15;
            this.RestartButton2.Text = "再　起　動";
            this.RestartButton2.UseVisualStyleBackColor = false;
            this.RestartButton2.Click += new System.EventHandler(this.RestartButton2_Click);
            // 
            // DetectDisableCheckBox2
            // 
            this.DetectDisableCheckBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectDisableCheckBox2.Appearance = System.Windows.Forms.Appearance.Button;
            this.DetectDisableCheckBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DetectDisableCheckBox2.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.DetectDisableCheckBox2.ForeColor = System.Drawing.Color.Black;
            this.DetectDisableCheckBox2.Location = new System.Drawing.Point(637, 175);
            this.DetectDisableCheckBox2.Name = "DetectDisableCheckBox2";
            this.DetectDisableCheckBox2.Size = new System.Drawing.Size(155, 40);
            this.DetectDisableCheckBox2.TabIndex = 14;
            this.DetectDisableCheckBox2.Text = "検知抑制";
            this.DetectDisableCheckBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DetectDisableCheckBox2.UseVisualStyleBackColor = false;
            this.DetectDisableCheckBox2.CheckedChanged += new System.EventHandler(this.DetectDisableCheckBox2_CheckedChanged);
            // 
            // MoveHomeButton2
            // 
            this.MoveHomeButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MoveHomeButton2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MoveHomeButton2.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.MoveHomeButton2.ForeColor = System.Drawing.Color.Black;
            this.MoveHomeButton2.Location = new System.Drawing.Point(637, 221);
            this.MoveHomeButton2.Name = "MoveHomeButton2";
            this.MoveHomeButton2.Size = new System.Drawing.Size(155, 40);
            this.MoveHomeButton2.TabIndex = 13;
            this.MoveHomeButton2.Text = "HOME移動";
            this.MoveHomeButton2.UseVisualStyleBackColor = false;
            this.MoveHomeButton2.Click += new System.EventHandler(this.MoveHomeButton2_Click);
            // 
            // SetHomeButton2
            // 
            this.SetHomeButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SetHomeButton2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SetHomeButton2.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.SetHomeButton2.ForeColor = System.Drawing.Color.Black;
            this.SetHomeButton2.Location = new System.Drawing.Point(637, 267);
            this.SetHomeButton2.Name = "SetHomeButton2";
            this.SetHomeButton2.Size = new System.Drawing.Size(155, 40);
            this.SetHomeButton2.TabIndex = 12;
            this.SetHomeButton2.Text = "HOME設定";
            this.SetHomeButton2.UseVisualStyleBackColor = false;
            this.SetHomeButton2.Click += new System.EventHandler(this.SetHomeButton2_Click);
            // 
            // DetectStateLabel2
            // 
            this.DetectStateLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectStateLabel2.BackColor = System.Drawing.Color.PaleGreen;
            this.DetectStateLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DetectStateLabel2.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.DetectStateLabel2.ForeColor = System.Drawing.Color.Maroon;
            this.DetectStateLabel2.Location = new System.Drawing.Point(637, 53);
            this.DetectStateLabel2.Name = "DetectStateLabel2";
            this.DetectStateLabel2.Size = new System.Drawing.Size(155, 40);
            this.DetectStateLabel2.TabIndex = 11;
            this.DetectStateLabel2.Text = "検知抑制中";
            this.DetectStateLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GateLivePanel2
            // 
            this.GateLivePanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateLivePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateLivePanel2.Controls.Add(this.GateLivePictureBoxIpl2);
            this.GateLivePanel2.Location = new System.Drawing.Point(0, 1);
            this.GateLivePanel2.Name = "GateLivePanel2";
            this.GateLivePanel2.Size = new System.Drawing.Size(79, 356);
            this.GateLivePanel2.TabIndex = 8;
            // 
            // GateLivePictureBoxIpl2
            // 
            this.GateLivePictureBoxIpl2.Location = new System.Drawing.Point(10, 82);
            this.GateLivePictureBoxIpl2.Name = "GateLivePictureBoxIpl2";
            this.GateLivePictureBoxIpl2.Size = new System.Drawing.Size(55, 115);
            this.GateLivePictureBoxIpl2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateLivePictureBoxIpl2.TabIndex = 16;
            this.GateLivePictureBoxIpl2.TabStop = false;
            this.GateLivePictureBoxIpl2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // GateAlarmPanel2
            // 
            this.GateAlarmPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateAlarmPanel2.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateAlarmPanel2.Controls.Add(this.GateAlarmPictureBox2);
            this.GateAlarmPanel2.Location = new System.Drawing.Point(85, 1);
            this.GateAlarmPanel2.Name = "GateAlarmPanel2";
            this.GateAlarmPanel2.Size = new System.Drawing.Size(477, 356);
            this.GateAlarmPanel2.TabIndex = 5;
            // 
            // GateAlarmPictureBox2
            // 
            this.GateAlarmPictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateAlarmPictureBox2.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPictureBox2.Location = new System.Drawing.Point(2, 2);
            this.GateAlarmPictureBox2.Name = "GateAlarmPictureBox2";
            this.GateAlarmPictureBox2.Size = new System.Drawing.Size(470, 349);
            this.GateAlarmPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateAlarmPictureBox2.TabIndex = 2;
            this.GateAlarmPictureBox2.TabStop = false;
            this.GateAlarmPictureBox2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // GateNameLabel2
            // 
            this.GateNameLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateNameLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateNameLabel2.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.GateNameLabel2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.GateNameLabel2.Location = new System.Drawing.Point(637, 4);
            this.GateNameLabel2.Name = "GateNameLabel2";
            this.GateNameLabel2.Size = new System.Drawing.Size(155, 40);
            this.GateNameLabel2.TabIndex = 1;
            this.GateNameLabel2.Text = "GATE 5";
            this.GateNameLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GatePanel3
            // 
            this.GatePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GatePanel3.Controls.Add(this.RestartButton3);
            this.GatePanel3.Controls.Add(this.DetectStateLabel3);
            this.GatePanel3.Controls.Add(this.DetectDisableCheckBox3);
            this.GatePanel3.Controls.Add(this.MoveHomeButton3);
            this.GatePanel3.Controls.Add(this.SetHomeButton3);
            this.GatePanel3.Controls.Add(this.GateLivePanel3);
            this.GatePanel3.Controls.Add(this.GateAlarmPanel3);
            this.GatePanel3.Controls.Add(this.GateNameLabel3);
            this.GatePanel3.Location = new System.Drawing.Point(0, 735);
            this.GatePanel3.Name = "GatePanel3";
            this.GatePanel3.Size = new System.Drawing.Size(800, 360);
            this.GatePanel3.TabIndex = 10;
            // 
            // RestartButton3
            // 
            this.RestartButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RestartButton3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RestartButton3.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.RestartButton3.ForeColor = System.Drawing.Color.Black;
            this.RestartButton3.Location = new System.Drawing.Point(637, 312);
            this.RestartButton3.Name = "RestartButton3";
            this.RestartButton3.Size = new System.Drawing.Size(155, 40);
            this.RestartButton3.TabIndex = 17;
            this.RestartButton3.Text = "再　起　動";
            this.RestartButton3.UseVisualStyleBackColor = false;
            this.RestartButton3.Click += new System.EventHandler(this.RestartButton3_Click);
            // 
            // DetectStateLabel3
            // 
            this.DetectStateLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectStateLabel3.BackColor = System.Drawing.Color.LawnGreen;
            this.DetectStateLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DetectStateLabel3.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.DetectStateLabel3.ForeColor = System.Drawing.Color.Black;
            this.DetectStateLabel3.Location = new System.Drawing.Point(637, 52);
            this.DetectStateLabel3.Name = "DetectStateLabel3";
            this.DetectStateLabel3.Size = new System.Drawing.Size(155, 40);
            this.DetectStateLabel3.TabIndex = 16;
            this.DetectStateLabel3.Text = "検知抑制中";
            this.DetectStateLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DetectDisableCheckBox3
            // 
            this.DetectDisableCheckBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectDisableCheckBox3.Appearance = System.Windows.Forms.Appearance.Button;
            this.DetectDisableCheckBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DetectDisableCheckBox3.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.DetectDisableCheckBox3.ForeColor = System.Drawing.Color.Black;
            this.DetectDisableCheckBox3.Location = new System.Drawing.Point(637, 174);
            this.DetectDisableCheckBox3.Name = "DetectDisableCheckBox3";
            this.DetectDisableCheckBox3.Size = new System.Drawing.Size(155, 40);
            this.DetectDisableCheckBox3.TabIndex = 15;
            this.DetectDisableCheckBox3.Text = "検知抑制";
            this.DetectDisableCheckBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DetectDisableCheckBox3.UseVisualStyleBackColor = false;
            this.DetectDisableCheckBox3.CheckedChanged += new System.EventHandler(this.DetectDisableCheckBox3_CheckedChanged);
            // 
            // MoveHomeButton3
            // 
            this.MoveHomeButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MoveHomeButton3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MoveHomeButton3.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.MoveHomeButton3.ForeColor = System.Drawing.Color.Black;
            this.MoveHomeButton3.Location = new System.Drawing.Point(637, 220);
            this.MoveHomeButton3.Name = "MoveHomeButton3";
            this.MoveHomeButton3.Size = new System.Drawing.Size(155, 40);
            this.MoveHomeButton3.TabIndex = 14;
            this.MoveHomeButton3.Text = "HOME移動";
            this.MoveHomeButton3.UseVisualStyleBackColor = false;
            this.MoveHomeButton3.Click += new System.EventHandler(this.MoveHomeButton3_Click);
            // 
            // SetHomeButton3
            // 
            this.SetHomeButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SetHomeButton3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SetHomeButton3.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold);
            this.SetHomeButton3.ForeColor = System.Drawing.Color.Black;
            this.SetHomeButton3.Location = new System.Drawing.Point(637, 266);
            this.SetHomeButton3.Name = "SetHomeButton3";
            this.SetHomeButton3.Size = new System.Drawing.Size(155, 40);
            this.SetHomeButton3.TabIndex = 13;
            this.SetHomeButton3.Text = "HOME設定";
            this.SetHomeButton3.UseVisualStyleBackColor = false;
            this.SetHomeButton3.Click += new System.EventHandler(this.SetHomeButton3_Click);
            // 
            // GateLivePanel3
            // 
            this.GateLivePanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateLivePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateLivePanel3.Controls.Add(this.GateLivePictureBoxIpl3);
            this.GateLivePanel3.Location = new System.Drawing.Point(0, 1);
            this.GateLivePanel3.Name = "GateLivePanel3";
            this.GateLivePanel3.Size = new System.Drawing.Size(82, 356);
            this.GateLivePanel3.TabIndex = 8;
            // 
            // GateLivePictureBoxIpl3
            // 
            this.GateLivePictureBoxIpl3.Location = new System.Drawing.Point(10, 15);
            this.GateLivePictureBoxIpl3.Name = "GateLivePictureBoxIpl3";
            this.GateLivePictureBoxIpl3.Size = new System.Drawing.Size(55, 115);
            this.GateLivePictureBoxIpl3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateLivePictureBoxIpl3.TabIndex = 18;
            this.GateLivePictureBoxIpl3.TabStop = false;
            this.GateLivePictureBoxIpl3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // GateAlarmPanel3
            // 
            this.GateAlarmPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateAlarmPanel3.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateAlarmPanel3.Controls.Add(this.GateAlarmPictureBox3);
            this.GateAlarmPanel3.Location = new System.Drawing.Point(85, 1);
            this.GateAlarmPanel3.Name = "GateAlarmPanel3";
            this.GateAlarmPanel3.Size = new System.Drawing.Size(476, 356);
            this.GateAlarmPanel3.TabIndex = 5;
            // 
            // GateAlarmPictureBox3
            // 
            this.GateAlarmPictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GateAlarmPictureBox3.BackColor = System.Drawing.Color.Black;
            this.GateAlarmPictureBox3.Location = new System.Drawing.Point(2, 2);
            this.GateAlarmPictureBox3.Name = "GateAlarmPictureBox3";
            this.GateAlarmPictureBox3.Size = new System.Drawing.Size(469, 349);
            this.GateAlarmPictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GateAlarmPictureBox3.TabIndex = 3;
            this.GateAlarmPictureBox3.TabStop = false;
            this.GateAlarmPictureBox3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDoubleClick);
            // 
            // GateNameLabel3
            // 
            this.GateNameLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GateNameLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GateNameLabel3.Font = new System.Drawing.Font("メイリオ", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.GateNameLabel3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.GateNameLabel3.Location = new System.Drawing.Point(637, 6);
            this.GateNameLabel3.Name = "GateNameLabel3";
            this.GateNameLabel3.Size = new System.Drawing.Size(155, 40);
            this.GateNameLabel3.TabIndex = 1;
            this.GateNameLabel3.Text = "GATE 6";
            this.GateNameLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ClockTimer
            // 
            this.ClockTimer.Tick += new System.EventHandler(this.ClockTimer_Tick);
            // 
            // ALARMBindingSource
            // 
            this.ALARMBindingSource.DataMember = "ALARM";
            this.ALARMBindingSource.DataSource = this.alarmDataSet;
            // 
            // alarmDataSet
            // 
            this.alarmDataSet.DataSetName = "AlarmDataSet";
            this.alarmDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MonitorForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1287, 1061);
            this.Controls.Add(this.GatePanel3);
            this.Controls.Add(this.GatePanel2);
            this.Controls.Add(this.GatePanel1);
            this.Controls.Add(this.AlarmInfoPanel);
            this.Font = new System.Drawing.Font("メイリオ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MonitorForm";
            this.Text = "Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorForm_FormClosing);
            this.Load += new System.EventHandler(this.MonitorForm_Load);
            this.Shown += new System.EventHandler(this.MonitorForm_Shown);
            this.GateAlarmPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox1)).EndInit();
            this.AlarmInfoPanel.ResumeLayout(false);
            this.ClockLabelPpanel.ResumeLayout(false);
            this.AlarmHistoryGroupBox.ResumeLayout(false);
            this.AlarmHistoryGroupBox.PerformLayout();
            this.GatePanel1.ResumeLayout(false);
            this.GateLivePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl1)).EndInit();
            this.GatePanel2.ResumeLayout(false);
            this.GateLivePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl2)).EndInit();
            this.GateAlarmPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox2)).EndInit();
            this.GatePanel3.ResumeLayout(false);
            this.GateLivePanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateLivePictureBoxIpl3)).EndInit();
            this.GateAlarmPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GateAlarmPictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ALARMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label GateNameLabel1;
        private System.Windows.Forms.Button MoveHomeButton1;
        private System.Windows.Forms.Button SetHomeButton1;
        private System.Windows.Forms.Panel GateAlarmPanel1;
        private System.Windows.Forms.Panel AlarmInfoPanel;
        private System.Windows.Forms.Label PlaceNameLabel;
        private System.Windows.Forms.Label ClockLabel;
        private System.Windows.Forms.GroupBox AlarmHistoryGroupBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker ToDateTimePicker;
        private System.Windows.Forms.DateTimePicker FromDateTimePicker;
        private System.Windows.Forms.CheckBox AlarmTypeCheckBoxC;
        private System.Windows.Forms.CheckBox AlarmTypeCheckBoxB;
        private System.Windows.Forms.CheckBox AlarmTypeCheckBoxA;
        private System.Windows.Forms.CheckBox GateCheckBox3;
        private System.Windows.Forms.CheckBox GateCheckBox2;
        private System.Windows.Forms.CheckBox GateCheckBox1;
        private System.Windows.Forms.CheckBox BuzzerDisableCheckBox;
        private System.Windows.Forms.Panel GatePanel1;
        private System.Windows.Forms.Panel GateLivePanel1;
        private System.Windows.Forms.Panel GatePanel2;
        private System.Windows.Forms.Panel GateLivePanel2;
        private System.Windows.Forms.Panel GateAlarmPanel2;
        private System.Windows.Forms.Label GateNameLabel2;
        private System.Windows.Forms.Panel GatePanel3;
        private System.Windows.Forms.Panel GateLivePanel3;
        private System.Windows.Forms.Panel GateAlarmPanel3;
        private System.Windows.Forms.Label GateNameLabel3;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateAlarmPictureBox1;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateAlarmPictureBox2;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateAlarmPictureBox3;
        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.Panel ClockLabelPpanel;
        private System.Windows.Forms.Label DetectStateLabel1;
        private System.Windows.Forms.CheckBox DetectDisableCheckBox1;
        private System.Windows.Forms.Label DetectStateLabel2;
        private System.Windows.Forms.Button MoveHomeButton2;
        private System.Windows.Forms.Button SetHomeButton2;
        private System.Windows.Forms.CheckBox DetectDisableCheckBox2;
        private System.Windows.Forms.Label DetectStateLabel3;
        private System.Windows.Forms.CheckBox DetectDisableCheckBox3;
        private System.Windows.Forms.Button MoveHomeButton3;
        private System.Windows.Forms.Button SetHomeButton3;
        private System.Windows.Forms.Button RestartButton1;
        private System.Windows.Forms.Button RestartButton2;
        private System.Windows.Forms.Button RestartButton3;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateLivePictureBoxIpl1;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateLivePictureBoxIpl2;
        private OpenCvSharp.UserInterface.PictureBoxIpl GateLivePictureBoxIpl3;
        private System.Windows.Forms.BindingSource ALARMBindingSource;
        private Data.AlarmDataSet alarmDataSet;
        private System.Windows.Forms.Button AlarmImageClearButton;
        private System.Windows.Forms.CheckBox NarrowDownCheckBox;
        private System.Windows.Forms.ListView AlarmListView;
        private System.Windows.Forms.ColumnHeader ColumnHeaderGate;
        private System.Windows.Forms.ColumnHeader ColumnHeaderAlarmType;
        private System.Windows.Forms.ColumnHeader ColumnHeaderDate;
        private System.Windows.Forms.Button ResearchButton;
        private System.Windows.Forms.Button SetupButton;
    }
}

