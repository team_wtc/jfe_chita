﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.SOLAS.Monitor
{
    using Properties;
    using Lib.UI;
    using Lib.ProcessIF.UDP;

    public partial class MonitorForm : Form
    {
        bool _ForceExit = false;
        DateTime _LastHeartBeatSendTime = DateTime.MinValue;
        DateTime _NextAlarmDeleteTime = DateTime.MinValue;
        MonitoringTriggerCallback _TriggerCallback;
        bool _DisableSendDetectSuppress1 = false;
        bool _DisableSendDetectSuppress2 = false;
        bool _DisableSendDetectSuppress3 = false;

        readonly int[] _DetectorIds = new int[ 3 ];

        PlayerFormThread _PlayerFormThread = null;

        public MonitorForm()
        {
            InitializeComponent();
        }

        private void MonitorForm_Load( object sender, EventArgs e )
        {
            try
            {
                _TriggerCallback = new MonitoringTriggerCallback();
                _TriggerCallback.TriggerReceive += this._TriggerCallback_TriggerReceive;
                MonitoringApp.Instance.NewFrameImage += this.Instance_NewFrameImage;
                MonitoringApp.Instance.NewAlarmImage += this.Instance_NewAlarmImage;
                MonitoringApp.Instance.NewAlarmReceive += this.Instance_NewAlarmReceive;

                MonitoringApp.Instance.AppInitialyze(
                    Settings.Default.DB_Server,
                    Settings.Default.DB_Password,
                    _TriggerCallback,
                    //null,
                    Settings.Default.LOG_Folder,
                    Application.ProductName,
                    Settings.Default.LOG_Ext,
                    Settings.Default.LOG_Size,
                    Settings.Default.LOG_Days,
                    Settings.Default.LOG_Debug );
            }
            catch ( Exception exp )
            {
                MessageBox.Show( $"{Application.ProductName} を起動できません。{Environment.NewLine}{exp.Message}", $"{Application.ProductName}起動失敗" );
                _ForceExit = true;
                Close();
                return;
            }

            InitScreen();

            _NextAlarmDeleteTime = DateTime.Today;
            ClockTimer.Enabled = true;

            MonitoringApp.Instance.InitMonitoring(
                GateLivePictureBoxIpl1, GateLivePictureBoxIpl2, GateLivePictureBoxIpl3,
                out int DetectorId1, out int DetectorId2, out int DetectorId3 );

            MonitoringApp.Instance.InitAlarmImageDrawer( GateAlarmPictureBox1, GateAlarmPictureBox2, GateAlarmPictureBox3 );

            GatePanel1.Tag = DetectorId1;
            GatePanel2.Tag = DetectorId2;
            GatePanel3.Tag = DetectorId3;

            _DetectorIds[ 0 ] = DetectorId1;
            _DetectorIds[ 1 ] = DetectorId2;
            _DetectorIds[ 2 ] = DetectorId3;

            GateNameLabel1.Text = MonitoringApp.Instance.GateName( DetectorId1 );
            GateNameLabel2.Text = MonitoringApp.Instance.GateName( DetectorId2 );
            GateNameLabel3.Text = MonitoringApp.Instance.GateName( DetectorId3 );

            GateCheckBox1.Text = MonitoringApp.Instance.GateName( DetectorId1 );
            GateCheckBox2.Text = MonitoringApp.Instance.GateName( DetectorId2 );
            GateCheckBox3.Text = MonitoringApp.Instance.GateName( DetectorId3 );

            GateNameLabel1.BackColor = MonitoringApp.Instance.DetectorBackColor( DetectorId1 );
            GateNameLabel1.ForeColor = MonitoringApp.Instance.DetectorForeColor( DetectorId1 );

            GateNameLabel2.BackColor = MonitoringApp.Instance.DetectorBackColor( DetectorId2 );
            GateNameLabel2.ForeColor = MonitoringApp.Instance.DetectorForeColor( DetectorId2 );

            GateNameLabel3.BackColor = MonitoringApp.Instance.DetectorBackColor( DetectorId3 );
            GateNameLabel3.ForeColor = MonitoringApp.Instance.DetectorForeColor( DetectorId3 );

            AlarmTypeCheckBoxA.Text = MonitoringApp.Instance.AlarmName( Lib.ALARM_TYPE.ALARM_TYPE_A );
            AlarmTypeCheckBoxB.Text = MonitoringApp.Instance.AlarmName( Lib.ALARM_TYPE.ALARM_TYPE_B );
            AlarmTypeCheckBoxC.Text = MonitoringApp.Instance.AlarmName( Lib.ALARM_TYPE.ALARM_TYPE_C );

            BuzzerDisableCheckBox.Checked = MonitoringApp.Instance.DsiableBuzzer;
            if ( BuzzerDisableCheckBox.Checked == true )
            {
                BuzzerDisableCheckBox.BackColor = Color.LightSalmon;
                BuzzerDisableCheckBox.ForeColor = Color.WhiteSmoke;
                BuzzerDisableCheckBox.Text = "ブザー OFF";
                //MonitoringApp.Instance.BuzzerStop();
            }
            else
            {
                BuzzerDisableCheckBox.BackColor = Color.WhiteSmoke;
                BuzzerDisableCheckBox.ForeColor = Color.Black;
                BuzzerDisableCheckBox.Text = "ブザー ON";
            }

            _LastHeartBeatSendTime = DateTime.Now;
            FromDateTimePicker.Value = MonitoringApp.Instance.MinimumDate;
            ToDateTimePicker.Value = DateTime.Now;

            _LastHeartBeatSendTime = DateTime.Now;
            MonitoringApp.Instance.HeartBeat();

            ResetListView( MonitoringApp.Instance.AllData() );
            DisplayLastAlarm( _DetectorIds[ 0 ] );
            DisplayLastAlarm( _DetectorIds[ 1 ] );
            DisplayLastAlarm( _DetectorIds[ 2 ] );
        }

        private void Instance_NewAlarmReceive( object sender, NewAlarmEventArgs e )
        {
            ToDateTimePicker.Value = DateTime.Now;

            var Item = MonitoringApp.Instance.CreateItem( e.GateId, e.AlarmId );
            AlarmListView.Items.Insert( 0, Item );
            Item.Selected = true;
        }

        private void Instance_NewAlarmImage( object sender, OpenCVImageDrawEventArgs e )
        {
            if ( _DetectorIds[ 0 ] == e.DetectorId )
            {
                GateAlarmPictureBox1.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );
                GateAlarmPictureBox2.BackColor = Color.Black;
                GateAlarmPictureBox3.BackColor = Color.Black;

                GateAlarmPanel1.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );
                GateAlarmPanel2.BackColor = Color.Black;
                GateAlarmPanel3.BackColor = Color.Black;

                GateAlarmPictureBox1.RefreshIplImage( e.Image );
            }
            if ( _DetectorIds[ 1 ] == e.DetectorId )
            {
                GateAlarmPictureBox1.BackColor = Color.Black;
                GateAlarmPictureBox2.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );
                GateAlarmPictureBox3.BackColor = Color.Black;

                GateAlarmPanel1.BackColor = Color.Black;
                GateAlarmPanel2.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );
                GateAlarmPanel3.BackColor = Color.Black;

                GateAlarmPictureBox2.RefreshIplImage( e.Image );
            }
            if ( _DetectorIds[ 2 ] == e.DetectorId )
            {
                GateAlarmPictureBox1.BackColor = Color.Black;
                GateAlarmPictureBox2.BackColor = Color.Black;
                GateAlarmPictureBox3.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );

                GateAlarmPanel1.BackColor = Color.Black;
                GateAlarmPanel2.BackColor = Color.Black;
                GateAlarmPanel3.BackColor = MonitoringApp.Instance.DetectorBackColor( e.DetectorId );

                GateAlarmPictureBox3.RefreshIplImage( e.Image );
            }
        }

        private void Instance_NewFrameImage( object sender, OpenCVImageDrawEventArgs e )
        {
            //MonitoringApp.Instance.Debug( $"Instance_NewFrameImage Event Handler e.DetectorId={e.DetectorId}" );
            if ( _DetectorIds[ 0 ] == e.DetectorId )
            {
                GateLivePictureBoxIpl1.ImageIpl?.Dispose();
                GateLivePictureBoxIpl1.ImageIpl = e.Image;
                //GateLivePictureBoxIpl1.RefreshIplImage( e.Image );
                //MonitoringApp.Instance.Debug( $"GateLivePictureBoxIpl1.RefreshIplImage" );
            }
            if ( _DetectorIds[ 1 ] == e.DetectorId )
            {
                GateLivePictureBoxIpl2.ImageIpl?.Dispose();
                GateLivePictureBoxIpl2.ImageIpl = e.Image;
                //GateLivePictureBoxIpl2.RefreshIplImage( e.Image );
                //MonitoringApp.Instance.Debug( $"GateLivePictureBoxIpl2.RefreshIplImage" );
            }
            if ( _DetectorIds[ 2 ] == e.DetectorId )
            {
                GateLivePictureBoxIpl3.ImageIpl?.Dispose();
                GateLivePictureBoxIpl3.ImageIpl = e.Image;
                //GateLivePictureBoxIpl3.RefreshIplImage( e.Image );
                //MonitoringApp.Instance.Debug( $"GateLivePictureBoxIpl3.RefreshIplImage" );
            }
        }

        private void _TriggerCallback_TriggerReceive( object sender, TriggerReceiveEventArgs e )
        {
            switch ( e.TriggerType )
            {
                case TRIGGER_TYPE.TRG_NEW_ALARM:
                    //GateNameLabel1.Text += $"ID={( ( Lib.ProcessIF.UDP.AlarmPacket )( e.Packet ) ).AlarmId} TYPE={( ( Lib.ProcessIF.UDP.AlarmPacket )( e.Packet ) ).AlarmType}";
                    break;
                case TRIGGER_TYPE.TRG_NEW_IMAGE:
                    //GateNameLabel1.Text += $"ID={( ( Lib.ProcessIF.UDP.ImagePacket )( e.Packet ) ).ImageId}";
                    break;
                case TRIGGER_TYPE.TRG_CURRENT_HOME:
                    if ( e.Packet is HomePositionPacket HomePosPkt )
                    {
                        string GateName = string.Empty;

                        if ( _DetectorIds[ 0 ] == e.ProcessId )
                        {
                            GateName = MonitoringApp.Instance.GateName( _DetectorIds[ 0 ] );
                            SetHomeButton1.Enabled = true;
                        }
                        if ( _DetectorIds[ 1 ] == e.ProcessId )
                        {
                            GateName = MonitoringApp.Instance.GateName( _DetectorIds[ 1 ] );
                            SetHomeButton2.Enabled = true;
                        }
                        if ( _DetectorIds[ 2 ] == e.ProcessId )
                        {
                            GateName = MonitoringApp.Instance.GateName( _DetectorIds[ 2 ] );
                            SetHomeButton3.Enabled = true;
                        }
                        if ( string.IsNullOrWhiteSpace( GateName ) == false )
                        {
                            //AutoCloseMessageBox.Show(
                            //    $"{GateName}カメラのホームポジションが設定されました。{Environment.NewLine}{Environment.NewLine}" +
                            //    $"Pan={HomePosPkt.Pan}、Tilt={HomePosPkt.Tilt}、Zoom={HomePosPkt.Zoom}、ZoomRatio={HomePosPkt.ZoomRatio}",
                            //    $"{GateName} Home Position",
                            //    MonitoringApp.Instance.MessageBoxTimeout );
                        }
                    }
                    break;
                case TRIGGER_TYPE.TRG_ERROR_MSG:
                    if ( e.Packet is ErrorMsgPacket MessagePacket )
                    {
                        //AutoCloseMessageBox.Show(
                        //$"{MonitoringApp.Instance.GateName( e.ProcessId )} でエラーが発生しました。{Environment.NewLine}{Environment.NewLine}{MessagePacket.Message}",
                        //$"{MonitoringApp.Instance.GateName( e.ProcessId )} Error",
                        //MonitoringApp.Instance.MessageBoxTimeout,
                        //MessageBoxButtons.OK,
                        //MessageBoxIcon.Error );
                    }
                    break;
                case TRIGGER_TYPE.TRG_APP_STATUS:
                    SetDetectorStatus( e );
                    break;
            }
        }


        private void MonitorForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( ( _ForceExit == false ) && ( e.CloseReason == CloseReason.UserClosing ) )
            {
                var Result = MessageBox.Show(
                    text: $"SOLASモニタリングソフトウェアを終了します。{Environment.NewLine}" +
                    $"よろしいですか？{Environment.NewLine}{Environment.NewLine}",
                    caption: $"{Application.ProductName}の終了",
                    buttons: MessageBoxButtons.OKCancel,
                    icon: MessageBoxIcon.Question,
                    defaultButton: MessageBoxDefaultButton.Button2 );

                if ( Result == DialogResult.Cancel )
                {
                    e.Cancel = true;
                    return;
                }
            }
            MonitoringApp.Instance.StopAlarmImageDrawer();
            MonitoringApp.Instance.StopMonitoring();
            MonitoringApp.Instance.AppFinalyze();
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            if ( ( keyData & Keys.Escape ) == keyData )
            {
                if ( ( _FullScreen == false ) && ( _FullScreenPictureBox == null ) )
                {
                    Close();
                }
                else
                {
                    FullScreen( false, null );
                }
                return true;
            }

            return base.ProcessCmdKey( ref msg, keyData );
        }



        private void ClockTimer_Tick( object sender, EventArgs e )
        {
            ClockLabel.Text = DateTime.Now.ToString( "yyyy/MM/dd (ddd) HH:mm:ss" );

            if ( _LastHeartBeatSendTime.AddSeconds( MonitoringApp.Instance.HeartBeatCycle ) < DateTime.Now )
            {
                _LastHeartBeatSendTime = DateTime.Now;
                MonitoringApp.Instance.HeartBeat();
            }

            if ( _NextAlarmDeleteTime < DateTime.Now )
            {
                _NextAlarmDeleteTime = DateTime.Today.AddDays( 1.0 );

                BeginInvoke( ( Action )DeleteRow );
            }
        }

        private void DeleteRow()
        {
            int? LastSelectAlarm = null;

            if ( ( AlarmListView.SelectedItems.Count > 0 ) &&
                ( AlarmListView.SelectedItems[ 0 ].Tag is Data.AlarmDataSet.ALARMRow AlarmRow ) )
            {
                LastSelectAlarm = AlarmRow.alarm_id;
            }

            AlarmListView.ItemSelectionChanged -= AlarmListView_ItemSelectionChanged;
            while ( AlarmListView.SelectedItems.Count > 0 )
            {
                AlarmListView.SelectedItems[ 0 ].Selected = false;
            }
            MonitoringApp.Instance.RemoveRow();

            if ( NarrowDownCheckBox.Checked == true )
            {
                UpdateView();
            }
            else
            {
                ResetListView( MonitoringApp.Instance.AllData() );
            }
            DisplayLastAlarm( _DetectorIds[ 0 ] );
            DisplayLastAlarm( _DetectorIds[ 1 ] );
            DisplayLastAlarm( _DetectorIds[ 2 ] );

            AlarmListView.ItemSelectionChanged += AlarmListView_ItemSelectionChanged;

            if ( LastSelectAlarm.HasValue == true )
            {
                foreach ( ListViewItem Item in AlarmListView.Items )
                {
                    if ( ( Item.Tag is Data.AlarmDataSet.ALARMRow AlarmRow2 ) &&
                        ( AlarmRow2.alarm_id == LastSelectAlarm.Value ) )
                    {
                        Item.Selected = true;
                        break;
                    }
                }
            }

            BeginInvoke( ( Action )DeleteLog );
        }

        private void DeleteLog()
        {
            MonitoringApp.Instance.LogDelete();
            BeginInvoke( ( Action )DeleteAlarm );
        }

        private void DeleteAlarm()
        {
            MonitoringApp.Instance.DeleteOldAlarm();
        }

        private void PictureBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left ) && ( sender is PictureBox PicBox ) )
            {
                FullScreen( _FullScreen == false, PicBox );
            }
        }

        #region Detector PC#1
        private void DetectDisableCheckBox1_CheckedChanged( object sender, EventArgs e )
        {
            if ( _DisableSendDetectSuppress1 == true )
            {
                _DisableSendDetectSuppress1 = false;
                return;
            }

            if ( GatePanel1.Tag is int DetectorId )
            {
                MonitoringApp.Instance.DetectSuppress( DetectorId, DetectDisableCheckBox1.Checked );
            }
        }

        private void MoveHomeButton1_Click( object sender, EventArgs e )
        {
            if ( GatePanel1.Tag is int DetectorId )
            {
                MonitoringApp.Instance.MoveHomePosition( DetectorId );
            }
        }
        private void SetHomeButton1_Click( object sender, EventArgs e )
        {
            if ( GatePanel1.Tag is int DetectorId )
            {
                SetHomeButton1.Enabled = false;
                MonitoringApp.Instance.SetHomePosition( DetectorId );
            }
        }

        private void RestartButton1_Click( object sender, EventArgs e )
        {
            if ( GatePanel1.Tag is int DetectorId )
            {
                RestartButton1.Enabled = false;
                //MonitoringApp.Instance.Restart( DetectorId );
                MonitoringApp.Instance.DetectorStop( DetectorId );
            }
        }
        #endregion

        #region Detector PC#2
        private void DetectDisableCheckBox2_CheckedChanged( object sender, EventArgs e )
        {
            if ( _DisableSendDetectSuppress2 == true )
            {
                _DisableSendDetectSuppress2 = false;
                return;
            }

            if ( GatePanel2.Tag is int DetectorId )
            {
                MonitoringApp.Instance.DetectSuppress( DetectorId, DetectDisableCheckBox2.Checked );
            }
        }
        private void MoveHomeButton2_Click( object sender, EventArgs e )
        {
            if ( GatePanel2.Tag is int DetectorId )
            {
                MonitoringApp.Instance.MoveHomePosition( DetectorId );
            }
        }

        private void SetHomeButton2_Click( object sender, EventArgs e )
        {
            if ( GatePanel2.Tag is int DetectorId )
            {
                SetHomeButton2.Enabled = false;
                MonitoringApp.Instance.SetHomePosition( DetectorId );
            }
        }

        private void RestartButton2_Click( object sender, EventArgs e )
        {
            if ( GatePanel2.Tag is int DetectorId )
            {
                RestartButton2.Enabled = false;
                //MonitoringApp.Instance.Restart( DetectorId );
                MonitoringApp.Instance.DetectorStop( DetectorId );
            }
        }
        #endregion

        #region Detector PC#3
        private void DetectDisableCheckBox3_CheckedChanged( object sender, EventArgs e )
        {
            if ( _DisableSendDetectSuppress3 == true )
            {
                _DisableSendDetectSuppress3 = false;
                return;
            }

            if ( GatePanel3.Tag is int DetectorId )
            {
                MonitoringApp.Instance.DetectSuppress( DetectorId, DetectDisableCheckBox3.Checked );
            }
        }

        private void MoveHomeButton3_Click( object sender, EventArgs e )
        {
            if ( GatePanel3.Tag is int DetectorId )
            {
                MonitoringApp.Instance.MoveHomePosition( DetectorId );
            }
        }

        private void SetHomeButton3_Click( object sender, EventArgs e )
        {
            if ( GatePanel3.Tag is int DetectorId )
            {
                SetHomeButton3.Enabled = false;
                MonitoringApp.Instance.SetHomePosition( DetectorId );
            }
        }

        private void RestartButton3_Click( object sender, EventArgs e )
        {
            if ( GatePanel3.Tag is int DetectorId )
            {
                RestartButton3.Enabled = false;
                //MonitoringApp.Instance.Restart( DetectorId );
                MonitoringApp.Instance.DetectorStop( DetectorId );
            }
        }
        #endregion

        private void BuzzerDisableCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( BuzzerDisableCheckBox.Checked == true )
            {
                BuzzerDisableCheckBox.BackColor = Color.LightSalmon;
                BuzzerDisableCheckBox.ForeColor = Color.WhiteSmoke;
                BuzzerDisableCheckBox.Text = "ブザー OFF";
                MonitoringApp.Instance.BuzzerStop();
            }
            else
            {
                BuzzerDisableCheckBox.BackColor = Color.WhiteSmoke;
                BuzzerDisableCheckBox.ForeColor = Color.Black;
                BuzzerDisableCheckBox.Text = "ブザー ON";
            }

            MonitoringApp.Instance.DsiableBuzzer = BuzzerDisableCheckBox.Checked;
        }

        private void MonitorForm_Shown( object sender, EventArgs e )
        {
        }

        private void AlarmImageClearButton_Click( object sender, EventArgs e )
        {
            ClearAlarmImage();
        }

        private void NarrowDownCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            ClearAlarmImage();

            if ( NarrowDownCheckBox.Checked == true )
            {
                NarrowDownCheckBox.BackColor = Color.LightSalmon;
                NarrowDownCheckBox.ForeColor = Color.WhiteSmoke;
                NarrowDownCheckBox.Text = "全件";
                NarrowDownCheckBox.Height = 40;
                ResearchButton.Visible = true;
                UpdateView();
            }
            else
            {
                NarrowDownCheckBox.BackColor = Color.WhiteSmoke;
                NarrowDownCheckBox.ForeColor = Color.Black;
                NarrowDownCheckBox.Text = "絞込";
                NarrowDownCheckBox.Height = 86;
                ResearchButton.Visible = false;
                ResetListView( MonitoringApp.Instance.AllData() );
            }
            DisplayLastAlarm( _DetectorIds[ 0 ] );
            DisplayLastAlarm( _DetectorIds[ 1 ] );
            DisplayLastAlarm( _DetectorIds[ 2 ] );
        }

        private void ResearchButton_Click( object sender, EventArgs e )
        {
            UpdateView();
            DisplayLastAlarm( _DetectorIds[ 0 ] );
            DisplayLastAlarm( _DetectorIds[ 1 ] );
            DisplayLastAlarm( _DetectorIds[ 2 ] );
        }

        private void AlarmListView_ItemSelectionChanged( object sender, ListViewItemSelectionChangedEventArgs e )
        {
            if ( ( e.IsSelected == true ) && ( e.Item.Tag is Data.AlarmDataSet.ALARMRow AlarmRow ) )
            {
                MonitoringApp.Instance.DisplayAlarmPicture( AlarmRow );
            }
        }

        private void AlarmListView_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( _PlayerFormThread != null )
            {
                if ( _PlayerFormThread.IsAlive == true )
                {
                    return;
                }
                _PlayerFormThread.Wait();
                _PlayerFormThread = null;
            }

            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                if ( ( AlarmListView.GetItemAt( e.X, e.Y ) is ListViewItem Item ) &&
                    ( Item.Tag is Data.AlarmDataSet.ALARMRow AlarmRow ) )
                {
                    _PlayerFormThread = new PlayerFormThread(
                        AlarmRow.alarm_id,
                        AlarmRow.alarm_gate_id,
                        AlarmRow.alarm_gate_name,
                        AlarmRow.alarm_type,
                        AlarmRow.alarm_date );
                    _PlayerFormThread.Start();
                }
            }
        }

        private void AlarmListView_DrawItem( object sender, DrawListViewItemEventArgs e )
        {

        }

        private void AlarmListView_DrawSubItem( object sender, DrawListViewSubItemEventArgs e )
        {
            //if ( e.Item.Tag is Data.AlarmDataSet.ALARMRow AlarmRow )
            //{
            //    if ( e.Item.Selected == true )
            //    {
            //        e.SubItem.BackColor = Color.DodgerBlue;
            //        e.SubItem.ForeColor = Color.White;
            //    }
            //    else
            //    {
            //        e.SubItem.BackColor = MonitoringApp.Instance.DetectorBackColor( AlarmRow.alarm_gate_id );
            //        e.SubItem.ForeColor = MonitoringApp.Instance.DetectorForeColor( AlarmRow.alarm_gate_id );
            //    }

            //    e.DrawText();
            //}
            //else
            //{
            //    e.DrawDefault = true;
            //}
            e.DrawDefault = true;
        }

        private void AlarmListView_DrawColumnHeader( object sender, DrawListViewColumnHeaderEventArgs e )
        {
            e.Graphics.FillRectangle( Brushes.Cornsilk, e.Bounds );
            e.DrawText();
        }

        private void SetupButton_Click( object sender, EventArgs e )
        {
            using ( var SetupForm = new SettingForm() )
            {
                SetupForm.ShowDialog();
            }
        }
    }
}
