﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

using OpenCvSharp.UserInterface;

namespace CES.SOLAS.Monitor
{
    using Lib.UI;
    using Lib.ProcessIF.UDP;

    public partial class MonitorForm
    {
        bool _FullScreen = false;
        PictureBox _FullScreenPictureBox = null;
        Rectangle? _GatePanelLocation = null;
        Rectangle? _PictureBoxPanelLocation = null;
        Rectangle? _PictureBoxLocation = null;

        void InitScreen()
        {
            Screen ThisScreen = Screen.PrimaryScreen;
            MonitoringApp.Instance.Info( $"モニター解像度 Width={ThisScreen.Bounds.Width} Height={ThisScreen.Bounds.Height}" );

            //  フォームのサイズをモニタ解像度に合わせる
            Location = new Point( ThisScreen.Bounds.Left, ThisScreen.Bounds.Top );
            Size = new Size( ThisScreen.Bounds.Width, ThisScreen.Bounds.Height );

            //  画像パネル部分の幅を算出
            int GatePanelWidth = ( ThisScreen.Bounds.Width / 3 * 2 ) + ( GateNameLabel1.Width + 6 );

            //  各画像パネル部分の位置とサイズを決定
            GatePanel1.Location = new Point( 0, 0 );
            GatePanel1.Size = new Size( GatePanelWidth, ThisScreen.Bounds.Height / 3 );

            GatePanel2.Location = new Point( 0, ThisScreen.Bounds.Height / 3 );
            GatePanel2.Size = new Size( GatePanelWidth, ThisScreen.Bounds.Height / 3 );

            GatePanel3.Location = new Point( 0, ThisScreen.Bounds.Height / 3 * 2 );
            GatePanel3.Size = new Size( GatePanelWidth, ThisScreen.Bounds.Height / 3 );

            //  警報リスト部分の位置とサイズを決定
            int AlarmPanelWidth = ( ThisScreen.Bounds.Width - GatePanelWidth );
            AlarmInfoPanel.Location = new Point( GatePanelWidth, 0 );
            AlarmInfoPanel.Size = new Size( AlarmPanelWidth, ThisScreen.Bounds.Height );

            //  画像パネル1内の各Controlの位置とサイズを決定
            GateLivePanel1.Location = new Point( 1, 1 );
            GateLivePanel1.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );
            GateAlarmPanel1.Location = new Point( ThisScreen.Bounds.Width / 3 - 4, 1 );
            GateAlarmPanel1.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );

            GateNameLabel1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, 8 );
            DetectStateLabel1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, GateNameLabel1.Bottom + 8 );
            DetectStateLabel1.BackColor = Color.Black;
            DetectStateLabel1.ForeColor = Color.Black;
            DetectStateLabel1.Text = string.Empty;

            RestartButton1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, GatePanel1.Size.Height - SetHomeButton1.Height - 8 );
            RestartButton1.Enabled = false;
            SetHomeButton1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, RestartButton1.Top - SetHomeButton1.Height - 8 );
            SetHomeButton1.Enabled = false;
            MoveHomeButton1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, SetHomeButton1.Top - MoveHomeButton1.Height - 8 );
            MoveHomeButton1.Enabled = false;
            DetectDisableCheckBox1.Location = new Point( GateLivePanel1.Size.Width + GateAlarmPanel1.Size.Width + 4, MoveHomeButton1.Top - DetectDisableCheckBox1.Height - 8 );
            DetectDisableCheckBox1.Enabled = false;
            GateLivePictureBoxIpl1.Location = new Point( 3, 3 );
            GateLivePictureBoxIpl1.Size = new Size( GateLivePanel1.ClientSize.Width - 6, GateLivePanel1.ClientSize.Height - 6 );

            //  画像パネル2内の各Controlの位置とサイズを決定
            GateLivePanel2.Location = new Point( 1, 1 );
            GateLivePanel2.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );
            GateAlarmPanel2.Location = new Point( ThisScreen.Bounds.Width / 3 - 4, 1 );
            GateAlarmPanel2.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );

            GateNameLabel2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, 8 );
            DetectStateLabel2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, GateNameLabel2.Bottom + 8 );
            DetectStateLabel2.BackColor = Color.Black;
            DetectStateLabel2.ForeColor = Color.Black;
            DetectStateLabel2.Text = string.Empty;

            RestartButton2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, GatePanel2.Size.Height - SetHomeButton2.Height - 8 );
            RestartButton2.Enabled = false;
            SetHomeButton2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, RestartButton2.Top - SetHomeButton2.Height - 8 );
            SetHomeButton2.Enabled = false;
            MoveHomeButton2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, SetHomeButton2.Top - MoveHomeButton2.Height - 8 );
            MoveHomeButton2.Enabled = false;
            DetectDisableCheckBox2.Location = new Point( GateLivePanel2.Size.Width + GateAlarmPanel2.Size.Width + 4, MoveHomeButton2.Top - DetectDisableCheckBox2.Height - 8 );
            DetectDisableCheckBox2.Enabled = false;
            GateLivePictureBoxIpl2.Location = new Point( 3, 3 );
            GateLivePictureBoxIpl2.Size = new Size( GateLivePanel2.ClientSize.Width - 6, GateLivePanel2.ClientSize.Height - 6 );

            //  画像パネル3内の各Controlの位置とサイズを決定
            GateLivePanel3.Location = new Point( 1, 1 );
            GateLivePanel3.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );
            GateAlarmPanel3.Location = new Point( ThisScreen.Bounds.Width / 3 - 4, 1 );
            GateAlarmPanel3.Size = new Size( ThisScreen.Bounds.Width / 3 - 4, ThisScreen.Bounds.Height / 3 - 4 );

            GateNameLabel3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, 8 );
            DetectStateLabel3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, GateNameLabel3.Bottom + 8 );
            DetectStateLabel3.BackColor = Color.Black;
            DetectStateLabel3.ForeColor = Color.Black;
            DetectStateLabel3.Text = string.Empty;

            RestartButton3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, GatePanel3.Size.Height - SetHomeButton3.Height - 8 );
            RestartButton3.Enabled = false;
            SetHomeButton3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, RestartButton3.Top - SetHomeButton3.Height - 8 );
            SetHomeButton3.Enabled = false;
            MoveHomeButton3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, SetHomeButton3.Top - MoveHomeButton3.Height - 8 );
            MoveHomeButton3.Enabled = false;
            DetectDisableCheckBox3.Location = new Point( GateLivePanel3.Size.Width + GateAlarmPanel3.Size.Width + 4, MoveHomeButton3.Top - DetectDisableCheckBox3.Height - 8 );
            DetectDisableCheckBox3.Enabled = false;
            GateLivePictureBoxIpl3.Location = new Point( 3, 3 );
            GateLivePictureBoxIpl3.Size = new Size( GateLivePanel3.ClientSize.Width - 6, GateLivePanel3.ClientSize.Height - 6 );

            //  Monitoringアプリの設置場所（正門/東詰所）を表示
            PlaceNameLabel.Text = MonitoringApp.Instance.DisplayName;
            //  時計部分に現在日時を表示
            ClockLabel.Text = DateTime.Now.ToString( "yyyy/MM/dd (ddd) HH:mm:ss" );

            //  ListView更新
            //ResetListView( MonitoringApp.Instance.AllData() );
        }

        private void FullScreen( bool Full, PictureBox TargetPictureBox )
        {
            if ( _FullScreen == Full )
            {
                return;
            }
            if ( ( Full == false ) && ( _FullScreenPictureBox == null ) )
            {
                return;
            }

            if ( Full == true )
            {
                SetFullScreen( TargetPictureBox );
            }
            else
            {
                ResetFullScreen();
            }
        }

        private void SetFullScreen( PictureBox TargetPictureBox )
        {
            //  PictureBoxの位置を保持
            _PictureBoxLocation = new Rectangle( TargetPictureBox.Location, TargetPictureBox.Size );

            //  PictureBoxの親コンテナ(Panel)を取得
            var PictureBoxPanel = TargetPictureBox.Parent;
            //  PictureBoxの親コンテナ(Panel)の位置を保持
            _PictureBoxPanelLocation = new Rectangle( PictureBoxPanel.Location, PictureBoxPanel.Size );

            //  ゲートのコンテナを取得
            var GatePanel = PictureBoxPanel.Parent;
            int GateId = 0;
            if ( GatePanel1 == GatePanel )
            {
                GateId = _DetectorIds[ 0 ];
            }
            else if ( GatePanel2 == GatePanel )
            {
                GateId = _DetectorIds[ 1 ];
            }
            else if ( GatePanel3 == GatePanel )
            {
                GateId = _DetectorIds[ 2 ];
            }

            //  ゲートのコンテナ(Panel)の位置を保持
            _GatePanelLocation = new Rectangle( GatePanel.Location, GatePanel.Size );

            //  ゲートのコンテナ以外のPanelを非表示にする
            AlarmInfoPanel.Visible = false;
            if ( GatePanel1 != GatePanel )
            {
                GatePanel1.Visible = false;
            }
            if ( GatePanel2 != GatePanel )
            {
                GatePanel2.Visible = false;
            }
            if ( GatePanel3 != GatePanel )
            {
                GatePanel3.Visible = false;
            }

            //  ゲートのコンテナの中で PictureBoxの親コンテナ以外のコントロールを非表示にする
            foreach ( var CntlObject in GatePanel.Controls )
            {
                if ( CntlObject is Control Cntl )
                {
                    if ( Cntl != PictureBoxPanel )
                    {
                        Cntl.Visible = false;
                    }
                }
            }

            //  モードと対象PictureBoxを保持
            _FullScreen = true;
            _FullScreenPictureBox = TargetPictureBox;

            //  ゲートコンテナのスタイルと位置を変更
            if ( GatePanel is Panel Pnl )
            {
                Pnl.BorderStyle = BorderStyle.None;
            }
            GatePanel.Location = new Point( 0, 0 );
            GatePanel.Size = ClientSize;

            //  PictureBoxの親コンテナ(Panel)の位置を変更
            PictureBoxPanel.Location = new Point( 0, 0 );
            PictureBoxPanel.Size = ClientSize;

            TargetPictureBox.Location = new Point( 0, 0 );
            TargetPictureBox.Size = PictureBoxPanel.ClientSize;

            //MonitoringApp.Instance.Redraw( GateId );
        }

        private void ResetFullScreen()
        {
            //  PictureBoxの親コンテナ(Panel)を取得
            var PictureBoxPanel = _FullScreenPictureBox.Parent;
            //  ゲートのコンテナを取得
            var GatePanel = PictureBoxPanel.Parent;
            int GateId = 0;
            if ( GatePanel1 == GatePanel )
            {
                GateId = _DetectorIds[ 0 ];
            }
            else if ( GatePanel2 == GatePanel )
            {
                GateId = _DetectorIds[ 1 ];
            }
            else if ( GatePanel3 == GatePanel )
            {
                GateId = _DetectorIds[ 2 ];
            }

            //  ゲートのコンテナ以外のPanelを表示する
            AlarmInfoPanel.Visible = true;
            GatePanel1.Visible = true;
            GatePanel2.Visible = true;
            GatePanel3.Visible = true;

            //  ゲートのコンテナの中で PictureBoxの親コンテナ以外のコントロールを表示する
            foreach ( var CntlObject in GatePanel.Controls )
            {
                if ( CntlObject is Control Cntl )
                {
                    Cntl.Visible = true;
                }
            }

            //  ゲートコンテナの位置とスタイルを戻す
            System.Diagnostics.Trace.Assert( _GatePanelLocation.HasValue == true, "_GatePanelLocation未設定" );
            GatePanel.Location = _GatePanelLocation.Value.Location;
            GatePanel.Size = _GatePanelLocation.Value.Size;
            _GatePanelLocation = null;
            if ( GatePanel is Panel Pnl )
            {
                Pnl.BorderStyle = BorderStyle.FixedSingle;
            }

            //  PictureBoxの親コンテナの位置を戻す
            System.Diagnostics.Trace.Assert( _PictureBoxPanelLocation.HasValue == true, "_PictureBoxPanelLocation" );
            PictureBoxPanel.Location = _PictureBoxPanelLocation.Value.Location;
            PictureBoxPanel.Size = _PictureBoxPanelLocation.Value.Size;
            _PictureBoxPanelLocation = null;

            //  PictureBoxの位置を戻す
            System.Diagnostics.Trace.Assert( _PictureBoxLocation.HasValue == true, "_PictureBoxLocation" );
            _FullScreenPictureBox.Location = _PictureBoxLocation.Value.Location;
            _FullScreenPictureBox.Size = _PictureBoxLocation.Value.Size;
            _PictureBoxLocation = null;


            //  モードと対象PictureBoxを解放
            _FullScreen = false;
            _FullScreenPictureBox = null;

            //
            Invalidate( true );
            //MonitoringApp.Instance.Redraw( GateId );
        }

        private void SetDetectorStatus( TriggerReceiveEventArgs e )
        {
            if ( e.Packet is AppStatusPacket StsPkt )
            {
                Label TargetLabel = null;
                CheckBox DetectDisableCheckBox = null;
                Panel TargetPanel = null;
                int Detect = -1;

                if ( ( GatePanel1.Tag is int Detector1 ) && ( Detector1 == e.ProcessId ) )
                {
                    TargetLabel = DetectStateLabel1;
                    DetectDisableCheckBox = DetectDisableCheckBox1;
                    TargetPanel = GateLivePanel1;
                    Detect = 1;
                }
                if ( ( GatePanel2.Tag is int Detector2 ) && ( Detector2 == e.ProcessId ) )
                {
                    TargetLabel = DetectStateLabel2;
                    DetectDisableCheckBox = DetectDisableCheckBox2;
                    TargetPanel = GateLivePanel2;
                    Detect = 2;
                }
                if ( ( GatePanel3.Tag is int Detector3 ) && ( Detector3 == e.ProcessId ) )
                {
                    TargetLabel = DetectStateLabel3;
                    DetectDisableCheckBox = DetectDisableCheckBox3;
                    TargetPanel = GateLivePanel3;
                    Detect = 3;
                }
                if ( ( TargetLabel == null ) || ( DetectDisableCheckBox == null ) || ( TargetPanel == null ) || ( Detect < 1 ) || ( Detect > 3 ) )
                {
                    return;
                }

                MonitoringApp.Instance.UpdateDetectorStatus( StsPkt.ProcessId, StsPkt.Status );

                switch ( StsPkt.Status )
                {
                    case Lib.APP_STATUS.APP_STS_INIT:
                    case Lib.APP_STATUS.APP_STS_STARTUP:
                        TargetLabel.BackColor = Color.WhiteSmoke;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = "起 動 中";
                        TargetPanel.BackColor = Color.Black;
                        MonitoringApp.Instance.StillImageClear( e.ProcessId );
                        MonitoringApp.Instance.StillAlarmImageClear( e.ProcessId );
                        DisplayLastAlarm( e.ProcessId );
                        switch ( Detect )
                        {
                            case 1:
                                RestartButton1.Enabled = false;
                                SetHomeButton1.Enabled = false;
                                MoveHomeButton1.Enabled = false;
                                break;
                            case 2:
                                RestartButton2.Enabled = false;
                                SetHomeButton2.Enabled = false;
                                MoveHomeButton2.Enabled = false;
                                break;
                            case 3:
                                RestartButton3.Enabled = false;
                                SetHomeButton3.Enabled = false;
                                MoveHomeButton3.Enabled = false;
                                break;
                        }
                        break;
                    case Lib.APP_STATUS.APP_STS_STOPED:
                        TargetLabel.BackColor = Color.WhiteSmoke;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = "停 止 中";
                        TargetPanel.BackColor = Color.Black;
                        MonitoringApp.Instance.StillImageClear( e.ProcessId );
                        //MonitoringApp.Instance.StillAlarmImageClear( e.ProcessId );
                        switch ( Detect )
                        {
                            case 1:
                                RestartButton1.Enabled = false;
                                SetHomeButton1.Enabled = false;
                                MoveHomeButton1.Enabled = false;
                                break;
                            case 2:
                                RestartButton2.Enabled = false;
                                SetHomeButton2.Enabled = false;
                                MoveHomeButton2.Enabled = false;
                                break;
                            case 3:
                                RestartButton3.Enabled = false;
                                SetHomeButton3.Enabled = false;
                                MoveHomeButton3.Enabled = false;
                                break;
                        }
                        break;
                    case Lib.APP_STATUS.APP_STS_RUNNING:
                        TargetLabel.BackColor = Color.DodgerBlue;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = "検 知 中";
                        TargetPanel.BackColor = Color.DodgerBlue;

                        DetectDisableCheckBox.Enabled = true;
                        DetectDisableCheckBox.BackColor = Color.WhiteSmoke;
                        DetectDisableCheckBox.ForeColor = Color.Black;
                        DetectDisableCheckBox.Text = "検 知 抑 制";
                        if ( DetectDisableCheckBox.Checked == true )
                        {
                            switch ( Detect )
                            {
                                case 1:
                                    _DisableSendDetectSuppress1 = true;
                                    break;
                                case 2:
                                    _DisableSendDetectSuppress2 = true;
                                    break;
                                case 3:
                                    _DisableSendDetectSuppress3 = true;
                                    break;
                            }
                            DetectDisableCheckBox.Checked = false;
                        }

                        switch ( Detect )
                        {
                            case 1:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton1.Enabled = true;
                                    MoveHomeButton1.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton1.Enabled = false;
                                    MoveHomeButton1.Enabled = false;
                                }
                                RestartButton1.Enabled = true;
                                break;
                            case 2:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton2.Enabled = true;
                                    MoveHomeButton2.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton2.Enabled = false;
                                    MoveHomeButton2.Enabled = false;
                                }
                                RestartButton2.Enabled = true;
                                break;
                            case 3:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton3.Enabled = true;
                                    MoveHomeButton3.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton3.Enabled = false;
                                    MoveHomeButton3.Enabled = false;
                                }
                                RestartButton3.Enabled = true;
                                break;
                        }
                        break;
                    case Lib.APP_STATUS.APP_STS_RUNNING_NO_DETECT:
                        TargetLabel.BackColor = Color.OrangeRed;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = "検知抑制中";
                        TargetPanel.BackColor = Color.OrangeRed;
                        MonitoringApp.Instance.StillImageClear( e.ProcessId, true );

                        DetectDisableCheckBox.Enabled = true;
                        DetectDisableCheckBox.BackColor = Color.Orange;
                        DetectDisableCheckBox.ForeColor = Color.WhiteSmoke;
                        DetectDisableCheckBox.Text = "抑 制 解 除";
                        if ( DetectDisableCheckBox.Checked == false )
                        {
                            switch ( Detect )
                            {
                                case 1:
                                    _DisableSendDetectSuppress1 = true;
                                    break;
                                case 2:
                                    _DisableSendDetectSuppress2 = true;
                                    break;
                                case 3:
                                    _DisableSendDetectSuppress3 = true;
                                    break;
                            }
                            DetectDisableCheckBox.Checked = true;
                        }
                        switch ( Detect )
                        {
                            case 1:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton1.Enabled = true;
                                    MoveHomeButton1.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton1.Enabled = false;
                                    MoveHomeButton1.Enabled = false;
                                }
                                RestartButton1.Enabled = true;
                                break;
                            case 2:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton2.Enabled = true;
                                    MoveHomeButton2.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton2.Enabled = false;
                                    MoveHomeButton2.Enabled = false;
                                }
                                RestartButton2.Enabled = true;
                                break;
                            case 3:
                                if ( MonitoringApp.Instance.DetectorEnableCameraControl( e.ProcessId ) == true )
                                {
                                    SetHomeButton3.Enabled = true;
                                    MoveHomeButton3.Enabled = true;
                                }
                                else
                                {
                                    SetHomeButton3.Enabled = false;
                                    MoveHomeButton3.Enabled = false;
                                }
                                RestartButton3.Enabled = true;
                                break;
                        }
                        break;
                    case Lib.APP_STATUS.APP_STS_FAIL:
                        TargetLabel.BackColor = Color.LightPink;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = "エ ラ ー";
                        TargetPanel.BackColor = Color.Black;
                        MonitoringApp.Instance.StillImageClear( e.ProcessId );
                        MonitoringApp.Instance.ApplicationError();
                        //MonitoringApp.Instance.StillAlarmImageClear( e.ProcessId );
                        switch ( Detect )
                        {
                            case 1:
                                SetHomeButton1.Enabled = false;
                                MoveHomeButton1.Enabled = false;
                                RestartButton1.Enabled = true;
                                break;
                            case 2:
                                SetHomeButton2.Enabled = false;
                                MoveHomeButton2.Enabled = false;
                                RestartButton2.Enabled = true;
                                break;
                            case 3:
                                SetHomeButton3.Enabled = false;
                                MoveHomeButton3.Enabled = false;
                                RestartButton3.Enabled = true;
                                break;
                        }
                        break;
                    case Lib.APP_STATUS.APP_STS_STOPING:
                    case Lib.APP_STATUS.APP_STS_UNKNOWN:
                        TargetLabel.BackColor = Color.Black;
                        TargetLabel.ForeColor = Color.Black;
                        TargetLabel.Text = string.Empty;
                        TargetPanel.BackColor = Color.Black;
                        MonitoringApp.Instance.StillImageClear( e.ProcessId );
                        //MonitoringApp.Instance.StillAlarmImageClear( e.ProcessId );
                        switch ( Detect )
                        {
                            case 1:
                                RestartButton1.Enabled = false;
                                SetHomeButton1.Enabled = false;
                                MoveHomeButton1.Enabled = false;
                                break;
                            case 2:
                                RestartButton2.Enabled = false;
                                SetHomeButton2.Enabled = false;
                                MoveHomeButton2.Enabled = false;
                                break;
                            case 3:
                                RestartButton3.Enabled = false;
                                SetHomeButton3.Enabled = false;
                                MoveHomeButton3.Enabled = false;
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void ClearAlarmImage()
        {
            GateAlarmPanel1.BackColor = Color.Black;
            GateAlarmPanel2.BackColor = Color.Black;
            GateAlarmPanel3.BackColor = Color.Black;

            GateAlarmPictureBox1.BackColor = Color.Black;
            GateAlarmPictureBox2.BackColor = Color.Black;
            GateAlarmPictureBox3.BackColor = Color.Black;

            MonitoringApp.Instance.StillAlarmImageClear( -1 );
        }

        private void UpdateView()
        {
            ResetListView( MonitoringApp.Instance.SearchData(
                ( GateCheckBox1.Checked == true ) ? _DetectorIds[ 0 ] : -1,
                ( GateCheckBox2.Checked == true ) ? _DetectorIds[ 1 ] : -1,
                ( GateCheckBox3.Checked == true ) ? _DetectorIds[ 2 ] : -1,
                ( AlarmTypeCheckBoxA.Checked == true ) ? Lib.ALARM_TYPE.ALARM_TYPE_A : Lib.ALARM_TYPE.ALARM_TYPE_UNKNOWN,
                ( AlarmTypeCheckBoxB.Checked == true ) ? Lib.ALARM_TYPE.ALARM_TYPE_B : Lib.ALARM_TYPE.ALARM_TYPE_UNKNOWN,
                ( AlarmTypeCheckBoxC.Checked == true ) ? Lib.ALARM_TYPE.ALARM_TYPE_C : Lib.ALARM_TYPE.ALARM_TYPE_UNKNOWN,
                FromDateTimePicker.Value,
                ToDateTimePicker.Value ) );

        }

        private void ResetListView( DataView View )
        {
            Dictionary<int, Data.AlarmDataSet.ALARMRow> LastAlarms = new Dictionary<int, Data.AlarmDataSet.ALARMRow>();

            AlarmListView.Items.Clear();
            AlarmListView.SuspendLayout();

            foreach ( DataRowView RowView in View )
            {
                if ( RowView.Row is Data.AlarmDataSet.ALARMRow AlarmRow )
                {
                    AlarmListView.Items.Add( MonitoringApp.Instance.CreateItem( AlarmRow ) );

                    if ( LastAlarms.ContainsKey( AlarmRow.alarm_gate_id ) == false )
                    {
                        LastAlarms[ AlarmRow.alarm_gate_id ] = AlarmRow;
                    }
                    else if ( LastAlarms[ AlarmRow.alarm_gate_id ] == null )
                    {
                        LastAlarms[ AlarmRow.alarm_gate_id ] = AlarmRow;
                    }
                    else if ( ( LastAlarms[ AlarmRow.alarm_gate_id ] is Data.AlarmDataSet.ALARMRow ) == false )
                    {
                        LastAlarms[ AlarmRow.alarm_gate_id ] = AlarmRow;
                    }
                    else
                    {
                        if ( AlarmRow.alarm_date > LastAlarms[ AlarmRow.alarm_gate_id ].alarm_date )
                        {
                            LastAlarms[ AlarmRow.alarm_gate_id ] = AlarmRow;
                        }
                    }
                }
            }

            AlarmListView.ResumeLayout();
        }

        private void DisplayLastAlarm( int DetectorId )
        {
            var ImagePath = MonitoringApp.Instance.LastImagePath( DetectorId );

            if ( string.IsNullOrWhiteSpace( ImagePath ) == false )
            {
                MonitoringApp.Instance.DisplayAlarmPicture( DetectorId, ImagePath );
            }
            else
            {
                MonitoringApp.Instance.StillAlarmImageClear( DetectorId );
            }
        }
    }
}
