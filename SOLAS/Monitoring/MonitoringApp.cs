﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

using OpenCvSharp;
using OpenCvSharp.Extensions;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Monitor
{
    using Lib;
    using Lib.UI;
    using Lib.AlarmDevice;
    using Lib.ProcessIF.TCP;
    using Lib.ProcessIF.UDP;
    using CES.SOLAS.Lib.Logging;

    public class MonitoringApp : SOLASApp<MonitoringApp>, ISOLASApp
    {
        public event EventHandler<OpenCVImageDrawEventArgs> NewFrameImage;
        public event EventHandler<OpenCVImageDrawEventArgs> NewAlarmImage;
        public event EventHandler<NewAlarmEventArgs> NewAlarmReceive;
        public event EventHandler DataViewListChange;


        protected readonly Dictionary<int, DirectXStreamDrawer> _DirectXStreamDrawers = new Dictionary<int, DirectXStreamDrawer>();
        //protected readonly Dictionary<int, DirectXIntervalDrawer> _DirectXImageDrawers = new Dictionary<int, DirectXIntervalDrawer>();
        protected readonly Dictionary<int, OpenCVImageDrawer> _OpenCVImageDrawers = new Dictionary<int, OpenCVImageDrawer>();
        //protected readonly Dictionary<int, DirectXIntervalDrawer> _DirectXAlarmDrawers = new Dictionary<int, DirectXIntervalDrawer>();
        protected readonly Dictionary<int, OpenCVImageDrawer> _AlarmDrawers = new Dictionary<int, OpenCVImageDrawer>();
        protected readonly Dictionary<int, OpenCvStreamReceiver> _OpenCvStreamReceiver = new Dictionary<int, OpenCvStreamReceiver>();

        protected Data.AlarmData _AlarmData;
        protected DN1500GX _Keiko;

        public string DisplayName => _DBAccess.DisplayName;

        public APP_STATUS DetectorStatus
        {
            get => throw new NotSupportedException( "アプリケーション区分エラー　DetectorStatus.get は Monitoring で使用しない" );
            set => throw new NotSupportedException( "アプリケーション区分エラー　DetectorStatus.set は Monitoring で使用しない" );
        }
        public bool EnableDetect
        {
            get => throw new NotSupportedException( "アプリケーション区分エラー　EnableDetect.get は Monitoring で使用しない" );
            set => throw new NotSupportedException( "アプリケーション区分エラー　EnableDetect.set は Monitoring で使用しない" );
        }
        public bool ManualDetectDisable
        {
            get => throw new NotSupportedException( "アプリケーション区分エラー　ManualDetectDisable.get は Monitoring で使用しない" );
            set => throw new NotSupportedException( "アプリケーション区分エラー　ManualDetectDisable.set は Monitoring で使用しない" );
        }

        public uint PersonId => _DBAccess.PersonId;

        public uint MarkerId => _DBAccess.MarkerId;

        public uint CarId => _DBAccess.CarId;

        public uint CareerPaletteId => _DBAccess.CareerPaletteId;

        public Scalar PersonColor => _DBAccess.PersonColor;

        public Scalar MarkerColor => _DBAccess.MarkerColor;

        public Scalar CarColor => _DBAccess.CarColor;

        public Scalar CareerPaletteColor => _DBAccess.CareerPaletteColor;

        public Scalar OKColor => _DBAccess.OKColor;

        public Scalar AlarmColor => _DBAccess.AlarmColor;

        public Scalar StagnantColor => _DBAccess.StagnantColor;

        public string PersonName => _DBAccess.PersonName;

        public string MarkerName => _DBAccess.MarkerName;

        public string CarName => _DBAccess.CarName;

        public string CareerPaletteName => _DBAccess.CareerPaletteName;

        public uint MaxPredictFrame => _DBAccess.MaxPredictFrame;

        public float IOUThreshold => _DBAccess.IOUThreshold;

        public int MessageBoxTimeout
        {
            get
            {
                try
                {
                    return ( _DBAccess.MessageBoxTimeout * 1000 );
                }
                catch
                {
                    return 5000;
                }
            }
        }

        public bool EnableCameraControl => _DBAccess.EnableCameraControl;

        public int StillImageShortInterval
        {
            get => _DBAccess.StillImageShortInterval;
            set => _DBAccess.StillImageShortInterval = value;
        }

        public int StillImageLongInterval
        {
            get => _DBAccess.StillImageLongInterval;
            set => _DBAccess.StillImageLongInterval = value;
        }

        public int TcpKeepAliveWait => _DBAccess.TcpKeepAliveWait;

        public int TcpKeepAliveInterval => _DBAccess.TcpKeepAliveInterval;

        public float StillImageShrinkRatio => _DBAccess.StillImageShrinkRatio;

        public int HeartBeatCycle => _DBAccess.HeartBeatCycle;

        public System.Drawing.Point[] DetectAreaA => throw new NotSupportedException( "アプリケーション区分エラー　DetectAreaA は Monitoring で使用しない" );

        public System.Drawing.Point[] DetectAreaB => throw new NotSupportedException( "アプリケーション区分エラー　DetectAreaB は Monitoring で使用しない" );

        public Scalar AreaColorA => throw new NotSupportedException( "アプリケーション区分エラー　AreaColorA は Monitoring で使用しない" );

        public Scalar AreaColorB => throw new NotSupportedException( "アプリケーション区分エラー　AreaColorB は Monitoring で使用しない" );

        public Scalar NeutralAreaColor => _DBAccess.NeutralAreaColor;

        public System.Drawing.Point[] DetectArea => throw new NotSupportedException( "アプリケーション区分エラー　DetectArea は Monitoring で使用しない" );

        public void ApplicationStatus( int MonitoringId, APP_STATUS Status )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　ApplicationStatus は Monitoring で使用しない" );
        }

        public void DetectSuppress( int DetectorId, bool Suppress )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( DetectorId < 0 ) || ( DetectorId == Key ) )
                    {
                        _PacketSenders[ Key ].DetectSuppress( Suppress );
                    }
                }
                catch
                {

                }
            }
        }

        public void ErrorMessage( string Message )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　ErrorMessage は Monitoring で使用しない" );
        }

        public void HeartBeat()
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    _PacketSenders[ Key ].HeartBeat();
                }
                catch
                {

                }
            }
        }

        public void UpdateDetectorStatus( int DetectorId, APP_STATUS Status )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( DetectorId == Key )
                    {
                        _PacketSenders[ Key ].RemoteStatus = Status;
                        break;
                    }
                }
                catch
                {

                }
            }
        }

        public void HomePosition( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　HomePosition は Monitoring で使用しない" );
        }

        public void InitMonitoring(
            Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3,
            out int DetectId1, out int DetectId2, out int DetectId3 )
        {
            _DBAccess.GetMonitorTarget( out int[] RecNo );
            Assert( RecNo != null && RecNo.Length == 3, "_DBAccess.GetMonitorTarget RecNo Get Error." );

            DetectId1 = DetectId_1 = RecNo[ 0 ];
            DetectId2 = DetectId_2 = RecNo[ 1 ];
            DetectId3 = DetectId_3 = RecNo[ 2 ];

            Control[] Ctrls = new Control[] { MonitorCntl1, MonitorCntl2, MonitorCntl3 };

            for ( int i = 0 ; i < Ctrls.Length ; i++ )
            {
                var DrawerName = $"CvImg-{RecNo[ i ]}";

                if ( MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                {
                    //_DirectXImageDrawers[ RecNo[ i ] ] = new DirectXIntervalDrawer( this, DrawerName, Ctrls[ i ].Handle, new ControlSizeChangeCallback( Ctrls[ i ] ) );
                    //_DirectXImageDrawers[ RecNo[ i ] ].Start();
                    //this.Debug( $"Create DirectXIntervalDrawer Name={DrawerName}" );
                    _OpenCVImageDrawers[ RecNo[ i ] ] = new OpenCVImageDrawer( this, RecNo[ i ], DrawerName );
                    _OpenCVImageDrawers[ RecNo[ i ] ].NewImage += this.MonitoringApp_NewImage;
                    _OpenCVImageDrawers[ RecNo[ i ] ].Start();
                    this.Debug( $"Create DirectXIntervalDrawer Name={DrawerName}" );
                }
                else if ( MonitoringType == MONITORING_TYPE.MON_STREAMING )
                {
                    //_DirectXStreamDrawers[ RecNo[ i ] ] = new DirectXStreamDrawer( this, DrawerName, Ctrls[ i ].Handle, new ControlSizeChangeCallback( Ctrls[ i ] ) );
                    //_DirectXStreamDrawers[ RecNo[ i ] ].Start();
                    _OpenCVImageDrawers[ RecNo[ i ] ] = new OpenCVImageDrawer( this, RecNo[ i ], DrawerName );
                    _OpenCVImageDrawers[ RecNo[ i ] ].NewImage += this.MonitoringApp_NewImage;
                    this.Debug( $"Create OpenCVStreamDrawer Name={DrawerName}" );

                    DrawerName = $"CvStream-{RecNo[ i ]}";

                    _OpenCvStreamReceiver[ RecNo[ i ] ] =
                        new OpenCvStreamReceiver(
                            this,
                            DrawerName,
                            MonitorStreamURL( RecNo[ i ] ),
                            _OpenCVImageDrawers[ RecNo[ i ] ] );

                    this.Debug( $"Create OpenCvStreamReceiver Name={DrawerName}" );
                    _OpenCVImageDrawers[ RecNo[ i ] ].Start();
                    _OpenCvStreamReceiver[ RecNo[ i ] ].Start();
                }
            }
        }

        private int DetectId_1 = 0;
        private int DetectId_2 = 0;
        private int DetectId_3 = 0;

        void D1AverageCallback()
        {
            //_StreamReceivers[ DetectId_1 ].RestartStreaming();
        }

        void D2AverageCallback()
        {
            //_StreamReceivers[ DetectId_2 ].RestartStreaming();
        }

        void D3AverageCallback()
        {
            //_StreamReceivers[ DetectId_3 ].RestartStreaming();
        }

        private void MonitoringApp_NewImage( object sender, OpenCVImageDrawEventArgs e )
        {
            if ( NewFrameImage != null )
            {
                //this.Debug( $"NewFrameImage Event Handler Calle.DetectorId = {e.DetectorId}" );
                if ( ( NewFrameImage.Target != null ) && ( NewFrameImage.Target is Control Ctrl ) )
                {
                    Ctrl.Invoke( NewFrameImage, this, e );
                }
                else
                {
                    NewFrameImage( this, e );
                }
            }
        }

        public void MoveHomePosition( int DetectorId )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( DetectorId < 0 ) || ( DetectorId == Key ) )
                    {
                        _PacketSenders[ Key ].MoveHomePosition();
                    }
                }
                catch
                {

                }
            }
        }

        public void NewAlarm( int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　NewAlarm は Monitoring で使用しない" );
        }

        public void NewImage( int ImageId, DateTime Timestamp )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　NewImage は Monitoring で使用しない" );
        }

        public void Restart( int DetectorId )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( DetectorId < 0 ) || ( DetectorId == Key ) )
                    {
                        _PacketSenders[ Key ].Restart();
                    }
                }
                catch
                {

                }
            }
        }

        public void DetectorStop( int DetectorId )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( DetectorId < 0 ) || ( DetectorId == Key ) )
                    {
                        _PacketSenders[ Key ].DetectorStop();
                    }
                }
                catch
                {

                }
            }
        }

        public void SetHomePosition( int DetectorId )
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                try
                {
                    if ( ( DetectorId < 0 ) || ( DetectorId == Key ) )
                    {
                        _PacketSenders[ Key ].SetHomePosition();
                    }
                }
                catch
                {

                }
            }
        }

        public void StopMonitoring()
        {
            foreach ( var Key in _OpenCVImageDrawers.Keys )
            {
                if ( _OpenCVImageDrawers[ Key ] != null )
                {
                    if ( _OpenCVImageDrawers[ Key ].IsAlive == true )
                    {
                        _OpenCVImageDrawers[ Key ].Stop();
                    }
                    else
                    {
                        _OpenCVImageDrawers[ Key ].Wait();
                    }
                }
            }
            foreach ( var Key in _DirectXStreamDrawers.Keys )
            {
                if ( _DirectXStreamDrawers[ Key ] != null )
                {
                    if ( _DirectXStreamDrawers[ Key ].IsAlive == true )
                    {
                        _DirectXStreamDrawers[ Key ].Stop();
                    }
                    else
                    {
                        _DirectXStreamDrawers[ Key ].Wait();
                    }
                }
            }
        }

        protected override void InitDBAccess( string DBServer, string SAPassword )
        {
            MonitoringDBAccess.Instance.InitDBAccess( DBServer, SAPassword );

            _ProcessType = MonitoringDBAccess.Instance.ProcessType;
            _ApplicationId = MonitoringDBAccess.Instance.ApplicationId;
            _DBAccess = MonitoringDBAccess.Instance;
        }

        //protected override void InitMovieReqListener( int Port )
        //{
        //    return;
        //}

        protected override void InitUdpSender()
        {
            _DBAccess.GetMonitorTarget( out int[] RecNo );
            int DetectorCount = _DBAccess.DetectorCount;
            for ( int i = 0 ; i < DetectorCount ; i++ )
            {
                _DBAccess.GetDetectorPCInfo( i, out int Id, out string Addr, out int Port );

                bool IdMatch = false;
                foreach ( var DetectorId in RecNo )
                {
                    if ( DetectorId == Id )
                    {
                        IdMatch = true;
                        break;
                    }
                }
                if ( IdMatch == true )
                {
                    _PacketSenders[ Id ] = new PacketSender( this, TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR, Addr, Port );
                    _PacketSenders[ Id ].Start();
                }
            }
        }

        public bool GetDetectorMovieRequestInfo( int DetectorId, out string Addr, out int Port )
        {
            _DBAccess.GetMonitorTarget( out int[] RecNo );
            int DetectorCount = _DBAccess.DetectorCount;
            for ( int i = 0 ; i < DetectorCount ; i++ )
            {
                _DBAccess.GetDetectorPCInfo( i, out int Id, out string DetectorAddr, out int DetectorPort );

                foreach ( var RecId in RecNo )
                {
                    if ( DetectorId == Id )
                    {
                        Addr = DetectorAddr;
                        Port = DetectorPort;
                        return true;
                    }
                }
            }

            Addr = string.Empty;
            Port = 0;
            return false;
        }

        protected override void TerminateUdpSender()
        {
            foreach ( var Key in _PacketSenders.Keys )
            {
                if ( _PacketSenders[ Key ] == null )
                {
                    continue;
                }

                if ( _PacketSenders[ Key ].IsAlive == true )
                {
                    _PacketSenders[ Key ].Stop();
                }
                else
                {
                    _PacketSenders[ Key ].Wait();
                }
            }
        }

        public override void AppInitialyze( string DBServer, string SAPassword, TriggerCallback TrgCallback, string LogFolder, string LogName, string LogExt = "log", long LogSize = 5242880, int SaveDays = 31, bool DebugLog = false )
        {
            base.AppInitialyze( DBServer, SAPassword, TrgCallback, LogFolder, LogName, LogExt, LogSize, SaveDays, DebugLog );

            _AlarmData = new Data.AlarmData( Properties.Settings.Default.ALARM_Image_Folder, ViewListChange );

            string AppErrorPattern = null;
            try
            {
                if ( Properties.Settings.Default[ "AppliccationErrorPattern" ] != null )
                {
                    AppErrorPattern = $"{Properties.Settings.Default[ "AppliccationErrorPattern" ]}";
                }
            }
            catch
            {
                AppErrorPattern = null;
            }

            _Keiko = new DN1500GX( _DBAccess.AlarmDeviceAddress, _DBAccess.AlarmDevicePort, AppErrorPattern )
            {
                DisableBuzzer = _DBAccess.DisableBuzzer
            };
        }

        public void ViewListChange()
        {
            if ( DataViewListChange != null )
            {
                if ( ( DataViewListChange.Target != null ) && ( DataViewListChange.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke( DataViewListChange, this, EventArgs.Empty );
                }
                else
                {
                    DataViewListChange( this, EventArgs.Empty );
                }
            }
        }

        //public DataView ALARM => _AlarmData.AlarmTable;

        public DataView AllData()
        {
            return _AlarmData.AllData();
        }

        public DataView SearchData(
            int Gate1,
            int Gate2,
            int Gate3,
            ALARM_TYPE AlarmA,
            ALARM_TYPE AlarmB,
            ALARM_TYPE AlarmC,
            DateTime FromDate,
            DateTime ToDate )
        {
            return _AlarmData.SearchData( _DBAccess.MaxDisplayDays, Gate1, Gate2, Gate3, AlarmA, AlarmB, AlarmC, FromDate, ToDate );
        }

        public DateTime MinimumDate => _AlarmData.MinimumDate( _DBAccess.MaxDisplayDays );

        public float StreamingImageShrinkRatio => _DBAccess.StreamingImageShrinkRatio;

        public int StreamingSkipFrame => _DBAccess.StreamingSkipFrame;

        public void RemoveRow()
        {
            _AlarmData.RemoveRow( _DBAccess.MaxDisplayDays );
        }

        public ListViewItem CreateItem(int GateId, int AlarmId )
        {
            return _AlarmData.CreateListViewItem( GateId, AlarmId );
        }

        public ListViewItem CreateItem( Data.AlarmDataSet.ALARMRow AlarmRow )
        {
            return new ListViewItem( new string[] { AlarmRow.alarm_gate_name, AlarmRow.alarm_type_name, AlarmRow.alarm_date.ToString( "yyyy/MM/dd HH:mm:ss" ) } )
            {
                Tag = AlarmRow,
                BackColor = MonitoringApp.Instance.DetectorBackColor( AlarmRow.alarm_gate_id ),
                ForeColor = MonitoringApp.Instance.DetectorForeColor( AlarmRow.alarm_gate_id )
            };
        }


        public string GateName( int DetectorId )
        {
            return _DBAccess.GateName( DetectorId );
        }

        public void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　GetCamera は Monitoring で使用しない" );
        }

        protected override void InitCameraAccess()
        {
        }

        protected override void TerminateCameraAccess()
        {
        }

        public void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　GetCamera は Monitoring で使用しない" );
        }

        public string AlarmName( ALARM_TYPE AlarmType )
        {
            return _DBAccess.AlarmName( AlarmType );
        }

        public bool DetectorEnableCameraControl( int DetectorId )
        {
            return _DBAccess.DetectorEnableCameraControl( DetectorId );
        }

        protected override void InitStillImageListener()
        {
            _ImageListener = new ImageListener( this, _DBAccess.TCPImageListenerPort, false, null );
            _ImageListener.Start();
        }

        protected override void InitImageSender()
        {
        }

        public void StillImageReceive( Lib.ProcessIF.TCP.ImagePacket Packet )
        {
            if ( MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
            {
                if ( _OpenCVImageDrawers.ContainsKey( Packet.DetectorId ) == false )
                {
                    return;
                }
            }

            if ( Packet.ImageLength > 0 )
            {
                if ( MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                {
                    //_DirectXImageDrawers[ Packet.DetectorId ].NewImage( Packet.ImageData );
                    _OpenCVImageDrawers[ Packet.DetectorId ].SetImage( Packet.ImageData );
                }

                if ( Packet.AlarmId > 0 )
                {
                    //_DirectXAlarmDrawers[ Packet.DetectorId ].NewImage( Packet.ImageData );
                    _AlarmDrawers[ Packet.DetectorId ].SetImage( Packet.ImageData );
                    int RecNo = _AlarmData.AddAlarm( Packet );
                    if ( ( NewAlarmReceive != null ) && ( RecNo == Packet.AlarmId ) )
                    {
                        if ( ( NewAlarmReceive.Target != null ) && ( NewAlarmReceive.Target is Control Cntl ) )
                        {
                            Cntl.BeginInvoke( NewAlarmReceive, this, new NewAlarmEventArgs( Packet.DetectorId, Packet.AlarmId ) );
                        }
                        else
                        {
                            NewAlarmReceive( this, new NewAlarmEventArgs( Packet.DetectorId, Packet.AlarmId ) );
                        }
                    }

                    if ( Enum.IsDefined( typeof( ALARM_TYPE ), Packet.AlarmType ) == true )
                    {
                        Task.Factory.StartNew( () =>
                        {
                            Alarm( Packet.DetectorId, ( ALARM_TYPE )Enum.ToObject( typeof( ALARM_TYPE ), Packet.AlarmType ) );
                        } );
                    }
                }
            }
            else
            {
                if ( MonitoringType == MONITORING_TYPE.MON_STILL_IMAGE )
                {
                    //_DirectXImageDrawers[ Packet.DetectorId ].NewImage( ( byte[] )null );
                    _OpenCVImageDrawers[ Packet.DetectorId ].Enqueue( null );
                }
            }
        }

        public void LiveImageReceive( int DetectorId, Bitmap Image )
        {
            if ( _DirectXStreamDrawers.ContainsKey( DetectorId ) == true )
            {
                _DirectXStreamDrawers[ DetectorId ].Enqueue( Image );
            }

            Image?.Dispose();
        }

        public void StillImageClear( int GateId , bool DetectDisable = false)
        {
            foreach ( var key in _OpenCVImageDrawers.Keys )
            {
                if ( ( GateId == -1 ) || ( GateId == key ) )
                {
                    if ( DetectDisable == false )
                    {
                        _OpenCVImageDrawers[ key ].Enqueue( null );
                    }
                    else
                    {
                        _OpenCVImageDrawers[ key ].Enqueue( BitmapConverter.ToMat( Properties.Resources.disable ) );
                    }
                }
            }
            foreach ( var key in _DirectXStreamDrawers.Keys )
            {
                if ( ( GateId == -1 ) || ( GateId == key ) )
                {
                    _DirectXStreamDrawers[ key ].Enqueue( ( Mat )null );
                }
            }
        }

        public void StillAlarmImageClear( int GateId )
        {
            foreach ( var key in _AlarmDrawers.Keys )
            {
                if ( ( GateId == -1 ) || ( GateId == key ) )
                {
                    //_DirectXAlarmDrawers[ key ].NewImage( ( byte[] )null );
                    _AlarmDrawers[ key ].Enqueue( null );
                }
            }
        }

        public string ImagePath( int GateId, int AlarmId )
        {
            return _AlarmData.ImagePath( GateId, AlarmId );
        }

        public string LastImagePath( int GateId )
        {
            return _AlarmData.LastImagePath( GateId );
        }

        public void StillImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat StillImage )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　StillImageSend は Monitoring で使用しない" );
        }

        public void DisplayAlarmPicture( int GateId, string ImagePath )
        {
            if ( ( _AlarmDrawers != null ) && ( _AlarmDrawers.ContainsKey( GateId ) == true ) )
            {
                //_DirectXAlarmDrawers[ GateId ].NewImage( _AlarmData.ReadImage( ImagePath ) );
                _AlarmDrawers[ GateId ].Enqueue( _AlarmData.ReadImage( ImagePath ) );
            }
        }

        public void DisplayAlarmPicture( Data.AlarmDataSet.ALARMRow AlarmRow )
        {
            if ( ( _AlarmDrawers != null ) && ( _AlarmDrawers.ContainsKey( AlarmRow.alarm_gate_id ) == true ) )
            {
                //_DirectXAlarmDrawers[ AlarmRow.alarm_gate_id ].NewImage( _AlarmData.ReadImage( AlarmRow.alarm_image_path ) );
                _AlarmDrawers[ AlarmRow.alarm_gate_id ].Enqueue( _AlarmData.ReadImage( AlarmRow.alarm_image_path ) );
            }
        }

        protected override void TerminateStillImageListener()
        {
            if ( _ImageListener != null )
            {
                if ( _ImageListener.IsAlive == true )
                {
                    _ImageListener.Stop();
                }
                else
                {
                    _ImageListener.Wait();
                }
            }
        }

        private void Alarm( int GateId, ALARM_TYPE AlarmType )
        {
            try
            {
                if ( ( Properties.Settings.Default[ "USE_Alarm_Device" ] == null ) ||
                     ( Properties.Settings.Default.USE_Alarm_Device == true ) )
                {
                    _Keiko?.Alarm( _DBAccess.AlarmPattern( GateId, AlarmType ) );
                }
            }
            catch ( Exception e )
            {
                this.Exception( $"警報装置制御失敗", e );
                //AutoCloseMessageBox.Show( $"警報装置制御に失敗しました。{Environment.NewLine}{e.Message}", "警報装置制御" );
            }
        }

        public bool DsiableBuzzer
        {
            get => ( _Keiko != null ) ? _Keiko.DisableBuzzer : false;
            set
            {
                if ( _Keiko != null )
                {
                    _Keiko.DisableBuzzer = value;
                }

                _DBAccess.DisableBuzzer = value;
            }
        }

        public void BuzzerStop()
        {
            Task.Factory.StartNew( () =>
            {
                try
                {
                    if ( ( Properties.Settings.Default[ "USE_Alarm_Device" ] == null ) ||
                         ( Properties.Settings.Default.USE_Alarm_Device == true ) )
                    {
                        _Keiko?.BuzzerStop();
                    }
                }
                catch ( Exception e )
                {
                    this.Exception( $"警報装置制御失敗", e );
                    //AutoCloseMessageBox.Show( $"警報装置制御に失敗しました。{Environment.NewLine}{e.Message}", "警報装置制御" );
                }
            } );
        }

        public void ApplicationError()
        {
            Task.Factory.StartNew( () =>
            {
                try
                {
                    _Keiko?.ApplicationError();
                }
                catch ( Exception e )
                {
                    this.Exception( $"警報装置制御失敗", e );
                    //AutoCloseMessageBox.Show( $"警報装置制御に失敗しました。{Environment.NewLine}{e.Message}", "警報装置制御" );
                }
            } );
        }

        protected override void TerminateStillImageSender()
        {
        }

        public void StopAlarmImageDrawer()
        {
            foreach ( var Key in _AlarmDrawers.Keys )
            {
                if ( _AlarmDrawers[ Key ] != null )
                {
                    if ( _AlarmDrawers[ Key ].IsAlive == true )
                    {
                        _AlarmDrawers[ Key ].Stop();
                    }
                    else
                    {
                        _AlarmDrawers[ Key ].Wait();
                    }
                }
            }
        }

        public void InitAlarmImageDrawer( Control MonitorCntl1, Control MonitorCntl2, Control MonitorCntl3 )
        {
            _DBAccess.GetMonitorTarget( out int[] RecNo );
            Assert( RecNo != null && RecNo.Length == 3, "_DBAccess.GetMonitorTarget RecNo Get Error." );

            Control[] Ctrls = new Control[] { MonitorCntl1, MonitorCntl2, MonitorCntl3 };

            for ( int i = 0 ; i < Ctrls.Length ; i++ )
            {
                //_OpenCVAlarmDrawers[ RecNo[ i ] ] = new OpenCVImageDrawer( this, RecNo[ i ], "OpenCV-A" );
                //_OpenCVAlarmDrawers[ RecNo[ i ] ].NewImage += this.MonitoringApp_AlarmImage;
                //_OpenCVAlarmDrawers[ RecNo[ i ] ].Start();
                _AlarmDrawers[ RecNo[ i ] ] = new OpenCVImageDrawer( this, RecNo[ i ], "OpenCV-A" );
                _AlarmDrawers[ RecNo[ i ] ].NewImage += this.MonitoringApp_AlarmImage;
                _AlarmDrawers[ RecNo[ i ] ].Start();

                //_DirectXAlarmDrawers[ RecNo[ i ] ] = new DirectXIntervalDrawer( this, $"DxAlm-{RecNo[ i ]}", Ctrls[ i ].Handle, new ControlSizeChangeCallback( Ctrls[ i ] ) );
                //_DirectXAlarmDrawers[ RecNo[ i ] ].Start();
            }
        }

        private void MonitoringApp_AlarmImage( object sender, OpenCVImageDrawEventArgs e )
        {
            if ( NewAlarmImage != null )
            {
                if ( ( NewAlarmImage.Target != null ) && ( NewAlarmImage.Target is Control Ctrl ) )
                {
                    Ctrl.Invoke( NewAlarmImage, this, e );
                }
                else
                {
                    NewAlarmImage( this, e );
                }
            }
        }

        public int InsertAlarm( ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage )
        {
            return _DBAccess.InsertAlarm( _ApplicationId, AlarmType, Timestamp, AlarmImage );
        }

        public Color DetectorBackColor( int GateId )
        {
            return _DBAccess.DetectorBackColor( GateId );
        }

        public Color DetectorForeColor( int GateId )
        {
            return _DBAccess.DetectorForeColor( GateId );
        }

        public int DeleteOldAlarm()
        {
            int Count = _DBAccess.DeleteOldAlarm();
            Info( $"[{Count}]件のALARMレコードを削除" );
            return Count;
        }

        public void WriteAlarmMovie( int AlarmId, DateTime Timestamp )
        {
            throw new NotImplementedException();
        }

        public void H264EncodedImageSend( int DetectCount, int AlarmId, ALARM_TYPE AlarmType, DateTime Timestamp, byte[] EncodedData )
        {
            throw new NotSupportedException( "アプリケーション区分エラー　H264EncodedImageSend は Monitoring で使用しない" );
        }

        public string MonitorStreamURL( int DetectorId )
        {
            return _DBAccess.MonitorStreamURL( DetectorId );
        }

        public override void AddStockImage( DateTime Timestamp, Mat FrameImage )
        {
            throw new NotImplementedException();
        }

        public override void EraceStockImage( DateTime Timestamp )
        {
            throw new NotImplementedException();
        }

        public override (long Ticks, Mat FrameImage) ReadStockImage( DateTime Timestamp )
        {
            throw new NotImplementedException();
        }

        public int AreaChangeWaitFrames => _DBAccess.AreaChangeWaitFrames;

        public int CarStayAlarmWaitSeconds => _DBAccess.CarStayAlarmWaitSeconds;

        public bool EnableCareerPaletteStayAlarm => _DBAccess.EnableCareerPaletteStayAlarm;

        public int AlarmMoviePreSeconds
        {
            get => _DBAccess.AlarmMoviePreSeconds;
            set => _DBAccess.AlarmMoviePreSeconds = value;
        }
        public int AlarmMoviePostSeconds
        {
            get => _DBAccess.AlarmMoviePostSeconds;
            set => _DBAccess.AlarmMoviePostSeconds = value;
        }

        public MONITORING_TYPE MonitoringType => _DBAccess.MonitoringType;

        public Logger Logger => _Logger;

        public float DetectThresholdPerson => throw new NotImplementedException();

        public float DetectThresholdMarker => throw new NotImplementedException();

        public float DetectThresholdCar => throw new NotImplementedException();

        public float DetectThresholdCareerPalette => throw new NotImplementedException();

        public bool EnableDetectAfterAlarm => throw new NotImplementedException();

        public float TrackingReductionRate => throw new NotImplementedException();

        public int AlarmSaveDays => _DBAccess.GetAlarmSaveDays;

        public string AlarmMovieFolder => throw new NotImplementedException();
    }
}
