﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using static System.Diagnostics.Debug;

namespace CES.SOLAS.Monitor
{
    using System.Drawing;
    using CES.SOLAS.Lib;
    using Lib.Data;
    using OpenCvSharp;

    public class MonitoringDBAccess : DBAccess<MonitoringDBAccess>, IDBAccess
    {
        protected TRIGGER_PROCESS_TYPE _ProcessType = TRIGGER_PROCESS_TYPE.TRG_PROC_UNKNOWN;
        protected int _ApplicationId = -1;
        protected SOLASDataSet.MONITORDataTable _OwnApplicationInfo = null;
        protected int _UdpPort = -1;
        protected int _TcpImagePort = -1;
        protected string _DisplayName = string.Empty;
        protected MONITORING_TYPE _MonitoringType = MONITORING_TYPE.MON_UNKNOWN;

        public TRIGGER_PROCESS_TYPE ProcessType => _ProcessType;

        public int ApplicationId => _ApplicationId;

        public int UDPListenerPort => _UdpPort;

        public int TCPMovieListenerPort => 0;

        public int TCPImageListenerPort => _TcpImagePort;

        public string DisplayName => _DisplayName;

        public MONITORING_TYPE MonitoringType => _MonitoringType;

        public string GetDetectTargetStream => throw new NotSupportedException( "MonitoringDBAccess では GetDetectTargetStream を使用しません。" );

        public int MonitoringCount => base.GetMonitoringCount;

        public int DetectorCount => base.GetDetectorCount;

        public bool EnableDetect
        {
            get => throw new NotSupportedException( "MonitoringDBAccess では EnableDetect.get を使用しません。" );
            set => throw new NotSupportedException( "MonitoringDBAccess では EnableDetect.set を使用しません。" );
        }

        public uint PersonId => base.GetPersonId;
        public uint MarkerId => base.GetMarkerId;
        public uint CarId => base.GetCarId;
        public uint CareerPaletteId => base.GetCareerPaletteId;

        public Scalar PersonColor => base.GetPersonColor;

        public Scalar MarkerColor => base.GetMarkerColor;

        public Scalar CarColor => base.GetCarColor;
        public Scalar CareerPaletteColor => base.GetCareerPaletteColor;

        public Scalar OKColor => base.GetOKColor;

        public Scalar AlarmColor => base.GetAlarmColor;

        public Scalar StagnantColor => base.GetStagnantColor;

        public string PersonName => base.GetPersonName;

        public string MarkerName => base.GetMarkerName;

        public string CarName => base.GetCarName;
        public string CareerPaletteName => base.GetCareerPaletteName;

        public uint MaxPredictFrame => base.GetMaxPredictFrame;

        public float IOUThreshold => base.GetIOUThreshold;

        public int MessageBoxTimeout => base.GetMessageBoxTimeout;

        public bool EnableCameraControl => throw new NotSupportedException( "MonitoringDBAccess では EnableCameraControl.get を使用しません。" );

        public int StillImageShortInterval
        {
            get => base.GetStillImageShortInterval;
            set => base.GetStillImageShortInterval = value;
        }

        public int StillImageLongInterval
        {
            get => base.GetStillImageLongInterval;
            set => base.GetStillImageLongInterval = value;
        }

        public int TcpKeepAliveWait => base.GetTcpKeepAliveWait;

        public int TcpKeepAliveInterval => base.GetTcpKeepAliveInterval;

        public float StillImageShrinkRatio => base.GetStillImageShrinkRatio;

        public int HeartBeatCycle => base.GetHeartBeatCycle;

        public System.Drawing.Point[] DetectArea => throw new NotSupportedException( "MonitoringDBAccess では DetectArea を使用しません。" );

        public System.Drawing.Point[] DetectAreaA => throw new NotSupportedException( "MonitoringDBAccess では DetectAreaA を使用しません。" );

        public System.Drawing.Point[] DetectAreaB => throw new NotSupportedException( "MonitoringDBAccess では DetectAreaB を使用しません。" );

        public Scalar AreaColorA => throw new NotSupportedException( "MonitoringDBAccess では AreaColorA を使用しません。" );

        public Scalar AreaColorB => throw new NotSupportedException( "MonitoringDBAccess では AreaColorB を使用しません。" );

        public int AddNewAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImageData )
        {
            throw new NotImplementedException();
        }

        public int AddNewImage( int DetectorId, DateTime Timestamp, Mat ImageData )
        {
            throw new NotImplementedException();
        }

        public byte[] AlarmImage( int AlarmId, bool Thumbnail )
        {
            throw new NotImplementedException();
        }

        public void DetectorHandShake()
        {
            throw new NotSupportedException( "MonitoringDBAccess では DetectorHandShake を使用しません。" );
        }

        public void GetDetectorPCInfo( int Index, out int DetectorId, out string IPAddress, out int Port )
        {
            Assert( Index >= 0 && Index < _SOLASDataSet.DETECTOR.Count, "Index out of range." );

            DetectorId = _SOLASDataSet.DETECTOR[ Index ].ai_id;
            IPAddress = _SOLASDataSet.DETECTOR[ Index ].ai_ip_address;
            Port = _SOLASDataSet.DETECTOR[ Index ].ai_trigger_port;
        }

        public void GetMonitoringPCInfo( int Index, out int MonitoringId, out string IPAddress, out int UdpUdpTriggerPortPort, out int TcpImageListenerPort, out MONITORING_TYPE MonitoringType )
        {
            throw new NotSupportedException( "MonitoringDBAccess では GetMonitoringPCInfo を使用しません。" );
        }

        public void GetMonitorTarget( out int[] RecNo )
        {
            RecNo = new int[ 3 ];

            RecNo[ 0 ] = _OwnApplicationInfo[ 0 ].mon_aipc_01;
            RecNo[ 1 ] = _OwnApplicationInfo[ 0 ].mon_aipc_02;
            RecNo[ 2 ] = _OwnApplicationInfo[ 0 ].mon_aipc_03;
        }

        public override void InitDBAccess( string DBServer, string SAPassword )
        {
            _Adapter = new Adapter( DBServer, SAPassword );
            MonitoringHandShake();
            LoadPCInfo();
            LoadSystemInfo();

            _ProcessType = TRIGGER_PROCESS_TYPE.TRG_PROC_MONITOR;
            _ApplicationId = _OwnApplicationInfo[ 0 ].mon_id;
            _UdpPort = _OwnApplicationInfo[ 0 ].mon_trigger_port;
            _DisplayName = _OwnApplicationInfo[ 0 ].mon_display_name;
            _TcpImagePort = _OwnApplicationInfo[ 0 ].mon_still_image_port;

            Assert( Enum.IsDefined( typeof( MONITORING_TYPE ), _OwnApplicationInfo[ 0 ].mon_monitor_mode ) == true, $"MONITORING_TYPEに不正な値{_OwnApplicationInfo[ 0 ].mon_monitor_mode}" );
            _MonitoringType = ( MONITORING_TYPE )Enum.ToObject( typeof( MONITORING_TYPE ), _OwnApplicationInfo[ 0 ].mon_monitor_mode );
        }

        public void MonitoringHandShake()
        {
            foreach ( var Adr in Dns.GetHostAddresses( Dns.GetHostName() ) )
            {
                if ( Adr.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork )
                {
                    continue;
                }
                if ( IPAddress.Loopback.Equals( Adr ) == true )
                {
                    continue;
                }

                using ( var Adapter = _Adapter.MONITORTableAdapter() )
                {
                    _OwnApplicationInfo = Adapter.GetDataByIPAddress( Adr.ToString() );
                    if ( ( _OwnApplicationInfo != null ) && ( _OwnApplicationInfo.Count == 1 ) )
                    {
                        return;
                    }

                    _OwnApplicationInfo?.Dispose();
                    _OwnApplicationInfo = null;
                }
            }

            throw new ApplicationException( $"{Dns.GetHostName()} の端末情報はデータベースに登録されていません。" );
        }

        public byte[] StillImage( int ImageId )
        {
            throw new NotImplementedException();
        }

        public new string GateName( int DetectorId )
        {
            return base.GateName( DetectorId );
        }

        public void GetCamera(
            out string CameraAddress,
            out string CameraAccount,
            out string CameraPassword,
            out int Pan,
            out int Tilt,
            out int Zoom,
            out int ZoomRatio,
            out int CameraPort,
            out int Interval,
            out int ResetWait )
        {
            throw new NotSupportedException( "MonitoringDBAccess では GetCamera を使用しません。" );
        }

        public void UpdatePTZ( int Pan, int Tilt, int Zoom, int ZoomRatio )
        {
            throw new NotSupportedException( "MonitoringDBAccess では UpdatePTZ を使用しません。" );
        }

        public string AlarmName( ALARM_TYPE AlarmType )
        {
            return base.GetAlarmName( AlarmType );
        }

        public bool DetectorEnableCameraControl( int DetectorId )
        {
            return base.GetDetectorEnableCameraControl( DetectorId );
        }

        public int InsertAlarm( int DetectorId, ALARM_TYPE AlarmType, DateTime Timestamp, Mat AlarmImage )
        {
            throw new NotSupportedException( "MonitoringDBAccess では InsertAlarm を使用しません。" );
        }

        public string AlarmPattern( int GateId, ALARM_TYPE AlarmType )
        {
            switch ( AlarmType )
            {
                case ALARM_TYPE.ALARM_TYPE_A:
                    if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_01 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_01_alarm_pattern_A;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_02 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_02_alarm_pattern_A;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_03 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_03_alarm_pattern_A;
                    }
                    break;
                case ALARM_TYPE.ALARM_TYPE_B:
                    if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_01 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_01_alarm_pattern_B;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_02 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_02_alarm_pattern_B;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_03 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_03_alarm_pattern_B;
                    }
                    break;
                case ALARM_TYPE.ALARM_TYPE_C:
                    if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_01 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_01_alarm_pattern_C;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_02 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_02_alarm_pattern_C;
                    }
                    else if ( GateId == _OwnApplicationInfo[ 0 ].mon_aipc_03 )
                    {
                        return _OwnApplicationInfo[ 0 ].mon_aipc_03_alarm_pattern_C;
                    }
                    break;
                default:
                    break;
            }

            return "00000";
        }

        public Color DetectorBackColor( int GateId )
        {
            return base.GetDetectorBackColor( ApplicationId, GateId );
        }

        public Color DetectorForeColor( int GateId )
        {
            return base.GetDetectorForeColor( ApplicationId, GateId );
        }

        public int DeleteOldAlarm()
        {
            return base.DeleteAlarm( GetIntValue( "AlarmSaveDays", 365 ) );
        }

        public string MonitorStreamURL( int DetectorId )
        {
            return base.GetMonitorStreamURL( DetectorId );
        }

        public int MaxDisplayDays => _OwnApplicationInfo[ 0 ].mon_alarm_max_day;

        public float StreamingImageShrinkRatio => base.GetStreamingImageShrinkRatio;

        public int StreamingSkipFrame => base.GetStreamingSkipFrame;

        public string AlarmDeviceAddress => _OwnApplicationInfo[ 0 ].mon_alarm_device_address;

        public int AlarmDevicePort => _OwnApplicationInfo[ 0 ].mon_alarm_device_port;

        public bool DisableBuzzer
        {
            get => _OwnApplicationInfo[ 0 ].mon_disable_alarm_buzzer;
            set
            {
                _OwnApplicationInfo[ 0 ].mon_disable_alarm_buzzer = value;
                using ( var Adapter = _Adapter.MONITORTableAdapter() )
                {
                    Adapter.UpdateDisableBuzzer( value, ApplicationId );
                }
            }
        }

        public int AreaChangeWaitFrames => base.GetAreaChangeWaitFrames;

        public int CarStayAlarmWaitSeconds => base.GetCarStayAlarmWaitSeconds;

        public bool EnableCareerPaletteStayAlarm => base.GetEnableCareerPaletteStayAlarm;
        public Scalar NeutralAreaColor => base.GetNeutralAreaColor;

        public int AlarmMoviePreSeconds
        {
            get => base.GetAlarmMoviePreSeconds;
            set => base.GetAlarmMoviePreSeconds = value;
        }

        public int AlarmMoviePostSeconds
        {
            get => base.GetAlarmMoviePostSeconds;
            set => base.GetAlarmMoviePostSeconds = value;
        }

        public float DetectThreshold => throw new NotImplementedException();

        public float DetectThresholdPerson => throw new NotImplementedException();

        public float DetectThresholdMarker => throw new NotImplementedException();

        public float DetectThresholdCar => throw new NotImplementedException();

        public float DetectThresholdCareerPalette => throw new NotImplementedException();

        public bool EnableDetectAfterAlarm => throw new NotImplementedException();

        public float TrackingReductionRate => throw new NotImplementedException();
    }
}
