﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CES.SOLAS.Monitor
{
    using Lib.ProcessIF.UDP;
    using Lib.UI;

    public class MonitoringTriggerCallback : TriggerCallback
    {
        protected override void TriggerReceiveCallback( IPacket Packet )
        {
            //string PacketInfo = $"Receive ({Packet.ProcessType}:{Packet.ProcessId}:{Packet.Trigger})";

            switch ( Packet.Trigger )
            {
                case TRIGGER_TYPE.TRG_NEW_ALARM:
                    //GateNameLabel1.Text += $"ID={( ( Lib.ProcessIF.UDP.AlarmPacket )( e.Packet ) ).AlarmId} TYPE={( ( Lib.ProcessIF.UDP.AlarmPacket )( e.Packet ) ).AlarmType}";
                    break;
                case TRIGGER_TYPE.TRG_NEW_IMAGE:
                    //GateNameLabel1.Text += $"ID={( ( Lib.ProcessIF.UDP.ImagePacket )( e.Packet ) ).ImageId}";
                    break;
                case TRIGGER_TYPE.TRG_ERROR_MSG:
                    this.EventHandlerCall( Packet );
                    //PacketInfo = $"{PacketInfo} Message={( ( ErrorMsgPacket )Packet ).Message}";
                    break;
                case TRIGGER_TYPE.TRG_CURRENT_HOME:
                    this.EventHandlerCall( Packet );
                    //PacketInfo = $"{PacketInfo} " +
                    //    $"Pan={( ( HomePositionPacket )Packet ).Pan} " +
                    //    $"Tilt={( ( HomePositionPacket )Packet ).Tilt} " +
                    //    $"Zoom={( ( HomePositionPacket )Packet ).Zoom} " +
                    //    $"ZoomRatio={( ( HomePositionPacket )Packet ).ZoomRatio}";
                    break;
                case TRIGGER_TYPE.TRG_APP_STATUS:
                    this.EventHandlerCall( Packet );
                    MonitoringApp.Instance.Info( "Status Change Event Handler Call." );
                    //PacketInfo = $"{PacketInfo} Status={( ( AppStatusPacket )Packet ).Status}";
                    break;
            }

            //MonitoringApp.Instance.Info( PacketInfo );
        }
    }
}
