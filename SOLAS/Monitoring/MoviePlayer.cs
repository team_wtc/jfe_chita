﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

using OpenCvSharp;

namespace CES.SOLAS.Monitor
{
    using Lib.Threads;

    public class MoviePlayer : OneshotThread
    {
        const int FEED_INTERVAL = 5;

        public event EventHandler<MovieOpenEventArgs> MovieOpen;
        public event EventHandler<MovieErrorEventArgs> MovieError;
        public event EventHandler<MovieStateChangeEventArgs> MovieState;
        public event EventHandler<MovieImageEventArgs> MovieImage;

        public enum PLAY_STATE
        {
            /// <summary>
            /// 停止
            /// </summary>
            STOP,
            /// <summary>
            /// 再生
            /// </summary>
            PLAY,
            /// <summary>
            /// 一時停止
            /// </summary>
            PAUSE,
            /// <summary>
            /// 早送り
            /// </summary>
            FEED
        }
        readonly string _MovieFile;
        readonly VideoCapture _VideoCapture;
        readonly AutoResetEvent _AutoResetEvent;

        PLAY_STATE _State = PLAY_STATE.STOP;
        int _FrameInterval = 0;
        int _Interval = 0;
        bool _Seek = false;

        public MoviePlayer( bool Repeat, string MovieFile ) : base( MonitoringApp.Instance, "Player", ThreadPriority.AboveNormal )
        {
            this.Repeat = Repeat;
            _MovieFile = MovieFile;
            _AutoResetEvent = new AutoResetEvent( true );
            _VideoCapture = new VideoCapture();
        }

        protected override bool Init()
        {
            try
            {
                _VideoCapture.Open( _MovieFile );
                SendOpenState();
                if ( _VideoCapture.IsOpened() == false )
                {
                    return false;
                }

                if ( ( _VideoCapture.Fps < 0.0 ) || ( _VideoCapture.Fps > 30.0 ) )
                {
                    _FrameInterval = Convert.ToInt32( 1000.0 / 15.0 );
                }
                else
                {
                    _FrameInterval = Convert.ToInt32( 1000.0 / _VideoCapture.Fps );
                }
                return true;
            }
            catch ( Exception e )
            {
                SendError( $"動画ファイルオープンエラー ({e.Message})" );
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _VideoCapture?.Dispose();
            }
            catch
            {
            }
            base.Terminate();
        }

        public bool Repeat { get; set; } = false;

        public void PlayerStop()
        {
            try
            {
                _AutoResetEvent.WaitOne();
                _State = PLAY_STATE.STOP;
                SendStatus();
            }
            catch
            {
            }
            finally
            {
                _AutoResetEvent.Set();
            }
        }

        public void PlayerPlay()
        {
            try
            {
                _AutoResetEvent.WaitOne();
                _State = PLAY_STATE.PLAY;
                _Interval = _FrameInterval;
                SendStatus();
            }
            catch
            {
            }
            finally
            {
                _AutoResetEvent.Set();
            }
        }

        public void PlayerPause()
        {
            try
            {
                _AutoResetEvent.WaitOne();
                _State = PLAY_STATE.PAUSE;
                SendStatus();
            }
            catch
            {
            }
            finally
            {
                _AutoResetEvent.Set();
            }
        }

        public void PlayerFeed()
        {
            try
            {
                _AutoResetEvent.WaitOne();
                _State = PLAY_STATE.FEED;
                _Interval = FEED_INTERVAL;
                SendStatus();
            }
            catch
            {
            }
            finally
            {
                _AutoResetEvent.Set();
            }
        }

        public void PlayerSeek( int Position )
        {
            try
            {
                _AutoResetEvent.WaitOne();
                if ( Position <= 0 )
                {
                    _VideoCapture.PosFrames = 0;
                }
                else if ( Position >= _VideoCapture.FrameCount )
                {
                    _VideoCapture.PosFrames = ( _VideoCapture.FrameCount - 1 );
                }
                else
                {
                    _VideoCapture.PosFrames = Position;
                }
                _Seek = true;
                if ( _State == PLAY_STATE.STOP )
                {
                    _State = PLAY_STATE.PAUSE;
                }
                SendStatus();
            }
            catch
            {
            }
            finally
            {
                _AutoResetEvent.Set();
            }
        }

        protected override void OneshotRoutine()
        {
            while ( true )
            {
                try
                {
                    var _StopWatch = Stopwatch.StartNew();

                    _AutoResetEvent.WaitOne();
                    if ( _State == PLAY_STATE.PAUSE )
                    {
                        if ( _Seek == false )
                        {
                            Thread.Sleep( _FrameInterval );
                            continue;
                        }

                        _Seek = false;
                    }
                    if ( _State == PLAY_STATE.STOP )
                    {
                        if ( _VideoCapture.PosFrames == 1 )
                        {
                            Thread.Sleep( _FrameInterval );
                            continue;
                        }
                        _VideoCapture.PosFrames = 0;
                    }

                    var FrameImage = new Mat();
                    var Result = _VideoCapture.Read( FrameImage );
                    if ( ( Result == false ) || ( FrameImage.Empty() == true ) )
                    {
                        if ( Repeat == true )
                        {
                            _VideoCapture.PosFrames = 0;
                        }
                    }
                    else
                    {
                        SendImage( _VideoCapture.PosFrames, _VideoCapture.PosMsec, FrameImage );
                    }

                    _StopWatch.Stop();

                    if ( _Interval > ( int )_StopWatch.ElapsedMilliseconds )
                    {
                        Thread.Sleep( _Interval - ( int )_StopWatch.ElapsedMilliseconds );
                    }
                    else
                    {
                        Thread.Sleep( 1 );
                    }

                }
                catch ( Exception e )
                {
                    SendError( e.Message );
                }
                finally
                {
                    _AutoResetEvent.Set();
                }
            }
        }

        private void SendOpenState()
        {
            if ( MovieOpen != null )
            {
                if ( MovieOpen.Target is Control Cntl )
                {
                    Cntl.BeginInvoke( MovieOpen, this, new MovieOpenEventArgs( _VideoCapture ) );
                }
                else
                {
                    MovieOpen( this, new MovieOpenEventArgs( _VideoCapture ) );
                }
            }
        }

        private void SendError( string ErrMessage )
        {
            if ( MovieError != null )
            {
                if ( MovieError.Target is Control Cntl )
                {
                    Cntl.BeginInvoke( MovieError, this, new MovieErrorEventArgs( ErrMessage ) );
                }
                else
                {
                    MovieError( this, new MovieErrorEventArgs( ErrMessage ) );
                }
            }
        }

        private void SendStatus()
        {
            if ( MovieState != null )
            {
                if ( MovieState.Target is Control Cntl )
                {
                    Cntl.BeginInvoke( MovieState, this, new MovieStateChangeEventArgs( _State ) );
                }
                else
                {
                    MovieState( this, new MovieStateChangeEventArgs( _State ) );
                }
            }
        }

        private void SendImage( int Pos, int Msec, Mat Img )
        {
            if ( MovieImage != null )
            {
                if ( MovieImage.Target is Control Cntl )
                {
                    Cntl.BeginInvoke( MovieImage, this, new MovieImageEventArgs( Pos, Msec, Img ) );
                }
                else
                {
                    MovieImage( this, new MovieImageEventArgs( Pos, Msec, Img ) );
                }
            }
        }

    }

    public class MovieOpenEventArgs : EventArgs
    {
        public readonly bool Opened;
        public readonly string Codec;
        public readonly double FrameRate;
        public readonly int Frames;
        public readonly int Width;
        public readonly int Height;

        public MovieOpenEventArgs( bool _Opened, string _Codec, double _FrameRate, int _Frames, int _Width, int _Height )
            :base()
        {
            Opened = _Opened;
            Codec = _Codec;
            FrameRate = _FrameRate;
            Frames = _Frames;
            Width = _Width;
            Height = _Height;
        }

        public MovieOpenEventArgs( VideoCapture Capture )
            :this( Capture .IsOpened(), Capture .FourCC, Capture .Fps, Capture .FrameCount, Capture .FrameWidth, Capture.FrameHeight )
        {
        }
    }

    public class MovieErrorEventArgs : EventArgs
    {
        public readonly string Message;
        public MovieErrorEventArgs(string ErrorMessage ) : base()
        {
            Message = ErrorMessage;
        }
    }

    public class MovieStateChangeEventArgs : EventArgs
    {
        public readonly MoviePlayer.PLAY_STATE State;
        public readonly string StateLabel;
        public MovieStateChangeEventArgs( MoviePlayer.PLAY_STATE _State ) : base()
        {
            State = _State;
            switch ( State )
            {
                case MoviePlayer.PLAY_STATE.FEED:
                    StateLabel = "早送り中...";
                    break;
                case MoviePlayer.PLAY_STATE.PAUSE:
                    StateLabel = "一時停止中";
                    break;
                case MoviePlayer.PLAY_STATE.PLAY:
                    StateLabel = "再生中...";
                    break;
                case MoviePlayer.PLAY_STATE.STOP:
                    StateLabel = "停止中";
                    break;
            }
        }
    }

    public class MovieImageEventArgs : EventArgs
    {
        public readonly Mat FrameImage;
        public readonly int Frame;
        public readonly int Duration;

        public MovieImageEventArgs(int Pos, int Msec, Mat Img ) : base()
        {
            Frame = Pos;
            Duration = Msec;
            FrameImage = Img;
        }
    }
}
