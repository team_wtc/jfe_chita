﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CES.SOLAS.Monitor
{
    public class NewAlarmEventArgs : EventArgs
    {
        public NewAlarmEventArgs( int GateId, int AlarmId )
        {
            this.GateId = GateId;
            this.AlarmId = AlarmId;
        }

        public int GateId
        {
            get;
            private set;
        }

        public int AlarmId
        {
            get;
           private set;
        }
    }
}
