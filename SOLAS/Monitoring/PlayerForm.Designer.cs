﻿namespace CES.SOLAS.Monitor
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.VideInfoLabel = new System.Windows.Forms.Label();
            this.StopButton = new System.Windows.Forms.Button();
            this.FrameTrackBar = new System.Windows.Forms.TrackBar();
            this.RepeatCheckBox = new System.Windows.Forms.CheckBox();
            this.FeedRadioButton = new System.Windows.Forms.RadioButton();
            this.PauseRadioButton = new System.Windows.Forms.RadioButton();
            this.PlayRadioButton = new System.Windows.Forms.RadioButton();
            this.FrameImagePictureBoxIpl = new OpenCvSharp.UserInterface.PictureBoxIpl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.MovieFileInfoLabel = new System.Windows.Forms.Label();
            this.FrameTimeLabel = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrameTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameImagePictureBoxIpl)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 645);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(915, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("メイリオ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Snow;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(798, 17);
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar1.Font = new System.Drawing.Font("メイリオ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar1.Step = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.FrameTimeLabel);
            this.groupBox1.Controls.Add(this.MovieFileInfoLabel);
            this.groupBox1.Controls.Add(this.VideInfoLabel);
            this.groupBox1.Controls.Add(this.StopButton);
            this.groupBox1.Controls.Add(this.FrameTrackBar);
            this.groupBox1.Controls.Add(this.RepeatCheckBox);
            this.groupBox1.Controls.Add(this.FeedRadioButton);
            this.groupBox1.Controls.Add(this.PauseRadioButton);
            this.groupBox1.Controls.Add(this.PlayRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 510);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(891, 130);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // VideInfoLabel
            // 
            this.VideInfoLabel.ForeColor = System.Drawing.Color.Snow;
            this.VideInfoLabel.Location = new System.Drawing.Point(427, 93);
            this.VideInfoLabel.Name = "VideInfoLabel";
            this.VideInfoLabel.Size = new System.Drawing.Size(458, 28);
            this.VideInfoLabel.TabIndex = 7;
            this.VideInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // StopButton
            // 
            this.StopButton.BackgroundImage = global::CES.SOLAS.Monitor.Properties.Resources.stop_up;
            this.StopButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.StopButton.Location = new System.Drawing.Point(147, 60);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(64, 64);
            this.StopButton.TabIndex = 6;
            this.toolTip1.SetToolTip(this.StopButton, "停止（先頭に戻る）");
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            this.StopButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StopButton_MouseDown);
            this.StopButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.StopButton_MouseUp);
            // 
            // FrameTrackBar
            // 
            this.FrameTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrameTrackBar.AutoSize = false;
            this.FrameTrackBar.Location = new System.Drawing.Point(6, 23);
            this.FrameTrackBar.Name = "FrameTrackBar";
            this.FrameTrackBar.Size = new System.Drawing.Size(879, 33);
            this.FrameTrackBar.TabIndex = 0;
            this.FrameTrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.FrameTrackBar.ValueChanged += new System.EventHandler(this.FrameTrackBar_ValueChanged);
            this.FrameTrackBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrameTrackBar_MouseDown);
            this.FrameTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrameTrackBar_MouseUp);
            // 
            // RepeatCheckBox
            // 
            this.RepeatCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RepeatCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.RepeatCheckBox.BackgroundImage = global::CES.SOLAS.Monitor.Properties.Resources.repeat_up;
            this.RepeatCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.RepeatCheckBox.Location = new System.Drawing.Point(16, 62);
            this.RepeatCheckBox.Name = "RepeatCheckBox";
            this.RepeatCheckBox.Size = new System.Drawing.Size(64, 64);
            this.RepeatCheckBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.RepeatCheckBox, "繰返し再生");
            this.RepeatCheckBox.UseVisualStyleBackColor = true;
            this.RepeatCheckBox.CheckedChanged += new System.EventHandler(this.RepeatCheckBox_CheckedChanged);
            // 
            // FeedRadioButton
            // 
            this.FeedRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FeedRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.FeedRadioButton.BackgroundImage = global::CES.SOLAS.Monitor.Properties.Resources.feed_up;
            this.FeedRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FeedRadioButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FeedRadioButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black;
            this.FeedRadioButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.FeedRadioButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.FeedRadioButton.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FeedRadioButton.Location = new System.Drawing.Point(357, 60);
            this.FeedRadioButton.Name = "FeedRadioButton";
            this.FeedRadioButton.Size = new System.Drawing.Size(64, 64);
            this.FeedRadioButton.TabIndex = 5;
            this.FeedRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.FeedRadioButton, "早送り");
            this.FeedRadioButton.UseVisualStyleBackColor = true;
            this.FeedRadioButton.CheckedChanged += new System.EventHandler(this.FeedRadioButton_CheckedChanged);
            // 
            // PauseRadioButton
            // 
            this.PauseRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PauseRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.PauseRadioButton.BackgroundImage = global::CES.SOLAS.Monitor.Properties.Resources.pause_up;
            this.PauseRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PauseRadioButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PauseRadioButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black;
            this.PauseRadioButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PauseRadioButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PauseRadioButton.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PauseRadioButton.Location = new System.Drawing.Point(217, 60);
            this.PauseRadioButton.Name = "PauseRadioButton";
            this.PauseRadioButton.Size = new System.Drawing.Size(64, 64);
            this.PauseRadioButton.TabIndex = 3;
            this.PauseRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.PauseRadioButton, "一時停止");
            this.PauseRadioButton.UseVisualStyleBackColor = true;
            this.PauseRadioButton.CheckedChanged += new System.EventHandler(this.PauseRadioButton_CheckedChanged);
            // 
            // PlayRadioButton
            // 
            this.PlayRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PlayRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.PlayRadioButton.BackgroundImage = global::CES.SOLAS.Monitor.Properties.Resources.play_up;
            this.PlayRadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlayRadioButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PlayRadioButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black;
            this.PlayRadioButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PlayRadioButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PlayRadioButton.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PlayRadioButton.Location = new System.Drawing.Point(287, 60);
            this.PlayRadioButton.Name = "PlayRadioButton";
            this.PlayRadioButton.Size = new System.Drawing.Size(64, 64);
            this.PlayRadioButton.TabIndex = 4;
            this.PlayRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.PlayRadioButton, "再生");
            this.PlayRadioButton.UseVisualStyleBackColor = true;
            this.PlayRadioButton.CheckedChanged += new System.EventHandler(this.PlayRadioButton_CheckedChanged);
            // 
            // FrameImagePictureBoxIpl
            // 
            this.FrameImagePictureBoxIpl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrameImagePictureBoxIpl.Location = new System.Drawing.Point(12, 12);
            this.FrameImagePictureBoxIpl.Name = "FrameImagePictureBoxIpl";
            this.FrameImagePictureBoxIpl.Size = new System.Drawing.Size(891, 492);
            this.FrameImagePictureBoxIpl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FrameImagePictureBoxIpl.TabIndex = 0;
            this.FrameImagePictureBoxIpl.TabStop = false;
            // 
            // MovieFileInfoLabel
            // 
            this.MovieFileInfoLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MovieFileInfoLabel.ForeColor = System.Drawing.Color.Snow;
            this.MovieFileInfoLabel.Location = new System.Drawing.Point(525, 62);
            this.MovieFileInfoLabel.Name = "MovieFileInfoLabel";
            this.MovieFileInfoLabel.Size = new System.Drawing.Size(360, 28);
            this.MovieFileInfoLabel.TabIndex = 8;
            this.MovieFileInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrameTimeLabel
            // 
            this.FrameTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FrameTimeLabel.ForeColor = System.Drawing.Color.Snow;
            this.FrameTimeLabel.Location = new System.Drawing.Point(427, 62);
            this.FrameTimeLabel.Name = "FrameTimeLabel";
            this.FrameTimeLabel.Size = new System.Drawing.Size(92, 28);
            this.FrameTimeLabel.TabIndex = 9;
            this.FrameTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PlayerForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(915, 667);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.FrameImagePictureBoxIpl);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "PlayerForm";
            this.Text = "PlayerForm";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayerForm_FormClosing);
            this.Load += new System.EventHandler(this.PlayerForm_Load);
            this.Shown += new System.EventHandler(this.PlayerForm_Shown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrameTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameImagePictureBoxIpl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private OpenCvSharp.UserInterface.PictureBoxIpl FrameImagePictureBoxIpl;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton PlayRadioButton;
        private System.Windows.Forms.TrackBar FrameTrackBar;
        private System.Windows.Forms.CheckBox RepeatCheckBox;
        private System.Windows.Forms.RadioButton FeedRadioButton;
        private System.Windows.Forms.RadioButton PauseRadioButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label VideInfoLabel;
        private System.Windows.Forms.Label MovieFileInfoLabel;
        private System.Windows.Forms.Label FrameTimeLabel;
    }
}