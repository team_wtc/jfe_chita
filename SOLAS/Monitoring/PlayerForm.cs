﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CES.SOLAS.Monitor
{
    using Lib;
    using Lib.ProcessIF.TCP;

    using static Properties.Resources;
    using static Properties.Settings;

    public partial class PlayerForm : Form
    {
        int _DetectorId;
        int _AlarmId;
        string _GateName;
        DateTime _AlarmTimestamp;
        ALARM_TYPE _AlarmType;
        MovieReceiver _MovieReceiver;
        MoviePlayer _MoviePlayer;

        public PlayerForm()
        {
            InitializeComponent();
        }

        public bool SetParams( int AlarmId, int DetectorId, string GateName, int AlarmType,DateTime AlarmTimestamp  )
        {
            _AlarmId = AlarmId;
            _DetectorId = DetectorId;
            _GateName = GateName;

            if ( Enum.IsDefined( typeof( ALARM_TYPE ), AlarmType ) == false )
            {
                return false;
            }
            _AlarmType = ( ALARM_TYPE )Enum.ToObject( typeof( ALARM_TYPE ), AlarmType );
            _AlarmTimestamp = AlarmTimestamp;

            return true;
        }

        public bool RestartPlayer( int AlarmId, int DetectorId, string GateName, int AlarmType, DateTime AlarmTimestamp )
        {
            if ( SetParams( AlarmId, DetectorId, GateName, AlarmType, AlarmTimestamp ) == false )
            {
                return false;
            }

            Invoke( ( MethodInvoker )StartPlayer );
            return true;
        }

        private void PlayerForm_Load( object sender, EventArgs e )
        {
        }

        private void PauseRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            PauseRadioButton.BackgroundImage = ( PauseRadioButton.Checked == true ) ? pause_down : pause_up;
            if ( PauseRadioButton.Checked == true )
            {
                _MoviePlayer?.PlayerPause();
            }
        }

        private void PlayRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            PlayRadioButton.BackgroundImage = ( PlayRadioButton.Checked == true ) ? play_down : play_up;
            if ( PlayRadioButton.Checked == true )
            {
                _MoviePlayer?.PlayerPlay();
            }
        }

        private void FeedRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            FeedRadioButton.BackgroundImage = ( FeedRadioButton.Checked == true ) ? feed_down : feed_up;
            if ( FeedRadioButton.Checked == true )
            {
                _MoviePlayer?.PlayerFeed();
            }
        }

        private void RepeatCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            RepeatCheckBox.BackgroundImage = ( RepeatCheckBox.Checked == true ) ? repeat_down : repeat_up;
            if ( ( _MoviePlayer != null ) && ( _MoviePlayer.IsAlive == true ) )
            {
                _MoviePlayer.Repeat = RepeatCheckBox.Checked;
            }
        }

        private void StopButton_MouseDown( object sender, MouseEventArgs e )
        {
            StopButton.BackgroundImage = stop_down;
        }

        private void StopButton_MouseUp( object sender, MouseEventArgs e )
        {
            StopButton.BackgroundImage = stop_up;
        }

        private void StopButton_Click( object sender, EventArgs e )
        {
            FeedRadioButton.Checked = false;
            PauseRadioButton.Checked = false;
            PlayRadioButton.Checked = false;
            _MoviePlayer?.PlayerStop();
        }

        private void PlayerForm_Shown( object sender, EventArgs e )
        {
            StartPlayer();
        }

        private void StartPlayer()
        {
            Text = $"警報動画再生 {_GateName} {_AlarmType} {_AlarmTimestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}";
            MovieFileInfoLabel.Text = $"No.{_AlarmId} {_AlarmTimestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )}";

            RepeatCheckBox.Checked = false;
            FeedRadioButton.Checked = false;
            PauseRadioButton.Checked = false;
            PlayRadioButton.Checked = false;

            var MovieFile = Path.Combine( Default.ALARM_Movie_Folder, $"{_DetectorId}", $"{_AlarmId}.mp4" );

            if ( File.Exists( MovieFile ) == false )
            {
                if ( MessageBox.Show(
                    $"警報 ({_GateName} {_AlarmType} {_AlarmTimestamp.ToString( "yyyy/MM/dd HH:mm:ss.fff" )})" +
                    $"の動画ファイルを{_GateName}のAI検知PCよりダウンロードします。{Environment.NewLine}" +
                    $"よろしいですか？",
                    "ダウンロード",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2 ) != DialogResult.Yes )
                {
                    Close();
                    return;
                }

                if ( MonitoringApp.Instance.GetDetectorMovieRequestInfo( _DetectorId, out string Addr, out int Port ) == false )
                {
                    MessageBox.Show(
                        $"AI検知PC({_GateName})の情報を取得できませんでした。{Environment.NewLine}" +
                        $"フォームを閉じます。",
                        "ダウンロード",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error );
                    Close();
                    return;
                }
                if ( _MovieReceiver != null )
                {
                    if ( _MovieReceiver.IsAlive == true )
                    {
                        _MovieReceiver.Stop();
                    }
                    else
                    {
                        _MovieReceiver.Wait();
                    }
                }
                _MovieReceiver = new MovieReceiver( MonitoringApp.Instance, Addr, Port, _DetectorId, _AlarmId, Default.ALARM_Movie_Folder );
                _MovieReceiver.DownloadProgress += this._MovieReceiver_DownloadProgress;
                _MovieReceiver.Start();
            }
            else
            {
                if ( _MoviePlayer != null )
                {
                    if ( _MoviePlayer.IsAlive == true )
                    {
                        _MoviePlayer.Stop();
                    }
                    else
                    {
                        _MoviePlayer.Wait();
                    }
                }
                _MoviePlayer = new MoviePlayer( RepeatCheckBox.Checked, MovieFile );
                _MoviePlayer.MovieOpen += this._MoviePlayer_MovieOpen;
                _MoviePlayer.MovieError += this._MoviePlayer_MovieError;
                _MoviePlayer.MovieImage += this._MoviePlayer_MovieImage;
                _MoviePlayer.MovieState += this._MoviePlayer_MovieState;
                _MoviePlayer.Start();
            }
        }

        private void _MovieReceiver_DownloadProgress( object sender, MovieDownloadStatusEventArgs e )
        {
            switch ( e.State )
            {
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_UNKNOWN:
                    toolStripStatusLabel1.Text =string.Empty;
                    toolStripProgressBar1.Value = 0;
                    break;
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_REQ:
                    toolStripStatusLabel1.Text = e.Message;
                    toolStripProgressBar1.Value = 0;
                    break;
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_DOWNLOADING:
                    toolStripStatusLabel1.Text = e.Message;
                    if ( toolStripProgressBar1.Maximum < e.TotalLen )
                    {
                        toolStripProgressBar1.Maximum = e.TotalLen;
                    }
                    toolStripProgressBar1.Value = e.RecvLen;
                    break;
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_COMPLETE:
                    toolStripStatusLabel1.Text = e.Message;
                    toolStripProgressBar1.Value = 0;

                    break;
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_LOCAL_FAIL:
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_NO_FILE:
                case MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_REMOTE_FAIL:
                    toolStripStatusLabel1.Text = string.Empty;
                    toolStripProgressBar1.Value = 0;
                    MessageBox.Show(
                        $"ダウンロードに失敗しました。{Environment.NewLine}{Environment.NewLine}" +
                        $"理由：{Environment.NewLine}" +
                        $"{e.Message}{Environment.NewLine}{Environment.NewLine}" +
                        $"フォームを閉じます。", "ダウンロード" );
                    Close();
                    break;
            }

            Invalidate();
            //  TODO
            //  再生開始
            if ( e.State == MovieDownloadStatusEventArgs.DOWNLOAD_STATE.DLST_COMPLETE )
            {
                if ( _MoviePlayer != null )
                {
                    if ( _MoviePlayer.IsAlive == true )
                    {
                        _MoviePlayer.Stop();
                    }
                    else
                    {
                        _MoviePlayer.Wait();
                    }
                }
                _MoviePlayer = new MoviePlayer( RepeatCheckBox.Checked, e.MovieFile );
                _MoviePlayer.MovieOpen += this._MoviePlayer_MovieOpen;
                _MoviePlayer.MovieError += this._MoviePlayer_MovieError;
                _MoviePlayer.MovieImage += this._MoviePlayer_MovieImage;
                _MoviePlayer.MovieState += this._MoviePlayer_MovieState;
                _MoviePlayer.Start();
            }
        }

        private void _MoviePlayer_MovieState( object sender, MovieStateChangeEventArgs e )
        {
            toolStripStatusLabel1.Text = e.StateLabel;

            switch ( e.State )
            {
                case MoviePlayer.PLAY_STATE.FEED:
                    _MoviePlayerPause = false;
                    PauseRadioButton.Checked = false;
                    PlayRadioButton.Checked = false;
                    FeedRadioButton.Checked = true;
                    break;
                case MoviePlayer.PLAY_STATE.PAUSE:
                    _MoviePlayerPause = true;
                    PauseRadioButton.Checked = true;
                    PlayRadioButton.Checked = false;
                    FeedRadioButton.Checked = false;
                    break;
                case MoviePlayer.PLAY_STATE.PLAY:
                    _MoviePlayerPause = false;
                    PauseRadioButton.Checked = false;
                    PlayRadioButton.Checked = true;
                    FeedRadioButton.Checked = false;
                    break;
                case MoviePlayer.PLAY_STATE.STOP:
                    PauseRadioButton.Checked = false;
                    PlayRadioButton.Checked = false;
                    FeedRadioButton.Checked = false;
                    break;
            }
        }

        private void _MoviePlayer_MovieImage( object sender, MovieImageEventArgs e )
        {
            FrameImagePictureBoxIpl.ImageIpl?.Dispose();
            FrameImagePictureBoxIpl.ImageIpl = e.FrameImage;

            if ( ( _TrackBarMouseDown == false ) && ( _MoviePlayerPause == false ) )
            {
                FrameTrackBar.Value = e.Frame;
            }
            var Duration = ( float )e.Duration / 1000.0F;
            FrameTimeLabel.Text = $"{Duration:F2} 秒";
        }

        private void _MoviePlayer_MovieError( object sender, MovieErrorEventArgs e )
        {
        }

        private void _MoviePlayer_MovieOpen( object sender, MovieOpenEventArgs e )
        {
            if ( e.Opened == false )
            {
                VideInfoLabel.Text = string.Empty;
                MessageBox.Show( "動画ファイルをオープンできません。", "ダウンロード" );
                return;
            }

            FrameTrackBar.Value = 0;
            FrameTrackBar.Maximum = e.Frames;
            VideInfoLabel.Text = $"{e.FrameRate:F2}fps size:{e.Width}x{e.Height} codec={e.Codec} frame={e.Frames}";
        }

        private void PlayerForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( _MovieReceiver != null )
            {
                if ( _MovieReceiver.IsAlive == true )
                {
                    _MovieReceiver.Stop();
                }
                else
                {
                    _MovieReceiver.Wait();
                }
            }
            if ( _MoviePlayer != null )
            {
                if ( _MoviePlayer.IsAlive == true )
                {
                    _MoviePlayer.Stop();
                }
                else
                {
                    _MoviePlayer.Wait();
                }
            }
        }

        private void FrameTrackBar_MouseDown( object sender, MouseEventArgs e )
        {
            //System.Diagnostics.Debug.WriteLine( $"FrameTrackBar_MouseDown Button={e.Button} X={e.X} Y={e.Y}" );
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                _MoviePlayer.PlayerPause();
                _TrackBarMouseDown = true;
            }
        }

        private void FrameTrackBar_MouseUp( object sender, MouseEventArgs e )
        {
            //System.Diagnostics.Debug.WriteLine( $"FrameTrackBar_MouseUp Button={e.Button} X={e.X} Y={e.Y}" );
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                _TrackBarMouseDown = false;
            }
        }

        private void FrameTrackBar_ValueChanged( object sender, EventArgs e )
        {
            //System.Diagnostics.Debug.WriteLine( $"FrameTrackBar_ValueChanged Position={FrameTrackBar.Value}" );
            //if ( ( _TrackBarMouseDown == true ) && ( _MoviePlayerPause == true ) )
            //{
            //    _MoviePlayer.PlayerSeek( FrameTrackBar.Value );
            //}
            if ( _MoviePlayerPause == true )
            {
                _MoviePlayer.PlayerSeek( FrameTrackBar.Value );
            }
        }

        bool _TrackBarMouseDown = false;
        bool _MoviePlayerPause = false;
    }
}
