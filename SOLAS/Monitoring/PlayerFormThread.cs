﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CES.SOLAS.Monitor
{
    using Lib;
    using Lib.UI;

    public class PlayerFormThread : FormThread<PlayerForm>
    {
        int _AlarmId;
        int _DetectorId;
        string _GateName;
        DateTime _AlarmTimestamp;
        int _AlarmType;

        public PlayerFormThread( int AlarmId, int DetectorId, string GateName, int AlarmType, DateTime AlarmTimestamp ) : base( MonitoringApp.Instance, "Player" )
        {
            _AlarmId = AlarmId;
            _DetectorId = DetectorId;
            _GateName = GateName;
            _AlarmType = AlarmType;
            _AlarmTimestamp = AlarmTimestamp;
        }
        public bool RestartPlayer( int AlarmId, int DetectorId, string GateName, int AlarmType, DateTime AlarmTimestamp )
        {
            _AlarmId = AlarmId;
            _DetectorId = DetectorId;
            _GateName = GateName;
            _AlarmType = AlarmType;
            _AlarmTimestamp = AlarmTimestamp;
            return _Instance.RestartPlayer( _AlarmId, _DetectorId, _GateName, _AlarmType, _AlarmTimestamp );
        }

        protected override bool Init()
        {
            _SOLASApp.Info( $"動画再生フォーム起動 Alarm={_AlarmId} Detector={_DetectorId}" );
            return base.Init();
        }
        protected override void Terminate()
        {
            _SOLASApp.Info( $"動画再生フォーム終了 Alarm={_AlarmId} Detector={_DetectorId}" );
            base.Terminate();
        }
        protected override bool FormInitialyze()
        {
            return _Instance.SetParams( _AlarmId, _DetectorId, _GateName, _AlarmType, _AlarmTimestamp );
        }
    }
}
