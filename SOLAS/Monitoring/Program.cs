﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace CES.SOLAS.Monitor
{
    static class Program
    {
        static Mutex _Mutex = null;

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [ STAThread]
        static void Main()
        {
            try
            {
                _Mutex = new Mutex( true, Application.ProductName, out bool CreateNew );
                if ( CreateNew == false )
                {
                    return;
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault( false );
                Application.Run( new MonitorForm() );

                _Mutex.ReleaseMutex();
            }
            catch
            {

            }
            finally
            {
                _Mutex.Close();
            }
        }
    }
}
