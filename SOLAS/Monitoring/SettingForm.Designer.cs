﻿namespace CES.SOLAS.Monitor
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpdateButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LongIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ShortIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PreAlarmNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.PostAlarmNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LongIntervalNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShortIntervalNumericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreAlarmNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostAlarmNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(530, 357);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(110, 35);
            this.UpdateButton.TabIndex = 0;
            this.UpdateButton.Text = "更新(&U)";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(646, 357);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(110, 35);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "閉じる(&X)";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ShortIntervalNumericUpDown);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.LongIntervalNumericUpDown);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(744, 186);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Live静止画通信設定";
            // 
            // LongIntervalNumericUpDown
            // 
            this.LongIntervalNumericUpDown.Location = new System.Drawing.Point(114, 62);
            this.LongIntervalNumericUpDown.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.LongIntervalNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LongIntervalNumericUpDown.Name = "LongIntervalNumericUpDown";
            this.LongIntervalNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.LongIntervalNumericUpDown.TabIndex = 0;
            this.LongIntervalNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.LongIntervalNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "秒";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "送信間隔(長)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "送信間隔(短)";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(238, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(500, 49);
            this.label4.TabIndex = 4;
            this.label4.Text = "AI検知アプリケーションが、検知対象物（人物や車両など）を検知していないときに静止画を送信する長い間隔を秒で指定します。";
            // 
            // ShortIntervalNumericUpDown
            // 
            this.ShortIntervalNumericUpDown.Location = new System.Drawing.Point(114, 126);
            this.ShortIntervalNumericUpDown.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.ShortIntervalNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ShortIntervalNumericUpDown.Name = "ShortIntervalNumericUpDown";
            this.ShortIntervalNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.ShortIntervalNumericUpDown.TabIndex = 5;
            this.ShortIntervalNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ShortIntervalNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 24);
            this.label5.TabIndex = 6;
            this.label5.Text = "秒";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(236, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(500, 49);
            this.label6.TabIndex = 7;
            this.label6.Text = "AI検知アプリケーションが、検知対象物（人物や車両など）を検知しているときに静止画を送信する短い間隔を秒で指定します。";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.PostAlarmNumericUpDown);
            this.groupBox2.Controls.Add(this.PreAlarmNumericUpDown);
            this.groupBox2.Location = new System.Drawing.Point(12, 204);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(744, 147);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "警報動画出力設定";
            // 
            // PreAlarmNumericUpDown
            // 
            this.PreAlarmNumericUpDown.Location = new System.Drawing.Point(114, 65);
            this.PreAlarmNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.PreAlarmNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PreAlarmNumericUpDown.Name = "PreAlarmNumericUpDown";
            this.PreAlarmNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.PreAlarmNumericUpDown.TabIndex = 1;
            this.PreAlarmNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PreAlarmNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // PostAlarmNumericUpDown
            // 
            this.PostAlarmNumericUpDown.Location = new System.Drawing.Point(114, 102);
            this.PostAlarmNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.PostAlarmNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PostAlarmNumericUpDown.Name = "PostAlarmNumericUpDown";
            this.PostAlarmNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.PostAlarmNumericUpDown.TabIndex = 2;
            this.PostAlarmNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PostAlarmNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(206, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 24);
            this.label7.TabIndex = 7;
            this.label7.Text = "秒";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 24);
            this.label8.TabIndex = 8;
            this.label8.Text = "秒";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "警報発報前";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 24);
            this.label10.TabIndex = 10;
            this.label10.Text = "警報発報後";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(236, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(474, 24);
            this.label11.TabIndex = 11;
            this.label11.Text = "警報発報の何秒前から警報動画として出力するかを設定します。";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(715, 24);
            this.label12.TabIndex = 8;
            this.label12.Text = "AI検知アプリケーションが、正門の制御アプリケーションに静止画を送信する間隔を設定します。";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(618, 24);
            this.label13.TabIndex = 12;
            this.label13.Text = "警報動画の出力において、警報発報時刻の前後何秒分を動画出力するか設定します。";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(238, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(490, 24);
            this.label14.TabIndex = 13;
            this.label14.Text = "警報発報の何秒後までを警報動画として出力するかを設定します。";
            // 
            // SettingForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(768, 403);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UpdateButton);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "設定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LongIntervalNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShortIntervalNumericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreAlarmNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostAlarmNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown ShortIntervalNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown LongIntervalNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown PostAlarmNumericUpDown;
        private System.Windows.Forms.NumericUpDown PreAlarmNumericUpDown;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
    }
}