﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.SOLAS.Monitor
{
    public partial class SettingForm : Form
    {
        public SettingForm()
        {
            InitializeComponent();
        }

        private void CloseButton_Click( object sender, EventArgs e )
        {
            Close();
        }

        private void UpdateButton_Click( object sender, EventArgs e )
        {
            const string UPDATE_MESSAGE=
                "システムの設定情報を更新します。\r\n" +
                "更新を反映するには、AI検知アプリケーションの再起動が必要です。\r\n" +
                "更新後にAI検知アプリケーションを再起動させますか？\r\n\r\n" +
                "「はい」更新終了後、自動でAI検知アプリケーションを再起動させます。\r\n" +
                "「いいえ」設定の更新のみ実行し、AI検知アプリケーションは再起動させません。\r\n" +
                "「キャンセル」更新を実行せずに設定画面に戻ります。";

            var Result = MessageBox.Show( UPDATE_MESSAGE, "設定更新", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
            if ( ( Result == DialogResult.Yes ) || ( Result == DialogResult.No ) )
            {
                MonitoringApp.Instance.StillImageLongInterval = Convert.ToInt32( LongIntervalNumericUpDown.Value );
                MonitoringApp.Instance.StillImageShortInterval = Convert.ToInt32( ShortIntervalNumericUpDown.Value );
                MonitoringApp.Instance.AlarmMoviePreSeconds = Convert.ToInt32( PreAlarmNumericUpDown.Value );
                MonitoringApp.Instance.AlarmMoviePostSeconds = Convert.ToInt32( PostAlarmNumericUpDown.Value );

                if ( Result == DialogResult.Yes )
                {
                    MonitoringApp.Instance.DetectorStop( -1 );
                }
            }
        }

        private void SettingForm_Load( object sender, EventArgs e )
        {
            LongIntervalNumericUpDown.Value = Convert.ToDecimal( MonitoringApp.Instance.StillImageLongInterval );
            ShortIntervalNumericUpDown.Value = Convert.ToDecimal( MonitoringApp.Instance.StillImageShortInterval );
            PreAlarmNumericUpDown.Value = Convert.ToDecimal( MonitoringApp.Instance.AlarmMoviePreSeconds );
            PostAlarmNumericUpDown.Value = Convert.ToDecimal( MonitoringApp.Instance.AlarmMoviePostSeconds );
        }

        private void SettingForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            const string CLOSE_MESSAGE =
                "設定値が変更されています。\r\n" +
                "システム設定を更新せずに画面を閉じてよろしいですか？";

            if ( ( MonitoringApp.Instance.StillImageLongInterval == Convert.ToInt32( LongIntervalNumericUpDown.Value ) ) &&
            ( MonitoringApp.Instance.StillImageShortInterval == Convert.ToInt32( ShortIntervalNumericUpDown.Value ) ) &&
            ( MonitoringApp.Instance.AlarmMoviePreSeconds == Convert.ToInt32( PreAlarmNumericUpDown.Value ) ) &&
            ( MonitoringApp.Instance.AlarmMoviePostSeconds == Convert.ToInt32( PostAlarmNumericUpDown.Value ) ) )
            {
                return;
            }

            if ( MessageBox.Show( CLOSE_MESSAGE, "設定更新", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) != DialogResult.Yes )
            {
                e.Cancel = true;
            }
        }
    }
}
